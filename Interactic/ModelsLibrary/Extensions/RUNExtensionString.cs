﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Extensions
{
	public static class RUNExtensionString
	{
		public static string WithFormatRUN(this string run)
		{
			int cont = 0;
			string format;
			if (run.Length == 0)
			{
				return "";
			}
			else
			{
				run = run.Replace(".", "");
				run = run.Replace("-", "");
				format = "-" + run.Substring(run.Length - 1);
				for (int i = run.Length - 2; i >= 0; i--)
				{
					format = run.Substring(i, 1) + format;
					cont++;
					if (cont == 3 && i != 0)
					{
						format = "." + format;
						cont = 0;
					}
				}
				return format;
			}
		}

		public static string WithoutFormatRUN(this string run)
		{
			return run.Replace(".", "").Replace("-", "").ToUpper();
		}

		public static bool IsValidRUN(this string run)
		{
			bool validacion = false;
			try
			{
				run = run.WithoutFormatRUN();

				long rutAux = long.Parse(run.Substring(0, run.Length - 1));

				char dv = char.Parse(run.Substring(run.Length - 1, 1));

				long m = 0, s = 1;
				for (; rutAux != 0; rutAux /= 10)
				{
					s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
				}
				if (dv == (char)(s != 0 ? s + 47 : 75))
				{
					validacion = true;
				}
			}
			catch (Exception)
			{
			}
			return validacion;
		}
	}
}