﻿namespace Enums
{
	public enum TypeQualification { Number = 0, Letter = 1 }

	public enum ForumType { Normal = 0, Social = 1 }
}