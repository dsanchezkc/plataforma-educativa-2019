﻿
using Model;
using System.Collections.Generic;
using System.IO;

namespace ViewModels
{
	public class UploadFileResponseViewModel
	{
		public int Id { set; get; }
		public string Name { set; get; }

		public UploadFileResponseViewModel() { }

		public UploadFileResponseViewModel(UploadFileStudent uploadFileStudent)
		{
			Id = uploadFileStudent.Id;
			Name = uploadFileStudent.Title;
		}
	}
}