﻿
using Model;
using System.Collections.Generic;
using System.IO;

namespace ViewModels
{
	public class PendingUploadFileResponseViewModel
	{
		public UploadFileStudentQualification UploadFileStudentQualification { set; get; }
		public CourseSubject CourseSubject { set; get; }
		public SubUnity SubUnity { set; get; }
	}
}