﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace ViewModels
{
	public class TopicResponseViewModel
	{
		public Topic Topic { set; get; }
		public DateTime LastUpdate { set; get; }
		public int Answers { set; get; }
	}
}