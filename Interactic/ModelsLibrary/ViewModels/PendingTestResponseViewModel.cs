﻿
using Model;
using System.Collections.Generic;
using System.IO;

namespace ViewModels
{
	public class PendingTestResponseViewModel
	{
		public SubUnity SubUnity { set; get; }
		public QuestionWrittenAnswer QuestionWrittenAnswer { set; get; }
		public CourseSubject CourseSubject { set; get; }
		public StudentAnswer StudentAnswer { set; get; }
	}
}