﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class Answer : ModelBase
	{
		[NotMapped]
		public override int Id { set; get; }

		[Required]
		public string Choice { set; get; }

		[Key]
		public int Question_Id { set; get; }
		[ForeignKey("Question_Id")]
		public virtual Question Question { set; get; }

		[Key]
		public string User_Id { set; get; }
		[ForeignKey("User_Id")]
		public virtual User User { set; get; }
	}
}