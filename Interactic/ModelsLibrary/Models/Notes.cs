﻿using System;
using System.Collections.Generic;

namespace Model
{
	public class Note : ModelBase
	{
		public virtual SubUnity SubUnity { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public string DocumentPath { set; get; }
		public string DocumentName { set; get; }
		public string DocumentType { set; get; }
        public string Link { set; get; }
        public EnumType Type { set; get; }
    }

    public enum EnumType {
        Video,
        Document
    }
}
