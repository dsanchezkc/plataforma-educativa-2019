﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class Region : ModelBase
	{
		public string Name { set; get; }
		public virtual ICollection<Province> Provinces { set; get; }
	}
}