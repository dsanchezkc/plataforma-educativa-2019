﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	/// <summary>
	/// Archivo subido por alumno
	/// Clases asociadas : UploadFileStudent
	/// </summary>
	public class UploadFileStudentItem : ModelBase
	{
		[Required]
		public string DocumentPath { set; get; }

		[Required]
		public string DocumentName { set; get; }

		[Required]
		public string DocumentType { set; get; }
		
		public virtual UploadFileStudentQualification UploadFileStudentQualification { set; get; }
	}
}
