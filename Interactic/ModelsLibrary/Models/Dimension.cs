﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Dimension : ModelBase
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public virtual ICollection<RubricDimension> RubricDimensions { set; get; }
    }
}