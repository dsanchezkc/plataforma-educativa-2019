﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class Attached : ModelBase
	{
		public string DocumentPath { set; get; }
		public string DocumentName { set; get; }
		public string DocumentType { set; get; }

		public virtual ICollection<MessageAttached> MessageAttacheds { set; get; }
	}
}