﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class SubUnity : ModelBase
	{
		public string Name { set; get; }
		public virtual Unity Unity { set; get; }
		public virtual ICollection<Test> Tests { set; get; }
        public virtual ICollection<SpecialTest> SpecialTests { set; get; }
        public virtual ICollection<Note> Notes { set; get; }
        public virtual ICollection<Forum> Forums { set; get; }
        public virtual ICollection<Activity> Activities { set; get; }
        public virtual ICollection<SpecialQuestion> SpecialQuestions { set; get; }
    }
}