﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class Commune : ModelBase
	{
		public string Name { set; get; }
		public virtual Province Province { set; get; }
		public virtual ICollection<EducationalEstablishment> EducationalEstablishments { set; get; }
	}
}