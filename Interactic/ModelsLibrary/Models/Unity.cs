﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class Unity : ModelBase
	{
		public string Name { set; get; }
		public int Year { set; get; }
		public virtual CourseSubject CourseSubject { set; get; }
		public virtual ICollection<SubUnity> SubUnits { set; get; }
	}
}