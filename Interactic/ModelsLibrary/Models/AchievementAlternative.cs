﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
	public class AchievementAlternative : ModelBase
	{
        [Required]
        public Question Question { set; get; }
        public virtual AchievementIndicator AchievementIndicator { set; get; }
	}
}
