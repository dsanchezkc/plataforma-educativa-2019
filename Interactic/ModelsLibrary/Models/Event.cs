﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Event : ModelBase
	{

        [Required]
		public string Title { set; get; }
        public string Description { set; get; }
        public string Color { set; get; }
        public DateTime Start { set; get; }
        public DateTime End { set; get; }
		public bool AllDay { set; get; } = true;
        public virtual Course Course { set; get; }
        public virtual User User { get; set; }
        public object Pdf { get; set; }
        public string Base64 { get; set; }
        public string NameDocument { get; set; }
        public string PathDocument { get; set; }
        public virtual ICollection<EventFile> EventFiles { set; get; }
    }
}