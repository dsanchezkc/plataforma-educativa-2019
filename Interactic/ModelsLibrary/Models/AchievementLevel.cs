﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class AchievementLevel : ModelBase
    {
        [Required]
        public string Name { set; get; }
        public int Rubric_Id { get; set; }
        [ForeignKey("Rubric_Id")]
        public virtual Rubric Rubric { set; get; }
        public virtual ICollection<DescriptionRubricItem> DescriptionRubricItems { set; get; }
        public virtual bool Inserted { set; get; }
        public int Dimension_Id { get; set; }
    }
}