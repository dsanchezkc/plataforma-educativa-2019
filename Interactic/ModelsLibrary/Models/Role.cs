﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
	public class Role : IdentityRole<string, UserRoleEducationalEstablishment>
	{
		[Required]
		public int Rank { set; get; }
		public virtual ICollection<UserRoleEducationalEstablishment> UserRoleEducationalEstablishments { set; get; }
    }
}
