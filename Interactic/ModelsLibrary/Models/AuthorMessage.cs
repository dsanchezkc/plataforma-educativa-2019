﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class AuthorMessage : ModelBase
    {
		public virtual User User { set; get; }
		public virtual ConversationMessage ConversationMessage { set; get; }
	}
}