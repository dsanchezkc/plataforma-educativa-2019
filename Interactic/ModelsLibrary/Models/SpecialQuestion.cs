﻿using ModelEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class SpecialQuestion : ModelBase
    {
        [Required]
        public int Order { set; get; }

        [Required]
        public QuestionType QuestionType { set; get; }

        public virtual string Name { set; get; }

        public string ComplementaryAnswer { set; get; } = "";

        public int? SpecialTest_Id { set; get; }
        [ForeignKey("SpecialTest_Id")]
        public virtual SpecialTest SpecialTest { set; get; }

        public int? Difficulty { set; get; }

        public int? SubUnity_Id { set; get; }
        [ForeignKey("SubUnity_Id")]
        public virtual SubUnity SubUnity { set; get; }

        public virtual ICollection<SpecialQuestionSpecialTest> SpecialQuestionSpecialTests { set; get; }
    }
}

namespace ModelEnum
{
    public enum QuestionType
    {
        WrittenAnswer = 1,
        TrueFalse = 2,
        PairedWords = 3,
        Alternatives = 4
    }
}
