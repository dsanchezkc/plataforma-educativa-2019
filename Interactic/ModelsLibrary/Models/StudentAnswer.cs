﻿using ModelEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class StudentAnswer: ModelBase
	{
		[NotMapped]
	    public override int Id { set; get; }

		[Key, Column(Order = 0)]		
		public int QuestionItem_Id { set; get; }
		[ForeignKey("QuestionItem_Id")]
		public virtual QuestionItem QuestionItem { set; get; }
		
		[Key, Column(Order = 1)]
		public string User_Id { set; get; }
		[Key, Column(Order = 2)]
		public int SpecialTest_Id { set; get; }
		[ForeignKey("User_Id, SpecialTest_Id")]
		public virtual StudentSpecialTest StudentSpecialTest { set; get; }

		public string Answer { set; get; }

		[Range(0,100)]
		public int Qualification { set; get; }
	}
}
