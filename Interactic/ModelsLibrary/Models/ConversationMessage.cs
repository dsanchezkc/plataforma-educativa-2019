﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class ConversationMessage : ModelBase
    {
		public bool IsForward { set; get; }
		public virtual Conversation Conversation { set; get; }
		public virtual Message Message { set; get; }

		public virtual ICollection<AuthorMessage> AuthorMessages { set; get; }
	}
}