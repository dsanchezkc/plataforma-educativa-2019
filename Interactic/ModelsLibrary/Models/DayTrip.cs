﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
	public class DayTrip : ModelBase
	{
		[Required]
	    public string Name { set; get; }

		public virtual ICollection<AttendanceDate> AttendanceDates { get; set; }
	}
}
