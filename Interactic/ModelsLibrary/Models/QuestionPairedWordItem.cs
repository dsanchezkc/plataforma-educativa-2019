﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionPairedWordItem : QuestionItem
	{
		[Required]
		public string Column_1_Text { set; get; }

		[Required]
		public int Column_1_Alternative { set; get; }

		[Required]
		public string Column_2_Text { set; get; }

		[Required]
		public int Column_2_Index { set; get; }

		public virtual QuestionPairedWord QuestionPairedWord { set; get; }

	}
}
