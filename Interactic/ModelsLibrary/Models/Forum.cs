﻿using Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Forum : ModelBase
	{
		[Required]
		public string Title { set; get; }		

		public string Description { set; get; }

		[Required]
		public ForumType Type { set; get; }
        
        public int? SubUnity_Id { set; get; }
        [ForeignKey("SubUnity_Id")]
        public virtual SubUnity SubUnity { set; get; }

        public int? EducationalEstablishment_Id { set; get; }
        [ForeignKey("EducationalEstablishment_Id")]
        public virtual EducationalEstablishment EducationalEstablishment { set; get; }
        public virtual ICollection<Topic> Topics { set; get; }

        public bool? General { set; get; }
    }
}
