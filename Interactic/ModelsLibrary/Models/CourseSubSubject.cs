﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class CourseSubSubject : ModelBase
    {
		[NotMapped]
		public override int Id { set; get; }
		
		[Key, Column(Order = 0)]
		public int Subject_Id { set; get; }
		[Key, Column(Order = 1)]
		public int Course_Id { set; get; }
		[ForeignKey("Subject_Id, Course_Id")]
		public virtual CourseSubject CourseSubject { set; get; }

		[Key, Column(Order = 2)]
		public int SubSubject_Id { set; get; }
		[ForeignKey("SubSubject_Id")]
		public virtual SubSubject SubSubject { set; get; }
    }
}