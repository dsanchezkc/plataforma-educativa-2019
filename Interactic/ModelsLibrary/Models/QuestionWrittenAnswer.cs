﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionWrittenAnswer : SpecialQuestion
	{
		[Required]
		public override string Name { set; get; }
		
		public virtual QuestionWrittenAnswerItem QuestionWrittenAnswerItem { set; get; }

    }
}
