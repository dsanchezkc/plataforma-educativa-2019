﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class EventFile: ModelBase
    {
        public int? Event_Id { set; get; }
        [ForeignKey("Event_Id")]
        public virtual Event Event { set; get; }
        public string Base64 { get; set; }
        public string NameDocument { get; set; }
        public string PathDocument { get; set; }
    }
}