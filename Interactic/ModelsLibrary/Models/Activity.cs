﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Activity : ModelBase
	{
		[Required]
		public int Order { set; get; }

		public virtual SubUnity SubUnity { set; get; }

		[Required]
		public string Name { set; get; }

		public virtual ICollection<Action> Actions { set; get; }
	}
}
