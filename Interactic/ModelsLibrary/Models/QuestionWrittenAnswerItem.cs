﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionWrittenAnswerItem : QuestionItem
	{
		public string Answer { set; get; } = "";

		public virtual QuestionWrittenAnswer QuestionWrittenAnswer { set; get; }
	}
}
