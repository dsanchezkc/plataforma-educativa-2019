﻿using Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Post : ModelBase
	{	
		[Required]
		public string Text { set; get; }

		public virtual User Author { set; get; }

		public virtual Topic Topic { set; get; }

		//public virtual ICollection<PostQualification> PostQualifications { set; get; }

		public virtual ICollection<PostFile> PostFiles { set; get; }
	}
}
