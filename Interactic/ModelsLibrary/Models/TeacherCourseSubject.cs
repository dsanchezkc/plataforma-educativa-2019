﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class TeacherCourseSubject : ModelBase
	{
		[NotMapped]
		public override int Id { set; get; }

		[Key, Column(Order = 0)]
		public int Subject_Id { set; get; }
		[Key, Column(Order = 1)]
		public int Course_Id { set; get; }
		[ForeignKey("Subject_Id, Course_Id")]
		public virtual CourseSubject CourseSubject { set; get; }

		[Key, Column(Order = 2)]
		public string User_Id { set; get; }
		[ForeignKey("User_Id")]
		public virtual User User { set; get; }		
	}
}