﻿using Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
	public class BaseSubject : ModelBase
	{
		[Required]
		public string Name { set; get; }

		public virtual ICollection<StudentQualification> StudentQualifications { set; get; }
		public virtual ICollection<Observation> Observations { set; get; }
		public virtual ICollection<AttendanceDate> AttendanceDates { set; get; }
	}

	public class Subject : BaseSubject
	{		
		public TypeQualification TypeQualification { set; get; }

		public virtual ICollection<SubSubject> SubSubjects { set; get; }
		public virtual ICollection<CourseSubject> CourseSubjects { set; get; }
        public virtual ICollection<Rubric> Rubrics { set; get; }
    }
}