﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class UserRoleEducationalEstablishment : IdentityUserRole<string>
	{
		[ForeignKey("UserId")]
		public virtual User User { set; get; }

		[ForeignKey("RoleId")]
		public virtual Role Role { set; get; }

		[Key]
		public int EducationalEstablishment_Id { set; get; }	
		[ForeignKey("EducationalEstablishment_Id")]
		public virtual EducationalEstablishment EducationalEstablishment { set; get; }

		public bool Status { set; get; } = true;
	}
}
