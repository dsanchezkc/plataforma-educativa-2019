﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class StudentCourse : ModelBase
	{
		[NotMapped]
		public override int Id { set; get; }

		public int EstablishmentRegisterNumber { get; set; } // Numero de matricula
        public int ListNumber { set; get; }

		public string User_Id { set; get; }
		[ForeignKey("User_Id")]
		public virtual User User { set; get; }

		public int Course_Id { set; get; }
		[ForeignKey("Course_Id")]
        public virtual Course Course { set; get; }
    }
}