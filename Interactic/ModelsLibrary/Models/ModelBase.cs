﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
	public abstract class ModelBase
	{
		public virtual int Id { set; get; }
		public DateTime CreatedAt { set; get; }
		public DateTime EditedAt { set; get; }	
		public bool Status { set; get; } = true;
	}
}
