﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class ActionFileGroup : ModelBase
	{
		[Required]
		public string Name { set; get; }

		public virtual ICollection<ActionFile> ActionFiles { set; get; }
    }
}
