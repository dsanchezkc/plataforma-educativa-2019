﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class CourseSubject : ModelBase
	{
		[NotMapped]
		public override int Id { set; get; }
		
		[Key]
		public int Subject_Id { set; get; }
		[ForeignKey("Subject_Id")]
		public virtual Subject Subject { set; get; }

		[Key]
		public int Course_Id { set; get; }
		[ForeignKey("Course_Id")]
		public virtual Course Course { set; get; }

        public virtual ICollection<CourseSubSubject> CourseSubSubjects { set; get; }
        public virtual ICollection<TeacherCourseSubject> TeacherCourseSubjects { set; get; }
		public virtual ICollection<Unity> Units { set; get; }
        public virtual ICollection<SpecialTest> SpecialTests { set; get; }

    }
}