﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionTrueFalse : SpecialQuestion
    {
		public virtual ICollection<QuestionTrueFalseItem> QuestionTrueFalseItems { set; get; }
    }
}
