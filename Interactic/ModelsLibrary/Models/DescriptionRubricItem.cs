﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class DescriptionRubricItem : ModelBase
    {
        public string Text { set; get; }
        public int AchievementLevel_Id { set; get; }
        [ForeignKey("AchievementLevel_Id")]
        public virtual AchievementLevel AchievementLevel { set; get; }
        public int Criterion_Id { set; get; }
        [ForeignKey("Criterion_Id")]
        public virtual Criterion Criterion { set; get; }
        public int Rubric_Id { set; get; }
        [ForeignKey("Rubric_Id")]
        public virtual Rubric Rubric { set; get; }
        public int Dimension_Id { get; set; }
        public int Score { get; set; }
    }
}
