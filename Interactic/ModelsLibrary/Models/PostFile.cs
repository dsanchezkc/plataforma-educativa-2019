﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class PostFile : ModelBase
	{
		[Required]
		public string DocumentPath { set; get; }

		[Required]
		public string DocumentName { set; get; }

		[Required]
		public string DocumentType { set; get; }

		public virtual Post Post { set; get; }
	}
}
