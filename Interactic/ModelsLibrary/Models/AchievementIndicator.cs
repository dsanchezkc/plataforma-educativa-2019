﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
	public class AchievementIndicator : ModelBase
	{
        [Required]
        public string Name { set; get; }
        public virtual Test Test { set; get; }
		public virtual ICollection<AchievementAlternative> AchievementAlternatives { set; get; }
	}
}
