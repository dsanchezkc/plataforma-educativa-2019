﻿using System;

namespace Model
{
    public class StudentTest : ModelBase
    {
        public virtual User User { set; get; }
        public virtual Test Test { set; get; }
        public virtual bool Active { set; get; } = false;
        public virtual bool Done { set; get; } = false;
    }
}
