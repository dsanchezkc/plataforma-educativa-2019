﻿using System;
using System.Collections.Generic;

namespace Model
{
	public class AttendanceDate : ModelBase
	{
		public DateTime Date { set; get; }
		public virtual Course Course { get; set; }
		public virtual PeriodDetail PeriodDetail { get; set; }
		public virtual DayTrip Daytrip { get; set; }
		public virtual BaseSubject BaseSubject { set; get; }

		public virtual ICollection<Absent> Absents { get; set; }
	}
}
