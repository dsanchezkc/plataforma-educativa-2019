﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Rubric : ModelBase
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public virtual ICollection<Criterion> Criterions { set; get; }
        public virtual ICollection<AchievementLevel> AchievementLevels { set; get; }
        public virtual ICollection<RubricDimension> RubricDimensions { set; get; }
        public virtual ICollection<EvaluationRubric> EvaluationRubrics { set; get; }
        public int? Subject_Id { set; get; }
        [ForeignKey("Subject_Id")]
        public virtual Subject Subject { set; get; }
        public virtual ICollection<DescriptionRubricItem> DescriptionRubricItems { set; get; }
    }
}