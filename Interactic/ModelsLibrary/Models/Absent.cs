﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Absent : ModelBase
	{
		[NotMapped]
		public override int Id { set; get; }

		[Key]
		public int AttendanceDate_Id { set; get; }
		[ForeignKey("AttendanceDate_Id")]
		public virtual AttendanceDate AttendanceDate { set; get; }

		[Key]
		public string User_Id { set; get; }
		[ForeignKey("User_Id")]
        public virtual User User { set; get; }


        /*
        public virtual StudentCourse StudentCourse { set; get; }
        */
    }
}
