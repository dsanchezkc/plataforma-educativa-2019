﻿using System;
using System.Collections.Generic;

namespace Model
{
	public class PeriodDetail : ModelBase
	{
        public string Name { set; get; }
        public virtual EstablishmentPeriod EstablishmentPeriod { set; get; }

        public virtual ICollection<StudentQualification> StudentQualifications { set; get; }
		public virtual ICollection<Observation> Observations { set; get; }
		public virtual ICollection<AttendanceDate> AttendanceDates { set; get; }
	}
}
