﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Action : ModelBase
	{
		[Required]
		public int Order { set; get; }


		public virtual Activity Activity { set; get; }

		/// <summary>
		/// Referencia a GeneralHelperLibray/ActionType
		/// </summary>
		[Required]		
		public int Type { set; get; }

		/// <summary>
		/// Hace referencia a la clase que debe ejecutar su tarea
		/// Publication/Link/ActionLesson/ActionFileGroup/Test
		/// </summary>
		[Required]
		public int Task_Id { set; get; }
	}
}
