﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    public class StudentSpecialTest : ModelBase
    {
        [NotMapped]
        public override int Id { set; get; }
        public bool Active { set; get; } = false;
        public int Attempts { set; get; }

        [Required]
        public bool Done { set; get; }

        public DateTime Start { set; get; }

        public string User_Id { set; get; }
        [ForeignKey("User_Id")]
        public virtual User User { set; get; }

        public int SpecialTest_Id { set; get; }
        [ForeignKey("SpecialTest_Id")]
        public virtual SpecialTest SpecialTest { set; get; }

        public virtual ICollection<StudentAnswer> StudentAnswers { set; get; }
    }
}