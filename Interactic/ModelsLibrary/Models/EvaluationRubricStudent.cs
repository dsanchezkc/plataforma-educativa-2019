﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class EvaluationRubricStudent : ModelBase
    {
        public int Id { set; get; }
        public int EvaluationRubric_Id { set; get; }
        [ForeignKey("EvaluationRubric_Id")]
        public virtual EvaluationRubric EvaluationRubric { set; get; }
        public int Criterion_Id { set; get; }
        [ForeignKey("Criterion_Id")]
        public virtual Criterion Criterion { set; get; }
        public string Student_Id { set; get; }
        [ForeignKey("Student_Id")]
        public virtual User Student { set; get; }
        public int Score { set; get; }
    }
}