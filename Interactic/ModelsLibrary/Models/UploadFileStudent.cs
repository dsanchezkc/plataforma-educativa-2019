﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	/// <summary>
	/// Clase que almacena la información por parte del profesor para que el alumno pueda subir archivos
	/// 
	/// Clases Asociadas : UploadFileStudentItem
	/// </summary>
	public class UploadFileStudent : ModelBase
	{
		[Required]
		public string Title { set; get; }

		public virtual ICollection<UploadFileStudentQualification> UploadFileStudentQualifications { set; get; }
	}
}
