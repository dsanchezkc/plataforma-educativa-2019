﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Model
{
	public class UnityViewed : ModelBase
	{
		/// <summary>
		/// Tiempo acumulado en una sesión
		/// Tiempo acumulado en minutos
		/// </summary>
		public int Timer { set; get; }

		public virtual User User { set; get; }

		public virtual Unity Unity { set; get; }

		/// <summary>
		/// Código de sesión
		/// Cada vez que un usuario inicia sesión se genera un código nuevo
		/// </summary>
		[Required]
		public string SessionCode { set; get; }
    }
}