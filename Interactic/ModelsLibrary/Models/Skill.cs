﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class Skill : ModelBase
	{		
		[Required]
		public string Name { set; get; }

		public virtual SkillsGroup SkillsGroup { set; get; }

		public virtual ICollection<Question> Questions { set; get; }
	}
}