﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionPairedWord : SpecialQuestion
    {
		public virtual ICollection<QuestionPairedWordItem> QuestionPairedWordItems { set; get; }

    }
}
