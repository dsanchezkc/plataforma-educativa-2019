﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Collections.Generic;

namespace Model
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser<string, IdentityUserLogin, UserRoleEducationalEstablishment, IdentityUserClaim>
    {
		[Required]
		public string Name { set; get; }
		[Required]
        public string MotherLastName { set; get; }
		[Required]
        public string FatherLastName { set; get; }
	
		public string Address { set; get; }
		public string ImagePath { set; get; }

		public bool Status { set; get; } = true;

		public virtual ICollection<Observation> Observations { set; get; }
		public virtual ICollection<StudentTest> UserTests { set; get; }
        public virtual ICollection<StudentSpecialTest> UserSpecialTests { set; get; }
        public virtual ICollection<StudentCourse> StudentCourses { set; get; }
		public virtual ICollection<Answer> Answers { set; get; }
		public virtual ICollection<TeacherCourseSubject> TeacherCourseSubjects { set; get; }
		public virtual ICollection<Guardian> Guardians { set; get; }
		public virtual ICollection<Conversation> Conversations { set; get; }
		public virtual ICollection<UserRoleEducationalEstablishment> UserRoleEducationalEstablishments { set; get; }
		public virtual ICollection<Absent> Absents { get; set; }
		public virtual ICollection<StudentQualification> StudentQualifications { set; get; }
        public virtual ICollection<Message> Messages { set; get; }
		public virtual ICollection<AuthorMessage> AuthorMessages { set; get; }
        public virtual ICollection<LogFirebase> LogFirebase { set; get; }
        public virtual ICollection<EvaluationRubricStudent> EvaluationRubricStudents { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
        public virtual ICollection<Topic> Topics { set; get; }
        //public virtual ICollection<PostQualification> PostQualifications { set; get; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }  
}