﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class MessageAttached : ModelBase
	{
		public virtual Message Message { set; get; }
		public virtual Attached Attached { set; get; }
	}
}