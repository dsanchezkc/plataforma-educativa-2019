﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Observation : ModelBase
	{
		[Required]
		public string Message { set; get; }

		[Column("User")]
		public virtual User User { set; get; }

		[Column("Teacher")]
		public virtual User Teacher { set; get; }

		public virtual Course Course { set; get; }

		public virtual PeriodDetail PeriodDetail { set; get; }

		public virtual BaseSubject BaseSubject { set; get; }		
	}
}
