﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	/// <summary>
	/// Grupo de archivos subidos por un alumno más su calificación
	/// Clases asociadas : UploadFileStudent
	/// </summary>
	public class UploadFileStudentQualification : ModelBase
	{
		[Required, Range(0,100)]
		public int Qualification { set; get; }

		public virtual UploadFileStudent UploadFileStudent { set; get; }

		public virtual User Student { set; get; }

		public virtual ICollection<UploadFileStudentItem> UploadFileStudentItems { set; get; }
	}
}
