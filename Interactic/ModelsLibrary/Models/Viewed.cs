﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Viewed : ModelBase
	{
		/// <summary>
		/// Nombre de la tabla  a que hace referencia
		/// </summary>
		public string ModelBase_Type { set; get; }

		public int ModelBase_Id { set; get; }

		public virtual User User { set; get; }

		public void SetData(ModelBase model, User user)
		{
			ModelBase_Id = model.Id;
			ModelBase_Type = model.GetType().FullName;
			User = user;
		}

		public bool IsContain(ModelBase model, User user)
		{
			return ModelBase_Type == model.GetType().FullName && ModelBase_Id == model.Id && User.Id == user.Id; 
		}
	}
}
