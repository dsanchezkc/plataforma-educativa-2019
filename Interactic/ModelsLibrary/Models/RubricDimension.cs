﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class RubricDimension : ModelBase
    {
        public int Id { set; get; }
        public int Rubric_Id { set; get; }
        [ForeignKey("Rubric_Id")]
        public virtual Rubric Rubric { set; get; }
        public int Dimension_Id { set; get; }
        [ForeignKey("Dimension_Id")]
        public virtual Dimension Dimension { set; get; }
    }
}