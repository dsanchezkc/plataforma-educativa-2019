﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class Guardian : ModelBase
	{
		[NotMapped]
		public override int Id { set; get; }

		[Key]
		public string User_Id { set; get; }
		[ForeignKey("User_Id")]
		public virtual User User { set; get; }

		[Key]
		public string User_Student_Id { set; get; }
		[ForeignKey("User_Student_Id")]
		public virtual User Student { set; get; }

		[Key]
		public int EducationalEstablishment_Id { set; get; }
		[ForeignKey("EducationalEstablishment_Id")]
		public virtual EducationalEstablishment EducationalEstablishment { set; get; }
	}
}