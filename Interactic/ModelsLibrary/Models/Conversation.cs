﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class Conversation : ModelBase
    {
		/// <summary>
		/// Indica si debe ser visible para la (Bandeja de entrada)
		/// Se supone que cuando se envía un mensaje este no aparece en la bandeja de 
		/// entrada de quien lo envió hasta que sea respondido.
		/// </summary>
		public virtual bool IsVisible { set; get; } = true;

		/// <summary>
        /// Usuario que recibió el mensaje
        /// </summary>
        public virtual User ToUser { set; get; }

        public bool Readed { set; get; }

		public string Subject { get; set; }

		public virtual ICollection<ConversationMessage> ConversationMessages { set; get; }
	}
}