﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class Province : ModelBase
	{
		public string Name { set; get; }
		public virtual Region Region { set; get; }
		public virtual ICollection<Commune> Communes { set; get; }
	}
}