﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    public class LogFirebase : ModelBase
    {
        [MaxLength(200)]
        public string Token_Extern { set; get; }

        public string User_Id { set; get; }
        [ForeignKey("User_Id")]
        public virtual User User { set; get; }

    }
}