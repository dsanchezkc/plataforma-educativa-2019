﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
	public class Course : ModelBase
	{
		public string Name { set; get; }
		public int Year { set; get; }
		public virtual EducationalEstablishment EducationalEstablishment { set; get; }
		public virtual ICollection<StudentCourse> StudentCourses { set; get; }
		public virtual ICollection<CourseSubject> CourseSubjects { set; get; }
        public virtual ICollection<Event> Events { set; get; }
        public virtual ICollection<CourseTest> CourseTests { set; get; }
        public virtual ICollection<Observation> Observations { set; get; }
		public virtual ICollection<AttendanceDate> AttendanceDates { get; set; }
	}
}