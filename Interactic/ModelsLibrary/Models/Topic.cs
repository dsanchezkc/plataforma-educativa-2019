﻿using Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Topic : ModelBase
	{
		[Required]
		public string Title { set; get; }

		[Required]
		public string Description { set; get; }

		[Required]
		public bool AllowPosting { set; get; } = true;

		public virtual User Author { set; get; }

		public virtual Forum Forum { set; get; }

		public virtual ICollection<Post> Posts { set; get; }
		public virtual ICollection<TopicFile> TopicFiles { set; get; }
	}
}
