﻿using ModelEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class SpecialQuestionSpecialTest : ModelBase
    {
        public int SpecialTest_Id { set; get; }
        [ForeignKey("SpecialTest_Id")]
        public virtual SpecialTest SpecialTest { set; get; }


        public int SpecialQuestion_Id { set; get; }
        [ForeignKey("SpecialQuestion_Id")]
        public virtual SpecialQuestion SpecialQuestion { set; get; }
    }
}

