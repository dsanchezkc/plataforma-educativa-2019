﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Test : ModelBase
    {
        public string Title { set; get; }

        public string NameDocument { set; get; }
        public string PathDocument { set; get; }
        public string NameDocumentCorrection { set; get; }
        public string PathDocumentCorrection { set; get; }

        /// <summary>
        /// Si es verdadero el alumno puede ver el documento cuando todos sus compañeros hayan respondido el Test
        /// Si es falso el alumno puede el documento después de realizar el test
        /// </summary>
        public bool IsPublishedDocumentCorrectionWithRestriction { set; get; }

        public int NumberAlternatives { set; get; }
        public int Timer { set; get; } = 0;
        public bool? Global { get; set; }
        public virtual SubUnity SubUnity { set; get; }
        public virtual ICollection<StudentTest> StudentTests { set; get; }
        public virtual ICollection<CourseTest> CourseTests { set; get; }
        public virtual ICollection<Question> Questions { set; get; }
        public virtual ICollection<AchievementIndicator> AchievementIndicator { set; get; }
        public int? Subject_Id { set; get; }
        [ForeignKey("Subject_Id")]
        public virtual Subject Subject { set; get; }
    }
}
