﻿using ModelEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class Question : ModelBase
	{
		public string Choice { set; get; }
		public int Order { set; get; }
		public virtual Test Test { set; get; }
        //public virtual SpecialTest SpecialTest { set; get; }
        public virtual Skill Skill { set; get; }
		public virtual ICollection<Answer> Answers { set; get; }
        public string ComplementaryAnswer { set; get; } = "";
        public virtual string Name { set; get; }
        public QuestionType QuestionType { set; get; }
    }
}

