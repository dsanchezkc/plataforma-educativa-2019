﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class Link : ModelBase
	{
		[Required]
		public string Url { set; get; }

		[Required]
		public string Name { set; get; }
    }
}
