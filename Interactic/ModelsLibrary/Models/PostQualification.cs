﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class PostQualification : ModelBase
	{
		[NotMapped]
		public override int Id { set; get; }

		[Range(0, 100)]
		public int Qualification { set; get; }

		[Key]
		public string User_Id { set; get; }

		[ForeignKey("User_Id")]
		public virtual User User { set; get; }

		[Key]
		public int Post_Id { set; get; }
		[ForeignKey("Post_Id")]
		public virtual Post Post { set; get; }
	}
}
