﻿using System;

namespace Model
{
	public class StudentQualification : ModelBase
	{
		public int Order { get; set; }
        public float Qualification { get; set; }
        public virtual User Student { set; get; }
		public virtual BaseSubject BaseSubject { set; get; }
		public virtual PeriodDetail PeriodDetail { set; get; }
    }
}
