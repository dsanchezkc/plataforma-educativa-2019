﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public enum Period { SemiAnnual =0, Quarterly=1, Annual=2 }

	public class EducationalEstablishment : ModelBase
	{
		[Required]
		public string Name { set; get; }

		public string NameImage { set; get; }
		public string PathImage { set; get; }

		[Required]
		public string Address { set; get; }
		public virtual Commune Commune { set; get; }
		public virtual EstablishmentPeriod EstablishmentPeriod { set; get; }
		public virtual ICollection<Course> Courses { set; get; }
		public virtual ICollection<UserRoleEducationalEstablishment> UserRoleEducationalEstablishments { set; get; }
		public virtual ICollection<Guardian> Guardians { set; get; }
	}
}