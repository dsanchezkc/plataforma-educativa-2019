﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
	public class Message : ModelBase
	{
        /// <summary>
        /// Usuario que envió el mensaje
        /// </summary>
        public virtual User FromUser { set; get; }
		public string Body { set; get; }	
		
		public virtual Message MessageParent { set; get; }		
		public virtual Message MessageChild { set; get; }

		public virtual ICollection<MessageAttached> MessageAttacheds { set; get; }
		public virtual ICollection<ConversationMessage> ConversationMessages { set; get; }
		
		public virtual ICollection<Message> MessageParents { set; get; }
		public virtual ICollection<Message> MessageChildren { set; get; }
	}
}