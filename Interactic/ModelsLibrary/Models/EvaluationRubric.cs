﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class EvaluationRubric : ModelBase
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public int? Course_Id { set; get; }
        [ForeignKey("Course_Id")]
        public virtual Course Course { set; get; }
        public int? Subject_Id { set; get; }
        [ForeignKey("Subject_Id")]
        public virtual Subject Subject { set; get; }
        public int? Rubric_Id { set; get; }
        [ForeignKey("Rubric_Id")]
        public virtual Rubric Rubric { set; get; }
        public string Description { set; get; }
        public virtual ICollection<EvaluationRubricStudent> EvaluationRubricStudents { set; get; }
    }
}