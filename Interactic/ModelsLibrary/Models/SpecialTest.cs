﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class SpecialTest : ModelBase
    {
        [Required]
        public int Attempts { set; get; } = 1;

        [Required]
        public int Timer { set; get; }

        [Required]
        public string Title { set; get; }

        public bool Global { set; get; }

        public bool? Random { set; get; }


        /// <summary>
        /// Se refiere si es Test para Nota Final (Solamente puede existir uno por Sub-Unidad)
        /// </summary>
        [Required]
        public bool IsTestReport { set; get; }

        public virtual CourseSubject CourseSubject { set; get; }

        public virtual Subject Subject { set; get; }

        public virtual SubUnity SubUnity { set; get; }

        public virtual ICollection<SpecialQuestion> SpecialQuestions { set; get; }

        public virtual ICollection<StudentSpecialTest> StudentSpecialTests { set; get; }

        public virtual ICollection<SpecialQuestionSpecialTest> SpecialQuestionSpecialTests { set; get; }
    }
}
