﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class SubSubject : BaseSubject
    {
        public virtual Subject Subject { set; get; }

		public virtual ICollection<CourseSubSubject> CourseSubSubjects { set; get; }		
    }
}