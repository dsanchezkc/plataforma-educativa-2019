﻿using ModelEnum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionItem : ModelBase 
	{
		public virtual string Text { set; get; } = "";

		public virtual bool Value { set; get; }

		[Required]
		public QuestionType QuestionType { set; get; }

		public virtual ICollection<StudentAnswer> StudentAnswers { set; get; }
        
    }
}
