﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionAlternative : SpecialQuestion
    {
		public virtual ICollection<QuestionAlternativeItem> QuestionAlternativeItems { set; get; }

    }
}
