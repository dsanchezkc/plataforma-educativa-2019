﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class ActionLesson : ModelBase
	{
		[Required]
		public string Title { set; get; }

		[Required]
		public string Description { set; get; }
    }
}
