﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class RoleName
    {
        public const string Administrador = "Administrador";
        public const string SubAdministrador = "SubAdministrador";
        public const string Teacher = "Profesor";
        public const string Student = "Alumno";
        public const string Guardian = "Apoderado";
    }
}