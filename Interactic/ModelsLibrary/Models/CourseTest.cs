﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class CourseTest : ModelBase
    {
        public int Test_Id { set; get; }
        [ForeignKey("Test_Id")]
        public virtual Test Test { set; get; }

        public int Course_Id { set; get; }
        [ForeignKey("Course_Id")]
        public virtual Course Course { set; get; }
        public virtual bool Active { set; get; } = false;
    }
}

