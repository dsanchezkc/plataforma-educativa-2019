﻿
using System;
using System.Collections.Generic;


namespace Model
{
    public class EstablishmentPeriod : ModelBase
    {
        public string Name { get; set; }
        public virtual ICollection<PeriodDetail> PeriodDetails { get; set; }
        public virtual ICollection<EducationalEstablishment> EducationalEstablishments { set; get; }
    }
}