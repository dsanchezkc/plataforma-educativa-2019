﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
	public class QuestionAlternativeItem : QuestionItem
	{
		public virtual QuestionAlternative QuestionAlternative { set; get; }

        //public virtual SpecialQuestion SpecialQuestion { set; get; }

        [Required]
		public override string Text { set; get; }

		[Required]
		public override bool Value { set; get; }
	}
}
