﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class ReportType
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}