﻿using System.Collections.Generic;

namespace GeneralHelper
{
	public enum ActionType
	{
		File =1,
		Test =2,
		Link =3,
		Forum = 4,
		ActionLesson= 5,
		UploadFileStudent=6
	}
}