﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web;

namespace GeneralHelper
{
	public static class ExcelHelper
	{
		private static string GetConnectionString(string file)
		{
			string extension = file.Split('.').Last();

			if (extension == "xls")
			{
				//Excel 2003 and Older
				return "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + file + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;ImportMixedTypes=Text;\"";
			}
			else if (extension == "xlsx")
			{
				//Excel 2007, 2010, 2012, 2013
				//props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
				//props["Extended Properties"] = "Excel 12.0 XML;HDR=NO;IMEX=1";

				return "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + file + ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1;ImportMixedTypes=Text;\"";
			}
			else
				throw new Exception(string.Format("error file: {0}", file));
		}

		public static DataSet GetDataSetFromExcelFile(string file)
		{
			DataSet ds = new DataSet();


			string connectionString = GetConnectionString(file);

			using (OleDbConnection conn = new OleDbConnection(connectionString))
			{
				conn.Open();
				OleDbCommand cmd = new OleDbCommand();
				cmd.Connection = conn;

				// Get all Sheets in Excel File
				DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
				
				// Loop through all Sheets to get data
				foreach (DataRow dr in dtSheet.Rows)
				{
					string sheetName = dr["TABLE_NAME"].ToString();

					if (!sheetName.EndsWith("$"))
						continue;

					// Get all rows from the Sheet
					cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

					DataTable dt = new DataTable();
					dt.TableName = sheetName;

					OleDbDataAdapter da = new OleDbDataAdapter(cmd);
					da.Fill(dt);

					ds.Tables.Add(dt);
				}

				cmd = null;
				conn.Close();
			}

			return ds;
		}
	}
}