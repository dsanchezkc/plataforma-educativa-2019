﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace GeneralHelper
{
	public class ImageResult
	{
		public bool Success { get; set; }
		public string ImageName { get; set; }
		public string ErrorMessage { get; set; }
	}

	public class ImageHelper
	{
		/// <summary>
		/// Guarda una imagen
		/// </summary>
		/// <param name="file"></param>
		/// <param name="path"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="resolutionMax"></param>
		/// <param name="autoSizeSymmetric"></param>
		/// <returns>Retorna el nombre y extención del archivo generado</returns>
		public static string SaveImage(HttpPostedFile file, string path, int width = 0, int height = 0, int resolutionMax = 0, bool autoSizeSymmetric = false)
		{
			Directory.CreateDirectory(path);
			var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
			path = Path.Combine(path, fileName);

			if (!ValidateExtension(Path.GetExtension(file.FileName)))
				return null;
			try
			{
				file.SaveAs(path);
				Image imgOriginal = Image.FromFile(path);
				Image imgActual = Scale(imgOriginal, width, height, resolutionMax, autoSizeSymmetric);
				imgOriginal.Dispose();
				imgActual.Save(path);
				imgActual.Dispose();
				return fileName;
			}
			catch (Exception)
			{
				return null;
			}
		}

		private static bool ValidateExtension(string extension)
		{
			switch (extension.ToLower())
			{
				case ".jpg":
					return true;
				case ".png":
					return true;
				case ".jpeg":
					return true;
				default:
					return false;
			}
		}

		private static Image Scale(Image imgPhoto,float width,float height, float resolutionMax, bool autoSizeSymmetric)
		{
			float sourceWidth = imgPhoto.Width;
			float sourceHeight = imgPhoto.Height;
			float destHeight = 0;
			float destWidth = 0;
			int sourceX = 0;
			int sourceY = 0;
			int destX = 0;
			int destY = 0;

			if (resolutionMax != 0)
			{
				if (width == 0 && height == 0)
				{
					width = sourceWidth;
					height = sourceHeight;
				}

				if (width > height && width > resolutionMax)
				{
					height = (resolutionMax / width) * height;
					width = resolutionMax;
				}
				else
				if (height > width && height > resolutionMax)
				{
					width = (resolutionMax / height) * width;
					height = resolutionMax;
				}				
			}

			// force resize, might distort image
			if (width != 0 && height != 0)
			{
				destWidth = width;
				destHeight = height;
			}
			// change size proportially depending on width or height
			else if (height != 0)
			{
				destWidth = (float)(height * sourceWidth) / sourceHeight;
				destHeight = height;
			}
			else if (width != 0)
			{
				destWidth = width;
				destHeight = (float)(sourceHeight * width / sourceWidth);
			}
			else
			{
				destWidth = sourceWidth;
				destHeight = sourceHeight;
			}

			if (autoSizeSymmetric)
			{
				height = destHeight;
				width = destWidth;

				if (destWidth > destHeight)
				{
					destX-= ((int)destWidth - (int)destHeight) / 2;
					destWidth = destHeight;					
				}

				if (destHeight > destWidth)
				{
					destY -= ((int)destHeight - (int)destWidth) / 2;
					destHeight = destWidth;					
				}
			}

			Bitmap bmPhoto = new Bitmap((int)destWidth, (int)destHeight,
										PixelFormat.Format32bppPArgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

			Graphics grPhoto = Graphics.FromImage(bmPhoto);
			grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

			if (autoSizeSymmetric)
				grPhoto.DrawImage(imgPhoto,
									new Rectangle(destX, destY, (int)width, (int)height),
									new Rectangle(sourceX, sourceY, (int)sourceWidth, (int)sourceHeight),
									GraphicsUnit.Pixel);
			else
				grPhoto.DrawImage(imgPhoto,
						new Rectangle(destX, destY, (int)destWidth, (int)destHeight),
						new Rectangle(sourceX, sourceY, (int)sourceWidth, (int)sourceHeight),
						GraphicsUnit.Pixel);

			grPhoto.Dispose();

			return bmPhoto;
		}
	}
}