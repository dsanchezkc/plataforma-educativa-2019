﻿using System.Collections.Generic;

namespace GeneralHelper
{
	public static class CollectionHelpers
	{
		public static void AddRange<T>(this ICollection<T> destination, IEnumerable<T> source)
		{
			foreach (T item in source)
				destination.Add(item);			
		}

		public static void RemoveRange<T>(this ICollection<T> destination, IEnumerable<T> source)
		{
			var list = new List<T>(source);

			for (var i = 0; i < list.Count; i++)
				destination.Remove(list[i]);
		}
	}
}