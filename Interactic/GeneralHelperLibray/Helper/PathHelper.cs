﻿using System.Collections.Generic;
using System.IO;

namespace GeneralHelper
{
	public static class PathHelper
	{
		public static string RootPath { get { return @"C:\EvaluandomeResources\"; } }

		public static string AvatarPath { get { return @"Images\Avatars\"; } }
		public static string EstablishmentPath { get { return @"Images\Establishment\"; } }
		public static string UnityResourcesPath { get { return @"UnityResources\"; } }
		public static string ForumTopicPath { get { return @"Forum-Topic\"; } }
		public static string ForumPostPath { get { return @"Forum-Post\"; } }
		public static string UploadFileStudentPath { get { return @"UploadFileStudent\"; } }

		public static string TemporalPath { get { return @"Temporal\"; } }


		public static string GetFullPath(string pathRelative)
		{
			return Path.Combine(RootPath, pathRelative);
		}

		public static PathInfo GeneratePath(string pathRelative)
		{
			return new PathInfo(Path.Combine(RootPath, pathRelative), pathRelative);
		}
	}

	public class PathInfo
	{
		public string fullPath;
		public string relativePath;

		public PathInfo(string fullPath, string relativePath)
		{
			this.fullPath = fullPath;
			this.relativePath = relativePath;
		}
	}
}