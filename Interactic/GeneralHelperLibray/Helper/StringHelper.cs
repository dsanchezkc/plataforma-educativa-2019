﻿using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace GeneralHelper
{
	public static class StringHelper
	{
		public static string ObjectToStringBase64(this object _object)
		{
			return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(new JavaScriptSerializer() { MaxJsonLength = int.MaxValue }.Serialize(_object)));
		}
	}
}