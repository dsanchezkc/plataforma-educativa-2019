﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace GeneralHelper
{
	public static class FileHelper
	{
		private static string GenerateSHA256String(string inputString)
		{
			SHA256 sha256 = SHA256Managed.Create();
			byte[] bytes = Encoding.UTF8.GetBytes(inputString);
			byte[] hash = sha256.ComputeHash(bytes);
			return GetStringFromHash(hash);
		}

		private static string GenerateSHA512String(string inputString)
		{
			SHA512 sha512 = SHA512Managed.Create();
			byte[] bytes = Encoding.UTF8.GetBytes(inputString);
			byte[] hash = sha512.ComputeHash(bytes);
			return GetStringFromHash(hash);
		}

		private static string GetStringFromHash(byte[] hash)
		{
			StringBuilder result = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
				result.Append(hash[i].ToString("X2"));			
			return result.ToString();
		}

		public static string ChangeNameFile(string path, string file)
		{
			var ext = Path.GetExtension(file);
			var name = GenerateSHA256String(file + DateTime.Now.ToString());

			if (File.Exists(path + @"\" + name + ext))
				return ChangeNameFile(path, name + ext);

			return name + ext;
		}

		public static byte[] GetBinaryFile(string path)
		{
			byte[] bytes;
			using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
			{
				bytes = new byte[file.Length];
				file.Read(bytes, 0, (int)file.Length);
			}

			return bytes;
		}

		public static string GetImageBase64(string path)
		{
			if (path == null || path == "")
				return "";

			try
			{
				var binary = FileHelper.GetBinaryFile(path);
				return "data:image/" + Path.GetExtension(path).Substring(1) + ";base64," + Convert.ToBase64String(binary, 0, binary.Length);
			}
			catch (Exception)
			{
				return "";
			}

		}
	}
}