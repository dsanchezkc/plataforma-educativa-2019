﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using WebApi.CloneModels;
using GeneralHelper;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Net.Http.Headers;
using System.Data;
using System.Text;
using System.IO;
using ClosedXML.Excel;
using OfficeOpenXml;


namespace WebApiEvaluandome.Controllers
{
    
    [Authorize(Roles ="Administrador, SubAdministrador, Profesor")]
	public class AttendanceDateController : ApiController
    {
        private InteracticContext db;

        public AttendanceDateController()
        {
            db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
        }

		/// <summary>
		/// Author AP
		/// Entrega el listado de registros.
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("Api/AttendanceDate/GetAttendanceDatesByCourseAndPeriodDetailAndBaseSubjectAndDayTrip")]
		public HttpResponseMessage GetAttendanceDatesByCourseAndPeriodDetailAndBaseSubjectAndDayTrip(int course_id, int perioddetail_id, int basesubject_id, int daytrip_id)
		{
			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == course_id && (p.Subject_Id == basesubject_id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == basesubject_id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == course_id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			var attendanceDates = db.AttendanceDate.Where(p => p.Status && p.Course.Id == course_id && p.Daytrip.Id == daytrip_id && p.PeriodDetail.Id == perioddetail_id && p.BaseSubject.Id == basesubject_id);

			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(attendanceDates, AttendanceDateCloneHelper.AttendanceDateMap_1));
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Post([FromBody] AttendanceDate attendanceDate)
		{
			if (attendanceDate == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == attendanceDate.Course.Id && (p.Subject_Id == attendanceDate.BaseSubject.Id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == attendanceDate.BaseSubject.Id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == attendanceDate.Course.Id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			attendanceDate.BaseSubject = db.BaseSubject.FirstOrDefault(p => p.Id == attendanceDate.BaseSubject.Id);
			attendanceDate.Course = db.Course.FirstOrDefault(p => p.Id == attendanceDate.Course.Id);
			attendanceDate.Daytrip = db.Daytrip.FirstOrDefault(p => p.Id == attendanceDate.Daytrip.Id);
			attendanceDate.PeriodDetail = db.PeriodDetail.FirstOrDefault(p => p.Id == attendanceDate.PeriodDetail.Id);			

			foreach (var absent in attendanceDate.Absents)
			{
				var studentCourse = db.StudentCourse.FirstOrDefault(p => p.Status && p.User_Id == absent.User.Id && p.Course_Id == attendanceDate.Course.Id);

				if (studentCourse != null)
					absent.User = studentCourse.User;

				absent.AttendanceDate = null;
			}

			db.AttendanceDate.Add(attendanceDate);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Put([FromBody] AttendanceDate _attendanceDate)
		{
			var attendanceDate = db.AttendanceDate.FirstOrDefault(p => p.Id == _attendanceDate.Id);

			if (attendanceDate == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == attendanceDate.Course.Id && (p.Subject_Id == attendanceDate.BaseSubject.Id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == attendanceDate.BaseSubject.Id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == attendanceDate.Course.Id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			db.Absent.RemoveRange(attendanceDate.Absents);
			db.SaveChanges();

			foreach (var _absent in _attendanceDate.Absents)
			{
				var studentCourse = db.StudentCourse.FirstOrDefault(p => p.Status && p.User_Id == _absent.User.Id && p.Course_Id == attendanceDate.Course.Id);

				if (studentCourse != null)
					_absent.User = studentCourse.User;

				_absent.AttendanceDate = null;
			}

			attendanceDate.Absents.AddRange(_attendanceDate.Absents);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}


		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Delete(int id)
		{
			var attendanceDate = db.AttendanceDate.FirstOrDefault(p => p.Id == id);

			if (attendanceDate == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == attendanceDate.Course.Id && (p.Subject_Id == attendanceDate.BaseSubject.Id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == attendanceDate.BaseSubject.Id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == attendanceDate.Course.Id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			db.Absent.RemoveRange(attendanceDate.Absents);
			db.AttendanceDate.Remove(attendanceDate);
			db.SaveChanges();

			return Request.CreateResponse(HttpStatusCode.OK);
		}

        [AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Authorize(Roles = "Administrador")]
        [Route("AttendanceForStudentExcel")]
        public HttpResponseMessage AttendanceForStudentExcel(string _course_id)
        {

            string str = "<table><tr><th>Informe de asistencia</th></tr>";
            
            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);
            var _coursId = _course_id;
            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var course = db.Course.Where(x => x.Status == true &&  x.Year == GlobalParameters.Year &&
                                                       x.Id == courseId &&
                                                       x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId
                                            ).FirstOrDefault();
            var absents = db.Absent.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == course.Id);
            //if (absents == null) return Content("No hay inasistencias registradas.");
            var periodDetails = db.PeriodDetail.Where(x => x.Status == true &&  x.EstablishmentPeriod.Id == Establishment.EstablishmentPeriod.Id).ToList();
            var attendanceDates = db.AttendanceDate.ToList();
            var dayTrips = db.Daytrip.ToList();
            var ids = db.StudentCourse.Where(x => x.Status == true &&  x.Course.Id == courseId).Select(x => x.User_Id).Distinct();
            var students = db.Users.Where(x => x.Status == true &&  ids.Contains(x.Id));

            str = string.Concat(str, "<tr><td><b>Establecimiento</b></td>", "<td>", Establishment.Name, "</td></tr>");
            str = string.Concat(str, "<tr><td><b>Curso</b></td>", "<td>", course.Name, "</td></tr>");
            str = string.Concat(str, "<tr></tr>");
            str = string.Concat(str, "</table>");

            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr><b>1.- Porcentaje de asistencia por alumno</b></tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>N°</th><th>Apellido P</th><th>Apellido M</th><th>Nombre</th>");
            for (int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            var count = 1;

            foreach (var student in students)
            {
                str = string.Concat(str, "<tr>");
                str = string.Concat(str, "<td>", count, "</td>");
                str = string.Concat(str, "<td>", student.FatherLastName, "</td>");
                str = string.Concat(str, "<td>", student.MotherLastName, "</td>");
                str = string.Concat(str, "<td>", student.Name, "</td>");
                for (int i = 1; i <= 12; i++)
                {

                    if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() > 0)
                    {
                        str = string.Concat(str, "<td>", Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Date.Month == i && x.AttendanceDate.Course.Id == courseId).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero), "</td>");

                    }
                    else
                    {
                        str = string.Concat(str, "<td> - </td>");
                    }

                 }
                if(attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count() > 0)
                {
                    str = string.Concat(str, "<td>", Math.Round((((double) attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Course.Id == courseId).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero), "</td>");
                }
                else
                {
                    str = string.Concat(str, "<td> - </td>");
                }
                str = string.Concat(str, "</tr>");
                
                count += 1;
            }
            str = string.Concat(str, "</table>");

            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr><b>2.- Porcentaje de asistencia por alumno - Jornada Mañana</b></tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>N°</th><th>Apellido P</th><th>Apellido M</th><th>Nombre</th>");
            for (int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            count = 1;

            foreach (var student in students)
            {
                str = string.Concat(str, "<tr>");
                str = string.Concat(str, "<td>", count, "</td>");
                str = string.Concat(str, "<td>", student.FatherLastName, "</td>");
                str = string.Concat(str, "<td>", student.MotherLastName, "</td>");
                str = string.Concat(str, "<td>", student.Name, "</td>");
                for (int i = 1; i <= 12; i++)
                {

                    if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() > 0)
                    {
                        str = string.Concat(str, "<td>", Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Date.Month == i && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Daytrip.Id == 1).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 1).Count()), 1, MidpointRounding.AwayFromZero), "</td>");

                    }
                    else
                    {
                        str = string.Concat(str, "<td> - </td>");
                    }

                }
                if (attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count() > 0)
                {
                    str = string.Concat(str, "<td>", Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.Daytrip.Id == 1).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Course.Id == courseId).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero), "</td>");
                }
                else
                {
                    str = string.Concat(str, "<td> - </td>");
                }
                str = string.Concat(str, "</tr>");

                count += 1;
            }
            str = string.Concat(str, "</table>");

            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr><b>3.- Porcentaje de asistencia por alumno - Jornada Tarde</b></tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>N°</th><th>Apellido P</th><th>Apellido M</th><th>Nombre</th>");
            for (int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            count = 1;

            foreach (var student in students)
            {
                str = string.Concat(str, "<tr>");
                str = string.Concat(str, "<td>", count, "</td>");
                str = string.Concat(str, "<td>", student.FatherLastName, "</td>");
                str = string.Concat(str, "<td>", student.MotherLastName, "</td>");
                str = string.Concat(str, "<td>", student.Name, "</td>");
                for (int i = 1; i <= 12; i++)
                {

                    if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() > 0)
                    {
                        str = string.Concat(str, "<td>", Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Date.Month == i && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Daytrip.Id == 2).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero), "</td>");

                    }
                    else
                    {
                        str = string.Concat(str, "<td> - </td>");
                    }

                }
                if (attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count() > 0)
                {
                    str = string.Concat(str, "<td>", Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.Daytrip.Id == 2).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Course.Id == courseId).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero), "</td>");
                }
                else
                {
                    str = string.Concat(str, "<td> - </td>");
                }
                str = string.Concat(str, "</tr>");

                count += 1;
            }
            str = string.Concat(str, "</table>");

            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr><b>4.- Porcentaje de asistencia por alumno - Jornada Noche</b></tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>N°</th><th>Apellido P</th><th>Apellido M</th><th>Nombre</th>");
            for (int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            count = 1;

            foreach (var student in students)
            {
                str = string.Concat(str, "<tr>");
                str = string.Concat(str, "<td>", count, "</td>");
                str = string.Concat(str, "<td>", student.FatherLastName, "</td>");
                str = string.Concat(str, "<td>", student.MotherLastName, "</td>");
                str = string.Concat(str, "<td>", student.Name, "</td>");
                for (int i = 1; i <= 12; i++)
                {

                    if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() > 0)
                    {
                        str = string.Concat(str, "<td>", Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Date.Month == i && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Daytrip.Id == 3).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero), "</td>");

                    }
                    else
                    {
                        str = string.Concat(str, "<td> - </td>");
                    }

                }
                if (attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count() > 0)
                {
                    str = string.Concat(str, "<td>", Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.Daytrip.Id == 3).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Course.Id == courseId).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero), "</td>");
                }
                else
                {
                    str = string.Concat(str, "<td> - </td>");
                }
                str = string.Concat(str, "</tr>");

                count += 1;
            }
            str = string.Concat(str, "</table>");
            str = string.Concat(str, "<tr><b>5.- Resumen de asistencia</b></tr>");
            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr>");
            for(int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            str = string.Concat(str, "</tr>");

            str = string.Concat(str, "<tr>");

            var absentAvg = 0;
            var globalAvg = 0;
            var countAvgs = 0;
            for(int i = 1; i <= 12; i++)
                {
                    absentAvg = 0;
                    foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId))
                    {
                        if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                        {
                            absentAvg++;
                        }
                    }

                    if (absentAvg != 0)
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count();
                        var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                        if (totalAttendanceDays != 0 && totalStudents != 0)
                        {
                            var totalOptions = (totalStudents) * totalAttendanceDays;
                            absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                            globalAvg = globalAvg + absentAvg;
                            countAvgs++;
                            str = string.Concat(str, "<td>", absentAvg, "</td>");
                        }
                        else
                        {
                        str = string.Concat(str, "<td>-</td>");

                        }
                    }
                    else
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count();
                        if (totalAttendanceDays == 0)
                        {
                            str = string.Concat(str, "<td>-</td>");
                        }
                        else
                        {
                            countAvgs++;
                            globalAvg = globalAvg + 100;
                            str = string.Concat(str, "<td>100 %</td>");
                        }
                    }
                }
            str = string.Concat(str, "</tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>Asistencia Anual</th>");
            globalAvg = countAvgs > 0 ? globalAvg / countAvgs : 0;
            str = string.Concat(str, "<td>", globalAvg , "% </td>");
            str = string.Concat(str, "</tr>");

            str = string.Concat(str, "</table>");
            str = string.Concat(str, "<tr><b>6.- Resumen de asistencia - Jornada Mañana</b></tr>");
            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr>");
            for (int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            str = string.Concat(str, "</tr>");

            str = string.Concat(str, "<tr>");

            absentAvg = 0;
            globalAvg = 0;
            countAvgs = 0;
            for (int i = 1; i <= 12; i++)
            {
                absentAvg = 0;
                foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId))
                {
                    if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                    {
                        absentAvg++;
                    }
                }

                if (absentAvg != 0)
                {
                    var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year && x.Daytrip.Id == 1).Count();
                    var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                    if (totalAttendanceDays != 0 && totalStudents != 0)
                    {
                        var totalOptions = (totalStudents) * totalAttendanceDays;
                        absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                        globalAvg = globalAvg + absentAvg;
                        countAvgs++;
                        str = string.Concat(str, "<td>", absentAvg, "</td>");
                    }
                    else
                    {
                        str = string.Concat(str, "<td>-</td>");

                    }
                }
                else
                {
                    var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 1 && x.Date.Year == DateTime.Today.Year).Count();
                    if (totalAttendanceDays == 0)
                    {
                        str = string.Concat(str, "<td>-</td>");
                    }
                    else
                    {
                        countAvgs++;
                        globalAvg = globalAvg + 100;
                        str = string.Concat(str, "<td>100 %</td>");
                    }
                }
            }
            str = string.Concat(str, "</tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>Asistencia Anual</th>");
            globalAvg = countAvgs > 0 ? globalAvg / countAvgs : 0;
            str = string.Concat(str, "<td>", globalAvg, "% </td>");
            str = string.Concat(str, "</tr>");

            str = string.Concat(str, "</table>");
            str = string.Concat(str, "<tr><b>7.- Resumen de asistencia - Jornada Tarde</b></tr>");
            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr>");
            for (int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            str = string.Concat(str, "</tr>");

            str = string.Concat(str, "<tr>");

            absentAvg = 0;
            globalAvg = 0;
            countAvgs = 0;
            for (int i = 1; i <= 12; i++)
            {
                absentAvg = 0;
                foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId))
                {
                    if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                    {
                        absentAvg++;
                    }
                }

                if (absentAvg != 0)
                {
                    var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 2 && x.Date.Year == DateTime.Today.Year).Count();
                    var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                    if (totalAttendanceDays != 0 && totalStudents != 0)
                    {
                        var totalOptions = (totalStudents) * totalAttendanceDays;
                        absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                        globalAvg = globalAvg + absentAvg;
                        countAvgs++;
                        str = string.Concat(str, "<td>", absentAvg, "</td>");
                    }
                    else
                    {
                        str = string.Concat(str, "<td>-</td>");

                    }
                }
                else
                {
                    var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year && x.Daytrip.Id == 2).Count();
                    if (totalAttendanceDays == 0)
                    {
                        str = string.Concat(str, "<td>-</td>");
                    }
                    else
                    {
                        countAvgs++;
                        globalAvg = globalAvg + 100;
                        str = string.Concat(str, "<td>100 %</td>");
                    }
                }
            }
            str = string.Concat(str, "</tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>Asistencia Anual</th>");
            globalAvg = countAvgs > 0 ? globalAvg / countAvgs : 0;
            str = string.Concat(str, "<td>", globalAvg, "% </td>");
            str = string.Concat(str, "</tr>");

            str = string.Concat(str, "</table>");
            str = string.Concat(str, "<tr><b>8.- Resumen de asistencia - Jornada Noche</b></tr>");
            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr>");
            for (int i = 1; i <= 12; i++)
            {
                var month = new DateTime(course.Year, i, 1);
                str = string.Concat(str, "<th>", month.ToString("MMMM").ElementAt(0).ToString().ToUpper(), "</th>");
            }
            str = string.Concat(str, "</tr>");

            str = string.Concat(str, "<tr>");

            absentAvg = 0;
            globalAvg = 0;
            countAvgs = 0;
            for (int i = 1; i <= 12; i++)
            {
                absentAvg = 0;
                foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId))
                {
                    if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                    {
                        absentAvg++;
                    }
                }

                if (absentAvg != 0)
                {
                    var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 3 && x.Date.Year == DateTime.Today.Year).Count();
                    var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                    if (totalAttendanceDays != 0 && totalStudents != 0)
                    {
                        var totalOptions = (totalStudents) * totalAttendanceDays;
                        absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                        globalAvg = globalAvg + absentAvg;
                        countAvgs++;
                        str = string.Concat(str, "<td>", absentAvg, "</td>");
                    }
                    else
                    {
                        str = string.Concat(str, "<td>-</td>");

                    }
                }
                else
                {
                    var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 3 && x.Date.Year == DateTime.Today.Year).Count();
                    if (totalAttendanceDays == 0)
                    {
                        str = string.Concat(str, "<td>-</td>");
                    }
                    else
                    {
                        countAvgs++;
                        globalAvg = globalAvg + 100;
                        str = string.Concat(str, "<td>100 %</td>");
                    }
                }
            }
            str = string.Concat(str, "</tr>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>Asistencia Anual</th>");
            globalAvg = countAvgs > 0 ? globalAvg / countAvgs : 0;
            str = string.Concat(str, "<td>", globalAvg, "% </td>");
            str = string.Concat(str, "</tr></table>");
            str = string.Concat(str, "<tr></tr>");
            str = string.Concat(str, "<tr><b>9.- Asistencia por Asignatura</b></tr>");
            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>Código</th>");
            str = string.Concat(str, "<th>Asignatura</th>");
            str = string.Concat(str, "<th>N° Clases</th>");
            str = string.Concat(str, "</tr>");

            var asignaturas = attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Select(x => x.BaseSubject).Distinct().ToList();
            count = 1;
            foreach(var asignatura in asignaturas)
            {
                str = string.Concat(str, "<tr>");
                str = string.Concat(str, "<td>", count, "<td>");
                str = string.Concat(str, "<td>", asignatura.Name, "<td>");
                str = string.Concat(str, "<td>", attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.BaseSubject.Id == asignatura.Id && x.Date.Year == DateTime.Today.Year).Count(), "<td>");

                str = string.Concat(str, "</tr>");
                count += 1;
            }
            str = string.Concat(str, "</table>");
            str = string.Concat(str, "<tr></tr>");
            str = string.Concat(str, "<table>");
            str = string.Concat(str, "<tr>");
            str = string.Concat(str, "<th>N°</th>");
            str = string.Concat(str, "<th>Apellido P</th>");
            str = string.Concat(str, "<th>Apellido M</th>");
            str = string.Concat(str, "<th>Nombre</th>");
            for(int i = 1; i <= asignaturas.Count(); i++)
            {
                str = string.Concat(str, "<th>", i,"</th>");
            }
            str = string.Concat(str, "<tr>");
                count = 1;
            foreach(var student in students)
            {
                str = string.Concat(str, "<tr>");
                str = string.Concat(str, "<td>", count, "</td>");
                str = string.Concat(str, "<td>", student.FatherLastName, "</td>");
                str = string.Concat(str, "<td>", student.MotherLastName, "</td>");
                str = string.Concat(str, "<td>", student.Name, "</td>");
                for(int i = 0; i < asignaturas.Count(); i++)
                {
                    if (attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.BaseSubject.Id == asignaturas.ElementAt(i).Id && x.Date.Year == DateTime.Today.Year).Count() > 0)
                    {
                        var days = attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.BaseSubject.Id == asignaturas.ElementAt(i).Id && x.Date.Year == DateTime.Today.Year).Count();
                        var absentsdays = absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId  && x.AttendanceDate.Date.Year == DateTime.Today.Year && x.User.Id == student.Id).Count();
                        var total = ((days - absentsdays) * 100) / days;

                        str = string.Concat(str, "<td>", total ,"%</td>");

                    }
                    else
                    {
                        str = string.Concat(str, "<td>-</td>");
                    }

                }
                str = string.Concat(str, "</tr>");
                count += 1;
            }
            str = string.Concat(str, "</table>");

            //foreach (var student in course.StudentCourses.Select(x => x.User).Distinct())
            //    {
            //    var avg = 0;
            //                str = string.Concat(str, "<tr>");
            //                str = string.Concat(str, "<td>", student.FatherLastName, "</td>");
            //                str = string.Concat(str, "<td>", student.MotherLastName, "</td>");
            //                str = string.Concat(str, "<td>", student.Name, "</td>");
            //                var totaAbsentsTimes = absents.Where(x => x.Status == true &&  x.User.Id == student.Id).Count();
            //                var totalAttendanceTimes = 2 * attendanceDates.Count();
            //                avg = totalAttendanceTimes > 0 ? ((totalAttendanceTimes - totaAbsentsTimes) * 100) / totalAttendanceTimes : 0;
            //                str = string.Concat(str, "<td> ", avg,  "% </td>");
            //                str = string.Concat(str, "</tr>");
            //    }

            //foreach (var user in users)
            //{
            //    str = string.Concat(str, "<tr> <td>",@user.Name,"</td> </tr>");
            //}
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent(str.ToString(), Encoding.UTF8 );
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment"); //attachment will force download
            result.Content.Headers.ContentDisposition.FileName = "data.xls";
            return result;
        }

        [AcceptVerbs("GET", "POST", "OPTIONS")]
        [System.Web.Http.HttpGet]
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("api/AttendanceDate/AttendanceExcel")]
        public System.Web.Mvc.FileResult AttendanceExcel(string _course_id)
        {
            ExcelPackage pck = new ExcelPackage();
            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);
            var _coursId = _course_id;
            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var course = db.Course.Where(x => x.Status == true &&  x.Year == GlobalParameters.Year &&
                                                       x.Id == courseId &&
                                                       x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId
                                            ).FirstOrDefault();
            var absents = db.Absent.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == course.Id);
            //if (absents == null) return Content("No hay inasistencias registradas.");
            var periodDetails = db.PeriodDetail.Where(x => x.Status == true &&  x.EstablishmentPeriod.Id == Establishment.EstablishmentPeriod.Id).ToList();
            var attendanceDates = db.AttendanceDate.ToList();
            var dayTrips = db.Daytrip.ToList();
            var ids = db.StudentCourse.Where(x => x.Status == true &&  x.Course.Id == courseId).Select(x => x.User_Id).Distinct();
            var students = db.Users.Where(x => x.Status == true &&  ids.Contains(x.Id));
            var absentAvg = 0;
            var globalAvg = 0;
            var countAvgs = 0;

            var ws = pck.Workbook.Worksheets.Add("Inicio");

            ws.Cells["A1"].Value = "Informe de asistencia";
            ws.Cells["A1"].Style.Font.Bold = true;
            ws.Cells["A2"].Value = "";
            ws.Cells["A3"].Value = "Establecimiento";
            ws.Cells["A3"].Style.Font.Bold = true;
            ws.Cells["B3"].Value = Establishment.Name;
            ws.Cells["A4"].Value = "Curso";
            ws.Cells["A4"].Style.Font.Bold = true;
            ws.Cells["B4"].Value = course.Name;

            ws = pck.Workbook.Worksheets.Add("Asistencia Anual");

            if (attendanceDates.Where(x => x.Status == true &&  x.Date.Year == DateTime.Today.Year && x.Course.Id == course.Id).Count() > 0)
            {

                ws.Cells["A1"].Value = "Resumen de asistencia Anual";
                ws.Cells["A1"].Style.Font.Bold = true;
                ws.Cells["A2"].Value = "";
                ws.Cells["A3"].Value = "Enero";
                ws.Cells["B3"].Value = "Febrero";
                ws.Cells["C3"].Value = "Marzo";
                ws.Cells["D3"].Value = "Abril";
                ws.Cells["E3"].Value = "Mayo";
                ws.Cells["F3"].Value = "Junio";
                ws.Cells["G3"].Value = "Julio";
                ws.Cells["H3"].Value = "Agosto";
                ws.Cells["I3"].Value = "Septiembre";
                ws.Cells["J3"].Value = "Octubre";
                ws.Cells["K3"].Value = "Noviembre";
                ws.Cells["L3"].Value = "Diciembre";

                for (int i = 1; i <= 12; i++)
                {
                    absentAvg = 0;
                    foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId))
                    {
                        if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                        {
                            absentAvg++;
                        }
                    }

                    if (absentAvg != 0)
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count();
                        var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                        if (totalAttendanceDays != 0 && totalStudents != 0)
                        {
                            var totalOptions = (totalStudents) * totalAttendanceDays;
                            absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                            globalAvg = globalAvg + absentAvg;
                            countAvgs++;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = absentAvg;
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                    }
                    else
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count();
                        if (totalAttendanceDays == 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                        else
                        {
                            countAvgs++;
                            globalAvg = globalAvg + 100;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "100 %";
                        }
                    }
                }

                ws.Cells["A6"].Value = "Porcentaje de asistencia por alumno";
                ws.Cells["A6"].Style.Font.Bold = true;

                ws.Cells["A8"].Value = "N°";
                ws.Cells["B8"].Value = "Apellido P";
                ws.Cells["C8"].Value = "Apellido M";
                ws.Cells["D8"].Value = "Nombre";
                ws.Cells["E8"].Value = "Enero";
                ws.Cells["F8"].Value = "Febrero";
                ws.Cells["G8"].Value = "Marzo";
                ws.Cells["H8"].Value = "Abril";
                ws.Cells["I8"].Value = "Mayo";
                ws.Cells["J8"].Value = "Junio";
                ws.Cells["K8"].Value = "Julio";
                ws.Cells["L8"].Value = "Agosto";
                ws.Cells["M8"].Value = "Septiembre";
                ws.Cells["N8"].Value = "Octubre";
                ws.Cells["O8"].Value = "Noviembre";
                ws.Cells["P8"].Value = "Diciembre";
                ws.Cells["Q8"].Value = "Total";

                var n = 9;
                var count = 1;

                foreach (var student in students)
                {
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = student.FatherLastName;
                    ws.Cells[string.Concat("C", n)].Value = student.MotherLastName;
                    ws.Cells[string.Concat("D", n)].Value = student.Name;

                    for (int i = 5; i <= 16; i++)
                    {

                        if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i - 4 && x.Course.Id == courseId).Count() > 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i - 4 && x.Course.Id == courseId).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Date.Month == i - 4 && x.AttendanceDate.Course.Id == courseId).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i - 4 && x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = "-";
                        }

                    }
                    if (attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count() > 0)
                    {
                        ws.Cells[string.Concat("Q", n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Course.Id == courseId).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Count()), 1, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        ws.Cells[string.Concat("Q", n)].Value = "-";
                    }
                    count += 1;
                    n += 1;
                }

                ws = pck.Workbook.Worksheets.Add("Asistencia Mañana");

                ws.Cells["A1"].Value = "Resumen de asistencia Anual - Jornada Mañana";
                ws.Cells["A1"].Style.Font.Bold = true;
                ws.Cells["A2"].Value = "";
                ws.Cells["A3"].Value = "Enero";
                ws.Cells["B3"].Value = "Febrero";
                ws.Cells["C3"].Value = "Marzo";
                ws.Cells["D3"].Value = "Abril";
                ws.Cells["E3"].Value = "Mayo";
                ws.Cells["F3"].Value = "Junio";
                ws.Cells["G3"].Value = "Julio";
                ws.Cells["H3"].Value = "Agosto";
                ws.Cells["I3"].Value = "Septiembre";
                ws.Cells["J3"].Value = "Octubre";
                ws.Cells["K3"].Value = "Noviembre";
                ws.Cells["L3"].Value = "Diciembre";

                absentAvg = 0;
                globalAvg = 0;
                countAvgs = 0;
                for (int i = 1; i <= 12; i++)
                {
                    absentAvg = 0;
                    foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Daytrip.Id
                        == 1 && x.AttendanceDate.Date.Year == DateTime.Today.Year))
                    {
                        if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                        {
                            absentAvg++;
                        }
                    }

                    if (absentAvg != 0)
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 1 && x.Date.Year == DateTime.Today.Year).Count();
                        var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                        if (totalAttendanceDays != 0 && totalStudents != 0)
                        {
                            var totalOptions = (totalStudents) * totalAttendanceDays;
                            absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                            globalAvg = globalAvg + absentAvg;
                            countAvgs++;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = absentAvg;
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                    }
                    else
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 1 && x.Date.Year == DateTime.Today.Year).Count();
                        if (totalAttendanceDays == 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                        else
                        {
                            countAvgs++;
                            globalAvg = globalAvg + 100;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "100 %";
                        }
                    }
                }

                ws.Cells["A6"].Value = "Porcentaje de asistencia por alumno";
                ws.Cells["A6"].Style.Font.Bold = true;

                ws.Cells["A8"].Value = "N°";
                ws.Cells["B8"].Value = "Apellido P";
                ws.Cells["C8"].Value = "Apellido M";
                ws.Cells["D8"].Value = "Nombre";
                ws.Cells["E8"].Value = "Enero";
                ws.Cells["F8"].Value = "Febrero";
                ws.Cells["G8"].Value = "Marzo";
                ws.Cells["H8"].Value = "Abril";
                ws.Cells["I8"].Value = "Mayo";
                ws.Cells["J8"].Value = "Junio";
                ws.Cells["K8"].Value = "Julio";
                ws.Cells["L8"].Value = "Agosto";
                ws.Cells["M8"].Value = "Septiembre";
                ws.Cells["N8"].Value = "Octubre";
                ws.Cells["O8"].Value = "Noviembre";
                ws.Cells["P8"].Value = "Diciembre";
                ws.Cells["Q8"].Value = "Total";

                n = 9;
                count = 1;

                foreach (var student in students)
                {
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = student.FatherLastName;
                    ws.Cells[string.Concat("C", n)].Value = student.MotherLastName;
                    ws.Cells[string.Concat("D", n)].Value = student.Name;

                    for (int i = 5; i <= 16; i++)
                    {

                        if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i - 4 && x.Daytrip.Id == 1 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() > 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 1 && x.Date.Month == i - 4 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Daytrip.Id == 1 && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Date.Year == DateTime.Today.Year && x.AttendanceDate.Date.Month == i - 4).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 1 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year && x.Date.Month == i - 4).Count()), 1, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = "-";
                        }

                    }
                    if (attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 1 && x.Course.Id == courseId).Count() > 0)
                    {
                        ws.Cells[string.Concat("Q", n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 1 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Daytrip.Id == 1 && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Date.Year == DateTime.Today.Year).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 1 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count()), 1, MidpointRounding.AwayFromZero);

                    }
                    else
                    {
                        ws.Cells[string.Concat("Q", n)].Value = "-";
                    }
                    count += 1;
                    n += 1;
                }

                ws = pck.Workbook.Worksheets.Add("Asistencia Tarde");

                ws.Cells["A1"].Value = "Resumen de asistencia Anual - Jornada Tarde";
                ws.Cells["A1"].Style.Font.Bold = true;

                ws.Cells["A2"].Value = "";
                ws.Cells["A3"].Value = "Enero";
                ws.Cells["B3"].Value = "Febrero";
                ws.Cells["C3"].Value = "Marzo";
                ws.Cells["D3"].Value = "Abril";
                ws.Cells["E3"].Value = "Mayo";
                ws.Cells["F3"].Value = "Junio";
                ws.Cells["G3"].Value = "Julio";
                ws.Cells["H3"].Value = "Agosto";
                ws.Cells["I3"].Value = "Septiembre";
                ws.Cells["J3"].Value = "Octubre";
                ws.Cells["K3"].Value = "Noviembre";
                ws.Cells["L3"].Value = "Diciembre";

                absentAvg = 0;
                globalAvg = 0;
                countAvgs = 0;
                for (int i = 1; i <= 12; i++)
                {
                    absentAvg = 0;
                    foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Daytrip.Id
                        == 2 && x.AttendanceDate.Date.Year == DateTime.Today.Year))
                    {
                        if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                        {
                            absentAvg++;
                        }
                    }

                    if (absentAvg != 0)
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 2 && x.Date.Year == DateTime.Today.Year).Count();
                        var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                        if (totalAttendanceDays != 0 && totalStudents != 0)
                        {
                            var totalOptions = (totalStudents) * totalAttendanceDays;
                            absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                            globalAvg = globalAvg + absentAvg;
                            countAvgs++;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = absentAvg;
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                    }
                    else
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 2 && x.Date.Year == DateTime.Today.Year).Count();
                        if (totalAttendanceDays == 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                        else
                        {
                            countAvgs++;
                            globalAvg = globalAvg + 100;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "100 %";
                        }
                    }
                }

                ws.Cells["A6"].Value = "Porcentaje de asistencia por alumno";
                ws.Cells["A6"].Style.Font.Bold = true;

                ws.Cells["A8"].Value = "N°";
                ws.Cells["B8"].Value = "Apellido P";
                ws.Cells["C8"].Value = "Apellido M";
                ws.Cells["D8"].Value = "Nombre";
                ws.Cells["E8"].Value = "Enero";
                ws.Cells["F8"].Value = "Febrero";
                ws.Cells["G8"].Value = "Marzo";
                ws.Cells["H8"].Value = "Abril";
                ws.Cells["I8"].Value = "Mayo";
                ws.Cells["J8"].Value = "Junio";
                ws.Cells["K8"].Value = "Julio";
                ws.Cells["L8"].Value = "Agosto";
                ws.Cells["M8"].Value = "Septiembre";
                ws.Cells["N8"].Value = "Octubre";
                ws.Cells["O8"].Value = "Noviembre";
                ws.Cells["P8"].Value = "Diciembre";
                ws.Cells["Q8"].Value = "Total";

                n = 9;
                count = 1;

                foreach (var student in students)
                {
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = student.FatherLastName;
                    ws.Cells[string.Concat("C", n)].Value = student.MotherLastName;
                    ws.Cells[string.Concat("D", n)].Value = student.Name;

                    for (int i = 5; i <= 16; i++)
                    {

                        if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i - 4 && x.Daytrip.Id == 2 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() > 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 2 && x.Date.Month == i - 4 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Daytrip.Id == 2 && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Date.Year == DateTime.Today.Year && x.AttendanceDate.Date.Month == i - 4).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 2 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year && x.Date.Month == i - 4).Count()), 1, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = "-";
                        }

                    }
                    if (attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 2 && x.Course.Id == courseId).Count() > 0)
                    {
                        ws.Cells[string.Concat("Q", n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 2 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Daytrip.Id == 2 && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Date.Year == DateTime.Today.Year).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 2 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count()), 1, MidpointRounding.AwayFromZero);

                    }
                    else
                    {
                        ws.Cells[string.Concat("Q", n)].Value = "-";
                    }
                    count += 1;
                    n += 1;
                }

                ws = pck.Workbook.Worksheets.Add("Asistencia Noche");

                ws.Cells["A1"].Value = "Resumen de asistencia Anual - Jornada Noche";
                ws.Cells["A1"].Style.Font.Bold = true;
                ws.Cells["A2"].Value = "";
                ws.Cells["A3"].Value = "Enero";
                ws.Cells["B3"].Value = "Febrero";
                ws.Cells["C3"].Value = "Marzo";
                ws.Cells["D3"].Value = "Abril";
                ws.Cells["E3"].Value = "Mayo";
                ws.Cells["F3"].Value = "Junio";
                ws.Cells["G3"].Value = "Julio";
                ws.Cells["H3"].Value = "Agosto";
                ws.Cells["I3"].Value = "Septiembre";
                ws.Cells["J3"].Value = "Octubre";
                ws.Cells["K3"].Value = "Noviembre";
                ws.Cells["L3"].Value = "Diciembre";

                absentAvg = 0;
                globalAvg = 0;
                countAvgs = 0;
                for (int i = 1; i <= 12; i++)
                {
                    absentAvg = 0;
                    foreach (var absent in absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Daytrip.Id
                        == 3 && x.AttendanceDate.Date.Year == DateTime.Today.Year))
                    {
                        if (absent.AttendanceDate.Date != null && absent.AttendanceDate.Date.Month == i)
                        {
                            absentAvg++;
                        }
                    }

                    if (absentAvg != 0)
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 3 && x.Date.Year == DateTime.Today.Year).Count();
                        var totalStudents = course.StudentCourses.Select(x => x.User).Distinct().Count();

                        if (totalAttendanceDays != 0 && totalStudents != 0)
                        {
                            var totalOptions = (totalStudents) * totalAttendanceDays;
                            absentAvg = ((totalOptions - absentAvg) * 100) / totalOptions;
                            globalAvg = globalAvg + absentAvg;
                            countAvgs++;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = absentAvg;
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                    }
                    else
                    {
                        var totalAttendanceDays = attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i && x.Course.Id == courseId && x.Daytrip.Id == 3 && x.Date.Year == DateTime.Today.Year).Count();

                        if (totalAttendanceDays == 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "-";
                        }
                        else
                        {
                            countAvgs++;
                            globalAvg = globalAvg + 100;
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), "4")].Value = "100 %";
                        }
                    }
                }

                ws.Cells["A6"].Value = "Porcentaje de asistencia por alumno";
                ws.Cells["A6"].Style.Font.Bold = true;
                ws.Cells["A8"].Value = "N°";
                ws.Cells["B8"].Value = "Apellido P";
                ws.Cells["C8"].Value = "Apellido M";
                ws.Cells["D8"].Value = "Nombre";
                ws.Cells["E8"].Value = "Enero";
                ws.Cells["F8"].Value = "Febrero";
                ws.Cells["G8"].Value = "Marzo";
                ws.Cells["H8"].Value = "Abril";
                ws.Cells["I8"].Value = "Mayo";
                ws.Cells["J8"].Value = "Junio";
                ws.Cells["K8"].Value = "Julio";
                ws.Cells["L8"].Value = "Agosto";
                ws.Cells["M8"].Value = "Septiembre";
                ws.Cells["N8"].Value = "Octubre";
                ws.Cells["O8"].Value = "Noviembre";
                ws.Cells["P8"].Value = "Diciembre";
                ws.Cells["Q8"].Value = "Total";

                n = 9;
                count = 1;

                foreach (var student in students)
                {
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = student.FatherLastName;
                    ws.Cells[string.Concat("C", n)].Value = student.MotherLastName;
                    ws.Cells[string.Concat("D", n)].Value = student.Name;

                    for (int i = 5; i <= 16; i++)
                    {

                        if (attendanceDates.Where(x => x.Status == true &&  x.Date.Month == i - 4 && x.Daytrip.Id == 1 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() > 0)
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 3 && x.Date.Month == i - 4 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Daytrip.Id == 3 && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Date.Year == DateTime.Today.Year && x.AttendanceDate.Date.Month == i - 4).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 3 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year && x.Date.Month == i - 4).Count()), 1, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = "-";
                        }

                    }
                    if (attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 3 && x.Course.Id == courseId).Count() > 0)
                    {
                        ws.Cells[string.Concat("Q", n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 3 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count() - absents.Where(x => x.Status == true &&  x.User_Id == student.Id && x.AttendanceDate.Daytrip.Id == 3 && x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Date.Year == DateTime.Today.Year).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Daytrip.Id == 3 && x.Course.Id == courseId && x.Date.Year == DateTime.Today.Year).Count()), 1, MidpointRounding.AwayFromZero)
    ;
                    }
                    else
                    {
                        ws.Cells[string.Concat("Q", n)].Value = "-";
                    }
                    count += 1;
                    n += 1;
                }

                ws = pck.Workbook.Worksheets.Add("Por asignatura");

                ws.Cells["A1"].Value = "Resumen de asistencia Anual - Por Asignatura";
                ws.Cells["A1"].Style.Font.Bold = true;
                ws.Cells["A2"].Value = "";
                ws.Cells["A3"].Value = "Código";
                ws.Cells["B3"].Value = "Asignatura";
                ws.Cells["C3"].Value = "N° Clases";

                var asignaturas = attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId).Select(x => x.BaseSubject).Distinct().ToList();
                count = 1;
                n = 4;
                foreach (var asignatura in asignaturas)
                {
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = asignatura.Name;
                    ws.Cells[string.Concat("C", n)].Value = attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.BaseSubject.Id == asignatura.Id && x.Date.Year == DateTime.Today.Year).Count();
                    count += 1;
                    n += 1;
                }
                n += 1;
                ws.Cells[string.Concat("A", n)].Value = "N°";
                ws.Cells[string.Concat("A", n)].Style.Font.Bold = true;
                ws.Cells[string.Concat("B", n)].Value = "Apellido P";
                ws.Cells[string.Concat("B", n)].Style.Font.Bold = true;
                ws.Cells[string.Concat("C", n)].Value = "Apellido M";
                ws.Cells[string.Concat("C", n)].Style.Font.Bold = true;
                ws.Cells[string.Concat("D", n)].Value = "Nombre";
                ws.Cells[string.Concat("D", n)].Style.Font.Bold = true;
                for (int i = 5; i < asignaturas.Count() + 5; i++)
                {
                    ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = i - 4;
                    ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Style.Font.Bold = true;
                }
                n += 1;
                count = 1;
                foreach (var student in students)
                {
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = student.FatherLastName;
                    ws.Cells[string.Concat("C", n)].Value = student.MotherLastName;
                    ws.Cells[string.Concat("D", n)].Value = student.Name;
                    int i = 5;
                    foreach (var asignatura in asignaturas)
                    {
                        if (attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.BaseSubject.Id == asignatura.Id && x.Date.Year == DateTime.Today.Year).Count() > 0)
                        {
                            var days = attendanceDates.Where(x => x.Status == true &&  x.Status == true && x.Course.Id == courseId && x.BaseSubject.Id == asignatura.Id && x.Date.Year == DateTime.Today.Year).Count();
                            var absentsdays = absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.Date.Year == DateTime.Today.Year && x.User.Id == student.Id).Count();
                            var total = ((days - absentsdays) * 100) / days;

                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = Math.Round((((double)attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.BaseSubject.Id == asignatura.Id && x.Date.Year == DateTime.Today.Year).Count() - absents.Where(x => x.Status == true &&  x.AttendanceDate.Course.Id == courseId && x.AttendanceDate.BaseSubject.Id == asignatura.Id && x.AttendanceDate.Date.Year == DateTime.Today.Year && x.User.Id == student.Id).Count()) * 100 / attendanceDates.Where(x => x.Status == true &&  x.Course.Id == courseId && x.BaseSubject.Id == asignatura.Id && x.Date.Year == DateTime.Today.Year).Count()), 1, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            ws.Cells[string.Concat(((char)(i + 64)).ToString(), n)].Value = "-";

                        }
                        i += 1;
                    }
                    n += 1;
                    count += 1;
                }
            }
            else {
                ws.Cells["A1"].Value = "No se han registrado días de asistencia";
            }
            var memorystream = new MemoryStream();
            pck.SaveAs(memorystream);
            return new System.Web.Mvc.FileStreamResult(memorystream, "application/vnd.ms-excel") { FileDownloadName = "PERS936AB.xls" };
        }

        public class Performance
        {
            public string StudentID { get; set; }
            public int PerformanceValue { get; set; }
            public string StudentFatherLastName { get; set; }
            public string StudentMotherLastName { get; set; }
            public string StudentName { get; set; }
            public double calificaction { get; set; }
            public string created { get; set; }
            public Boolean isAnswered { get; set; }
        }

        [AcceptVerbs("GET", "POST", "OPTIONS")]
        [System.Web.Http.HttpGet]
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("api/AttendanceDate/TestExcel")]
        public System.Web.Mvc.FileResult TestExcel(string _test_id, string _subunity, string _unity, string _subject, string _course, string _teacher = null)
        {
            ExcelPackage pck = new ExcelPackage();
            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);
            string teacherId = ""; ;
            if (_teacher == null)
                teacherId = GlobalParameters.UserId;
            else
                teacherId = Encryptor.Decrypt(_teacher);
            var courseId = Int32.Parse(Encryptor.Decrypt(_course));
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subject));
            var unityId = Int32.Parse(Encryptor.Decrypt(_unity));
            var subUnityId = Int32.Parse(Encryptor.Decrypt(_subunity));
            var testId = Int32.Parse(Encryptor.Decrypt(_test_id));

            var courseSubject = db.TeacherCourseSubject
                                      .Where(x => x.Status == true &&  x.User.Id == teacherId &&
                                                  x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                  x.CourseSubject.Course_Id == courseId &&
                                                  x.CourseSubject.Subject_Id == subjectId
                                              )
                                      .Select(x => x.CourseSubject)
                                      .OrderBy(x => x.Course_Id)
                                      .SingleOrDefault();
            if (courseSubject == null) return null; // Content("No se pudo encontrar el profesor indicado.");  // Exception if couldn't find a teacher

            var subUnit = db.SubUnity.Where(x => x.Status == true && 
                                        x.Id == subUnityId &&
                                        x.Unity.Id == unityId &&
                                        x.Unity.CourseSubject.Course_Id == courseSubject.Course_Id &&
                                        x.Unity.CourseSubject.Subject_Id == courseSubject.Subject_Id
                                    ).SingleOrDefault();

            var test = db.Test.Where(x =>  
                                       x.Id == testId 
                                   ).SingleOrDefault();

            if (test.StudentTests.Count == 0) return null; //Content("No existen alumnos asociados a este test.");

            test.StudentTests.OrderBy(x => x.User.StudentCourses.Where(p => p.User_Id == x.User.Id).Select(p => p.ListNumber));

            var answers = db.Answer.Where(x => x.Status == true && 
                                        x.Question.Test.Id == testId &&
                                        x.Question.Test.SubUnity.Id == subUnit.Id &&
                                        x.Question.Test.SubUnity.Unity.Id == unityId
                                    );
            var teacher = db.Users.Where(x => x.Status == true &&  x.Id == teacherId).SingleOrDefault();
            var contents = db.AchievementIndicator.Where(x => x.Status == true &&  x.Test.Id == test.Id && x.Status);
            //);
            var ids = db.Question.Where(x => x.Status == true &&  x.Test.Id == testId).Select(x => x.Skill.Id).Distinct();
            var skills = db.Skill.Where(x => x.Status == true &&  ids.Contains(x.Id));
            var questions = db.Question.Where(x => x.Status == true &&  x.Test.Id == testId);

            var performanceList = new List<Performance>();
            var performanceListA = new List<Performance>();
            var performanceListE = new List<Performance>();
            var performanceListI = new List<Performance>();
            var performanceListH = new List<double>();

            var ws = pck.Workbook.Worksheets.Add("Inicio");

            ws.Cells["A1"].Value = "Rendimiento por Test";
            ws.Cells["A1"].Style.Font.Bold = true;
            ws.Cells["A2"].Value = "";
            ws.Cells["A3"].Value = "Establecimiento";
            ws.Cells["A3"].Style.Font.Bold = true;
            ws.Cells["B3"].Value = Establishment.Name;
            ws.Cells["A4"].Value = "Profesor";
            ws.Cells["B4"].Value = string.Concat(teacher.Name, " ", teacher.FatherLastName , " ", teacher.MotherLastName);
            ws.Cells["A5"].Value = "Curso";
            ws.Cells["A5"].Style.Font.Bold = true;
            ws.Cells["B5"].Value = courseSubject.Course.Name;
            ws.Cells["A6"].Value = "Asignatura";
            ws.Cells["A6"].Style.Font.Bold = true;
            ws.Cells["B6"].Value = courseSubject.Subject.Name;

            ws.Cells["A8"].Value = "Eje Temático";
            ws.Cells["A8"].Style.Font.Bold = true;
            ws.Cells["B8"].Value = subUnit.Unity.Name;
            ws.Cells["A9"].Value = "Tema";
            ws.Cells["A9"].Style.Font.Bold = true;
            ws.Cells["B9"].Value = subUnit.Name;
            ws.Cells["A10"].Value = "Objetivo";
            ws.Cells["A10"].Style.Font.Bold = true;
            ws.Cells["B10"].Value = test.Title;


            ws = pck.Workbook.Worksheets.Add("Test");

            

            var totalStudent = test.StudentTests.Count;
            var totalQuestions = test.Questions.Count;
            var totalDone = test.StudentTests.Where(x => x.Status == true &&  x.Done == true).Count();
            var rightAnswers = 0;
            foreach(var question in test.Questions)
            { 
                foreach (var answer in question.Answers)
                {
                    if (question.Choice == answer.Choice)
                    {
                        rightAnswers++;
                    }
                }
            }
            var totalPosibleAnswer = totalDone* totalQuestions;
            var performance = (totalDone == 0 || totalQuestions == 0) ? 0 : (rightAnswers * 100) / totalPosibleAnswer;

            var insuficiente = 0;
            var suficiente = 0;
            var adecuado = 0;
            var count = 0;
            var sumaDE = 0.0d;
            var desviacionEstandar = 0.0d;

            foreach(var studentTest in test.StudentTests)
            {
                rightAnswers = 0;
                bool isAnswered = true;
                foreach (var question in test.Questions)
                {
                    var answersStudent = question.Answers.Where(x => x.Status == true &&  x.User_Id == studentTest.User.Id).ToList();
                    if (answersStudent.Count() > 0)
                    {
                        foreach (var answer in answersStudent)
                        {
                            if (question.Choice == answer.Choice)
                            {
                                rightAnswers++;
                            }
                        }
                    }
                    else
                    {
                        isAnswered = false;
                    }

                }

                var studentPerformance = (rightAnswers * 100) / totalQuestions;
                double calification = 0.0;
                if (studentPerformance < 60)
                {
                    var f = (((double)studentPerformance / 60) * 3) + 1;
                    calification = Math.Round(f, 1, MidpointRounding.AwayFromZero);
                }
                else if (studentPerformance >= 60)
                {
                    calification = Math.Round((((double)(studentPerformance - 60) / 40) * 3) + 4, 1, MidpointRounding.AwayFromZero);
                }

                var perf = new Performance()
                {
                    StudentID = studentTest.User.Id,
                    PerformanceValue = studentPerformance,
                    StudentFatherLastName = studentTest.User.FatherLastName,
                    StudentMotherLastName = studentTest.User.MotherLastName,
                    StudentName = studentTest.User.Name,
                    calificaction = calification,
                    created = studentTest.Test.CreatedAt.ToString("dd/MM/yyyy"),
                    isAnswered = isAnswered
                };
                performanceList.Add(perf);
                if (studentPerformance < 60 && studentTest.Done == true) { performanceListI.Add(perf); }
                else if (studentPerformance >= 60 && studentPerformance < 80) { performanceListE.Add(perf); }
                else if (studentPerformance >= 80) { performanceListA.Add(perf); }

            }
            foreach(var perf in performanceList)
            {
                if (perf.PerformanceValue < 60 && perf.isAnswered == true)
                {
                    insuficiente++;
                }
                else if (perf.PerformanceValue < 80 && perf.PerformanceValue >= 60)
                {
                    suficiente++;
                }
                else if (perf.PerformanceValue >= 80)
                {
                    adecuado++;
                }
                if (perf.isAnswered)
                {
                    sumaDE += Math.Pow(Math.Abs((double)perf.PerformanceValue - (double)performanceList.Average(a => a.PerformanceValue)), 2);
                }
            }
            var insuficienteP = totalDone != 0 ? Math.Round((Double)(insuficiente * 100) / totalDone, 1) : 0;
            var suficienteP = totalDone != 0 ? Math.Round((Double)(suficiente * 100) / totalDone, 1) : 0;
            var adecuadoP = totalDone != 0 ? Math.Round((Double)(adecuado * 100) / totalDone, 1) : 0;
            var insuficientePGlobal = totalDone != 0 ? Math.Round((Double)(insuficiente * 100) / totalDone, 1) : 0;
            var suficientePGlobal = totalDone != 0 ? Math.Round((Double)(suficiente * 100) / totalDone, 1) : 0;
            var adecuadoPGlobal = totalDone != 0 ? Math.Round((Double)(adecuado * 100) / totalDone, 1) : 0;

            if (performanceList.Where(x =>  x.isAnswered == true).Count() > 0)
            {

                ws.Cells["A1"].Value = "Resumen";
                ws.Cells["A3"].Value = "Total Rendidos";
                ws.Cells["A3"].Style.Font.Bold = true;
                ws.Cells["A4"].Value = string.Concat(totalDone, " de ", totalStudent);

                ws.Cells["B3"].Value = "x̅";
                ws.Cells["B3"].Style.Font.Bold = true;
                ws.Cells["B4"].Value = string.Concat(performance, " %");

                ws.Cells["C3"].Value = "Mín";
                ws.Cells["C3"].Style.Font.Bold = true;
                if (performanceList.Where(x => x.isAnswered == true).Count() > 0)
                {
                    ws.Cells["C4"].Value = String.Concat(performanceList.Where(x => x.isAnswered == true).Min(item => item.PerformanceValue), " %");
                }

                ws.Cells["D3"].Value = "Máx";
                ws.Cells["D3"].Style.Font.Bold = true;
                if (performanceList.Where(x => x.isAnswered == true).Count() > 0)
                {
                    ws.Cells["D4"].Value = String.Concat(performanceList.Where(x => x.isAnswered == true).Max(item => item.PerformanceValue), " %");
                }

                ws.Cells["E3"].Value = "D. E.";
                ws.Cells["E3"].Style.Font.Bold = true;
                if (performanceList.Where(x => x.isAnswered == true).Count() > 0)
                {
                    ws.Cells["E4"].Value = String.Concat(Math.Round((Math.Sqrt(sumaDE / (performanceList.Where(p => p.isAnswered).Count())) * 100) / 100), " %");
                }

                ws.Cells["F3"].Value = "A";
                ws.Cells["F3"].Style.Font.Bold = true;
                ws.Cells["F4"].Value = String.Concat(adecuadoP, " %");

                ws.Cells["G3"].Value = "E";
                ws.Cells["G3"].Style.Font.Bold = true;
                ws.Cells["G4"].Value = String.Concat(suficienteP, " %");

                ws.Cells["H3"].Value = "I";
                ws.Cells["H3"].Style.Font.Bold = true;
                ws.Cells["H4"].Value = String.Concat(insuficienteP, " %");

                count = 0;

                ws.Cells["A6"].Value = "Rendimiento General";

                ws.Cells["A8"].Value = "N°";
                ws.Cells["A8"].Style.Font.Bold = true;
                ws.Cells["B8"].Value = "Apellido P";
                ws.Cells["B8"].Style.Font.Bold = true;
                ws.Cells["C8"].Value = "Apellido M";
                ws.Cells["C8"].Style.Font.Bold = true;
                ws.Cells["D8"].Value = "Nombre";
                ws.Cells["D8"].Style.Font.Bold = true;
                ws.Cells["E8"].Value = "Rendimiento";
                ws.Cells["E8"].Style.Font.Bold = true;
                ws.Cells["F8"].Value = "Calificación";
                ws.Cells["F8"].Style.Font.Bold = true;
                ws.Cells["G8"].Value = "Fecha";
                ws.Cells["G8"].Style.Font.Bold = true;

                var n = 9;
                foreach (var student in performanceList)
                {
                    count += 1;
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = student.StudentFatherLastName;
                    ws.Cells[string.Concat("C", n)].Value = student.StudentMotherLastName;
                    ws.Cells[string.Concat("D", n)].Value = student.StudentName;
                    if (student.isAnswered)
                    {
                        ws.Cells[string.Concat("E", n)].Value = student.PerformanceValue;
                        ws.Cells[string.Concat("F", n)].Value = student.calificaction;
                    }
                    else
                    {
                        ws.Cells[string.Concat("E", n)].Value = "NR";
                        ws.Cells[string.Concat("F", n)].Value = "NR";
                    }
                    ws.Cells[string.Concat("G", n)].Value = student.created;
                    n += 1;
                }
                int[] distribution = new int[11];
                foreach (var perf in performanceList)
                {
                    if (perf.isAnswered == true)
                    {
                        var range = 10;
                        var aux = 0;
                        if (perf.PerformanceValue > 0)
                        {
                            aux = (perf.PerformanceValue) / range;
                        }
                        switch (aux)
                        {
                            case (0):
                                distribution[0]++;
                                break;
                            case (1):
                                distribution[1]++;
                                break;
                            case (2):
                                distribution[2]++;
                                break;
                            case (3):
                                distribution[3]++;
                                break;
                            case (4):
                                distribution[4]++;
                                break;
                            case (5):
                                distribution[5]++;
                                break;
                            case (6):
                                distribution[6]++;
                                break;
                            case (7):
                                distribution[7]++;
                                break;
                            case (8):
                                distribution[8]++;
                                break;
                            case (9):
                                distribution[9]++;
                                break;
                            case (10):
                                distribution[9]++;
                                break;

                            default:
                                break;
                        }

                    }
                }

                ws = pck.Workbook.Worksheets.Add("Resumen Nivel Logro");

                ws.Cells["A1"].Value = "Distribución del curso por nivel de logro";

                ws.Cells["A3"].Value = "Adecuado";
                ws.Cells["A3"].Style.Font.Bold = true;
                ws.Cells["B3"].Value = adecuado;
                ws.Cells["C3"].Value = string.Concat(adecuadoPGlobal, " %");

                ws.Cells["A4"].Value = "Elemental";
                ws.Cells["A4"].Style.Font.Bold = true;
                ws.Cells["B4"].Value = suficiente;
                ws.Cells["C4"].Value = string.Concat(suficientePGlobal, " %");

                ws.Cells["A5"].Value = "Insuficiente";
                ws.Cells["A5"].Style.Font.Bold = true;
                ws.Cells["B5"].Value = insuficiente;
                ws.Cells["C5"].Value = string.Concat(insuficientePGlobal, " %");

                //ws = pck.Workbook.Worksheets.Add("Nivel Adecuado");
                //count = 0; sumaDE = 0;

                //ws.Cells["A1"].Value = "Alumnos en nivel Adecuado";

                //count = 0;

                //ws.Cells["A3"].Value = "Total Rendidos";
                //ws.Cells["A3"].Style.Font.Bold = true;
                //ws.Cells["A4"].Value = string.Concat(performanceListA.Count, " de ", totalStudent);

                //ws.Cells["B3"].Value = "x̅";
                //ws.Cells["B3"].Style.Font.Bold = true;
                //if (performanceListA.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["B4"].Value = String.Concat(Math.Round(performanceListA.Average(item => item.PerformanceValue), 1, MidpointRounding.AwayFromZero), " %");
                //}

                //ws.Cells["C3"].Value = "Mín";
                //ws.Cells["C3"].Style.Font.Bold = true;
                //if (performanceListA.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["C4"].Value = performanceListA.Min(item => item.PerformanceValue);
                //}

                //ws.Cells["D3"].Value = "Máx";
                //ws.Cells["D3"].Style.Font.Bold = true;
                //if (performanceListA.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["D4"].Value = performanceListA.Max(item => item.PerformanceValue);
                //}

                //ws.Cells["E3"].Value = "D. E.";
                //ws.Cells["E3"].Style.Font.Bold = true;
                //if (performanceListA.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    foreach (var perf in performanceListA)
                //    {
                //        if (perf.isAnswered)
                //        {
                //            sumaDE += Math.Pow(Math.Abs((double)perf.PerformanceValue - (double)performanceListA.Average(a => a.PerformanceValue)), 2);
                //        }
                //    }
                //    ws.Cells["E4"].Value = String.Concat(Math.Round((Math.Sqrt(sumaDE / (performanceListA.Where(p => p.isAnswered).Count())) * 100) / 100), " %");
                //}

                //count = 0;

                //ws.Cells["A6"].Value = "N°";
                //ws.Cells["A6"].Style.Font.Bold = true;
                //ws.Cells["B6"].Value = "Apellido P";
                //ws.Cells["B6"].Style.Font.Bold = true;
                //ws.Cells["C6"].Value = "Apellido M";
                //ws.Cells["C6"].Style.Font.Bold = true;
                //ws.Cells["D6"].Value = "Nombre";
                //ws.Cells["D6"].Style.Font.Bold = true;
                //ws.Cells["E6"].Value = "Rendimiento";
                //ws.Cells["E6"].Style.Font.Bold = true;
                //ws.Cells["F6"].Value = "Calificación";
                //ws.Cells["F6"].Style.Font.Bold = true;
                //ws.Cells["G6"].Value = "Fecha";
                //ws.Cells["G6"].Style.Font.Bold = true;

                //n = 7;
                //foreach (var student in performanceListA.OrderByDescending(o => o.PerformanceValue).ToList())
                //{
                //    count += 1;
                //    ws.Cells[string.Concat("A", n)].Value = count;
                //    ws.Cells[string.Concat("B", n)].Value = student.StudentFatherLastName;
                //    ws.Cells[string.Concat("C", n)].Value = student.StudentMotherLastName;
                //    ws.Cells[string.Concat("D", n)].Value = student.StudentName;
                //    if (student.isAnswered)
                //    {
                //        ws.Cells[string.Concat("E", n)].Value = student.PerformanceValue;
                //        ws.Cells[string.Concat("F", n)].Value = student.calificaction;
                //    }
                //    else
                //    {
                //        ws.Cells[string.Concat("E", n)].Value = "NR";
                //        ws.Cells[string.Concat("F", n)].Value = "NR";
                //    }
                //    ws.Cells[string.Concat("G", n)].Value = student.created;
                //    n += 1;
                //}

                //ws = pck.Workbook.Worksheets.Add("Nivel Elemental");
                //count = 0; sumaDE = 0;

                //ws.Cells["A1"].Value = "Alumnos en nivel Elemental";

                //count = 0;

                //ws.Cells["A3"].Value = "Total Rendidos";
                //ws.Cells["A3"].Style.Font.Bold = true;
                //ws.Cells["A4"].Value = string.Concat(performanceListE.Count, " de ", totalStudent);

                //ws.Cells["B3"].Value = "x̅";
                //ws.Cells["B3"].Style.Font.Bold = true;
                //if (performanceListE.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["B4"].Value = String.Concat(Math.Round(performanceListE.Average(item => item.PerformanceValue), 1, MidpointRounding.AwayFromZero), " %");
                //}

                //ws.Cells["C3"].Value = "Mín";
                //ws.Cells["C3"].Style.Font.Bold = true;
                //if (performanceListE.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["C4"].Value = performanceListE.Min(item => item.PerformanceValue);
                //}

                //ws.Cells["D3"].Value = "Máx";
                //ws.Cells["D3"].Style.Font.Bold = true;
                //if (performanceListE.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["D4"].Value = performanceListE.Max(item => item.PerformanceValue);
                //}

                //ws.Cells["E3"].Value = "D. E.";
                //ws.Cells["E3"].Style.Font.Bold = true;
                //if (performanceListE.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    foreach (var perf in performanceListE)
                //    {
                //        if (perf.isAnswered)
                //        {
                //            sumaDE += Math.Pow(Math.Abs((double)perf.PerformanceValue - (double)performanceListE.Average(a => a.PerformanceValue)), 2);
                //        }
                //    }
                //    ws.Cells["E4"].Value = String.Concat(Math.Round((Math.Sqrt(sumaDE / (performanceListE.Where(p => p.isAnswered).Count())) * 100) / 100), " %");
                //}

                //count = 0;

                //ws.Cells["A6"].Value = "N°";
                //ws.Cells["A6"].Style.Font.Bold = true;
                //ws.Cells["B6"].Value = "Apellido P";
                //ws.Cells["B6"].Style.Font.Bold = true;
                //ws.Cells["C6"].Value = "Apellido M";
                //ws.Cells["C6"].Style.Font.Bold = true;
                //ws.Cells["D6"].Value = "Nombre";
                //ws.Cells["D6"].Style.Font.Bold = true;
                //ws.Cells["E6"].Value = "Rendimiento";
                //ws.Cells["E6"].Style.Font.Bold = true;
                //ws.Cells["F6"].Value = "Calificación";
                //ws.Cells["F6"].Style.Font.Bold = true;
                //ws.Cells["G6"].Value = "Fecha";
                //ws.Cells["G6"].Style.Font.Bold = true;

                //n = 7;
                //foreach (var student in performanceListE.OrderByDescending(o => o.PerformanceValue).ToList())
                //{
                //    count += 1;
                //    ws.Cells[string.Concat("A", n)].Value = count;
                //    ws.Cells[string.Concat("B", n)].Value = student.StudentFatherLastName;
                //    ws.Cells[string.Concat("C", n)].Value = student.StudentMotherLastName;
                //    ws.Cells[string.Concat("D", n)].Value = student.StudentName;
                //    if (student.isAnswered)
                //    {
                //        ws.Cells[string.Concat("E", n)].Value = student.PerformanceValue;
                //        ws.Cells[string.Concat("F", n)].Value = student.calificaction;
                //    }
                //    else
                //    {
                //        ws.Cells[string.Concat("E", n)].Value = "NR";
                //        ws.Cells[string.Concat("F", n)].Value = "NR";
                //    }
                //    ws.Cells[string.Concat("G", n)].Value = student.created;
                //    n += 1;
                //}

                //ws = pck.Workbook.Worksheets.Add("Nivel Insuficiente");
                //count = 0; sumaDE = 0;

                //ws.Cells["A1"].Value = "Alumnos en nivel Insuficiente";

                //count = 0;

                //ws.Cells["A3"].Value = "Total Rendidos";
                //ws.Cells["A3"].Style.Font.Bold = true;
                //ws.Cells["A4"].Value = string.Concat(performanceListI.Count, " de ", totalStudent);

                //ws.Cells["B3"].Value = "x̅";
                //ws.Cells["B3"].Style.Font.Bold = true;
                //if (performanceListI.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["B4"].Value = String.Concat(Math.Round(performanceListI.Average(item => item.PerformanceValue), 1, MidpointRounding.AwayFromZero), " %");
                //}

                //ws.Cells["C3"].Value = "Mín";
                //ws.Cells["C3"].Style.Font.Bold = true;
                //if (performanceListI.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["C4"].Value = performanceListI.Min(item => item.PerformanceValue);
                //}

                //ws.Cells["D3"].Value = "Máx";
                //ws.Cells["D3"].Style.Font.Bold = true;
                //if (performanceListI.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    ws.Cells["D4"].Value = performanceListI.Max(item => item.PerformanceValue);
                //}

                //ws.Cells["E3"].Value = "D. E.";
                //ws.Cells["E3"].Style.Font.Bold = true;
                //if (performanceListI.Where(x => x.isAnswered == true).Count() > 0)
                //{
                //    foreach (var perf in performanceListI)
                //    {
                //        if (perf.isAnswered)
                //        {
                //            sumaDE += Math.Pow(Math.Abs((double)perf.PerformanceValue - (double)performanceListI.Average(a => a.PerformanceValue)), 2);
                //        }
                //    }
                //    ws.Cells["E4"].Value = String.Concat(Math.Round((Math.Sqrt(sumaDE / (performanceListI.Where(p => p.isAnswered).Count())) * 100) / 100), " %");
                //}

                //count = 0;

                //ws.Cells["A6"].Value = "N°";
                //ws.Cells["A6"].Style.Font.Bold = true;
                //ws.Cells["B6"].Value = "Apellido P";
                //ws.Cells["B6"].Style.Font.Bold = true;
                //ws.Cells["C6"].Value = "Apellido M";
                //ws.Cells["C6"].Style.Font.Bold = true;
                //ws.Cells["D6"].Value = "Nombre";
                //ws.Cells["D6"].Style.Font.Bold = true;
                //ws.Cells["E6"].Value = "Rendimiento";
                //ws.Cells["E6"].Style.Font.Bold = true;
                //ws.Cells["F6"].Value = "Calificación";
                //ws.Cells["F6"].Style.Font.Bold = true;
                //ws.Cells["G6"].Value = "Fecha";
                //ws.Cells["G6"].Style.Font.Bold = true;

                //n = 7;
                //foreach (var student in performanceListI.OrderByDescending(o => o.PerformanceValue).ToList())
                //{
                //    count += 1;
                //    ws.Cells[string.Concat("A", n)].Value = count;
                //    ws.Cells[string.Concat("B", n)].Value = student.StudentFatherLastName;
                //    ws.Cells[string.Concat("C", n)].Value = student.StudentMotherLastName;
                //    ws.Cells[string.Concat("D", n)].Value = student.StudentName;
                //    if (student.isAnswered)
                //    {
                //        ws.Cells[string.Concat("E", n)].Value = student.PerformanceValue;
                //        ws.Cells[string.Concat("F", n)].Value = student.calificaction;
                //    }
                //    else
                //    {
                //        ws.Cells[string.Concat("E", n)].Value = "NR";
                //        ws.Cells[string.Concat("F", n)].Value = "NR";
                //    }
                //    ws.Cells[string.Concat("G", n)].Value = student.created;
                //    n += 1;
                //}

                count = 0; Double rendimiento = 0.0f;
                float correctas;
                float todasLasRespuestas;

                ws = pck.Workbook.Worksheets.Add("Por Contenido");

                ws.Cells["A1"].Value = "Rendimiento por Contenido";

                count = 0;

                ws.Cells["A3"].Value = "N°";
                ws.Cells["A3"].Style.Font.Bold = true;
                ws.Cells["B3"].Value = "Tema";
                ws.Cells["B3"].Style.Font.Bold = true; ws.Cells["A3"].Value = "N°";
                ws.Cells["C3"].Value = "Preguntas Asociadas";
                ws.Cells["C3"].Style.Font.Bold = true;
                ws.Cells["D3"].Style.Font.Bold = true;
                ws.Cells["D3"].Value = "Rendimiento";

                n = 4;
                foreach (var achievementIndicator in contents)
                {
                    count = count + 1;
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = achievementIndicator.Name;
                    ws.Cells[string.Concat("C", n)].Value = "";
                    foreach (var achievementAlternatives in achievementIndicator.AchievementAlternatives)
                    {
                        ws.Cells[string.Concat("C", n)].Value = string.Concat(ws.Cells[string.Concat("C", n)].Value, " ", achievementAlternatives.Question.Order);
                        //comparar choice de question por la
                        correctas = answers.Where(x => x.Status == true && x.Choice == achievementAlternatives.Question.Choice && x.Question_Id == achievementAlternatives.Question.Id).ToList().Count();
                        todasLasRespuestas = answers.Where(x => x.Status == true && x.Question_Id == achievementAlternatives.Question.Id && x.Question_Id == achievementAlternatives.Question.Id).ToList().Count();
                        rendimiento = totalDone != 0 ? Math.Round((float)correctas / (float)todasLasRespuestas * 100) : 0;
                    }
                    ws.Cells[string.Concat("D", n)].Value = string.Concat(rendimiento, " %");
                    n += 1;
                }

                count = 1;
                ws = pck.Workbook.Worksheets.Add("Por Habilidad Cognitiva");

                ws.Cells["A1"].Value = "Rendimiento por Habilidad Cognitiva";

                ws.Cells["A3"].Value = "N°";
                ws.Cells["A3"].Style.Font.Bold = true;
                ws.Cells["B3"].Value = "Habilidad Cognitiva";
                ws.Cells["B3"].Style.Font.Bold = true;
                ws.Cells["C3"].Value = "Preguntas Asociadas";
                ws.Cells["C3"].Style.Font.Bold = true;
                ws.Cells["D3"].Style.Font.Bold = true;
                ws.Cells["D3"].Value = "Rendimiento";

                n = 4;
                foreach (var skill in skills)
                {
                    ws.Cells[string.Concat("A", n)].Value = count;
                    ws.Cells[string.Concat("B", n)].Value = skill.Name;
                    ws.Cells[string.Concat("C", n)].Value = "";
                    foreach (var question in questions.Where(x => x.Status == true && x.Skill.Id == skill.Id))
                    {
                        ws.Cells[string.Concat("C", n)].Value = string.Concat(ws.Cells[string.Concat("C", n)].Value, " ", question.Order);
                    }
                    var respuestas = answers.Where(x => x.Status == true && x.Choice == x.Question.Choice && x.Question.Test.Id == test.Id && x.Question.Skill.Id == skill.Id).Count();
                    todasLasRespuestas = test.Questions.Where(x => x.Status == true && x.Skill.Id == skill.Id && x.Test.Id == test.Id).Count();
                    rendimiento = totalDone != 0 ? Math.Round(respuestas / (todasLasRespuestas * totalDone) * 100) : 0;
                    performanceListH.Add(rendimiento);
                    ws.Cells[string.Concat("D", n)].Value = string.Concat(rendimiento, " %");
                    count += 1;
                    n += 1;
                }

                count = 1;
                //ws = pck.Workbook.Worksheets.Add("Respuestas por alumno");

                //ws.Cells["A1"].Value = "Detalle de respuestas por alumno";

                //ws.Cells["A3"].Value = "N°";
                //ws.Cells["A3"].Style.Font.Bold = true;
                //ws.Cells["B3"].Value = "Apellido P";
                //ws.Cells["B3"].Style.Font.Bold = true;
                //ws.Cells["C3"].Value = "Apellido M";
                //ws.Cells["C3"].Style.Font.Bold = true;
                //ws.Cells["D3"].Value = "Nombre";
                //ws.Cells["D3"].Style.Font.Bold = true;

                //n = 4;
                //var i = 1;
                //foreach (var respuesta in test.Questions)
                //{
                //    ws.Cells[string.Concat(((char)(i + 64 + 4)).ToString(), "3")].Value = i;
                //    ws.Cells[string.Concat(((char)(i + 64 + 4)).ToString(), "3")].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //    i += 1;
                //}

                //foreach (var student in performanceList)
                //{
                //    i = 1;
                //    ws.Cells[string.Concat("A", n)].Value = count;
                //    ws.Cells[string.Concat("A", n)].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //    ws.Cells[string.Concat("B", n)].Value = student.StudentFatherLastName;
                //    ws.Cells[string.Concat("C", n)].Value = student.StudentMotherLastName;
                //    ws.Cells[string.Concat("D", n)].Value = student.StudentName;
                //    foreach (var respuesta in test.Questions)
                //    {
                //        if (student.isAnswered)
                //        {
                //            ws.Cells[string.Concat(((char)(i + 64 + 4)).ToString(), n)].Value = answers.Where(x => x.Status == true && x.Question.Id == respuesta.Id && x.User.Id == student.StudentID).FirstOrDefault().Choice;
                //            ws.Cells[string.Concat(((char)(i + 64 + 4)).ToString(), n)].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //        }
                //        else
                //        {
                //            ws.Cells[string.Concat(((char)(i + 64 + 4)).ToString(), n)].Value = "NR";
                //        }
                //        i += 1;
                //    }
                //    n += 1;
                //    count += 1;
                //}
            }
            else {
                ws.Cells["A1"].Value = "El test aun no se ha rendido";
            }



            var memorystream = new MemoryStream();
            pck.SaveAs(memorystream);
            return new System.Web.Mvc.FileStreamResult(memorystream, "application/vnd.ms-excel");
        }

        }

    }
