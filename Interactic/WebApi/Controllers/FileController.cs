﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModels;
using Rotativa;
using Rotativa.Options;
using Helper;
using System.Reflection;
using GeneralHelper;
using System.Data;
using System.IO;
using ClosedXML.Excel;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
    public class FileController : Controller
    {
        private InteracticContext db;

        public FileController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = true;      
        }

		[HttpPost]
		[Route("File/Download")]
        public ActionResult ViewFile(string path)
        {
			var binary = FileHelper.GetBinaryFile(path);
			string base64PDF = System.Convert.ToBase64String(binary, 0, binary.Length);

			HttpContext.Response.AppendHeader("Content-Disposition", "attachment;filename=" + "asdasd");
			return Content(base64PDF);
        }

    }
}