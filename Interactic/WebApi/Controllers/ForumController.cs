﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using ViewModels;
using System.Web;
using System.Web.Script.Serialization;
using Generic;
using System.IO;
using Enums;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class ForumController : ApiController
    {
        private InteracticContext db;

        public ForumController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = true;
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
        public HttpResponseMessage Get(int id)
        {
            var forum = db.Forum.FirstOrDefault(e => e.Status && e.Id == id);

            //var action = db.Action.FirstOrDefault(p => p.Type == (int)ActionType.Forum && p.Task_Id == forum.Id);

            //if (forum != null && TopicController.TopicAccess(action))
            if (forum != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObject(forum));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Foro no encontrado.");
        }

        [HttpGet, Route("Api/Forum/GetForumsBySubunity")]
        public object GetForumsBySubunity(int? id)
        {
            
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Forum.Where(x => x.Status && x.SubUnity_Id == id && x.General != true), ForumCloneHelper.Map));
        }

        [HttpGet, Route("Api/Forum/GetGlobalForumsByEducationalEstablishment")]
        public object GetGlobalForumsByEducationalEstablishment()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Forum.Where(x => x.Status && x.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId && x.General == true), ForumCloneHelper.Map));
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Post([FromBody] ForumRequestViewModel publicationRVM)
        {
            var SubUnity = db.SubUnity.FirstOrDefault(e => e.Status && e.Id == publicationRVM.SubUnity_Id);
            var forum = new Forum()
            {
                Title = publicationRVM.Name,
                Description = publicationRVM.Description,
                Type = publicationRVM.Type,
                SubUnity = db.SubUnity.FirstOrDefault(e => e.Status && e.Id == publicationRVM.SubUnity_Id),
                EducationalEstablishment_Id = GlobalParameters.EducationalEstablishmentId,
                General = SubUnity != null ? false : true
            };

            db.Forum.Add(forum);
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Put([FromBody] ForumRequestViewModel publicationRVM)
        {
            var forum = db.Forum.FirstOrDefault(p => p.Id == publicationRVM.Publication_Id);

            if (forum == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            forum.Title = publicationRVM.Name;
            forum.Description = publicationRVM.Description;
            forum.Type = publicationRVM.Type;
            forum.SubUnity = db.SubUnity.FirstOrDefault(e => e.Status && e.Id == publicationRVM.SubUnity_Id);
            forum.EducationalEstablishment_Id = GlobalParameters.EducationalEstablishmentId;
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Delete(int id)
        {
            var forum = db.Forum.FirstOrDefault(e => e.Id == id);

            if (forum != null)
            {
                forum.Status = false;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Foro no encontrado.");
        }
    }
}
