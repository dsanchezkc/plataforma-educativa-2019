﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Data.Entity;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using Helper;
using WebApi.CloneModels;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class ObservationController : ApiController
	{
		private InteracticContext db;

		public ObservationController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Get(int id)
		{
			var observation = db.Observation.FirstOrDefault(e => e.Status && e.Id == id);

			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == observation.Course.Id && (p.Subject_Id == observation.BaseSubject.Id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == observation.BaseSubject.Id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == observation.Course.Id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (observation != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(observation, ObservationCloneHelper.ObservationMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Anotación no encontrada.");
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		[Route("Api/Observation/GetObservationsByUserAndCourseAndBaseSubjectAndPeriodDetail")]
		public HttpResponseMessage GetObservationsByUserAndCourseAndBaseSubjectAndPeriodDetail(string user_id, int course_id, int basesubject_id, int perioddetail_id)
		{
			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == course_id && (p.Subject_Id == basesubject_id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == basesubject_id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == course_id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			var observations = db.Observation.AsNoTracking().Where(p => p.Status && p.User.Id == user_id && p.Course.Id == course_id && p.BaseSubject.Id == basesubject_id && p.PeriodDetail.Id == perioddetail_id).OrderByDescending(p => p.CreatedAt);

			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(observations, ObservationCloneHelper.ObservationMap_1));
		}


		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Post([FromBody] Observation observation)
		{
			if (observation == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == observation.Course.Id && (p.Subject_Id == observation.BaseSubject.Id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == observation.BaseSubject.Id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == observation.Course.Id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			db.Observation.Add(new Observation()
			{
				Message = observation.Message,
				BaseSubject = db.BaseSubject.FirstOrDefault(p => p.Id == observation.BaseSubject.Id),
				Course = db.Course.FirstOrDefault(p => p.Id == observation.Course.Id),
				User = db.Users.FirstOrDefault(p => p.Id == observation.User.Id && p.StudentCourses.Any(q => q.Status && q.Course.Id == observation.Course.Id)),
				Teacher = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId),
				PeriodDetail = db.PeriodDetail.FirstOrDefault(p => p.Id == observation.PeriodDetail.Id)
			});

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Put([FromBody] Observation _observation)
		{
			if (_observation == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			var observation = db.Observation.FirstOrDefault(p => p.Status && p.Id == _observation.Id);

			if (observation == null)
				return Request.CreateResponse(HttpStatusCode.NotFound);

			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == observation.Course.Id && (p.Subject_Id == observation.BaseSubject.Id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == observation.BaseSubject.Id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == observation.Course.Id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);			

			observation.Message = _observation.Message;
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Delete(int id)
		{
			var observation = db.Observation.FirstOrDefault(p => p.Id == id);

			if(observation == null)
				return Request.CreateResponse(HttpStatusCode.NotFound);

			if (GlobalParameters.Rol == "Profesor")
				if (!db.TeacherCourseSubject.Any(p => p.Course_Id == observation.Course.Id && (p.Subject_Id == observation.BaseSubject.Id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == observation.BaseSubject.Id))))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			if (GlobalParameters.Rol == "SubAdministrador")
				if (!db.Course.Any(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Id == observation.Course.Id))
					return Request.CreateResponse(HttpStatusCode.Unauthorized);

			db.Observation.Remove(observation);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}
	}
}