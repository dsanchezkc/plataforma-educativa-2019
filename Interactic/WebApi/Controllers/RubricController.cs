﻿
using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;
using WebApi.CloneModels;
using System.Web;
using System.Web.Script.Serialization;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class RubricController : ApiController
    {
        private InteracticContext db;

        public RubricController()
        {
            db = new InteracticContext();
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Rubric.Where(x=> x.Status), RubricCloneHelper.RubricMap_3));
        }

        public HttpResponseMessage Get(int id)
        {
            var rubric = db.Rubric.FirstOrDefault(e => e.Id == id && e.Status == true);

            if (rubric != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(rubric, RubricCloneHelper.RubricMap_1));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Rúbrica no encontrada.");
        }

        [Authorize(Roles = "Profesor")]
        [HttpGet, Route("Api/Rubric/GetRubricsBySubject")]
        public HttpResponseMessage GetRubricsBySubject(int subject_id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Rubric.Where(x => x.Status && x.Subject_Id == subject_id), RubricCloneHelper.RubricMap_3));
        }

        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Post()
        {
            var form = HttpContext.Current.Request.Form;
            var rubric = new JavaScriptSerializer().Deserialize<Rubric>(form["Rubric"]);
            db.Rubric.Add(rubric);
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, rubric.Id);
        }

        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Put()
        {
            var form = HttpContext.Current.Request.Form;
            var _rubric = new JavaScriptSerializer().Deserialize<Rubric>(form["Rubric"]);
            var rubric = db.Rubric.FirstOrDefault(p => p.Id == _rubric.Id);
            var al = db.AchievementLevel.Where(x => x.Rubric_Id == _rubric.Id);
            rubric.Name = _rubric.Name;
            
            var listCriterions = _rubric.Criterions.Select(x => x.Id).ToList();

            var listAchievementLevels = _rubric.AchievementLevels.Select(x => x.Id).ToList();

            foreach (var item in db.Criterion.Where(x=> x.Rubric_Id == rubric.Id).Select(x=> x.Id).ToList()) {
                if (listCriterions.Contains(item))
                {
                    var cr = _rubric.Criterions.FirstOrDefault(x => x.Id == item);
                    _rubric.Criterions.Remove(cr);
                }
                else {
                    var cr = db.Criterion.FirstOrDefault(x => x.Id == item);
                    db.Criterion.Remove(cr);
                }
            }


            foreach (var item in db.AchievementLevel.Where(x => x.Rubric_Id == rubric.Id).Select(x => x.Id).ToList())
            {
                if (listAchievementLevels.Contains(item))
                {
                    var cr = _rubric.AchievementLevels.FirstOrDefault(x => x.Id == item);
                    _rubric.AchievementLevels.Remove(cr);
                }
                else
                {
                    var cr = db.AchievementLevel.FirstOrDefault(x => x.Id == item);
                    db.AchievementLevel.Remove(cr);
                }
            }

            var delete = db.DescriptionRubricItem.Where(x => x.Rubric.Id == _rubric.Id);
            foreach (var rd in delete)
            {
                db.DescriptionRubricItem.Remove(rd);
            }

            var dimensions = db.RubricDimension.Where(x => x.Rubric.Id == _rubric.Id);
            foreach (var rd in dimensions)
            {
                db.RubricDimension.Remove(rd);
            }
            
            rubric.AchievementLevels = _rubric.AchievementLevels;
            rubric.Criterions = _rubric.Criterions;
            rubric.RubricDimensions = _rubric.RubricDimensions;
            rubric.DescriptionRubricItems = _rubric.DescriptionRubricItems;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, rubric.Id);
        }

        [Authorize(Roles = "Profesor")]
        [HttpGet, Route("Api/Rubric/GetCriterionsAndLevelsByRubric")]
        public HttpResponseMessage GetCriterionsAndLevelsByRubric(int rubric_id)
        {
            var rubric = db.Rubric.FirstOrDefault(p => p.Id == rubric_id);
            if (rubric != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(rubric, RubricCloneHelper.RubricMap_2));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Rúbrica no encontrada.");
        }

        [Authorize(Roles = "Profesor")]
        [HttpGet, Route("Api/Rubric/SetCriterionText")]
        public HttpResponseMessage SetCriterionText(int criterion_id, string text)
        {
            var criterion = db.Criterion.FirstOrDefault(p => p.Id == criterion_id);
            if (criterion != null)
            {
                criterion.Name = text;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Criterio no encontrado.");
            }
        }

        [Authorize(Roles = "Profesor")]
        [HttpGet, Route("Api/Rubric/SetAchievementLevelText")]
        public HttpResponseMessage SetAchievementLevelText(int achievementlevel_id, string text)
        {
            var achievement = db.AchievementLevel.FirstOrDefault(p => p.Id == achievementlevel_id);
            if (achievement != null)
            {
                achievement.Name = text;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Nivel de logro no encontrado.");
            }
        }

        [Authorize(Roles = "Profesor")]
        [HttpPut, Route("Api/Rubric/RegisterDescriptionsRubric")]
        public HttpResponseMessage RegisterDescriptionsRubric()
        {
            var form = HttpContext.Current.Request.Form;
            var _rubric = new JavaScriptSerializer().Deserialize<Rubric>(form["Rubric"]);
            if (_rubric.DescriptionRubricItems != null) {
                var delete = db.DescriptionRubricItem.Where(x => x.Rubric.Id == _rubric.Id);
                foreach (var rd in delete)
                {
                    db.DescriptionRubricItem.Remove(rd);
                }
                foreach (var dri in _rubric.DescriptionRubricItems) {
                    db.DescriptionRubricItem.Add(dri);
                }
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Delete(int id)
        {
            var rubric = db.Rubric.FirstOrDefault(e => e.Id == id);

            if (rubric != null)
            {
                rubric.Status = false;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Curso no encontrado.");
        }
    }
}
