﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using System.Collections.ObjectModel;
using WebApi.CloneModels;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class SubjectController : ApiController
    {
        private InteracticContext db;

        public SubjectController()
        {
            db = new InteracticContext();
        }

		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Subject.Where(p => p.Status && p.CourseSubjects.FirstOrDefault(q => q.Status && q.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId) != null), SubjectCloneHelper.SubjectMap_1));
		}

		public HttpResponseMessage Get(int id)
		{
			var subject = db.Subject.FirstOrDefault(e => e.Status && e.Id == id);

			if (subject != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(subject, SubjectCloneHelper.SubjectMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Asignatura no encontrada.");
		}

		[Route("Api/Subject/GetSubjectsByCourse")]
		public HttpResponseMessage GetSubjectsByCourse(int course_id)
		{
			var subjects = db.CourseSubject.Where(e => e.Status && e.Course_Id == course_id).Select(e => e.Subject);

			if (subjects != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(subjects, SubjectCloneHelper.SubjectMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Asignatura no encontrada.");
		}

		/// <summary>
		/// @Author AP
		/// Entrega un listado de asignaturas asociadas a un Curso y filtrado por el usuario logeado.
		/// Con ello lleva la lista de SubAsignaturas asociadas al curso.
		/// </summary>
		/// <returns></returns>
		[Route("Api/Subject/GetSubjectsWithSubSubjectsByCourseFilteredByUser")]
		public HttpResponseMessage GetSubjectsWithSubSubjectsByCourseFilteredByUser(int course_id)
		{
			IEnumerable<Subject> subjects = null;

			switch (GlobalParameters.Rol)
			{
				case "Profesor":
					subjects = CloneHelper.CloneListMap(db.TeacherCourseSubject.Where(p => p.Status && p.CourseSubject.Subject.Status && p.User.Id == GlobalParameters.UserId && p.CourseSubject.Course_Id == course_id)
																												  .Select(p => p.CourseSubject.Subject), SubjectCloneHelper.SubjectMap_2);
				break;

				case "Administrador":
					subjects = CloneHelper.CloneListMap(db.CourseSubject.Where(e => e.Status && e.Subject.Status && e.Course_Id == course_id).Select(e => e.Subject), SubjectCloneHelper.SubjectMap_2);
				break;

				case "SubAdministrador":
					subjects = CloneHelper.CloneListMap(db.CourseSubject.Where(e => e.Status && e.Subject.Status && e.Course_Id == course_id).Select(e => e.Subject), SubjectCloneHelper.SubjectMap_2);
				break;

				default:
					return Request.CreateResponse(HttpStatusCode.BadRequest);
			}

			if (subjects != null)
				foreach (var subject in subjects)
				{
					var subSubjects = subject.SubSubjects.ToList();
					for (var i = 0; i < subSubjects.Count; i++)
					{
						var subSubject = subSubjects[i];
						var isContain = db.CourseSubSubject.AsNoTracking().FirstOrDefault(p => p.Subject_Id == subject.Id && p.Course_Id == course_id && p.SubSubject_Id == subSubject.Id) != null;

						if (!isContain)
							subject.SubSubjects.Remove(subSubject);
					}
				}

			return Request.CreateResponse(HttpStatusCode.OK, subjects.OrderBy(p => p.SubSubjects.Count));
		}

		/// <summary>
		/// @Author AP
		/// Entrega un listado de asignaturas asociadas a un Curso y filtrado por el usuario logeado.
		/// </summary>
		/// <returns></returns>
		[Route("Api/Subject/GetSubjectsByCourseFilteredByUser")]
		public HttpResponseMessage GetSubjectsByCourseFilteredByUser(int course_id)
		{
			switch (GlobalParameters.Rol)
			{
				case "Profesor":
					return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.TeacherCourseSubject.Where(p => p.Status && p.User.Id == GlobalParameters.UserId && p.CourseSubject.Course_Id == course_id)
																												  .Select(p => p.CourseSubject.Subject)));
				case "Alumno":
					var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
					return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(user.StudentCourses.FirstOrDefault(p => p.Status && p.Course_Id == course_id)?.Course.CourseSubjects.Select(q => q.Subject)));
				default:
					return GetSubjectsByCourse(course_id);
			}
		}

		// Carga select utilizado para generacion de informes.
		[Route("api/subject/report/select")]
        public IHttpActionResult GetSubjectBySchool(string _coursId, string _teachId = null )
        {
            var teacher = GlobalParameters.UserId;            
            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));

            var selectOptions = new List<GeneralSelectDto>();

            if (User.IsInRole(RoleName.Teacher))
            {
                selectOptions = db.TeacherCourseSubject
                                         .Where(x => x.User.Id == teacher &&
                                                     x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                     x.CourseSubject.Course_Id == courseId
                                                 )
                                         .Select(x => new GeneralSelectDto
                                         {
                                             Id = x.CourseSubject.Subject.Id.ToString(),
                                             Name = x.CourseSubject.Subject.Name
                                         })
                                         .ToList();
            }
            else {
                if (User.IsInRole(RoleName.Administrador) || User.IsInRole(RoleName.SubAdministrador))
                    selectOptions = db.CourseSubject
                                         .Where(x => 
                                                     x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                     x.Course_Id == courseId
                                                 )
                                         .Select(x => new GeneralSelectDto
                                         {
                                             Id = x.Subject.Id.ToString(),
                                             Name = x.Subject.Name
                                         })
                                         .ToList();
            }

            if (selectOptions == null) return BadRequest();

            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add( new GeneralSelectDto()
                {
                    Id= Encryptor.Encrypt( item.Id.ToString()),
                    Name = item.Name
                });
            }
            return Ok(select);
        }

       

        // Carga select utilizado para generacion de informes.
        //Revisar de nuevo ahora la clase implementar Prefijo de Ruta
        [Route("api/subjectForStudent/report/select")]
        public IHttpActionResult GetSubjectForStudent(string _coursId, string _studentId = null)
        {
            var studentId = Encryptor.Decrypt(_studentId) ?? GlobalParameters.UserId;
            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));

            var courseSubjects = db.CourseSubject
                                        .Where(x => x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.Course.Year == GlobalParameters.Year &&
                                                    x.Course.StudentCourses.Any(a => a.User.Id == studentId &&
                                                    x.Course.Id == courseId &&
                                                    x.Status &&
                                                    x.Subject.Status)
                                                )
                                        .ToList();

            if (courseSubjects == null) return BadRequest("Alumno sin curso");

            var selectOptions = courseSubjects
                                        .Select(x => new { x.Subject.Id, x.Subject.Name })
                                        .Distinct()
                                        .ToList();

            if (selectOptions == null) return BadRequest("No se pudo cargar el select");

            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add(new GeneralSelectDto()
                {
                    Id = Encryptor.Encrypt(item.Id.ToString()),
                    Name = item.Name
                });
            }
            return Ok(select);
        }

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Post([FromBody] Subject subject)
		{
			if (subject != null)
			{
				var courseSubjects = new Collection<CourseSubject>();

				foreach (var item in subject.CourseSubjects)
				{
					var _item = db.Course.FirstOrDefault(p => p.Id == item.Course_Id);

					if(_item==null)
						return Request.CreateResponse(HttpStatusCode.BadRequest);

					courseSubjects.Add(new CourseSubject() { Course = _item });
				}

				subject.CourseSubjects = courseSubjects;
				db.Subject.Add(subject);
				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
			}

			return Request.CreateResponse(HttpStatusCode.BadRequest);
		}

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Put([FromBody] Subject _subject)
		{
			var subject = db.Subject.FirstOrDefault(e => e.Id == _subject.Id);
			var courseSubjects = subject.CourseSubjects.Where(p => p.Course.Year == GlobalParameters.Year && p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId);

			if (subject != null)
			{
				foreach (var courseSubject in courseSubjects.Where(p => _subject.CourseSubjects.FirstOrDefault(q => q.Course_Id == p.Course_Id) == null))
					courseSubject.Status = false;

				foreach (var _cs in _subject.CourseSubjects)
				{
					var _item = courseSubjects.FirstOrDefault(p => p.Course_Id == _cs.Course_Id);

					if (_item == null)
						subject.CourseSubjects.Add(new CourseSubject() { Course = db.Course.FirstOrDefault(p => p.Id == _cs.Course_Id) });
					else
						_item.Status = true;
				}

				subject.TypeQualification = _subject.TypeQualification;
				subject.Name = _subject.Name;

				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Curso no encontrado.");
		}

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Delete(int id)
		{
			var subject = db.Subject.FirstOrDefault(e => e.Id == id);

			if (subject != null)
			{
				subject.Status = false;
				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Curso no encontrado.");
		}
	}
}
