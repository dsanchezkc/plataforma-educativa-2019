﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using ViewModels;
using System.Web;
using System.Web.Script.Serialization;
using Generic;
using System.IO;
using Enums;
using System.Net.Mail;
using System.Text;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class PostController : ApiController
	{
		private InteracticContext db;

		public PostController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		public HttpResponseMessage Get(int id)
		{
			var post = db.Post.FirstOrDefault(p => p.Status && p.Id == id);
			
			if(post == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Comentario no encontrado.");

			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Comentario no encontrado.");
		}

		[Route("Api/Post/Qualification")]
		[HttpPost]
		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Qualification([FromBody] Post _post)
		{
			if (_post == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			var post = db.Post.FirstOrDefault(p => p.Status && p.Id == _post.Id);

			if (post == null)
				return Request.CreateResponse(HttpStatusCode.NotFound);
            /*
			var action = db.Action.FirstOrDefault(p => p.Type == (int)ActionType.Forum && p.Task_Id == post.Topic.Forum.Id);

            //Verificando que los alumnos no tengan ya nota en el Topic
            foreach (var item in _post.PostQualifications)
			{
				var postQualification = db.PostQualification.AsNoTracking().FirstOrDefault(p => p.Status && p.User_Id == item.User.Id && p.Post.Topic.Id == post.Topic.Id && p.Post_Id != post.Id && p.Qualification!=0);

				if (postQualification != null)
					return Request.CreateResponse(HttpStatusCode.BadRequest, "El alumno [" + postQualification.User.UserName.WithFormatRUN() + "] " + postQualification.User.Name + " ya tiene registrada una nota en esta entrada. No es posible continuar. Desvincule este alumno de esta entrada o de la que esté asociado para poder registrar.");
			}

			foreach (var _postQualification in _post.PostQualifications)
				if (_postQualification.Qualification <= 0)
					_postQualification.Qualification = 1;
				else
					if (_postQualification.Qualification >= 101)
					_postQualification.Qualification = 100;

			foreach (var postQualification in post.PostQualifications)
			{
				var _postQualification = _post.PostQualifications.FirstOrDefault(p => p.User.Id == postQualification.User_Id && _post.Id == postQualification.Post_Id);

				postQualification.Status = _postQualification != null;

				if (_postQualification != null)
					postQualification.Qualification = _postQualification.Qualification;
			}

			//Preguntamos si el autor fue desvinculado del Post para cambiar por otro.
			if (!post.PostQualifications.Any(p => p.User.Id == post.Author.Id && p.Status))
			{
				post.Author = post.PostQualifications.FirstOrDefault(p => p.Status)?.User;

				if (post.Author == null)
					return Request.CreateResponse(HttpStatusCode.BadRequest, "Al menos debe existir un alumno que represente esta entrada");
			}

			foreach (var _postQualification in _post.PostQualifications)
			{
				var postQualification = post.PostQualifications.FirstOrDefault(p => p.User.Id == _postQualification.User.Id && p.Post.Id == _post.Id);

				if (postQualification == null)
					post.PostQualifications.Add(new PostQualification()
					{
						User = db.Users.FirstOrDefault(p => p.Id == _postQualification.User.Id),
						Post = post,
						Qualification = _postQualification.Qualification
					});
			}
            */


            db.SaveChanges(); 
            return Request.CreateResponse(HttpStatusCode.OK);
		}


		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
		public HttpResponseMessage Post()
		{
			var _post = Serializer.Deseralize<Post>(HttpContext.Current.Request.Form["Post"]);

			if (_post.Text != null || _post.Text != "")
				_post.Text = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(_post.Text));

			var files = HttpContext.Current.Request.Files;

			if (_post == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			var post = new Post()
			{
				Text = _post.Text,
				Topic = db.Topic.FirstOrDefault(p => p.Id == _post.Topic.Id),
				PostFiles = new List<PostFile>(),
				Author = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId),
				/*
                PostQualifications = new List<PostQualification>()
				{
					new PostQualification()
					{
						User = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId)
					}
				}
                */
			};

            
            //var students = post.Topic.Forum.SubUnity.Unity.CourseSubject.Course.StudentCourses.Select(p => p.User);

            /*
            foreach (var postQualification in _post.PostQualifications)
			{
				if (!post.PostQualifications.Any(p => p.User.Id == postQualification.User.Id))
				{
					var user = students.FirstOrDefault(p => p.Id == postQualification.User.Id);

					if (user != null)
						post.PostQualifications.Add(new PostQualification()
						{
							User = user
						});
				}				
			}
            */
			
			db.Post.Add(post);
			db.SaveChanges();

			for (var i = 0; i < files.Count; i++)
			{
				var file = files[i];

				if (file != null && file.ContentLength != 0)
				{
					var relativePath = PathHelper.ForumPostPath + post.Id;
					var path = PathHelper.GetFullPath(relativePath);

					try
					{
						Directory.CreateDirectory(path);
						var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
						path = Path.Combine(path, fileName);
						file.SaveAs(path);

						post.PostFiles.Add(new PostFile()
						{
							DocumentName = Path.GetFileName(file.FileName),
							DocumentPath = Path.Combine(relativePath, fileName),
							DocumentType = file.ContentType.ToLower()
						});
					}
					catch (Exception)
					{
						continue;
					}
				}
			}
			
			db.SaveChanges();
            /*
            var list = post.Topic.Forum.SubUnity.Unity.CourseSubject.Course.StudentCourses.Select(p => p.User.Id);
            var mails = db.UserRoleEducationalEstablishment.Where(x => list.Contains(x.User.Id) && x.Status == true && x.User.Email != "" &&  x.EducationalEstablishment.Id == post.Topic.Forum.SubUnity.Unity.CourseSubject.Course.EducationalEstablishment.Id).Select(x => x.User.Email).ToArray();
            //var mails = db.Users.Where(x => list.Contains(x.Id) && x.Email != "" && x.Status == true).Select(x => x.Email).ToArray();
            int j = mails.Count();
            MailMessage mail = new MailMessage();
            SmtpClient MyMail = new SmtpClient();
            MailMessage MyMsg = new MailMessage();
            MyMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            MyMail.EnableSsl = true;
            MyMail.Host = "smtp.gmail.com";
            MyMail.Port = 587;

            MyMsg.Priority = MailPriority.Normal;
            MyMsg.Subject = "Nuevo comentario en el foro";
            MyMsg.SubjectEncoding = Encoding.UTF8;
            MyMsg.IsBodyHtml = true;
            MyMsg.From = new MailAddress("interacticp@gmail.com", "Plataforma Interactic");
            MyMsg.BodyEncoding = Encoding.UTF8;
            MyMsg.Body = "Se ha agregado un nuevo comentario al foro: " + post.Text
                + "<br><br><b>Autor: </b> " + post.Author.Name + " " + post.Author.FatherLastName + " " + post.Author.MotherLastName
                + "<br><b>Tema: </b>" + post.Topic.Title
                + "<br><b>Fecha:</b> " + post.CreatedAt
                + "<br><b>Sub-Unidad: </b> " + post.Topic.Forum.SubUnity.Name
                + "<br><b>Unidad:</b> " + post.Topic.Forum.SubUnity.Unity.Name
                + "<br><b>Curso: </b>" + post.Topic.Forum.SubUnity.Unity.CourseSubject.Course.Name
                + "<br><b>Establecimiento Educacional: </b>" + post.Topic.Forum.SubUnity.Unity.CourseSubject.Course.EducationalEstablishment.Name
                + "<br><b>Asignatura: </b>" + post.Topic.Forum.SubUnity.Unity.CourseSubject.Subject.Name
                + "<br><b>Profesor: </b>";
            foreach (var item in post.Topic.Forum.SubUnity.Unity.CourseSubject.TeacherCourseSubjects.Where(x => x.Status == true))
            {
                MyMsg.Body = MyMsg.Body + item.User.Name + " " + item.User.FatherLastName + " " + item.User.MotherLastName;
            }

            MyMsg.Body = MyMsg.Body + "<br><br> Para ingresar a la plataforma presione <b><a href='http://www.evaluandome.org/'>aquí</a></b>"
             ;
            MyMail.UseDefaultCredentials = false;
            NetworkCredential MyCredentials = new NetworkCredential("interacticp@gmail.com", "claveinteractic");
            MyMail.Credentials = MyCredentials;
            foreach (var ml in mails)
            {
                MyMsg.To.Add(ml);
            }
            try
            {
                MyMail.Send(MyMsg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            */

            //MailMessage mail = new MailMessage();
            //SmtpClient MyMail = new SmtpClient();
            //MailMessage MyMsg = new MailMessage();
            //MyMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            //MyMail.Host = "191.96.42.216";
            //MyMail.Port = 25;

            //MyMsg.Priority = MailPriority.Normal;
            //MyMsg.Subject = "Hola";
            //MyMsg.SubjectEncoding = Encoding.UTF8;
            //MyMsg.IsBodyHtml = true;
            //MyMsg.From = new MailAddress("desp002@gmail.com", "Daniel Sanchez");
            //MyMsg.BodyEncoding = Encoding.UTF8;
            //MyMsg.Body = String.Concat(post.Author, " ha escrito el siguiente mensaje: ", post.Text);
            //MyMail.UseDefaultCredentials = false;
            //NetworkCredential MyCredentials = new NetworkCredential("desp002@gmail.com", "caballodetroyas");
            //MyMail.Credentials = MyCredentials;
            //foreach (var ml in mails)
            //{
            //    MyMsg.To.Add(ml);
            //}
            //try
            //{
            //    MyMail.Send(MyMsg);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            return Request.CreateResponse(HttpStatusCode.OK);
		}		

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Delete(int id)
		{
			var post = db.Post.FirstOrDefault(e => e.Id == id);

			if (post != null)
			{
				post.Status = false;
				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Post no encontrado.");
		}
	}
}
