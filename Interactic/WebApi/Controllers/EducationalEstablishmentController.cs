﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.IO;
using System.Data.Entity;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using Helper;
using GeneralHelper;
using WebApi.CloneModels;
using System.Net.Http.Headers;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class EducationalEstablishmentController : ApiController
    {
        private InteracticContext db;

        public EducationalEstablishmentController()
        {
            db = new InteracticContext();
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.EducationalEstablishment.Where(p => p.Status), EducationalEstablishmentCloneHelper.EducationalEstablishment_1));
        }

        public HttpResponseMessage Get(int id)
        {
            var educationalEstablishment = db.EducationalEstablishment.FirstOrDefault(e => e.Status && e.Id == id);

            if (educationalEstablishment != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(educationalEstablishment, EducationalEstablishmentCloneHelper.EducationalEstablishment_1));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Establecimiento no encontrado.");
        }

        [AllowAnonymous]
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
        [Route("Api/EducationalEstablishment/GetStructureJSON")]
        public HttpResponseMessage GetStructureJSON()
        {
            var subUnities = db.SubUnity.Where(p => p.Status && p.Unity != null && p.Unity.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId);
            switch (GlobalParameters.Rol)
            {
                case "Profesor":
                    //if (subUnities != null)
                    //return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(subUnities, SubUnityCloneHelper.SubUnityMap_1), MediaTypeHeaderValue.Parse("application/json"));
                    // else
                    //return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");
                    var courses = db.TeacherCourseSubject.Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year && p.CourseSubject.Course.Status)
                     .Select(p => p.CourseSubject.Course.Id);
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.SubUnity.Where(p => p.Status && courses.Contains(p.Unity.CourseSubject.Course.Id)), SubUnityCloneHelper.SubUnityMap_1));
                //var subUnitiesTeacher = db.SubUnity.Where(p => p.Status && p.Unity.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && courses.Contains(p.Unity.CourseSubject.Course.Id));
                //if (subUnities != null)
                //    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(subUnitiesTeacher, SubUnityCloneHelper.SubUnityMap_1), MediaTypeHeaderValue.Parse("application/json"));
                //else
                //  return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");
                case "Alumno":
                    if (subUnities != null)
                        return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(subUnities, SubUnityCloneHelper.SubUnityMap_1), MediaTypeHeaderValue.Parse("application/json"));
                    else
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");
                //var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
                //var course = db.TeacherCourseSubject.Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year && p.CourseSubject.Course.Status)
                //                                                                                            .Select(p => p.CourseSubject.Course.Id);
                //return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.SubUnity.Where(p => p.Status && course.Contains(p.Unity.CourseSubject.Course.Id)), CourseCloneHelper.CourseMap_1));

                case "Administrador":
                    if (subUnities != null)
                        return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(subUnities, SubUnityCloneHelper.SubUnityMap_1), MediaTypeHeaderValue.Parse("application/json"));
                    else
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");

                case "SubAdministrador":
                    if (subUnities != null)
                        return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(subUnities, SubUnityCloneHelper.SubUnityMap_1), MediaTypeHeaderValue.Parse("application/json"));
                    else
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");

                default: return Request.CreateResponse(HttpStatusCode.OK);
            }

        }

        [AllowAnonymous]
        [Route("Api/EducationalEstablishment/GetEducationalEstablishmentsById_Anonymous")]
        public HttpResponseMessage GetEducationalEstablishmentsById_Anonymous(int id)
        {
            var educationalEstablishment = db.EducationalEstablishment.FirstOrDefault(e => e.Status && e.Id == id);

            if (educationalEstablishment == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Establecimientos no encontrados.");

            var _educationalEstablishment = CloneHelper.CloneObject(educationalEstablishment, 1) as EducationalEstablishment;
            _educationalEstablishment.NameImage = _educationalEstablishment.PathImage = "";
            return Request.CreateResponse(HttpStatusCode.OK, _educationalEstablishment);
        }

        [Route("Api/EducationalEstablishment/GetCurrentEducationalEstablishment")]
        public HttpResponseMessage GetCurrentEducationalEstablishment()
        {
            return Get(GlobalParameters.EducationalEstablishmentId);
        }

        [AllowAnonymous]
        [Route("Api/EducationalEstablishment/GetEducationalEstablishmentsByCommune")]
        public HttpResponseMessage GetEducationalEstablishmentsByCommune(int commune_id)
        {
            var educationalEstablishments = db.EducationalEstablishment.Where(e => e.Status && e.Commune.Id == commune_id);

            var _educationalEstablishments = CloneHelper.CloneList(educationalEstablishments, 1);

            foreach (var _educationalEstablishment in _educationalEstablishments)
                _educationalEstablishment.NameImage = _educationalEstablishment.PathImage = "";

            if (educationalEstablishments != null)
                return Request.CreateResponse(HttpStatusCode.OK, _educationalEstablishments);
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Establecimientos no encontrados.");
        }

        [Route("Api/EducationalEstablishment/GetEducationalEstablishmentsFilteredByUser")]
        public HttpResponseMessage GetEducationalEstablishmentsFilteredByUser()
        {
            var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
            var isSuperAdmin = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.Status && p.Role.Name == "Administrador") != null;

            if (isSuperAdmin)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.EducationalEstablishment.Where(p => p.Status)));
            else
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(user.UserRoleEducationalEstablishments.Where(p => p.Status).Select(p => p.EducationalEstablishment).Distinct(), 1));
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        [Route("Api/EducationalEstablishment/GetEducationalEstablishmentsFilteredByUser_AdminAndSubAdmin")]
        public HttpResponseMessage GetEducationalEstablishmentsFilteredByUser_AdminAndSubAdmin()
        {
            var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
            var isSuperAdmin = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.Status && p.Role.Name == "Administrador") != null;

            if (isSuperAdmin)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.EducationalEstablishment.Where(p => p.Status)));
            else
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(user.UserRoleEducationalEstablishments.Where(p => p.Status && p.Role.Name == "SubAdministrador").Select(p => p.EducationalEstablishment).Distinct(), 1));
        }

        [Authorize(Roles = "Alumno")]
        [Route("Api/EducationalEstablishment/GetEducationalEstablishmentsFilteredByUser_Student")]
        public HttpResponseMessage GetEducationalEstablishmentsFilteredByUser_Student()
        {
            var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(user.UserRoleEducationalEstablishments.Where(p => p.Status && p.Role.Name == "Alumno").Select(p => p.EducationalEstablishment).Distinct(), 1));
        }

        [Route("Api/EducationalEstablishment/GetEducationalEstablishmentsByCommuneFilteredByUser")]
        public HttpResponseMessage GetEducationalEstablishmentsByCommuneFilteredByUser(int commune_id)
        {
            var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
            var isSuperAdmin = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.Status && p.Role.Name == "Administrador") != null;

            if (isSuperAdmin)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.EducationalEstablishment.Where(e => e.Commune.Id == commune_id), 1));
            else
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(user.UserRoleEducationalEstablishments.Where(p => p.EducationalEstablishment.Commune.Id == commune_id && p.Status).Select(p => p.EducationalEstablishment), 1));
        }

        [Authorize(Roles = "Administrador")]
        public HttpResponseMessage Post()
        {
            var form = HttpContext.Current.Request.Form;
            var file = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files[0] : null;

            var educationalEstablishment = new EducationalEstablishment()
            {
                Name = form["Name"],
                Address = form["Address"],
                Commune = new Commune() { Id = int.Parse(form["Select_Commune_2"]) },
                EstablishmentPeriod = new EstablishmentPeriod() { Id = int.Parse(form["EstablishmentPeriod"]) }
            };

            educationalEstablishment.Commune = db.Commune.FirstOrDefault(p => p.Id == educationalEstablishment.Commune.Id);
            educationalEstablishment.EstablishmentPeriod = db.EstablishmentPeriod.FirstOrDefault(p => p.Id == educationalEstablishment.EstablishmentPeriod.Id);

            db.EducationalEstablishment.Add(educationalEstablishment);
            db.SaveChanges();

            if (file != null && file.ContentLength != 0)
            {
                if (file.ContentType.ToLower() != "image/png" && file.ContentType.ToLower() != "image/jpeg" && file.ContentType.ToLower() != "image/pjpeg")
                    return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

                var path = @"C:\EvaluandomeResources\Images\Establishment\" + educationalEstablishment.Id;
                var physicalPath = ImageHelper.SaveImage(file, path, resolutionMax: 500);

                if (physicalPath != null)
                {
                    educationalEstablishment.NameImage = Path.GetFileName(file.FileName);
                    educationalEstablishment.PathImage = physicalPath;
                }
            }

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        public HttpResponseMessage Put()
        {
            var form = HttpContext.Current.Request.Form;
            var file = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files[0] : null;

            var educationalEstablishment = new EducationalEstablishment()
            {
                Id = int.Parse(form["Id"]),
                Name = form["Name"],
                Address = form["Address"],
                Commune = new Commune() { Id = int.Parse(form["Select_Commune_2"]) },
                EstablishmentPeriod = new EstablishmentPeriod() { Id = int.Parse(form["EstablishmentPeriod"]) }
            };

            var item = db.EducationalEstablishment.FirstOrDefault(p => p.Id == educationalEstablishment.Id);

            //El establecimiento por defecto no puede ser modificado
            if (item == null || item.Id == 1) return Request.CreateResponse(HttpStatusCode.BadRequest);

            if (GlobalParameters.Rol == "SubAdministrador")
            {
                var contain = db.UserRoleEducationalEstablishment.FirstOrDefault(p => p.Status && p.User.Id == GlobalParameters.UserId && p.Role.Name == "SubAdministrador" && p.EducationalEstablishment_Id == educationalEstablishment.Id);

                if (contain == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            item.Name = educationalEstablishment.Name;
            item.Address = educationalEstablishment.Address;
            item.Commune = db.Commune.FirstOrDefault(p => p.Id == educationalEstablishment.Commune.Id);
            item.EstablishmentPeriod = db.EstablishmentPeriod.FirstOrDefault(p => p.Id == educationalEstablishment.EstablishmentPeriod.Id);

            if (file != null && file.ContentLength != 0)
            {
                if (file.ContentType.ToLower() != "image/png" && file.ContentType.ToLower() != "image/jpeg" && file.ContentType.ToLower() != "image/pjpeg")
                    return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

                var path = @"C:\EvaluandomeResources\Images\Establishment\" + item.Id;
                var physicalPath = ImageHelper.SaveImage(file, path, resolutionMax: 500);

                if (physicalPath != null)
                {
                    item.NameImage = Path.GetFileName(file.FileName);
                    item.PathImage = physicalPath;
                }
            }

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Administrador")]
        public HttpResponseMessage Delete(int id)
        {
            var item = db.EducationalEstablishment.FirstOrDefault(p => p.Id == id);

            //El establecimiento default no puede ser eliminado
            if (item == null || item.Id == 1 || item.UserRoleEducationalEstablishments.Count != 0 || item.Courses.Count != 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            db.EducationalEstablishment.Remove(item);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}