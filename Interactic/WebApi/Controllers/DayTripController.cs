﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class DayTripController : ApiController
    {
        private InteracticContext db;

        public DayTripController()
        {
            db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
        }

		/// <summary>
		/// Author AP
		/// Entrega el listado de jornadas.
		/// </summary>
		/// <returns></returns>
		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.Daytrip.OrderBy(p => p.Id)));
		}
	}
}
