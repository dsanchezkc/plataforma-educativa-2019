﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using ViewModels;
using System.Web;
using System.Web.Script.Serialization;
using Generic;
using System.IO;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class TopicFileController : ApiController
	{
		private InteracticContext db;

		public TopicFileController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		[HttpGet]
		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
		[Route("Api/TopicFile/Download")]
		public HttpResponseMessage Download(int topicfile_id)
		{
			var topicFile = db.TopicFile.AsNoTracking().FirstOrDefault(p => p.Status && p.Id == topicfile_id);

			if (topicFile == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error.");
			try
			{
				var binary = FileHelper.GetBinaryFile(PathHelper.GetFullPath(topicFile.DocumentPath));
				return Request.CreateResponse(HttpStatusCode.OK, Convert.ToBase64String(binary, 0, binary.Length));
			}
			catch (Exception)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, "Aún no ha sido subo el archivo.");
			}
		}
	}
}
