﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using Model;
using System.Web.Http.Cors;
using Helper;
using WebApi.CloneModels;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using GeneralHelper;
using Generic;

namespace WebApiEvaluandome.Controllers
{
    [Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
    public class EventController : ApiController
    {
        private InteracticContext db;

        public EventController()
        {
            db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
        }

        public HttpResponseMessage Get()
        {
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Event, EventCloneHelper.EventMap_1));
        }

        public HttpResponseMessage Get(int id)
        {    
            var _event = db.Event.FirstOrDefault(e => e.Id == id);

			if (_event != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObject(_event, 1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Evento no encontrado.");
        }

		[AllowAnonymous]
		[HttpGet]
		[Route("Api/Event/GetEventsByCourse")]
		public HttpResponseMessage GetEventsByCourse(int course_id)
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Event.Where(p => p.Course.Id == course_id).OrderBy(p => p.Start), EventCloneHelper.EventMap_1));
		}


        [Authorize(Roles ="Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Post()
        {
            var _event = Serializer.Deseralize<Event>(HttpContext.Current.Request.Form["Event"]);

            var files = HttpContext.Current.Request.Files;
            if (_event != null)
            {
                _event.Course = db.Course.FirstOrDefault(p => p.Id == _event.Course.Id);
                _event.User = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
                //if (_event.Base64 != null)
                //{
                //    _event.NameDocument = FileHelper.ChangeNameFile(@"C:\EvaluandomeResources\Pdfs\", Path.GetFileName(_event.NameDocument));
                //    _event.PathDocument = @"C:\EvaluandomeResources\Pdfs\" + _event.NameDocument;
                //    File.WriteAllBytes(@"C:\EvaluandomeResources\Pdfs\" + _event.NameDocument, Convert.FromBase64String(_event.Base64));
                //}
                db.Event.Add(_event);
                db.SaveChanges();
                for (var i = 0; i < files.Count; i++)
                {
                    var file = files[i];

                    if (file != null)
                    {
                        try
                        {
                            string nameDocument = Path.GetFileName(file.FileName);
                            var path = @"C:\EvaluandomeResources\Pdfs";
                            Directory.CreateDirectory(path);
                            var physicalPath = path + @"\" + FileHelper.ChangeNameFile(path, Path.GetFileName(file.FileName));
                            file.SaveAs(physicalPath);

                            db.EventFile.Add(new EventFile()
                            {
                                Event_Id = _event.Id,
                                NameDocument = file.FileName,
                                PathDocument = physicalPath
                            });
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                }
                db.SaveChanges();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]		
		public HttpResponseMessage Delete(int id)
        {
            var _event = db.Event.FirstOrDefault(e => e.Id == id);

            if (_event != null)
            {
                db.Event.Remove(_event);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            	return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Evento no encontrado.");
        }

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Put()
        {
            var _event = Serializer.Deseralize<Event>(HttpContext.Current.Request.Form["Event"]);
            var eventEntity = db.Event.FirstOrDefault(e => e.Id == _event.Id);

            var files = HttpContext.Current.Request.Files;
            if (GlobalParameters.Rol == "Profesor" && eventEntity.User.Id != GlobalParameters.UserId)
				return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Edición no autorizada.");

			if (eventEntity != null)
            {
                eventEntity.Title = _event.Title;
                eventEntity.Description = _event.Description;
                eventEntity.Color = _event.Color;
                eventEntity.Start = _event.Start;
                eventEntity.End = _event.End;

                for (var i = 0; i < files.Count; i++)
                {
                    var file = files[i];

                    if (file != null)
                    {
                        try
                        {
                            string nameDocument = Path.GetFileName(file.FileName);
                            var path = @"C:\EvaluandomeResources\Pdfs";
                            Directory.CreateDirectory(path);
                            var physicalPath = path + @"\" + FileHelper.ChangeNameFile(path, Path.GetFileName(file.FileName));
                            file.SaveAs(physicalPath);

                            db.EventFile.Add(new EventFile()
                            {
                                Event_Id = _event.Id,
                                NameDocument = file.FileName,
                                PathDocument = physicalPath
                            });
                            db.SaveChanges();
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                }
                //eventEntity.User = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
                db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
            }

			return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Evento no encontrado.");
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
        [AllowAnonymous]
        [HttpGet]
        [Route("Api/Event/DownloadPDF")]
        public HttpResponseMessage DownloadPDF(int id)
        {
            var file = db.EventFile.FirstOrDefault(p => p.Id == id && p.Status);

            if (file == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Documento no encontrado.");
            
            var binary = FileHelper.GetBinaryFile(file.PathDocument);
            string base64PDF = Convert.ToBase64String(binary, 0, binary.Length);
            string[] array = { base64PDF, file.PathDocument };

            return Request.CreateResponse(HttpStatusCode.OK, array);
            
        }
    }
}
