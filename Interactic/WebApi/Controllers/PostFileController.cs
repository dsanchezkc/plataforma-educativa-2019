﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using ViewModels;
using System.Web;
using System.Web.Script.Serialization;
using Generic;
using System.IO;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class PostFileController : ApiController
	{
		private InteracticContext db;

		public PostFileController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		[HttpGet]
		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
		[Route("Api/PostFile/Download")]
		public HttpResponseMessage Download(int postfile_id)
		{
			var postFile = db.PostFile.AsNoTracking().FirstOrDefault(p => p.Status && p.Id == postfile_id);

			if (postFile == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error.");
			try
			{
				var binary = FileHelper.GetBinaryFile(PathHelper.GetFullPath(postFile.DocumentPath));
				return Request.CreateResponse(HttpStatusCode.OK, Convert.ToBase64String(binary, 0, binary.Length));
			}
			catch (Exception)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, "Aún no ha sido subo el archivo.");
			}
		}
	}
}
