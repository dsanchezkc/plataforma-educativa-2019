﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class StudentController : ApiController
    {
        private InteracticContext db;

        public StudentController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        // Carga datos a tabla de filtros para generar informe rendimiento por alumno.
        [Route("api/student/report/table")]
        public IHttpActionResult GetStudent(string _coursId, string _subjecId, string _teachId = null)
        {
            var teacher = GlobalParameters.UserId;
            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));

            var course = new Course();

            if (User.IsInRole(RoleName.Teacher))
                course = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course.Id == courseId &&
                                                    x.CourseSubject.Subject.Id == subjectId
                                                )
                                        .Select(x => x.CourseSubject.Course)
                                        .SingleOrDefault();

            else
                course = db.CourseSubject
                                            .Where(x =>
                                                        x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                        x.Course.Id == courseId &&
                                                        x.Subject.Id == subjectId
                                                    )
                                            .Select(x => x.Course)
                                            .SingleOrDefault();

            if (course == null) return BadRequest();

            var studentCourses = db.StudentCourse.Where(x => x.Course.Id == course.Id).Include(x => x.User).ToList();

            if (studentCourses == null) return BadRequest();

            var select = new List<ReportTableDto>();

            foreach (var item in studentCourses)
            {
                select.Add(new ReportTableDto()
                {
                    Id = Encryptor.Encrypt(item.User.Id),
                    Name = item.User.Name,
                    LastNameP = item.User.FatherLastName,
                    LastNameM = item.User.MotherLastName
                });
            }
            return Ok(select);
        }

        [Route("api/student/studentForEducationalEstablishment/count")]
        public IHttpActionResult GetStudentForEducationalEstablishment()
        {
            var count = db.UserRoleEducationalEstablishment.Where(x => x.Role.Name == RoleName.Student && 
            x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && 
            x.User.Status).ToList().Count;

            return Ok(count);
        }

        // Carga datos a tabla de filtros para generar informe rendimiento por alumno.
        [Route("api/studentForCourse/report/table")]
        public IHttpActionResult GetStudentForCourse(string _coursId)
        {
            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));

            var studentCourses = db.StudentCourse.Where(x => x.Course.Id == courseId).Include(x => x.User).Distinct().ToList();

            if (studentCourses == null) return BadRequest();

            var select = new List<ReportTableDto>();

            foreach (var item in studentCourses)
            {
                select.Add(new ReportTableDto()
                {
                    Id = Encryptor.Encrypt(item.User.Id),
                    Name = item.User.Name,
                    LastNameP = item.User.FatherLastName,
                    LastNameM = item.User.MotherLastName
                });
            }
            return Ok(select);
        }

        // Carga estudiantes de un apoderado
        [Route("api/studentbyguardian/report/select/students")]
        public IHttpActionResult GetStudentByGuardian()
        {
            var guardianId = GlobalParameters.UserId;

            var studentCourses = db.Guardian.Where(x => x.User_Id == guardianId && x.Status).Select(x => x.Student);

            if (studentCourses == null) return BadRequest();

            var select = new List<ReportTableDto>();

            foreach (var item in studentCourses)
            {
                select.Add(new ReportTableDto()
                {
                    Id = Encryptor.Encrypt(item.Id),
                    Name = item.Name,
                    LastNameP = item.FatherLastName,
                    LastNameM = item.MotherLastName
                });
            }
            return Ok(select);
        }



    }
}
