﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using WebApi.CloneModels;
using System.Net.Http.Headers;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class NoteController : ApiController
    {
        private InteracticContext db;

        public NoteController()
        {
            db = new InteracticContext();
        }
		
		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Note, NoteCloneHelper.NoteMap_1));
		}
		
		public HttpResponseMessage Get(int id)
		{
			var note = db.Note.FirstOrDefault(e => e.Id == id);

			if (note != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(note, NoteCloneHelper.NoteMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Apunte no encontrado.");
		}

		[Route("Api/Note/GetNotesBySubUnity")]
		public HttpResponseMessage GetNotesBySubUnity(int subunity_id)
		{
			var notes = db.Note.Where(p => p.SubUnity.Id == subunity_id).OrderByDescending(p => p.CreatedAt);

			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(notes, NoteCloneHelper.NoteMap_1));
		}

		[Authorize(Roles = "Profesor")]
		public HttpResponseMessage Post()
		{
			var form = HttpContext.Current.Request.Form;
			var note = new JavaScriptSerializer().Deserialize<Note>(form["Note"]);
			var file = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files[0] : null;

			var subUnity = db.SubUnity.FirstOrDefault(p => p.Id == note.SubUnity.Id);
			note.SubUnity = subUnity;

			if (subUnity == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			if (!db.TeacherCourseSubject.Any(p => p.Status && p.Subject_Id == subUnity.Unity.CourseSubject.Subject_Id && p.Course_Id == subUnity.Unity.CourseSubject.Course_Id))
				return Request.CreateResponse(HttpStatusCode.Unauthorized);

            if (file != null && file.ContentLength != 0)
            {
                if (file.ContentType.ToLower() != "application/pdf" &&
                    file.ContentType.ToLower() != "application/vnd.ms-powerpoint" && file.ContentType.ToLower() != "application/mspowerpoint" && file.ContentType.ToLower() != "application/powerpoint" && file.ContentType.ToLower() != "application/x-mspowerpoint" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.presentationml.presentation" &&
                    file.ContentType.ToLower() != "application/vnd.ms-excel" && file.ContentType.ToLower() != "application/excel" && file.ContentType.ToLower() != "application/x-excel" && file.ContentType.ToLower() != "application/x-msexcel" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
                    file.ContentType.ToLower() != "application/msword" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                    return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

                string documentName = Path.GetFileName(file.FileName);
                var path = @"C:\EvaluandomeResources\Notes\" + subUnity.Id;

                Directory.CreateDirectory(path);

                var physicalPath = path + @"\" + Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                file.SaveAs(physicalPath);

                note.DocumentType = file.ContentType.ToLower();
                note.DocumentName = documentName;
                note.DocumentPath = physicalPath;
            }


			db.Note.Add(note);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Profesor")]
		public HttpResponseMessage Put()
		{
			var form = HttpContext.Current.Request.Form;
			var _note = new JavaScriptSerializer().Deserialize<Note>(form["Note"]);
			var file = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files[0] : null;

			var note = db.Note.FirstOrDefault(p => p.Id == _note.Id);
			if (note == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			var subUnity = db.SubUnity.FirstOrDefault(p => p.Id == _note.SubUnity.Id);
			note.SubUnity = subUnity;

			if (subUnity == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			if (!db.TeacherCourseSubject.Any(p => p.Status && p.Subject_Id == subUnity.Unity.CourseSubject.Subject_Id && p.Course_Id == subUnity.Unity.CourseSubject.Course_Id))
				return Request.CreateResponse(HttpStatusCode.Unauthorized);			

			if (file != null && file.ContentLength != 0)
			{
				if (file.ContentType.ToLower() != "application/pdf" &&
					file.ContentType.ToLower() != "application/vnd.ms-powerpoint" && file.ContentType.ToLower() != "application/mspowerpoint" && file.ContentType.ToLower() != "application/powerpoint" && file.ContentType.ToLower() != "application/x-mspowerpoint" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.presentationml.presentation" &&
					file.ContentType.ToLower() != "application/vnd.ms-excel" && file.ContentType.ToLower() != "application/excel" && file.ContentType.ToLower() != "application/x-excel" && file.ContentType.ToLower() != "application/x-msexcel" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" &&
					file.ContentType.ToLower() != "application/msword" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
					return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

				string documentName = Path.GetFileName(file.FileName);
				var path = @"C:\EvaluandomeResources\Notes\" + subUnity.Id;

				Directory.CreateDirectory(path);

				var physicalPath = path + @"\" + Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
				file.SaveAs(physicalPath);

				note.DocumentType = file.ContentType.ToLower();
				note.DocumentName = documentName;
				note.DocumentPath = physicalPath;
			}

			note.Name = _note.Name;
			note.Description = _note.Description;
            note.Link = _note.Link;
            note.Type = _note.Type;
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Profesor")]
		public HttpResponseMessage Delete(int id)
		{
			var item = db.Note.FirstOrDefault(p => p.Id == id);

			if (item == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			db.Note.Remove(item);
			db.SaveChanges();

			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[HttpGet]
		[Authorize(Roles ="Profesor, Alumno")]
		[Route("Api/Note/DownloadFile")]
		public HttpResponseMessage DownloadFile(int note_id)
		{
			var note = db.Note.FirstOrDefault(p => p.Id == note_id);

			if (note == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Apunte no encontrado.");

			try
			{
				var binary = FileHelper.GetBinaryFile(note.DocumentPath);
				return Request.CreateResponse(HttpStatusCode.OK, Convert.ToBase64String(binary, 0, binary.Length));
			}
			catch (Exception)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, "Aún no ha sido subo el apunte.");
			}
		}		
	}
}