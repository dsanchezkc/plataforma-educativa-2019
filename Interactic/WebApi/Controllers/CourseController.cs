﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using System.Net.Http.Headers;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class CourseController : ApiController
    {
        private InteracticContext db;

        public CourseController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = true;
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Course.Where(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year), CourseCloneHelper.CourseMap_2));
        }

        public HttpResponseMessage Get(int id)
        {
            var course = db.Course.FirstOrDefault(e => e.Status && e.Id == id && e.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && e.Year == GlobalParameters.Year);
            if (course != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObject(course, 2));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Curso no encontrado.");
        }

        [AllowAnonymous]
        [Route("Api/Course/GetCoursesByEducationalEstablishment")]
        [HttpGet]
        public HttpResponseMessage GetCoursesByEducationalEstablishment(int educationalEstablishment_id)
        {
            var _courses = db.Course.AsNoTracking().Where(p => p.Status && p.EducationalEstablishment.Id == educationalEstablishment_id);

            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(_courses, 1));
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/Course/GetCoursesBySubject")]
        public HttpResponseMessage GetCoursesBySubject(int id)
        {
            var courses = db.CourseSubject.Where(e => e.Status && e.Subject.Id == id && e.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && e.Course.Year == GlobalParameters.Year).Select(a => a.Course);
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(courses, 1));
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/Course/GetCoursesGlobalTest")]
        public object GetCoursesGlobalTest(int test_id)
        {
            var test = db.Test.AsNoTracking().FirstOrDefault(p => p.Id == test_id);
            var coursestests = test.CourseTests.Where(x => x.Test_Id == test_id).Select(y => y.Course_Id).Distinct().ToList();
            var courses = db.Course.Where(z => coursestests.Contains(z.Id));

            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(courses, 1));
        }

        [Route("Api/Course/GetStudentsByForum")]
        public HttpResponseMessage GetStudentsByForum(int id)
        {
            var forum = db.Forum.FirstOrDefault(p => p.Status && p.Id == id);

            var students = forum.SubUnity.Unity.CourseSubject.Course.StudentCourses.Where(p => p.Status).Select(p => p.User);
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(students, UserCloneHelper.UserMap_3));
        }
        /// <summary>
        /// @Author AP
        /// Entrega un listado de cursos asociado al usuario logeado.
        /// </summary>
        /// <returns></returns>
        [Route("Api/Course/GetCoursesFilteredByUser")]
        public HttpResponseMessage GetCoursesFilteredByUser()
        {
            switch (GlobalParameters.Rol)
            {
                case "Profesor":
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.TeacherCourseSubject.Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year && p.CourseSubject.Course.Status)
                                                                                                                  .Select(p => p.CourseSubject.Course).Distinct(), CourseCloneHelper.CourseMap_1));
                case "Alumno":
                    var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(user.StudentCourses.Where(p => p.Status && p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Course.Year == GlobalParameters.Year).Select(q => q.Course).Distinct(), CourseCloneHelper.CourseMap_1));

                case "Administrador":
                    return Get();

                case "SubAdministrador":
                    return Get();

                default: return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        [Route("Api/Course/GetCoursesFilteredByUserJSON")]
        public HttpResponseMessage GetCoursesFilteredByUserJSON()
        {
            switch (GlobalParameters.Rol)
            {
                case "Profesor":
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.TeacherCourseSubject.Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year && p.CourseSubject.Course.Status)
                                                                                                                  .Select(p => p.CourseSubject.Course).Distinct(), CourseCloneHelper.CourseMap_1), MediaTypeHeaderValue.Parse("application/json"));
                case "Alumno":
                    //return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Course.Where(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year), CourseCloneHelper.CourseMap_2), MediaTypeHeaderValue.Parse("application/json"));
                    var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Course.Where(p => p.Status && p.StudentCourses.Select(x => x.User.Id).Contains(GlobalParameters.UserId) && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year), CourseCloneHelper.CourseMap_2), MediaTypeHeaderValue.Parse("application/json"));
                case "Administrador":
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Course.Where(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year), CourseCloneHelper.CourseMap_2), MediaTypeHeaderValue.Parse("application/json"));

                case "SubAdministrador":
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Course.Where(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year), CourseCloneHelper.CourseMap_2), MediaTypeHeaderValue.Parse("application/json"));
                default: return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        /// <summary>
        /// @Author AP
        /// Entrega un listado de cursos asociado al usuario logeado.
        /// Con la información de sus Asignaturas + SubAsignaturas asociadas. (Información utilizada para representar el Form de Course)
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/Course/GetCourseWithSubjectAndSubSubject")]
        public HttpResponseMessage GetCourseWithSubjectAndSubSubject(int id)
        {
            var course = db.Course.FirstOrDefault(e => e.Status && e.Id == id && e.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && e.Year == GlobalParameters.Year);
            course = CloneHelper.CloneObjectMap(course, CourseCloneHelper.CourseMap_2);
            course.CourseSubjects.RemoveRange(course.CourseSubjects.Where(p => !p.Subject.Status));
            return Request.CreateResponse(HttpStatusCode.OK, course);
        }

        /// <summary>
        /// @Author AP
        /// Entrega un curso con los alumnos asociados a el más sus calificaciones asociadas al periodo.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/Course/GetCourseWithUsersAndQualificationsByCourseAndBaseSubjectAndPeriodDetail")]
        public HttpResponseMessage GetCourseWithUsersAndQualificationsByCourseAndBaseSubjectAndPeriodDetail(int course_id, int basesubject_id, int perioddetail_id)
        {
            var course = db.Course.AsNoTracking().FirstOrDefault(p => p.Status && p.Id == course_id);

            foreach (var studentCourse in course.StudentCourses)
                studentCourse.User.StudentQualifications = studentCourse.User.StudentQualifications.Where(p => p.BaseSubject.Id == basesubject_id && p.PeriodDetail.Id == perioddetail_id).OrderBy(p => p.Order).ToList();

            course.StudentCourses = course.StudentCourses.OrderBy(p => p.ListNumber).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(course, CourseCloneHelper.CourseMap_3));
        }



        [Route("api/course/courseForEducationalEstablishment/count")]
        public IHttpActionResult GetCourseForEducationalEstablishment()
        {
            var count = db.Course.Where(x => x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                        x.Status).ToList().Count;

            return Ok(count);
        }

        /// <summary>
        /// @Author AP
        /// Registra calificaciones de un curso completo.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/Course/RegisterQualifications")]
        public HttpResponseMessage RegisterQualifications([FromBody] Course _course)
        {
            var baseSubject_id = _course.CourseSubjects.FirstOrDefault().Subject_Id;
            var periodDetail_id = _course.EducationalEstablishment.EstablishmentPeriod.PeriodDetails.FirstOrDefault().Id;
            var course_id = _course.Id;
            var course = db.Course.FirstOrDefault(p => p.Id == course_id);
            var periodDetail = db.PeriodDetail.FirstOrDefault(p => p.Id == periodDetail_id);
            var baseSubject = db.BaseSubject.FirstOrDefault(p => p.Id == baseSubject_id);

            //Verificar si es profesor y tiene acceso al curso
            if (GlobalParameters.Rol == "Profesor")
                if (!db.TeacherCourseSubject.Any(p => p.Status && p.User_Id == GlobalParameters.UserId && p.Course_Id == course_id && (p.Subject_Id == baseSubject_id || p.CourseSubject.Subject.SubSubjects.Any(q => q.Id == baseSubject_id))))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);

            //Verificando asignatura en el curso.
            if (!course.CourseSubjects.Any(p => p.Subject_Id == baseSubject_id || p.CourseSubSubjects.Any(q => q.SubSubject_Id == baseSubject_id)))
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            //Verificando periodo establecimiento
            if (!db.EducationalEstablishment.Any(p => p.Id == GlobalParameters.EducationalEstablishmentId && p.EstablishmentPeriod.PeriodDetails.Any(q => q.Id == periodDetail_id)))
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            foreach (var _studentCourse in _course.StudentCourses)
            {
                StudentCourse studentCourse = null;
                //Verificando estudiente con el curso
                if ((studentCourse = course.StudentCourses.FirstOrDefault(p => p.User.UserName == _studentCourse.User.UserName.WithoutFormatRUN())) == null)
                    continue;

                var qualifications = studentCourse.User.StudentQualifications.Where(p => p.PeriodDetail.Id == periodDetail_id && p.BaseSubject.Id == baseSubject_id).ToList();
                var _qualifications = _studentCourse.User.StudentQualifications;

                //Removiendo las calificaciones que fueron eliminadas
                db.StudentQualification.RemoveRange(qualifications.Where(p => !_qualifications.Any(q => q.Order == p.Order)));

                //Agregando o modificando calificaciones
                foreach (var item in _qualifications)
                {
                    var qualification = qualifications.FirstOrDefault(p => p.Order == item.Order);

                    if (qualification != null)
                        qualification.Qualification = item.Qualification;
                    else
                        studentCourse.User.StudentQualifications.Add(new StudentQualification()
                        {
                            Order = item.Order,
                            Qualification = item.Qualification,
                            BaseSubject = baseSubject,
                            PeriodDetail = periodDetail
                        });
                }
            }
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// @Author JI
        /// Carga select utilizado para generacion de informes.
        /// </summary>
        /// <param name="_teachId"></param>
        /// <returns></returns>		
        [Route("api/course/report/select")]
        public IHttpActionResult GetCoursesForTeacher(string _teachId = null)
        {
            //var selectOptions = new List <<anonymous type: int Id, string Name >>();
            var selectOptions = new List<GeneralSelectDto>();

            string teacher = "";
            if (_teachId != null)
                teacher = Encryptor.Decrypt(_teachId);


            if (User.IsInRole(RoleName.Teacher))
            {
                //var teacher = ReportHelper.detectTeacher(_teachId);
                // Si _teachId es null quiere decir que el administrador esta accediendo a obtener informes, por tanto debe regresar la lista todos los cursos asociados al establecimiento
                selectOptions = db.TeacherCourseSubject
                                             .OrderBy(x => x.CourseSubject.Course.Name)
                                             .Where(x => x.User.Id == GlobalParameters.UserId &&
                                                         x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                         x.CourseSubject.Course.Status &&
                                                         x.Status
                                                     )
                                             .Select(x => new GeneralSelectDto
                                             {
                                                 Id = x.CourseSubject.Course.Id.ToString(),
                                                 Name = x.CourseSubject.Course.Name
                                             }
                                             )
                                             .Distinct()
                                             .ToList();
            }
            else
            {
                if (User.IsInRole(RoleName.Administrador) || User.IsInRole(RoleName.SubAdministrador))
                    if (_teachId != null)
                        selectOptions = db.TeacherCourseSubject
                                             .OrderBy(x => x.CourseSubject.Course.Name)
                                             .Where(x => x.User.Id == teacher &&
                                                         x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                         x.CourseSubject.Course.Status &&
                                                         x.Status
                                                     )
                                             .Select(x => new GeneralSelectDto
                                             {
                                                 Id = x.CourseSubject.Course.Id.ToString(),
                                                 Name = x.CourseSubject.Course.Name
                                             }
                                             )
                                             .Distinct()
                                             .ToList();
                    else
                        selectOptions = db.Course.Where(x => x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                 x.Year == GlobalParameters.Year && x.Status)
                                    .Select(x => new GeneralSelectDto { Id = x.Id.ToString(), Name = x.Name })
                                    .ToList();
                else
                    if (User.IsInRole(RoleName.Student))
                {
                    selectOptions = db.StudentCourse
                                             .OrderBy(x => x.Course.Name)
                                             .Where(x => x.User.Id == GlobalParameters.UserId &&
                                                         x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                         x.Course.Status &&
                                                         x.Status

                                                     )
                                             .Select(x => new GeneralSelectDto
                                             {
                                                 Id = x.Course.Id.ToString(),
                                                 Name = x.Course.Name
                                             }
                                             )
                                             .Distinct()
                                             .ToList();
                }
            }

            foreach (var item in selectOptions)
            {
                item.Id = Encryptor.Encrypt(item.Id);
            }
            return Ok(selectOptions);
        }


        /// <summary>
        /// @Author JI
        /// Carga select de cursos por un alumno determinado, es utilizado para generacion de informes.
        /// </summary>
        /// <param name="_teachId"></param>
        /// <returns></returns>		
        [Route("api/course/report/select/students")]
        public IHttpActionResult GetCoursesByStudent(string _studentId = null)
        {
            var selectOptions = new List<GeneralSelectDto>();
            var studentId = Encryptor.Decrypt(_studentId) ?? GlobalParameters.UserId;

            selectOptions = db.StudentCourse.OrderBy(x => x.Course.Name)
                                             .Where(x => x.User.Id == studentId &&
                                                         x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                         x.Course.Status &&
                                                         x.Status)
                                             .Select(x => new GeneralSelectDto
                                             {
                                                 Id = x.Course.Id.ToString(),
                                                 Name = x.Course.Name
                                             }
                                             )
                                             .Distinct()
                                             .ToList();

            foreach (var item in selectOptions)
            {
                item.Id = Encryptor.Encrypt(item.Id);
            }

            return Ok(selectOptions);
        }


        /// <summary>
        /// @Author JI
        /// Carga select utilizado para generacion de informes.
        /// </summary>
        /// <returns></returns>
        [Route("api/coursesForEstablishment/report/select")]
        public IHttpActionResult GetCourses()
        {
            var courses = db.Course.Where(x => x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                x.Year == GlobalParameters.Year && x.Status
                                          )
                                    .Select(x => new { x.Id, x.Name })
                                    .ToList();

            var select = new List<GeneralSelectDto>();
            foreach (var item in courses)
            {
                select.Add(new GeneralSelectDto()
                {
                    Id = Encryptor.Encrypt(item.Id.ToString()),
                    //Id = item.Id.ToString(),
                    Name = item.Name,


                });
            }
            return Ok(select);
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        public HttpResponseMessage Post([FromBody] Course course)
        {
            if (course != null)
            {
                course.EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(p => p.Id == GlobalParameters.EducationalEstablishmentId);
                course.Year = GlobalParameters.Year;
                db.Course.Add(course);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Authorize(Roles = "Administrador, SubAdministrador")]
        [Route("Api/Course/RegisterSubSubjects")]
        public HttpResponseMessage RegisterSubSubjects([FromBody] Course _course)
        {
            var course = db.Course.FirstOrDefault(e => e.Status && e.Id == _course.Id && e.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && e.Year == GlobalParameters.Year);

            if (course != null)
            {
                foreach (var courseSubject in course.CourseSubjects.Where(p => p.Status))
                {
                    var _courseSubject = _course.CourseSubjects.FirstOrDefault(p => _course.Id == courseSubject.Course_Id && p.Subject_Id == courseSubject.Subject_Id);

                    if (_courseSubject != null)
                        foreach (var courseSubSubject in courseSubject.CourseSubSubjects)
                        {
                            var _courseSubSubject = _courseSubject.CourseSubSubjects.FirstOrDefault(p => p.SubSubject_Id == courseSubSubject.SubSubject_Id);
                            courseSubSubject.Status = _courseSubSubject != null;
                        }
                }

                foreach (var _courseSubject in _course.CourseSubjects)
                {
                    var _subSubjectIds = _courseSubject.CourseSubSubjects.Select(p => p.SubSubject_Id);

                    foreach (var subSubjectId in _subSubjectIds)
                    {
                        var item = db.CourseSubSubject.FirstOrDefault(p => p.SubSubject_Id == subSubjectId &&
                                                                      p.Subject_Id == _courseSubject.Subject_Id &&
                                                                      p.Course_Id == _course.Id);

                        if (item != null)
                            item.Status = true;
                        else
                            db.CourseSubSubject.Add(new CourseSubSubject()
                            {
                                CourseSubject = db.CourseSubject.FirstOrDefault(p => p.Course_Id == _course.Id && p.Subject_Id == _courseSubject.Subject_Id),
                                SubSubject = db.SubSubject.FirstOrDefault(p => p.Id == subSubjectId)
                            });
                    }
                }

                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        public HttpResponseMessage Put([FromBody] Course course)
        {
            var _course = db.Course.FirstOrDefault(e => e.Id == course.Id);

            if (_course != null)
            {
                _course.Name = course.Name;

                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Curso no encontrado.");
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        public HttpResponseMessage Delete(int id)
        {
            var course = db.Course.FirstOrDefault(e => e.Id == id);

            if (course != null)
            {
                if (course.CourseSubjects.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Curso contiene asignaturas, eliminelas primero.");
                }
                else
                {
                    db.Course.Remove(course);
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Curso no encontrado.");
        }
    }
}
