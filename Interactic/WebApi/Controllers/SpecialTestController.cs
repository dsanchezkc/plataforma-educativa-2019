﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using WebApi.CloneModels;
using GeneralHelper;
using ViewModels;
using ModelEnum;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class SpecialTestController : ApiController
    {
        private InteracticContext db;

        public SpecialTestController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = true;
        }

        [Authorize(Roles = "Alumno, Administrador")]
        [Route("Api/SpecialTest/GetComplementaryAnswers")]
        [HttpGet]
        public HttpResponseMessage GetComplementaryAnswers(int test_id)
        {
            var studentTest = db.StudentSpecialTest.FirstOrDefault(p => p.SpecialTest.Status && p.SpecialTest_Id == test_id && p.User_Id == GlobalParameters.UserId);

            if (studentTest == null || !studentTest.Done)
                return Request.CreateResponse(HttpStatusCode.Unauthorized, "Acceso denegado a respuestas complementarias.");

            var listAnswer = new List<object>();

            foreach (var question in studentTest.SpecialTest.SpecialQuestions)
            {
                switch (question.QuestionType)
                {
                    case QuestionType.WrittenAnswer:
                        var questionWrittenAnswerItem = ((QuestionWrittenAnswer)question).QuestionWrittenAnswerItem;

                        if (questionWrittenAnswerItem == null || questionWrittenAnswerItem.Answer == null || questionWrittenAnswerItem.Answer.Trim() == "")
                            break;

                        listAnswer.Add(new { Id = questionWrittenAnswerItem.Id, Type = question.QuestionType, Answer = questionWrittenAnswerItem.Answer });
                        break;

                    case QuestionType.Alternatives:
                    case QuestionType.PairedWords:
                    case QuestionType.TrueFalse:
                        if (question.ComplementaryAnswer == null || question.ComplementaryAnswer.Trim() == "")
                            break;

                        listAnswer.Add(new { Id = question.Id, Type = question.QuestionType, Answer = question.ComplementaryAnswer });
                        break;
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK, listAnswer);
        }

        [Route("Api/SpecialTest/GetSpecialGlobalTestsBySubject")]
        public object GetSpecialGlobalTestsBySubject(int id)
        {
            var tests = db.SpecialTest.Where(p => p.Status && p.CourseSubject.Subject_Id == id && p.Global == true);

            if (tests != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(tests, SpecialTestCloneHelper.TestMap_1));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        }


        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/SpecialTest/GetAnswerTestsBySubUnityAndUser")]
        public HttpResponseMessage GetAnswerTestsBySubUnityAndUser(int subunity_id, string user_id)
        {
            IQueryable<CourseSubject> courseSubjects = null;
            var subUnity = db.SubUnity.FirstOrDefault(p => p.Status && p.Id == subunity_id);
            var user = db.Users.FirstOrDefault(p => p.Status && p.Id == user_id);

            if (subUnity == null || user == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            switch (GlobalParameters.Rol)
            {
                case "Profesor":
                    courseSubjects = db.TeacherCourseSubject.AsNoTracking().Where(p =>
                        p.Status &&
                        p.User.Id == GlobalParameters.UserId &&
                        p.CourseSubject.Course_Id == subUnity.Unity.CourseSubject.Course_Id &&
                        p.CourseSubject.Subject_Id == subUnity.Unity.CourseSubject.Subject_Id &&
                        p.CourseSubject.Course.Year == GlobalParameters.Year).Select(p => p.CourseSubject);
                    break;

                case "Administrador":
                case "SubAdministrador":
                    courseSubjects = db.CourseSubject.AsNoTracking().Where(p =>
                        p.Status &&
                        p.Course_Id == subUnity.Unity.CourseSubject.Course_Id &&
                        p.Subject_Id == subUnity.Unity.CourseSubject.Subject_Id &&
                        p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                        p.Course.Year == GlobalParameters.Year);
                    break;
            }

            var studentAnswers = new List<StudentAnswer>();

            foreach (var courseSubject in courseSubjects)
                foreach (var studentAnswer in db.StudentAnswer.AsNoTracking().Where(p => p.Status &&
                p.QuestionItem.QuestionType == QuestionType.WrittenAnswer &&
                p.StudentSpecialTest.SpecialTest.CourseSubject.Course_Id == courseSubject.Course_Id &&
                p.StudentSpecialTest.SpecialTest.CourseSubject.Subject_Id == courseSubject.Subject_Id &&
                p.StudentSpecialTest.SpecialTest.Status))
                {
                    studentAnswers.Add(studentAnswer);
                }
            return GetPendingTestResponseViewModel(studentAnswers.Where(p => p.StudentSpecialTest.User_Id == user_id ));
        }


        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/SpecialTest/GetPendingTests")]
        public HttpResponseMessage GetPendingTests()
        {
            return GetPendingTestResponseViewModel(_GetPendingTests());
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/SpecialTest/GetPendingTestsCount")]
        public HttpResponseMessage GetPendingTestsCount()
        {
            return Request.CreateResponse(HttpStatusCode.OK, _GetPendingTests().Count());
        }

        private List<StudentAnswer> _GetPendingTests()
        {
            IQueryable<CourseSubject> courseSubjects = null;

            switch (GlobalParameters.Rol)
            {
                case "Profesor":
                    courseSubjects = db.TeacherCourseSubject.AsNoTracking().Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year).Select(p => p.CourseSubject);
                    break;

                case "Administrador":
                case "SubAdministrador":
                    courseSubjects = db.CourseSubject.AsNoTracking().Where(p => p.Status && p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Course.Year == GlobalParameters.Year);
                    break;
            }

            var studentAnswers = new List<StudentAnswer>();

            foreach (var courseSubject in courseSubjects)
            {
                foreach (var studentAnswer in db.StudentAnswer.AsNoTracking().Where(p => p.Status &&
                                                                                p.QuestionItem.QuestionType == QuestionType.WrittenAnswer && p.Qualification <= 0 &&
                                                                                p.StudentSpecialTest.SpecialTest.CourseSubject.Course_Id == courseSubject.Course_Id &&
                                                                                p.StudentSpecialTest.SpecialTest.CourseSubject.Subject_Id == courseSubject.Subject_Id &&
                                                                                p.StudentSpecialTest.SpecialTest.Status))
                {
                    studentAnswers.Add(studentAnswer);
                }
            }

            return studentAnswers;
        }

        private HttpResponseMessage GetPendingTestResponseViewModel(IEnumerable<StudentAnswer> studentAnswers)
        {
            var pendingTestRVMs = new List<PendingTestResponseViewModel>();

            ///La carga de información es extremadamente grande. Se usan los parametros del header para enviar solamente 
            ///una parte de todo.
            foreach (var studentAnswer in studentAnswers.Skip(GlobalParameters.ResultPag * GlobalParameters.ResultLimit).Take(GlobalParameters.ResultLimit))
            {
                PendingTestResponseViewModel pendingTestResponseViewModel = null;
                pendingTestRVMs.Add(pendingTestResponseViewModel = new PendingTestResponseViewModel()
                {
                    SubUnity = studentAnswer.StudentSpecialTest.SpecialTest.SubUnity,
                    CourseSubject = studentAnswer.StudentSpecialTest.SpecialTest.CourseSubject,
                    QuestionWrittenAnswer = (studentAnswer.QuestionItem as QuestionWrittenAnswerItem)?.QuestionWrittenAnswer,
                    StudentAnswer = studentAnswer
                });

                //Eliminando Pregunta - Puede contener un peso demasiado elevado para enviar.
                //Solamente enviar cuando se carga en un formulario directo.
                if (pendingTestResponseViewModel.QuestionWrittenAnswer != null)
                    pendingTestResponseViewModel.QuestionWrittenAnswer.Name = "";

            }

            ///Enviamos la nueva información.
            ResponseParameters.ResultLimit = GlobalParameters.ResultLimit;
            ResponseParameters.ResultTotal = studentAnswers.Count();
            ResponseParameters.ResultPag = GlobalParameters.ResultPag;

            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(pendingTestRVMs, PendingTestResponseViewModelCloneHelper.PendingTestResponseViewModelMap_1));
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/SpecialTest/RegisterQualification")]
        [HttpPost]
        public HttpResponseMessage RegisterQualification([FromBody] StudentAnswer _studentAnswer)
        {

            var studentAnswer = db.StudentAnswer.FirstOrDefault(p => p.QuestionItem_Id == _studentAnswer.QuestionItem_Id && p.User_Id == _studentAnswer.User_Id);

            if (studentAnswer == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            //Validando acceso
            switch (GlobalParameters.Rol)
            {
                case "Profesor":
                    if (!db.TeacherCourseSubject.AsNoTracking().Any(p =>
                        p.Status &&
                        p.CourseSubject.Course.Year == GlobalParameters.Year &&
                        p.User.Id == GlobalParameters.UserId &&
                        p.Course_Id == studentAnswer.StudentSpecialTest.SpecialTest.CourseSubject.Course_Id &&
                        p.Subject_Id == studentAnswer.StudentSpecialTest.SpecialTest.CourseSubject.Subject_Id))
                        return Request.CreateResponse(HttpStatusCode.Unauthorized);
                    break;

                case "SubAdministrador":
                    if (!db.CourseSubject.AsNoTracking().Any(p =>
                         p.Status &&
                         p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                         p.Course.Year == GlobalParameters.Year &&
                         p.Course_Id == studentAnswer.StudentSpecialTest.SpecialTest.CourseSubject.Course_Id &&
                         p.Subject_Id == studentAnswer.StudentSpecialTest.SpecialTest.CourseSubject.Subject_Id))
                        return Request.CreateResponse(HttpStatusCode.Unauthorized);
                    break;
            }

            if (_studentAnswer.Qualification < 0)
                _studentAnswer.Qualification = 0;

            if (_studentAnswer.Qualification > 100)
                _studentAnswer.Qualification = 100;

            studentAnswer.Qualification = _studentAnswer.Qualification;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Alumno, Administrador")]
        [HttpPost]
        [Route("Api/SpecialTest/RegisterStudentQuestions")]
        public HttpResponseMessage RegisterStudentQuestions([FromBody] TestAnswersRequestViewModel testARVM)
        {
            var studentTest = db.StudentSpecialTest.FirstOrDefault(p => p.SpecialTest.Status && p.SpecialTest_Id == testARVM.Test_Id && p.User_Id == GlobalParameters.UserId);

            if (studentTest == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Test no asociado.");

            var test = studentTest.SpecialTest;
            var user = studentTest.User;

            if (test.Timer != 0 && DateTime.Now > studentTest.Start.AddMinutes(test.Timer + 1))
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Tiempo terminado. No es posible registrar.");

            db.StudentAnswer.RemoveRange(db.StudentAnswer.Where(p => p.StudentSpecialTest.User_Id == studentTest.User_Id && p.StudentSpecialTest.SpecialTest_Id == studentTest.SpecialTest_Id));

            try
            {
                //Preguntas de desarrollo
                foreach (var qwai in testARVM.QuestionWrittenAnswerItems)
                {
                    studentTest.StudentAnswers.Add(new StudentAnswer()
                    {
                        QuestionItem = db.QuestionWrittenAnswerItem.FirstOrDefault(p => p.QuestionWrittenAnswer.SpecialTest.Id == test.Id && p.Id == qwai.Id),
                        Answer = qwai.Answer,
                    });
                }

                //Preguntas de alternativa
                foreach (var qai in testARVM.QuestionAlternativeItems)
                {
                    studentTest.StudentAnswers.Add(new StudentAnswer()
                    {
                        QuestionItem = db.QuestionAlternativeItem.FirstOrDefault(p => p.QuestionAlternative.SpecialTest.Id == test.Id && p.Id == qai.Id),
                        Answer = qai.Value.ToString()
                    });
                }

                //Preguntas de terminos pariados
                foreach (var qpwi in testARVM.QuestionPairedWordItems)
                {
                    studentTest.StudentAnswers.Add(new StudentAnswer()
                    {
                        QuestionItem = db.QuestionPairedWordItem.FirstOrDefault(p => p.QuestionPairedWord.SpecialTest.Id == test.Id && p.Id == qpwi.Id),
                        Answer = qpwi.Column_1_Alternative.ToString()
                    });
                }

                //Preguntas de verdadero/falso
                foreach (var qtfi in testARVM.QuestionTrueFalseItems)
                {
                    studentTest.StudentAnswers.Add(new StudentAnswer()
                    {
                        QuestionItem = db.QuestionTrueFalseItem.FirstOrDefault(p => p.QuestionTrueFalse.SpecialTest.Id == test.Id && p.Id == qtfi.Id),
                        Answer = qtfi.Value.ToString()
                    });
                }

                studentTest.Done = true;
                studentTest.Attempts++;
                db.SaveChanges();
            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "El test no ha podido ser procesado.");
            }

            return Request.CreateResponse(HttpStatusCode.OK, GetQualification(studentTest));
        }


        /// <summary>
        /// La nota puede ser como mínimo un 1 y como máximo un 100.
        /// -1 indica que no se puede calcular la nota(Por ejemplo el test tenga preguntas con desarrollo).
        /// -2 indica que se ha intentado revisar las respuestas de desarrollo pero aún no han sido evaluadas por el profesor.
        /// </summary>
        public static double GetQualification(StudentSpecialTest studentTest, bool includeWrittenAnswer = false)
        {
            var test = studentTest.SpecialTest;
            var studentAnswer = studentTest.StudentAnswers;

            if (!includeWrittenAnswer)
                if (test.SpecialQuestions.Any(p => p.Status && p.QuestionType == QuestionType.WrittenAnswer))
                    return -1;

            var count = 0.0;
            var countQuestionItem = 0;

            foreach (var question in test.SpecialQuestions.Where(p => p.Status))
            {
                switch (question.QuestionType)
                {
                    case QuestionType.TrueFalse:
                        var questionTrueFalseItems = (question as QuestionTrueFalse).QuestionTrueFalseItems.Where(p => p.Status);
                        countQuestionItem += questionTrueFalseItems.Count();

                        foreach (var questionItem in questionTrueFalseItems)
                        {
                            var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
                            if (item == null) continue;
                            count += questionItem.Value.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
                        }
                        break;

                    case QuestionType.Alternatives:
                        var questionAlternativeItems = (question as QuestionAlternative).QuestionAlternativeItems.Where(p => p.Status);
                        var questionAlternativeItem = questionAlternativeItems.FirstOrDefault(p => p.Value);
                        if (questionAlternativeItem == null) continue;
                        count += studentAnswer.Any(p => p.QuestionItem_Id == questionAlternativeItem.Id) ? 1 : 0;
                        countQuestionItem += 1;
                        break;

                    case QuestionType.PairedWords:
                        var questionPairedWordItems = (question as QuestionPairedWord).QuestionPairedWordItems.Where(p => p.Status);
                        countQuestionItem += questionPairedWordItems.Count();

                        foreach (var questionItem in questionPairedWordItems)
                        {
                            var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
                            if (item == null) continue;
                            count += questionItem.Column_1_Alternative.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
                        }
                        break;

                    case QuestionType.WrittenAnswer:
                        var writtenAnswerItem = (question as QuestionWrittenAnswer).QuestionWrittenAnswerItem;
                        var qualification = studentAnswer.FirstOrDefault(p => p.Status && p.QuestionItem_Id == writtenAnswerItem.Id)?.Qualification;
                        countQuestionItem++;

                        if (qualification.HasValue && qualification.Value == 0)
                            return -2;

                        if (!qualification.HasValue)
                            continue;

                        count += (double)qualification.Value / 100.0;
                        break;
                }
            }

            if (countQuestionItem == 0)
                return 0;

            return Math.Round((((double)count / (double)countQuestionItem) * 99) + 1, 1);
        }

        public static QualificationInfoViewModel GetQualificationWithInfo(StudentSpecialTest studentTest)
        {
            var qualificationInfo = new QualificationInfoViewModel();

            if (studentTest == null)
                return qualificationInfo;

            var test = studentTest.SpecialTest;
            var studentAnswer = studentTest.StudentAnswers;
            var value = 0.0;

            foreach (var question in test.SpecialQuestions.Where(p => p.Status))
            {
                switch (question.QuestionType)
                {
                    case QuestionType.TrueFalse:
                        var questionTrueFalseItems = (question as QuestionTrueFalse).QuestionTrueFalseItems.Where(p => p.Status);

                        foreach (var questionItem in questionTrueFalseItems)
                        {
                            var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
                            if (item == null)
                            {
                                qualificationInfo.TrueFalse.AddQualification(0, false);
                                continue;
                            }
                            value = questionItem.Value.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
                            qualificationInfo.TrueFalse.AddQualification(value, value == 1);
                        }
                        break;

                    case QuestionType.Alternatives:
                        var questionAlternativeItems = (question as QuestionAlternative).QuestionAlternativeItems.Where(p => p.Status);
                        var questionAlternativeItem = questionAlternativeItems.FirstOrDefault(p => p.Value);
                        if (questionAlternativeItem == null)
                        {
                            qualificationInfo.Alternatives.AddQualification(0, false);
                            continue;
                        }
                        value = studentAnswer.Any(p => p.QuestionItem_Id == questionAlternativeItem.Id) ? 1 : 0;
                        qualificationInfo.Alternatives.AddQualification(value, value == 1);
                        break;

                    case QuestionType.PairedWords:
                        var questionPairedWordItems = (question as QuestionPairedWord).QuestionPairedWordItems.Where(p => p.Status);

                        foreach (var questionItem in questionPairedWordItems)
                        {
                            var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
                            if (item == null)
                            {
                                qualificationInfo.PairedWords.AddQualification(0, false);
                                continue;
                            }
                            value = questionItem.Column_1_Alternative.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
                            qualificationInfo.PairedWords.AddQualification(value, value == 1);
                        }
                        break;

                    case QuestionType.WrittenAnswer:
                        var writtenAnswerItem = (question as QuestionWrittenAnswer).QuestionWrittenAnswerItem;
                        var qualification = studentAnswer.FirstOrDefault(p => p.Status && p.QuestionItem_Id == writtenAnswerItem.Id)?.Qualification;

                        if (qualification.HasValue && qualification.Value == 0)
                        {
                            qualificationInfo.IsWrittenAnswerPending = true;
                            continue;
                        }

                        if (!qualification.HasValue)
                        {
                            qualificationInfo.WrittenAnswer.AddQualification(0);
                            continue;
                        }

                        value = (double)qualification.Value / 100.0;
                        qualificationInfo.WrittenAnswer.AddQualification(value);
                        break;
                }
            }

            return qualificationInfo;
        }


        /// <summary>
        /// Valida si es posible manipular el test
        /// </summary>
        /// <returns></returns>
        private bool ValidateTest(SpecialTest test)
        {
            if (test == null)
                return false;
            if (test.Global == true) {
                return true;
            }
            var course = test.SubUnity.Unity.CourseSubject.Course;

            //Verificamos si el usuario que está pidiendo el Test tiene acceso a el
            IQueryable<Course> courses_currentUser = null;
            switch (GlobalParameters.Rol)
            {
                case "Administrador":
                case "SubAdministrador":
                    courses_currentUser = db.Course.AsNoTracking().Where(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year);
                    break;

                case "Profesor":
                    courses_currentUser = db.TeacherCourseSubject.AsNoTracking().Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year).Select(p => p.CourseSubject.Course).Distinct();
                    break;

                case "Alumno":
                    var user = db.Users.AsNoTracking().FirstOrDefault(p => p.Id == GlobalParameters.UserId);
                    courses_currentUser = user.StudentCourses.Where(p => p.Status && p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Course.Year == GlobalParameters.Year).Select(q => q.Course).Distinct().AsQueryable();
                    break;
            }

            if (courses_currentUser == null)
                return false;

            if (!courses_currentUser.Any(p => p.Id == course.Id))
                return false;

            return true;
        }

        [HttpGet, Route("Api/SpecialTest/GetTest_Edit")]
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage GetTest_Edit(int id)
        {
            var test = db.SpecialTest.FirstOrDefault(p => p.Status && p.Id == id);

            if (!ValidateTest(test))
                return Request.CreateResponse(HttpStatusCode.Unauthorized);


            var _test = CloneHelper.CloneObject(test) as SpecialTest;


            if (_test == null)
                return Request.CreateResponse(HttpStatusCode.InternalServerError);

            var sqst = db.SpecialQuestionSpecialTest.Where(e => e.SpecialTest_Id == test.Id);

            var qwa = new List<QuestionWrittenAnswer>();
            var qtf = new List<QuestionTrueFalse>();
            var qpw = new List<QuestionPairedWord>();
            var qa = new List<QuestionAlternative>();

            foreach (var item in sqst)
            {
                var sq = db.SpecialQuestion.FirstOrDefault(e => e.Id == item.SpecialQuestion_Id);
                if (sq.QuestionType == QuestionType.Alternatives) qa.Add(sq as QuestionAlternative);
                if (sq.QuestionType == QuestionType.PairedWords) qpw.Add(sq as QuestionPairedWord);
                if (sq.QuestionType == QuestionType.TrueFalse) qtf.Add(sq as QuestionTrueFalse);
                if (sq.QuestionType == QuestionType.WrittenAnswer) qwa.Add(sq as QuestionWrittenAnswer);
            }

            var questionWrittenAnswers = CloneHelper.CloneListMap(qwa, QuestionWrittenAnswerCloneHelper.QuestionWrittenAnswerMap_1);
            _test.SpecialQuestions.AddRange(questionWrittenAnswers);

            var questionTrueFalses = CloneHelper.CloneListMap(qtf, QuestionTrueFalseCloneHelper.QuestionTrueFalseMap_1);
            _test.SpecialQuestions.AddRange(questionTrueFalses);

            var questionPairedWords = CloneHelper.CloneListMap(qpw, QuestionPairedWordCloneHelper.QuestionPairedWordMap_1);
            foreach (var item in questionPairedWords)
                item.QuestionPairedWordItems = item.QuestionPairedWordItems.OrderBy(p => p.Column_2_Index).ToList();
            _test.SpecialQuestions.AddRange(questionPairedWords);

            var questionAlternatives = CloneHelper.CloneListMap(qa, QuestionAlternativeCloneHelper.QuestionAlternativeMap_1);
            _test.SpecialQuestions.AddRange(questionAlternatives);

            _test.SpecialQuestions = _test.SpecialQuestions.OrderBy(p => p.Order).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, _test);
        }

        [HttpGet, Route("Api/SpecialTest/StudentTest_Todo")]
        [Authorize(Roles = "Alumno")]
        public HttpResponseMessage StudentTest_Todo(int id)
        {
            var test = db.SpecialTest.FirstOrDefault(p => p.Status && p.Id == id);

            var _test = CloneHelper.CloneObject(test) as SpecialTest;


            if (_test == null)
                return Request.CreateResponse(HttpStatusCode.InternalServerError);

            var sqst = db.SpecialQuestionSpecialTest.Where(e => e.SpecialTest_Id == test.Id);

            var qwa = new List<QuestionWrittenAnswer>();
            var qtf = new List<QuestionTrueFalse>();
            var qpw = new List<QuestionPairedWord>();
            var qa = new List<QuestionAlternative>();

            foreach (var item in sqst)
            {
                var sq = db.SpecialQuestion.FirstOrDefault(e => e.Id == item.SpecialQuestion_Id);
                if (sq.QuestionType == QuestionType.Alternatives) qa.Add(sq as QuestionAlternative);
                if (sq.QuestionType == QuestionType.PairedWords) qpw.Add(sq as QuestionPairedWord);
                if (sq.QuestionType == QuestionType.TrueFalse) qtf.Add(sq as QuestionTrueFalse);
                if (sq.QuestionType == QuestionType.WrittenAnswer) qwa.Add(sq as QuestionWrittenAnswer);
            }

            var questionWrittenAnswers = CloneHelper.CloneListMap(qwa, QuestionWrittenAnswerCloneHelper.QuestionWrittenAnswerMap_1);
            _test.SpecialQuestions.AddRange(questionWrittenAnswers);

            var questionTrueFalses = CloneHelper.CloneListMap(qtf, QuestionTrueFalseCloneHelper.QuestionTrueFalseMap_1);
            _test.SpecialQuestions.AddRange(questionTrueFalses);

            var questionPairedWords = CloneHelper.CloneListMap(qpw, QuestionPairedWordCloneHelper.QuestionPairedWordMap_1);
            foreach (var item in questionPairedWords)
                item.QuestionPairedWordItems = item.QuestionPairedWordItems.OrderBy(p => p.Column_2_Index).ToList();
            _test.SpecialQuestions.AddRange(questionPairedWords);

            var questionAlternatives = CloneHelper.CloneListMap(qa, QuestionAlternativeCloneHelper.QuestionAlternativeMap_1);
            _test.SpecialQuestions.AddRange(questionAlternatives);

            _test.SpecialQuestions = _test.SpecialQuestions.OrderBy(p => p.Order).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, _test);
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
        public HttpResponseMessage Get(int id)
        {
            var test = db.SpecialTest.FirstOrDefault(p => p.Status && p.Id == id);

            if (!ValidateTest(test))
                return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Acceso denegado" });

            //Iniciando la evaluación
            var studentTest = db.StudentSpecialTest.FirstOrDefault(p => p.User_Id == GlobalParameters.UserId && p.SpecialTest_Id == test.Id);
            if (studentTest == null && (GlobalParameters.Rol == "Alumno" || GlobalParameters.Rol == "Administrador")) {
                db.StudentSpecialTest.Add(studentTest = new StudentSpecialTest()
                    {
                    User = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId),
                    SpecialTest = test,
                    Start = DateTime.Now,
                    Attempts = 0
                });
                db.SaveChanges();
            }
                
            object info = new { CurrentTime = test.Timer * 60, AttemptsAvailable = 0 };

            if (studentTest != null && (GlobalParameters.Rol == "Alumno" || GlobalParameters.Rol == "Administrador"))
            {
                if (test.Attempts <= studentTest.Attempts)
                    return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });

                var timer = test.Timer * 60 - (DateTime.Now - studentTest.Start).TotalSeconds;
                if ((timer <= 0 && test.Timer > 0) || studentTest.Done)
                {
                    studentTest.Attempts++;
                    studentTest.Start = DateTime.Now;
                    timer = test.Timer * 60 - (DateTime.Now - studentTest.Start).TotalSeconds;
                }

                if (test.Attempts <= studentTest.Attempts)
                {
                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });
                }

                studentTest.Done = false;
                db.SaveChanges();

                info = new { CurrentTime = timer < 0 ? 0 : timer, AttemptsAvailable = test.Attempts - studentTest.Attempts <= 0 ? 0 : test.Attempts - studentTest.Attempts };
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { Error = "", Info = info, Test = SpecialTestCloneHelper.GetTestWithoutAnswers(test) });
        }

        [Authorize(Roles = "Alumno")]
        [Route("Api/SpecialTest/GetInfo")]
        public HttpResponseMessage GetInfo(int id)
        {
            var test = db.SpecialTest.FirstOrDefault(p => p.Status && p.Id == id);

            if (!ValidateTest(test))
                return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Acceso denegado" });

            var studentTest = db.StudentSpecialTest.FirstOrDefault(p => p.User_Id == GlobalParameters.UserId && p.SpecialTest_Id == test.Id);

            var timer = 0.0;

            if (studentTest != null)
            {
                if (test.Attempts <= studentTest.Attempts)
                    return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });

                timer = test.Timer * 60 - (DateTime.Now - studentTest.Start).TotalSeconds;

                if (studentTest.Done)
                    timer = -1;

                if (studentTest.Done && studentTest.Attempts + 1 >= test.Attempts)
                    return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });

                if (timer <= 0 && test.Timer > 0)
                    studentTest.Attempts++;

                if (test.Timer == 0 && studentTest.Done)
                    studentTest.Attempts++;

                if (test.Attempts <= studentTest.Attempts)
                    return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });
            }

            return Request.CreateResponse(HttpStatusCode.OK,
                new
                {
                    Error = "",
                    Info = new
                    {
                        Timer = test.Timer * 60,
                        CurrentAttempts = studentTest != null ? test.Attempts - studentTest.Attempts : test.Attempts,
                        Attempts = test.Attempts,
                        CurrentTimer = timer,
                        IsFirtTime = studentTest == null
                    }
                });
        }


        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Post([FromBody] TestRequestViewModel testRVM)
        {
            var questionsToAdd = new List<int>();
            var subunity = db.SubUnity.FirstOrDefault();
            var subject = db.Subject.FirstOrDefault();
            if (testRVM.SubUnity_Id != null)
            {
                var s_id = (int)(int.Parse(testRVM.SubUnity_Id));
                subunity = db.SubUnity.FirstOrDefault(p => p.Id == s_id);
            }

            if (testRVM.Subject_Id != null)
            {
                var subject_id = (int)(int.Parse(testRVM.Subject_Id));
                subject = db.Subject.FirstOrDefault(p => p.Id == subject_id);
            }

            var test = new SpecialTest()
            {
                Title = testRVM.SpecialTest.Title,
                Timer = testRVM.SpecialTest.Timer < 0 ? 0 : testRVM.SpecialTest.Timer,
                Attempts = testRVM.SpecialTest.Attempts < 1 ? 1 : testRVM.SpecialTest.Attempts,
                IsTestReport = testRVM.SpecialTest.IsTestReport,
                CourseSubject = testRVM.Subject_Id != null ? subject.CourseSubjects.FirstOrDefault(x => x.Subject_Id == (int)(int.Parse(testRVM.Subject_Id)) && x.Course_Id == (int)(int.Parse(testRVM.Course_Id))) : subunity.Unity.CourseSubject,
                SubUnity = testRVM.SubUnity_Id != null ? subunity : null,
                Global = testRVM.Global,
                Random = testRVM.Random,
                SpecialQuestions = new List<SpecialQuestion>()
            };
            if (testRVM.Random == true)
            {
                db.SpecialTest.Add(test);
                db.SaveChanges();

                foreach (var item in testRVM.RandomQuestionSet)
                {
                    var numeros = new List<int>();
                    var questions = db.SpecialQuestion.Where(e=> e.Status && e.SubUnity_Id == item.Subunity_Id).ToArray();
                    while (numeros.ToArray().Length < item.Count) { 
                        int numeroAleatorio = new Random().Next(0, questions.Length);
                        //Sólo si el número generado no existe en lalista se agrega
                        if (!numeros.Contains(numeroAleatorio))
                        {
                            numeros.Add(numeroAleatorio);
                            db.SpecialQuestionSpecialTest.Add(new SpecialQuestionSpecialTest()
                            {
                                SpecialQuestion_Id = questions.ElementAt(numeroAleatorio).Id,
                                SpecialTest_Id = test.Id
                            });
                            test.SpecialQuestions.Add(questions.ElementAt(numeroAleatorio));
                            db.SaveChanges();
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, test.Id);
            }
            else {
                if (testRVM.QuestionWrittenAnswers.Count == 0 && testRVM.QuestionTrueFalses.Count == 0 &&
                    testRVM.QuestionPairedWords.Count == 0 && testRVM.QuestionAlternatives.Count == 0)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Al menos debe existir una pregunta");

                //Registrando Respuesta Escrita
                AssociateQuestion_WrittenAnswer(testRVM.QuestionWrittenAnswers, test, questionsToAdd);

                //Registrando Verdadero/ Falso
                AssociateQuestion_TrueFalse(testRVM.QuestionTrueFalses, test, questionsToAdd);

                //Registrando Terminos pariados
                AssociateQuestion_QuestionPairedWords(testRVM.QuestionPairedWords, test, questionsToAdd);

                //Registrando Alternativas
                AssociateQuestion_Alternative(testRVM.QuestionAlternatives, test, questionsToAdd);

                db.SpecialTest.Add(test);
                db.SaveChanges();

                foreach (var item in test.SpecialQuestions)
                {
                    db.SpecialQuestionSpecialTest.Add(new SpecialQuestionSpecialTest()
                    {
                        SpecialQuestion_Id = item.Id,
                        SpecialTest_Id = test.Id
                    });
                    db.SaveChanges();
                }

                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, test.Id);
            }
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Put([FromBody] TestRequestViewModel testRVM)
        {
            var questionsToAdd = new List<int>();
            var test = db.SpecialTest.FirstOrDefault(p => p.Id == testRVM.SpecialTest.Id);

            if (test == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest);

            test.Title = testRVM.SpecialTest.Title;
            test.Timer = testRVM.SpecialTest.Timer < 0 ? 0 : testRVM.SpecialTest.Timer;
            test.Attempts = testRVM.SpecialTest.Attempts < 1 ? 1 : testRVM.SpecialTest.Attempts;
            test.IsTestReport = testRVM.SpecialTest.IsTestReport;

            //Verificando por si ya existe otro informe con Nota Final
            /*
            if (test.IsTestReport)
            {
                foreach (var _action in test.SubUnity.Activities.Where(p => p.Status).SelectMany(p => p.Actions.Where(q => q.Status && q.Type == ((int)ActionType.Test) && q.Task_Id != test.Id)))
                {
                    var _test = db.SpecialTest.AsNoTracking().FirstOrDefault(p => p.Status && p.IsTestReport);

                    if (_test != null)
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "El Test " + _test.Title + ", ya está registrado como Test Final, solamente puede existir un test final por Sub-Unidad.");
                }
            }
            */

            if (testRVM.QuestionWrittenAnswers.Count == 0 && testRVM.QuestionTrueFalses.Count == 0 &&
                testRVM.QuestionPairedWords.Count == 0 && testRVM.QuestionAlternatives.Count == 0)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Al menos debe existir una pregunta");

            //Registrando Respuesta Escrita
            AssociateQuestion_WrittenAnswer(testRVM.QuestionWrittenAnswers, test, questionsToAdd);

            //Registrando Verdadero/Falso
            AssociateQuestion_TrueFalse(testRVM.QuestionTrueFalses, test, questionsToAdd);

            //Registrando Terminos pariados
            AssociateQuestion_QuestionPairedWords(testRVM.QuestionPairedWords, test, questionsToAdd);

            //Registrando Alternativas
            AssociateQuestion_Alternative(testRVM.QuestionAlternatives, test, questionsToAdd);

            db.SaveChanges();

            var questions = db.SpecialQuestionSpecialTest.Where(e => e.Status && e.SpecialTest_Id == test.Id);
            foreach (var item in questions)
            {
                db.SpecialQuestionSpecialTest.Remove(item);
            }

            foreach (var item in test.SpecialQuestions)
            {
                db.SpecialQuestionSpecialTest.Add(new SpecialQuestionSpecialTest()
                {
                    SpecialQuestion_Id = item.Id,
                    SpecialTest_Id = test.Id
                });
                db.SaveChanges();
            }

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
		/// Obtiene la lista de Test relacionado al SubUnity
		/// </summary>
		[Route("Api/SpecialTest/GetSpecialTestsBySubUnity")]
        public object GetSpecialTestsBySubUnity(int id)
        {
            var tests = db.SpecialTest.Where(p => p.Status && p.SubUnity.Id == id);

            if (tests != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(tests, TestCloneHelper.TestMap_1));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Delete(int id)
        {
            var test = db.SpecialTest.FirstOrDefault(e => e.Id == id);

            if (test != null)
            {
                test.Status = false;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        }

        /// <summary>
        /// Author AP
        /// Entrega el Test asociado a la lista de usuarios con el curso respectivo al Test.
        /// StudentTest no se registra en ningun lado automáticamente, es por eso que se debe consultar el listado 
        /// de usuarios asociados al Curso para poder generar la lista, posteriormente esa lista es registrada en otro proceso.
        /// </summary>
        [Route("Api/SpecialTest/GetTestAssociateStudentTests")]
        public HttpResponseMessage GetTestAssociateStudentTests(int test_id)
        {
            var test = db.SpecialTest.AsNoTracking().FirstOrDefault(p => p.Id == test_id);
            test.StudentSpecialTests.Clear();

            var course = test.SubUnity.Unity.CourseSubject.Course;
            var studentCourses = CloneHelper.CloneListMap(db.StudentCourse.Where(p => p.Course_Id == course.Id).OrderBy(p => p.ListNumber), StudentCourseCloneHelper.StudentCourseMap_1);

            foreach (var studentCourse in studentCourses)
            {
                studentCourse.User.StudentCourses.Clear();
                studentCourse.User.StudentCourses.Add(studentCourse);

                var studentTest = db.StudentSpecialTest.AsNoTracking().FirstOrDefault(p => p.SpecialTest.Id == test_id && p.User.Id == studentCourse.User_Id);

                if (studentTest == null)
                    test.StudentSpecialTests.Add(new StudentSpecialTest());
                else
                    test.StudentSpecialTests.Add(studentTest);

                test.StudentSpecialTests.Last().User = studentCourse.User;
            }



            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(test, SpecialTestCloneHelper.TestMap_3));
        }

		[Authorize(Roles = "Profesor, Administrador, SubAdministrador")]
        [HttpPost, Route("Api/SpecialTest/RegisterConfigurationTestAndUsers")]
        public HttpResponseMessage RegisterConfigurationTestAndUsers([FromBody] SpecialTest _test)
        {
            var test = db.SpecialTest.FirstOrDefault(p => p.Id == _test.Id);
            test.Timer = (_test.Timer <= 0) ? 0 : _test.Timer;

            foreach (var _studentTest in _test.StudentSpecialTests)
            {
                var studentTest = db.StudentSpecialTest.FirstOrDefault(p => p.User.Id == _studentTest.User.Id && p.SpecialTest.Id == _test.Id);

                if (studentTest == null)
                    db.StudentSpecialTest.Add(new StudentSpecialTest()
                    {
                        SpecialTest = test,
                        User = db.Users.FirstOrDefault(p => p.Id == _studentTest.User.Id),
                        Active = _studentTest.Active,
                        Start = DateTime.Now
                    });
                else
                    studentTest.Active = _studentTest.Active;
            }

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Profesor, Administrador, SubAdministrador")]
        [HttpGet, Route("Api/SpecialTest/RestartTest")]
        public HttpResponseMessage RestartTest(string user_id, int sst_id)
        {
            var testStudent = db.StudentSpecialTest.FirstOrDefault(e => e.User_Id == user_id && e.SpecialTest_Id == sst_id);
            var answers = db.StudentAnswer.Where(e => e.SpecialTest_Id == testStudent.SpecialTest.Id && e.User_Id == testStudent.User.Id);

            if (testStudent != null)
            {
                testStudent.Done = false;
                testStudent.Attempts = 0;
                if (answers != null)
                {
                    foreach (var answer in answers)
                    {
                        db.StudentAnswer.Remove(answer);
                    }
                }
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        }

        private void AssociateQuestion_TrueFalse(List<QuestionTrueFalse> local_qtfs, SpecialTest test, List<int> questionsToAdd)
        {
            if (local_qtfs == null) return;

            var qtfs = test.SpecialQuestions.OfType<QuestionTrueFalse>();

            foreach (var qtf in qtfs)
            {
                var local_qtf = local_qtfs.FirstOrDefault(p => p.Id == qtf.Id);
                qtf.Status = local_qtf != null;

                if (qtf.Status)
                {
                    qtf.Order = local_qtf.Order;
                    qtf.Name = local_qtf.Name;
                    qtf.Difficulty = 1;
                    qtf.ComplementaryAnswer = local_qtf.ComplementaryAnswer;

                    foreach (var qtfi in qtf.QuestionTrueFalseItems)
                    {
                        var local_qtfi = local_qtf.QuestionTrueFalseItems.FirstOrDefault(p => p.Id == qtfi.Id);
                        qtfi.Status = local_qtfi != null;

                        if (local_qtfi != null)
                        {
                            qtfi.Text = local_qtfi.Text;
                            qtfi.Value = local_qtfi.Value;
                        }
                    }

                    foreach (var local_qtfi in local_qtf.QuestionTrueFalseItems)
                        if (local_qtfi.Id == 0 || !qtf.QuestionTrueFalseItems.Any(p => p.Id == local_qtfi.Id))
                            qtf.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
                            {
                                QuestionType = QuestionType.TrueFalse,
                                Text = local_qtfi.Text,
                                Value = local_qtfi.Value
                            });
                }
            }

            foreach (var local_qtf in local_qtfs)
                if (local_qtf.Id == 0 || !qtfs.Any(p => p.Id == local_qtf.Id))
                {
                    var qtf = new QuestionTrueFalse()
                    {
                        QuestionType = QuestionType.TrueFalse,
                        Order = local_qtf.Order,
                        Name = local_qtf.Name,
                        Difficulty = 1,
                        ComplementaryAnswer = local_qtf.ComplementaryAnswer,
                        QuestionTrueFalseItems = new List<QuestionTrueFalseItem>()
                    };

                    if (local_qtf.QuestionTrueFalseItems != null)
                        foreach (var local_qtfi in local_qtf.QuestionTrueFalseItems)
                            qtf.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
                            {
                                QuestionType = QuestionType.TrueFalse,
                                Text = local_qtfi.Text,
                                Value = local_qtfi.Value
                            });
                    else
                        continue;

                    test.SpecialQuestions.Add(qtf);
                }
        }

        private void AssociateQuestion_WrittenAnswer(List<QuestionWrittenAnswer> local_qwas, SpecialTest test, List<int> questionsToAdd)
        {
            if (local_qwas == null) return;

            var qwas = test.SpecialQuestions.OfType<QuestionWrittenAnswer>();

            foreach (var qwa in qwas)
            {
                var local_qwa = local_qwas.FirstOrDefault(p => p.Id == qwa.Id);
                qwa.Status = local_qwa != null;

                if (local_qwa != null)
                {
                    qwa.Order = local_qwa.Order;
                    qwa.Name = local_qwa.Name;
                    qwa.Difficulty = 1;
                    qwa.QuestionWrittenAnswerItem.Answer = local_qwa.QuestionWrittenAnswerItem.Answer ?? "";
                }
            }

            foreach (var local_qwa in local_qwas)
                if (local_qwa.Id == 0 || !qwas.Any(p => p.Id == local_qwa.Id)) {
                    test.SpecialQuestions.Add(new QuestionWrittenAnswer()
                    {
                        QuestionType = QuestionType.WrittenAnswer,
                        Order = local_qwa.Order,
                        Name = local_qwa.Name,
                        Difficulty = 1,
                        QuestionWrittenAnswerItem = new QuestionWrittenAnswerItem()
                        {
                            QuestionType = QuestionType.WrittenAnswer,
                            Answer = local_qwa.QuestionWrittenAnswerItem.Answer ?? ""
                        }
                    });
                }
        }

        private void AssociateQuestion_QuestionPairedWords(List<QuestionPairedWord> local_qtws, SpecialTest test, List<int> questionsToAdd)
        {
            if (local_qtws == null) return;

            var qtws = test.SpecialQuestions.OfType<QuestionPairedWord>();

            foreach (var qtw in qtws)
            {
                var local_qtw = local_qtws.FirstOrDefault(p => p.Id == qtw.Id);
                qtw.Status = local_qtw != null;

                if (qtw.Status)
                {
                    qtw.Order = local_qtw.Order;
                    qtw.Name = local_qtw.Name;
                    qtw.Difficulty = 1;
                    qtw.ComplementaryAnswer = local_qtw.ComplementaryAnswer;

                    foreach (var qtwi in qtw.QuestionPairedWordItems)
                    {
                        var local_qtwi = local_qtw.QuestionPairedWordItems.FirstOrDefault(p => p.Id == qtwi.Id);
                        qtwi.Status = local_qtwi != null;

                        if (local_qtwi != null)
                        {
                            qtwi.Column_1_Text = local_qtwi.Column_1_Text;
                            qtwi.Column_1_Alternative = local_qtwi.Column_1_Alternative;
                            qtwi.Column_2_Text = local_qtwi.Column_2_Text;
                            qtwi.Column_2_Index = local_qtwi.Column_2_Index;
                        }
                    }

                    foreach (var local_qtwi in local_qtw.QuestionPairedWordItems)
                        if (local_qtwi.Id == 0 || !qtw.QuestionPairedWordItems.Any(p => p.Id == local_qtwi.Id))
                            qtw.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
                            {
                                QuestionType = QuestionType.PairedWords,
                                Column_1_Text = local_qtwi.Column_1_Text,
                                Column_1_Alternative = local_qtwi.Column_1_Alternative,
                                Column_2_Text = local_qtwi.Column_2_Text,
                                Column_2_Index = local_qtwi.Column_2_Index
                            });
                }
            }

            foreach (var local_qtw in local_qtws)
                if (local_qtw.Id == 0 || !qtws.Any(p => p.Id == local_qtw.Id))
                {
                    var qtw = new QuestionPairedWord()
                    {
                        QuestionType = QuestionType.PairedWords,
                        Order = local_qtw.Order,
                        Name = local_qtw.Name,
                        Difficulty = 1,
                        ComplementaryAnswer = local_qtw.ComplementaryAnswer,
                        QuestionPairedWordItems = new List<QuestionPairedWordItem>()
                    };

                    if (local_qtw.QuestionPairedWordItems != null)
                        foreach (var local_qtwi in local_qtw.QuestionPairedWordItems)
                            qtw.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
                            {
                                QuestionType = QuestionType.PairedWords,
                                Column_1_Text = local_qtwi.Column_1_Text,
                                Column_1_Alternative = local_qtwi.Column_1_Alternative,
                                Column_2_Text = local_qtwi.Column_2_Text,
                                Column_2_Index = local_qtwi.Column_2_Index
                            });
                    else
                        continue;

                    test.SpecialQuestions.Add(qtw);
                }
        }

        private void AssociateQuestion_Alternative(List<QuestionAlternative> local_qas, SpecialTest test, List<int> questionsToAdd)
        {
            
            if (local_qas == null) return;

            var qas = test.SpecialQuestions.OfType<QuestionAlternative>();

            foreach (var qa in qas)
            {
                var local_qa = local_qas.FirstOrDefault(p => p.Id == qa.Id);
                qa.Status = local_qa != null;

                if (qa.Status)
                {
                    qa.Order = local_qa.Order;
                    qa.Name = local_qa.Name;
                    qa.Difficulty = 1;
                    qa.ComplementaryAnswer = local_qa.ComplementaryAnswer;

                    foreach (var qai in qa.QuestionAlternativeItems)
                    {
                        var local_qai = local_qa.QuestionAlternativeItems.FirstOrDefault(p => p.Id == qai.Id);
                        qai.Status = local_qai != null;

                        if (local_qai != null)
                        {
                            qai.Text = local_qai.Text;
                            qai.Value = local_qai.Value;
                        }
                    }

                    foreach (var local_qai in local_qa.QuestionAlternativeItems)
                        if (local_qai.Id == 0 || !qa.QuestionAlternativeItems.Any(p => p.Id == local_qai.Id))
                            qa.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
                            {
                                QuestionType = QuestionType.Alternatives,
                                Text = local_qai.Text,
                                Value = local_qai.Value,
                            });
                }
            }

            foreach (var local_qa in local_qas)
                if (local_qa.Id == 0 || !qas.Any(p => p.Id == local_qa.Id))
                {
                    var qa = new QuestionAlternative()
                    {
                        QuestionType = QuestionType.Alternatives,
                        Order = local_qa.Order,
                        Name = local_qa.Name,
                        ComplementaryAnswer = local_qa.ComplementaryAnswer,
                        Difficulty = 1,
                        QuestionAlternativeItems = new List<QuestionAlternativeItem>()
                    };

                    if (local_qa.QuestionAlternativeItems != null)
                        foreach (var local_qai in local_qa.QuestionAlternativeItems)
                            qa.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
                            {
                                QuestionType = QuestionType.Alternatives,
                                Text = local_qai.Text,
                                Value = local_qai.Value
                            });
                    else
                        continue;

                    test.SpecialQuestions.Add(qa);
                }
        }
    }
}
