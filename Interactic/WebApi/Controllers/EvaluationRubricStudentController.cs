﻿
using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;
using WebApi.CloneModels;
using System.Web;
using System.Web.Script.Serialization;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class EvaluationRubricStudentController : ApiController
    {
        private InteracticContext db;

        public EvaluationRubricStudentController()
        {
            db = new InteracticContext();
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.EvaluationRubricStudent.Where(x => x.Status), EvaluationRubricStudentCloneHelper.EvaluationRubricStudentMap_0));
        }

        public HttpResponseMessage Get(int id)
        {
            var er = db.EvaluationRubricStudent.FirstOrDefault(e => e.Id == id && e.Status == true);

            if (er != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(er, EvaluationRubricStudentCloneHelper.EvaluationRubricStudentMap_0));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Evaluación no encontrada.");
        }

        [HttpGet]
        [Route("Api/EvaluationRubricStudent/GetScoresByStudent")]
        public HttpResponseMessage GetScoresByStudent(string student_id, int evaluation_id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.EvaluationRubricStudent.Where(e => e.Student_Id == student_id && e.EvaluationRubric_Id == evaluation_id && e.Status == true), EvaluationRubricStudentCloneHelper.EvaluationRubricStudentMap_0));
        }


        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Post()
        {
            var form = HttpContext.Current.Request.Form;
            var evaluation = new JavaScriptSerializer().Deserialize<EvaluationRubricStudent>(form["EvaluationRubric"]);
            db.EvaluationRubricStudent.Add(evaluation);
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, evaluation.Id);
        }

        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Delete(int id)
        {
            var er = db.EvaluationRubricStudent.FirstOrDefault(e => e.Id == id);

            if (er != null)
            {
                er.Status = false;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Evaluación no encontrada.");
        }
    }
}
