﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GeneralHelper;
using Helper;
using Model;
using WebApi.CloneModels;

namespace WebApi.Controllers
{
    [Authorize(Roles = "Profesor")]
    public class StudentTestForTeacherController : ApiController
    {
        private InteracticContext db = new InteracticContext();


        /// <summary>
        /// Author Jean, copia de AP
        /// Obtener test realizado por el alumno con sus respuestas. Pasando id de alumno y test
        /// </summary>
        [Route("Api/StudentTestForTeacher/GetStudentTestByTestAndStudent")]
        public HttpResponseMessage GetStudentTestByTestFilteredByUser(int test_id, String student_id)
        {
            var studentTest = CloneHelper.CloneObjectMap(db.StudentTest.FirstOrDefault(p => p.User.Id == student_id && p.Active && p.Test.Id == test_id && p.Done), StudentTestCloneHelper.StudentTestMap_1);

            if (studentTest == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            studentTest.User.Answers.RemoveRange(studentTest.User.Answers.Where(p => p.Question.Test.Id != studentTest.Test.Id));

            var questions_order = studentTest.Test.Questions.OrderBy(p => p.Order).ToList();
            studentTest.Test.Questions.Clear();
            studentTest.Test.Questions.AddRange(questions_order);

            var answers_order = studentTest.User.Answers.OrderBy(p => p.Question.Order).ToList();
            studentTest.User.Answers.Clear();
            studentTest.User.Answers.AddRange(answers_order);

            return Request.CreateResponse(HttpStatusCode.OK, studentTest);
        }


        /// <summary>
        /// Author Jean, copia de AP
        /// Obtiene el test aun no realizado, pasando id de alumno y test
        /// </summary>
        [Route("Api/StudentTestForTeacher/GetStudentTestToDoByTestAndStudent")]
        public HttpResponseMessage GetStudentTestFilteredByUser(int test_id, String student_id)
        {
            var studentTest = db.StudentTest.FirstOrDefault(e => e.Test.Id == test_id && e.User.Id == student_id && e.Status && e.Active);
            var _studentTest = CloneHelper.CloneObjectMap(studentTest, StudentTestCloneHelper.StudentTestMap_3);

            var questions_order = _studentTest.Test.Questions.OrderBy(p => p.Order).ToList();
            _studentTest.Test.Questions.Clear();
            _studentTest.Test.Questions.AddRange(questions_order);

            return Request.CreateResponse(HttpStatusCode.OK, _studentTest);
        }

        /// <summary>
		/// Author Jeann, copia de AP
		/// Registrar Test realizado por un alumno
		/// </summary>
		[HttpPost, Route("Api/StudentTestForTeacher/RegisterTest")]
        public HttpResponseMessage RegisterTest([FromBody] StudentTest _studentTest)
        {
            var correct_Count = 0;

            var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == _studentTest.User.Id && p.Test.Id == _studentTest.Test.Id);

             if (!studentTest.Active || studentTest.Done)
               return Request.CreateResponse(HttpStatusCode.BadRequest, "Test no se encuentra en estado para ser registrado");

            foreach (var question in studentTest.Test.Questions)
            {
                var answer = _studentTest.User.Answers.FirstOrDefault(p => p.Question.Id == question.Id);

                if (answer.Choice == null || answer.Choice == "" || !"ABCDE".Contains(answer.Choice))
                    answer.Choice = "-";

                if (answer == null)
                    studentTest.User.Answers.Add(new Answer() { Question = question, Choice = " " });
                else
                {
                    studentTest.User.Answers.Add(new Answer() { Question = question, Choice = answer.Choice });

                    if (question.Choice == answer.Choice)
                        correct_Count++;
                }
            }

            studentTest.Done = true;
            db.SaveChanges();

            var performance = (float)correct_Count / (float)studentTest.Test.Questions.Count;

            return Request.CreateResponse(HttpStatusCode.OK, performance);
        }

        /// <summary>
        /// Author Jean
        /// Actualiza un test ya hecho por el profesor
        /// </summary>
        public HttpResponseMessage Put([FromBody] StudentTest _studentTest)
        {

            //REVISAR BIEN

            var correct_Count = 0;

            if (_studentTest == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

            var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == _studentTest.User.Id && p.Test.Id == _studentTest.Test.Id);

            if (studentTest == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);


            foreach (var question in studentTest.Test.Questions)
            {
                var cont = 0;
                var _answer = _studentTest.User.Answers.FirstOrDefault(p => p.Question.Id == question.Id);


                if (_answer == null)
                {
                    studentTest.User.Answers.Add(new Answer() { Question = question, Choice = _answer.Choice });
 
                }

                var answer = studentTest.User.Answers.FirstOrDefault(p => p.Question.Id == question.Id);
                if (_answer.Choice == null || _answer.Choice == "" || !"ABCDE".Contains(_answer.Choice))
                {
                    answer.Choice = "-";
                }
                else
                {
                    answer.Choice = _answer.Choice;
                    if (question.Choice == answer.Choice)
                        correct_Count++;
                }

            }
            /*  foreach (var question in studentTest.Test.Questions)
              {
                  var answer = _studentTest.User.Answers.FirstOrDefault(p => p.Question.Id == question.Id);

                  if (answer.Choice == null || answer.Choice == "" || !"ABCDE".Contains(answer.Choice))
                      answer.Choice = "-";

                  // if (answer == null)
                  //studentTest.User.Answers.Add(new Answer() { Question = question, Choice = " " });
                  studentTest.User.Answers.Add(_studentTest.User.Answers[0]);
                  //else
                  // {
                  studentTest.User.Answers.Add(new Answer() { Question = question, Choice = answer.Choice });

                      if (question.Choice == answer.Choice)
                          correct_Count++;
                  //}
              }*/
            //studentTest.User = _studentTest.User;
            //  studentTest.Done = true;
            //studentTest.User.Answers = _studentTest.User.Answers;
            db.SaveChanges();

            var performance = (float)correct_Count / (float)studentTest.Test.Questions.Count;

            return Request.CreateResponse(HttpStatusCode.OK, performance);
        }

    }
}