﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class TeacherController : ApiController
    {
        private InteracticContext db;

        public TeacherController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = false;
        }

        // Carga select utilizado para generacion de informes.
        [Route("api/teacher/report/select")]
        public IHttpActionResult GetTeacherBySchool()
        {
            var selectOptions = db.UserRoleEducationalEstablishment
                                        .Include(x=> x.User)
                                        .Where(x =>
                                                   x.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.Role.Name == RoleName.Teacher
                                               )
                                        .Select(x => new { x.User.Id, x.User.Name, x.User.FatherLastName, x.User.MotherLastName }).ToList();

            if (selectOptions == null) return BadRequest();

            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add( new GeneralSelectDto()
                {
                    Id= Encryptor.Encrypt( item.Id),
                    Name = item.FatherLastName + " " + item.MotherLastName + " " + item.Name
                });
            }
            return Ok(select);
        }

        

        [Route("api/teacher/teacherForEducationalEstablishment/count")]
        public IHttpActionResult GetTeacherForEducationalEstablishment()
        {
            var count = db.TeacherCourseSubject.Where(x => x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                        x.Status).GroupBy(x => x.User_Id).ToList().Count;

            return Ok(count);
        }

    }
}
