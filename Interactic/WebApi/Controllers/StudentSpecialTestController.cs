﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using WebApi.CloneModels;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
    [Authorize(Roles = "Alumno")]
    public class StudentSpecialTestController : ApiController
    {
        private InteracticContext db;

        public StudentSpecialTestController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = true;
        }

        /// <summary>
        /// Author AP
        /// Obtiene la lista de Test relacionado al usuario
        /// </summary>
        [Route("Api/StudentSpecialTest/GetStudentTestsFilteredByUser")]
        public HttpResponseMessage GetStudentTestsFilteredByUser()
        {
            switch (GlobalParameters.Rol)
            {
                case "Alumno":
                    var studentTests = CloneHelper.CloneListMap(db.StudentSpecialTest.Where(p => p.User.Id == GlobalParameters.UserId ).OrderByDescending(p => p.CreatedAt), StudentSpecialTestCloneHelper.StudentTestMap_1);

                    foreach (var studentTest in studentTests)
                        studentTest.User.Answers.RemoveRange(studentTest.User.Answers.Where(p => p.Question.Test.Id != studentTest.SpecialTest.Id));

                    return Request.CreateResponse(HttpStatusCode.OK, studentTests);
                default:
                    return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Author AP
        /// Obtiene la lista de Test relacionado al usuario y la subunitidad
        /// </summary>
        [Route("Api/StudentSpecialTest/GetStudentTestsBySubUnityFilteredByUser")]
        public HttpResponseMessage GetStudentTestsBySubUnityFilteredByUser(int subunity_id)
        {
            switch (GlobalParameters.Rol)
            {
                case "Alumno":
                    var studentTests = CloneHelper.CloneListMap(db.StudentSpecialTest.Where(p => p.User.Id == GlobalParameters.UserId && p.Active && p.SpecialTest.SubUnity.Id == subunity_id && p.Done).OrderByDescending(p => p.CreatedAt), StudentSpecialTestCloneHelper.StudentTestMap_1);

                    foreach (var studentTest in studentTests)
                        studentTest.User.Answers.RemoveRange(studentTest.User.Answers.Where(p => p.Question.Test.Id != studentTest.SpecialTest.Id));

                    return Request.CreateResponse(HttpStatusCode.OK, studentTests);
                default:
                    return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Author AP
        /// Obtiene el Test realizado por el Alumno + sus respuestas.
        /// </summary>
        [Authorize(Roles = "Alumno")]
        [Route("Api/StudentSpecialTest/GetStudentTestByTestFilteredByUser")]
        public HttpResponseMessage GetStudentTestByTestFilteredByUser(int test_id)
        {
            var studentTest = db.StudentSpecialTest.FirstOrDefault(p => p.User.Id == GlobalParameters.UserId && p.Active && p.SpecialTest.Id == test_id && p.SpecialTest.Status && p.Done);
            var _studentTest = CloneHelper.CloneObjectMap(studentTest, StudentSpecialTestCloneHelper.StudentTestMap_1);

            if (_studentTest == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            _studentTest.User.Answers.RemoveRange(_studentTest.User.Answers.Where(p => p.Question.Test.Id != _studentTest.SpecialTest.Id));

            var questions_order = _studentTest.SpecialTest.SpecialQuestions.Where(p => p.Status).OrderBy(p => p.Order).ToList();
            _studentTest.SpecialTest.SpecialQuestions.Clear();
            _studentTest.SpecialTest.SpecialQuestions.AddRange(questions_order);

            var answers_order = _studentTest.User.Answers.OrderBy(p => p.Question.Order).ToList();
            _studentTest.User.Answers.Clear();
            _studentTest.User.Answers.AddRange(answers_order);

            var countSuccess = 0;
            var countError = 0;
            var countSkip = 0;

            /*
            foreach (var question in _studentTest.SpecialTest.SpecialQuestions.Where(p => p.Status))
            {
                var studentChoice = _studentTest.User.Answers.FirstOrDefault(p => p.Question_Id == question.Id)?.Choice;

                if (studentChoice != null)
                    if (question.Choice == studentChoice)
                        countSuccess++;
                    else
                    if (studentChoice == "-")
                        countSkip++;
                    else
                        countError++;
            }*/

            //Si el test es restrictivo - No es posible ver las respuesta hasta que todos hayan respondido.
            /*if (_studentTest.SpecialTest.)
            {
                var course = db.Course.AsNoTracking().FirstOrDefault(p => p.Id == studentTest.Test.SubUnity.Unity.CourseSubject.Course_Id);
                if (studentTest.Test.StudentTests.Count(p => p.Status && p.Done) != course.StudentCourses.Count())
                    foreach (var question in _studentTest.Test.Questions.Where(p => p.Status))
                        question.Choice = "-";
            }
            */
            return Request.CreateResponse(HttpStatusCode.OK, new { Info = new { Success = countSuccess, Skip = countSkip, Error = countError }, StudentTest = _studentTest });
        }

        /// <summary>
        /// Author AP
        /// Obtiene la lista de Test no realizados aún
        /// </summary>
        [Route("Api/StudentSpecialTest/GetTestsYetToBeDone")]
        public HttpResponseMessage GetTestsYetToBeDone()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.StudentSpecialTest.Where(p => p.User.Id == GlobalParameters.UserId && p.Active  && p.SpecialTest.Status), StudentSpecialTestCloneHelper.StudentTestMap_2));
        }

        [Route("Api/StudentSpecialTest/GetStudentTestFilteredByUser")]
        public HttpResponseMessage GetStudentTestFilteredByUser(int test_id)
        {
            var studentTest = db.StudentSpecialTest.FirstOrDefault(e => e.SpecialTest.Id == test_id && e.User.Id == GlobalParameters.UserId && e.Status && e.Active);
            studentTest.Start = DateTime.Now;
            db.SaveChanges();
            var _studentTest = CloneHelper.CloneObjectMap(studentTest, StudentSpecialTestCloneHelper.StudentTestMap_3);

            var questions_order = _studentTest.SpecialTest.SpecialQuestions.OrderBy(p => p.Order).ToList();
            _studentTest.SpecialTest.SpecialQuestions.Clear();

            var questionWrittenAnswers = CloneHelper.CloneListMap(studentTest.SpecialTest.SpecialQuestions.OfType<QuestionWrittenAnswer>(), QuestionWrittenAnswerCloneHelper.QuestionWrittenAnswerMap_1);
            _studentTest.SpecialTest.SpecialQuestions.AddRange(questionWrittenAnswers);

            var questionTrueFalses = CloneHelper.CloneListMap(studentTest.SpecialTest.SpecialQuestions.OfType<QuestionTrueFalse>(), QuestionTrueFalseCloneHelper.QuestionTrueFalseMap_1);
            _studentTest.SpecialTest.SpecialQuestions.AddRange(questionTrueFalses);

            var questionPairedWords = CloneHelper.CloneListMap(studentTest.SpecialTest.SpecialQuestions.OfType<QuestionPairedWord>(), QuestionPairedWordCloneHelper.QuestionPairedWordMap_1);
            foreach (var qpw in questionPairedWords)
                qpw.QuestionPairedWordItems = qpw.QuestionPairedWordItems.OrderBy(p => p.Column_2_Index).ToList();
            _studentTest.SpecialTest.SpecialQuestions.AddRange(questionPairedWords);

            var questionAlternatives = CloneHelper.CloneListMap(studentTest.SpecialTest.SpecialQuestions.OfType<QuestionAlternative>(), QuestionAlternativeCloneHelper.QuestionAlternativeMap_1);
            _studentTest.SpecialTest.SpecialQuestions.AddRange(questionAlternatives);

            _studentTest.SpecialTest.SpecialQuestions = _studentTest.SpecialTest.SpecialQuestions.OrderBy(p => p.Order).ToList();

            /*foreach (var sq in _studentTest.SpecialTest.SpecialQuestions) {
                var alternatives = db.QuestionAlternativeItem.FirstOrDefault(e => e.SpecialQuestion);

                if (sq.QuestionType == ModelEnum.QuestionType.Alternatives) {
                    sq.QuestionAlternativeItems.AddRange(alternatives);
                }
            } */

            return Request.CreateResponse(HttpStatusCode.OK, _studentTest);
        }

        /// <summary>
        /// Author AP
        /// Registrar Test realizado por un alumno
        /// </summary>
        [HttpPost, Route("Api/StudentSpecialTest/RegisterTest")]
        public HttpResponseMessage RegisterTest([FromBody] StudentTest _studentTest)
        {
            /*
            var correct_Count = 0;

            var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == GlobalParameters.UserId && p.Test.Id == _studentTest.Test.Id);

            if (!studentTest.Active || studentTest.Done)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Test no se encuentra en estado para ser registrado");

            foreach (var question in studentTest.Test.Questions.Where(p => p.Status))
            {
                var answer = _studentTest.User.Answers.FirstOrDefault(p => p.Question.Id == question.Id);

                if (answer.Choice == null || answer.Choice == "" || !"ABCDE".Contains(answer.Choice))
                    answer.Choice = "-";

                if (answer == null)
                    studentTest.User.Answers.Add(new Answer() { Question = question, Choice = " " });
                else
                {
                    studentTest.User.Answers.Add(new Answer() { Question = question, Choice = answer.Choice });

                    if (question.Choice == answer.Choice)
                        correct_Count++;
                }
            }

            studentTest.Done = true;
            db.SaveChanges();

            var performance = (float)correct_Count / (float)studentTest.Test.Questions.Where(p => p.Status).Count();
            */

            //return Request.CreateResponse(HttpStatusCode.OK, performance);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}