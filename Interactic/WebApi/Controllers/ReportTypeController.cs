﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class ReportTypesController : ApiController
    {
        public IHttpActionResult Get()
        {
            var role = GlobalParameters.Rol;

            
            List<ReportType> reportTypes = new List<ReportType>();

            if (role == RoleName.Student || role == RoleName.Guardian)
            {
                reportTypes.Add(new ReportType() { Id = "myTestsPerformed", Name = "Mis test realizados" });
                reportTypes.Add(new ReportType() { Id = "MyResults", Name = "Mis resultados" });
                reportTypes.Add(new ReportType() { Id = "MyQualificationReport", Name = "Mi informe de notas" });
                reportTypes.Add(new ReportType() { Id = "BadQualificationsAlert", Name = "Alerta de calificaciones deficientes" });
                reportTypes.Add(new ReportType() { Id = "EventReport", Name = "Mi Calendario" });

            }
            else
            {
                reportTypes.Add(new ReportType() { Id = "PerformanceByTest", Name = "Rendimiento por test" });
                reportTypes.Add(new ReportType() { Id = "PerformanceByUnity", Name = "Rendimiento por tema" });
                reportTypes.Add(new ReportType() { Id = "PerformanceByStudent", Name = "Rendimiento por alumno" });
                reportTypes.Add(new ReportType() { Id = "PerformanceByCourseSubject", Name = "Rendimiento por curso" });
                reportTypes.Add(new ReportType() { Id = "MySubjects", Name = "Relación profesor curso asignatura" });
                reportTypes.Add(new ReportType() { Id = "StudentQualification", Name = "Calificaciones" });
                reportTypes.Add(new ReportType() { Id = "BadQualificationsAlert", Name = "Alerta de calificaciones deficientes" });
                reportTypes.Add(new ReportType() { Id = "AttendanceReport", Name = "Asistencia" });
                reportTypes.Add(new ReportType() { Id = "AttendanceForStudent", Name = "Asistencia por alumno" });
                reportTypes.Add(new ReportType() { Id = "EventReport", Name = "Calendario por curso" });
                reportTypes.Add(new ReportType() { Id = "StudentCourse", Name = "Alumnos por curso" });

            }

            return Ok(reportTypes);
        }
    }
}
