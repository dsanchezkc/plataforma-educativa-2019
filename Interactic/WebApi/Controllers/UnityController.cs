﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;
using WebApi.CloneModels;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class UnityController : ApiController
    {
        private InteracticContext db;

        public UnityController()
        {
            db = new InteracticContext();
        }

		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Unity, UnityCloneHelper.UnityMap_1));
		}

		public HttpResponseMessage Get(int id)
		{
			var unity = db.Unity.FirstOrDefault(e => e.Id == id);

			if (unity != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(unity, UnityCloneHelper.UnityMap_2));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Eje temático no encontrado.");
		}

		[Route("Api/Unity/GetUnitiesBySubjectAndCourse")]
		public HttpResponseMessage GetUnitiesBySubjectAndCourse(int subject_id, int course_id)
		{
			var unities = db.Unity.Where(e => e.CourseSubject.Subject_Id == subject_id && e.CourseSubject.Course_Id == course_id);

			if (unities != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(unities, UnityCloneHelper.UnityMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Eje temático no encontrado.");
		}

        [Route("Api/Unity/GetTotalQuestionsCount")]
        public HttpResponseMessage GetTotalQuestionsCount(int subject_id, int course_id)
        {
            var unities = db.Unity.Where(e => e.Status && e.CourseSubject.Subject_Id == subject_id && e.CourseSubject.Course_Id == course_id);
            List<Subunity> subunities = new List<Subunity>();
            
            foreach (var item in unities)
            {
                foreach(var subu in item.SubUnits.Where(x => x.Status) )
                subunities.Add(new Subunity()
                {
                    Id = subu.Id,
                    Name = subu.Name,
                    Count = db.SpecialQuestion.Where(e=> e.Status && e.SubUnity_Id == subu.Id).Count()
                });
            }

            return Request.CreateResponse(HttpStatusCode.OK, subunities);
        }

        [Route("Api/Unity/GetTotalQuestionsCountByDifficulty")]
        public HttpResponseMessage GetTotalQuestionsCountByDifficulty(int subject_id, int course_id, int difficulty)
        {
            var unities = db.Unity.Where(e => e.Status && e.CourseSubject.Subject_Id == subject_id && e.CourseSubject.Course_Id == course_id);
            List<Subunity> subunities = new List<Subunity>();

            foreach (var item in unities)
            {
                foreach (var subu in item.SubUnits.Where(x => x.Status))
                    subunities.Add(new Subunity()
                    {
                        Id = subu.Id,
                        Name = subu.Name,
                        Count = db.SpecialQuestion.Where(e => e.Status && e.SubUnity_Id == subu.Id && e.Difficulty == difficulty).Count(),
                        Difficulty = difficulty
                    });
            }

            return Request.CreateResponse(HttpStatusCode.OK, subunities);
        }

        public class Subunity
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public int Count { get; set; }

            public int Difficulty { get; set; }
        }

        // Carga select utilizado para generacion de informes.
        [Route("api/unity/report/select")]
        public IHttpActionResult GetUnityForSchool(string _coursId, string _subjecId, string _teachId = null)
        {
            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = Encryptor.Decrypt(_teachId);

            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));
			db.Configuration.ProxyCreationEnabled = true;

			var unityes = db.TeacherCourseSubject
                          .Where(x => x.User.Id == teacher &&
								x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                                   x.CourseSubject.Course.Id == courseId &&
                                                                   x.CourseSubject.Subject.Id == subjectId
                                                               )
                                                       .Select(x => x.CourseSubject.Units)
                                                       .SingleOrDefault();

            if (unityes == null) return BadRequest();

            var selectOptions = unityes.Select(x => new { x.Id, x.Name }).ToList();

                       
            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add( new GeneralSelectDto()
                {
                    Id= Encryptor.Encrypt( item.Id.ToString()),
                    Name = item.Name
                });
            }

            return Ok(select);
        }

        // Carga select utilizado para generacion de informes.
        [Route("api/unityForStudent/report/select")]
        public IHttpActionResult GetUnityForStudent( string _subjecId, string _studentId = null)
        {
            var studentId = Encryptor.Decrypt(_studentId) ?? GlobalParameters.UserId;
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));

            var unityes = db.CourseSubject
                                          .Where(x =>
                                                      x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                      x.Course.StudentCourses.Any(z => z.User.Id == studentId) &&
                                                      x.Course.Year == GlobalParameters.Year &&
                                                      x.Subject.Id == subjectId && x.Subject.Status && x.Course.Status
                                              )
                                        .Select(x => x.Units)
                                        .SingleOrDefault();

            if (unityes == null) return BadRequest();

            var selectOptions = unityes.Select(x => new { x.Id, x.Name }).ToList();


            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add(new GeneralSelectDto()
                {
                    Id = Encryptor.Encrypt(item.Id.ToString()),
                    Name = item.Name
                });
            }
            return Ok(select);
        }

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Post([FromBody] Unity unity)
		{
			if (unity == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			db.Unity.Add(new Unity()
			{
				Year = GlobalParameters.Year,
				Name = unity.Name,
				CourseSubject = db.CourseSubject
									   .FirstOrDefault(p => p.Subject.Id == unity.CourseSubject.Subject.Id &&
															p.Course.Id == unity.CourseSubject.Course.Id)
			});

			db.SaveChanges();

			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Put([FromBody] Unity unity)
		{
			if (unity == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			var item = db.Unity.FirstOrDefault(p => p.Id == unity.Id);

			if (item == null) return Request.CreateResponse(HttpStatusCode.BadRequest, "No encontrado.");

			item.Name = unity.Name;
			item.CourseSubject = db.CourseSubject.FirstOrDefault(p => p.Subject.Id == unity.CourseSubject.Subject.Id && p.Course.Id == unity.CourseSubject.Course.Id);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Delete(int id)
		{			
			var item = db.Unity.Include(p => p.SubUnits).FirstOrDefault(p => p.Id == id);

			if (item == null || item.SubUnits.Count != 0) return Request.CreateResponse(HttpStatusCode.BadRequest);

			db.Unity.Remove(item);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}
	}
}
