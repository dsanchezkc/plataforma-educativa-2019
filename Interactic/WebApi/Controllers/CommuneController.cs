﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Data.Entity;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class CommuneController : ApiController
	{
		private InteracticContext db;

		public CommuneController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = false;
		}

		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, db.Commune);
		}

		[AllowAnonymous]
		[Route("Api/Commune/GetCommunesByProvince")]
		public HttpResponseMessage GetCommunesByProvince(int province_id)
		{
			return Request.CreateResponse(HttpStatusCode.OK, db.Commune.Where(p => p.Province.Id == province_id));
		}
	}
}