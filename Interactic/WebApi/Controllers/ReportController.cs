﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModels;
using Rotativa;
using Rotativa.Options;
using Helper;
using System.Reflection;
using GeneralHelper;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using System.Web.UI;
using System.Drawing;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private InteracticContext db;

        public ReportController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = true;
        }


        //  Despliega reporte 
        [HttpGet]
        [Route("report/{reportName}")]
        public ActionResult ShowReport(string reportName, string _teachId, string _coursId, string _subjecId, string _unity,
                                        string _subunity, string _studentId, string _testId, string _periodId, string _monthId)
        {
            var vm = InvokeStringMethod(reportName, _teachId, _coursId, _subjecId, _unity, _subunity, _studentId, _testId, _periodId, _monthId);

            //return View(reportName, vm);
            var pdf = new ViewAsPdf("NoData");
            if (vm != null) pdf = new ViewAsPdf(reportName, vm)
            {
                FileName = reportName + ".pdf"
            };

            var binary = pdf.BuildFile(this.ControllerContext);
            string base64PDF = System.Convert.ToBase64String(binary, 0, binary.Length);
            return Content(base64PDF);
        }

        //  Descarga reporte 
        [HttpPost]
        [Route("report/{reportName}/download")]
        public ActionResult DownloadReport(string reportName, string _teachId, string _coursId, string _subjecId, string _unity,
                                        string _subunity, string _studentId, string _testId, string _periodId, string _monthId)
        {
            var vm = InvokeStringMethod(reportName, _teachId, _coursId, _subjecId, _unity, _subunity, _studentId, _testId, _periodId, _monthId);
            return new ViewAsPdf(reportName, vm)
            {
                FileName = reportName + ".pdf"
            };
        }


        public ReportViewModel InvokeStringMethod(string methodName, string _teachId, string _coursId, string _subjecId, string _unity,
                                                    string _subunity, string _studentId, string _testId, string _periodId, string _monthId)
        {
            var list = new List<InputDataViewModel>();
            list.Add(new InputDataViewModel() { Id = "_teachId", Value = _teachId });
            list.Add(new InputDataViewModel() { Id = "_coursId", Value = _coursId });
            list.Add(new InputDataViewModel() { Id = "_subjecId", Value = _subjecId });
            list.Add(new InputDataViewModel() { Id = "_unity", Value = _unity });
            list.Add(new InputDataViewModel() { Id = "_subunity", Value = _subunity });
            list.Add(new InputDataViewModel() { Id = "_studentId", Value = _studentId });
            list.Add(new InputDataViewModel() { Id = "_testId", Value = _testId });
            list.Add(new InputDataViewModel() { Id = "_periodId", Value = _periodId });
            list.Add(new InputDataViewModel() { Id = "_monthId", Value = _monthId });


            methodName = methodName + "Data";
            Type calledType = this.GetType();
            var vm = (ReportViewModel)calledType.InvokeMember(
                                                                methodName,
                                                                BindingFlags.InvokeMethod,
                                                                null,
                                                                this,
                                                                new object[] { list }
                                                                );
            return vm;
        }


        /************ Obtiene datos para generar el reporte de notas por alumno ************/

        public ReportViewModel MyQualificationReportData(List<InputDataViewModel> list)
        {
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _studentId = list.First(x => x.Id == "_studentId").Value;

            var courseId = Int32.Parse(_coursId);
            var course = db.Course.SingleOrDefault(X => X.Id == courseId);

            var studentId = Encryptor.Decrypt(_studentId) ?? GlobalParameters.UserId;
            var student = db.Users.Where(X => X.Id == studentId).SingleOrDefault();
            if (student == null) return null; // Content("No se pudo encontrar el alumno indicado."); // throw new HttpException(400, "Error"); // Exception if couldn't find a student

            var subjects = db.CourseSubject
                                        .Where(x =>
                                                    x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.Course_Id == courseId &&
                                                    x.Course.Year == GlobalParameters.Year
                                                )
                                        .OrderBy(x => x.Course_Id).Select(x => x.Subject).ToList();

            var courseSubjects = db.CourseSubject.Where(x => x.Course.Id == courseId && x.Course.Status).ToList();

            if (subjects == null) return null;// Content("No se pudo encontrar el curso indicado."); // throw new HttpException(400, "Error"); // Exception if couldn't find a course subject


            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);


            var periodDetails = db.PeriodDetail.Where(
                            x => x.EstablishmentPeriod.Id == Establishment.EstablishmentPeriod.Id
                           ).ToList();

            var observations = db.Observation.Where(x => x.User.Id == student.Id && x.Course.Id == courseId);

            var vm = new ReportViewModel()
            {
                Establishment = Establishment,
                Student = student,
                Subjects = subjects,
                CurrentYear = GlobalParameters.Year,
                PeriodDetails = periodDetails,
                Observations = observations,
                Course = course,
                CourseSubjects = courseSubjects

            };

            return vm;
        }

        /************ Obtiene datos para generar el reporte de asignaturas ************/

        public ReportViewModel MySubjectsData(List<InputDataViewModel> list)
        {
            string teacher;
            var _teachId = list.First(x => x.Id == "_teachId").Value;
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = Encryptor.Decrypt(_teachId);

            var courseSubjects = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId)
                                        .Select(x => x.CourseSubject)
                                        .OrderBy(x => x.Course_Id)
                                        .ToList();


            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = db.Users.SingleOrDefault(x => x.Id == teacher),
                CourseSubjects = courseSubjects
            };
            return vm;
        }

        /************ Obtiene datos para generar el reporte por curso asignatura ************/

        public ReportViewModel PerformanceByCourseSubjectData(List<InputDataViewModel> list)
        {
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _teachId = list.First(x => x.Id == "_teachId").Value;
            var courseId = Int32.Parse(_coursId);
            var subjectId = Int32.Parse(_subjecId);
            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = _teachId;

            var courseSubject = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course_Id == courseId 
                                                    //x.CourseSubject.Subject_Id == subjectId
                                                )
                                        .Select(x => x.CourseSubject)
                                        .OrderBy(x => x.Course_Id)
                                        .SingleOrDefault();

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = db.Users.SingleOrDefault(x => x.Id == teacher),
                CourseSubject = courseSubject
            };

            return vm;
        }

        /************ Obtiene datos para generar el reporte por unidad ************/
        public ReportViewModel PerformanceByUnityData(List<InputDataViewModel> list)
        {
            var _teachId = list.First(x => x.Id == "_teachId").Value;
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _unity = list.First(x => x.Id == "_unity").Value;
            var _subunity = list.First(x => x.Id == "_subunity").Value;

            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = _teachId;
            var courseId = Int32.Parse(_coursId);
            var subjectId = Int32.Parse(_subjecId);
            var unitId = Int32.Parse(_unity);
            var subunitId = Int32.Parse(_subunity);


            var un = db.Unity.Single(x => x.Id == unitId);

            var courseSubject = db.TeacherCourseSubject.Where(x =>
                                                            x.User.Id == teacher &&
                                                            x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                            x.CourseSubject.Course_Id == courseId &&
                                                            x.CourseSubject.Subject_Id == subjectId
                                                            )
                                                    .Select(x => x.CourseSubject)
                                                    .SingleOrDefault();
            var subUnity = db.SubUnity.Where(
                                    x => x.Id == subunitId
                                )
                        .SingleOrDefault();

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = db.Users.SingleOrDefault(x => x.Id == teacher),
                SubUnity = subUnity,
                CourseSubject = courseSubject
            };
            return vm;
        }


        /************ Obtiene datos para generar el reporte de rendimiento por estudiante ************/

        public ReportViewModel PerformanceByStudentData(List<InputDataViewModel> list)
        {
            var _teachId = list.First(x => x.Id == "_teachId").Value;
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _studentId = list.First(x => x.Id == "_studentId").Value;

            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = _teachId;
            var courseId = Int32.Parse(_coursId);
            var subjectId = Int32.Parse(_subjecId);
            var studentId = _studentId ?? GlobalParameters.UserId;

            var studentcourse = db.StudentCourse.Where(x =>
                                                    x.User.Id == studentId &&
                                                    x.Course.Id == courseId
                                                ).SingleOrDefault();

            if (studentcourse == null) return null;// throw new HttpException(400, "Bad Request"); // Estudiante no pertenece al curso 

            var courseSubject = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course_Id == courseId &&
                                                    x.CourseSubject.Subject_Id == subjectId &&
                                                    x.CourseSubject.Course.StudentCourses.Any(z => z.User.Id == studentcourse.User.Id)
                                                )
                                        .Select(x => x.CourseSubject)
                                        .SingleOrDefault();

            if (courseSubject == null) return null; // throw new HttpException(400, "Bad Request"); // Exception if couldn't find a teacher


            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = db.Users.SingleOrDefault(x => x.Id == teacher),
                CourseSubject = courseSubject,
                Student = studentcourse.User
            };

            return vm;
        }

        public ReportViewModel EventReportData(List<InputDataViewModel> list)
        {
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _month = list.First(x => x.Id == "_monthId").Value;
            var month = Int32.Parse(_month);
            var courseId = Int32.Parse(_coursId);
            var course = db.Course.Where(x => x.Id == courseId && x.Status).FirstOrDefault();
            var events = db.Event.Where(x => x.Course.Id == courseId && x.Start.Month == month && x.Status).OrderBy(x => x.Start).ToList();

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Events = events,
                Course = course,
                Month = month
            };
            return vm;
        }

        /************ Obtiene datos para reporte de rendimiento por test ************/

        public ReportViewModel PerformanceByTestData(List<InputDataViewModel> list)
        {
            var _teachId = list.First(x => x.Id == "_teachId").Value;
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _unity = list.First(x => x.Id == "_unity").Value;
            var _subUnity = list.First(x => x.Id == "_subunity").Value;
            var _testId = list.First(x => x.Id == "_testId").Value;

            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = _teachId;
            var courseId = Int32.Parse(_coursId);
            var subjectId = Int32.Parse(_subjecId);
            var unityId = Int32.Parse(_unity);
            var subUnityId = Int32.Parse(_subUnity);
            var testId = Int32.Parse(_testId);

            var courseSubject = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course_Id == courseId &&
                                                    x.CourseSubject.Subject_Id == subjectId
                                                )
                                        .Select(x => x.CourseSubject)
                                        .OrderBy(x => x.Course_Id)
                                        .SingleOrDefault();
            if (courseSubject == null) return null; // Content("No se pudo encontrar el profesor indicado.");  // Exception if couldn't find a teacher

            var subUnit = db.SubUnity.Where(x =>
                                        x.Id == subUnityId &&
                                        x.Unity.Id == unityId &&
                                        x.Unity.CourseSubject.Course_Id == courseSubject.Course_Id &&
                                        x.Unity.CourseSubject.Subject_Id == courseSubject.Subject_Id
                                    ).SingleOrDefault();

            var test = db.Test.Where(x =>
                                       x.Id == testId &&
                                       x.SubUnity.Id == subUnit.Id
                                   ).SingleOrDefault();

            if (test.StudentTests.Count == 0) return null;//Content("No existen alumnos asociados a este test.");

            test.StudentTests.OrderBy(x => x.User.StudentCourses.Where(p => p.User_Id == x.User.Id).Select(p => p.ListNumber));

            var answers = db.Answer.Where(x =>
                                        x.Question.Test.Id == testId &&
                                        x.Question.Test.SubUnity.Id == subUnit.Id &&
                                        x.Question.Test.SubUnity.Unity.Id == unityId
                                    );

            var contents = db.AchievementIndicator.Where(x => x.Test.Id == test.Id && x.Status);
            //);
            var ids = db.Question.Where(x => x.Test.Id == testId).Select(x => x.Skill.Id).Distinct();
            var skills = db.Skill.Where(x => ids.Contains(x.Id));
            var questions = db.Question.Where(x => x.Test.Id == testId);

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = db.Users.SingleOrDefault(x => x.Id == teacher),
                CourseSubject = courseSubject,
                Answers = answers,
                SubUnity = subUnit,
                Test = test,
                AchievementIndicator = contents,
                Skills = skills,
                Questions = questions
            };
            return vm;
        }

        /************ Obtiene datos para generar el reporte de respuestas correctas de un test ************/

        public ReportViewModel RightTestAnwersData(List<InputDataViewModel> list)
        {
            var _teachId = list.First(x => x.Id == "_teachId").Value;
            var _testId = list.First(x => x.Id == "_testId").Value;

            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = Encryptor.Decrypt(_teachId);
            var testId = Int32.Parse(Encryptor.Decrypt(_testId));

            var test = db.Test.Where(x => x.Id == testId
                                   ).SingleOrDefault();

            if (test == null) return null; // Content("No se pudo encontrar el profesor indicado."); // throw new HttpException(400, "Error"); // Exception if couldn't find a teacher

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = db.Users.SingleOrDefault(x => x.Id == teacher),
                Test = test
            };
            return vm;
        }


        /************ Obtiene datos para generar el reporte de respuestas incorrectas ************/

        public ReportViewModel StudentQualificationData(List<InputDataViewModel> list)
        {
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _studentId = list.First(x => x.Id == "_studentId").Value;

            var courseId = Int32.Parse(_coursId);
            var subjectId = Int32.Parse(_subjecId);
            var studentId = _studentId;

            var student = db.Users.Where(X => X.Id == studentId).SingleOrDefault();
            if (student == null) return null; // Content("No se pudo encontrar el alumno indicado."); // throw new HttpException(400, "Error"); // Exception if couldn't find a student

            var courseSubject = db.CourseSubject
                                        .Where(x =>
                                                    x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.Course_Id == courseId &&
                                                    x.Course.Year == GlobalParameters.Year &&
                                                    x.Subject_Id == subjectId
                                                )
                                        .OrderBy(x => x.Course_Id)
                                        .SingleOrDefault();

            if (courseSubject == null) return null;// Content("No se pudo encontrar el curso indicado."); // throw new HttpException(400, "Error"); // Exception if couldn't find a course subject


            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);


            var periodDetails = db.PeriodDetail.Where(
                            x => x.EstablishmentPeriod.Id == Establishment.EstablishmentPeriod.Id
                           ).ToList();

            var observations = db.Observation.Where(x => x.User.Id == student.Id &&
                                                        x.BaseSubject.Id == subjectId);

            var vm = new ReportViewModel()
            {
                Establishment = Establishment,
                Student = student,
                CourseSubject = courseSubject,
                CurrentYear = GlobalParameters.Year,
                PeriodDetails = periodDetails,
                Observations = observations

            };

            return vm;
        }

        /************ Obtiene datos para generar el reporte de respuestas incorrectas ************/

        public ReportViewModel WrongAnswersData(List<InputDataViewModel> list)
        {
            var _teachId = list.First(x => x.Id == "_teachId").Value;
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _unity = list.First(x => x.Id == "_unity").Value;
            var _subUnity = list.First(x => x.Id == "_subunity").Value;
            var _testId = list.First(x => x.Id == "_testId").Value;
            var _studentId = list.First(x => x.Id == "_studentId").Value;

            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = Encryptor.Decrypt(_teachId);
            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));
            var unityId = Int32.Parse(Encryptor.Decrypt(_unity));
            var subUnityId = Int32.Parse(Encryptor.Decrypt(_subUnity));
            var testId = Int32.Parse(Encryptor.Decrypt(_testId));
            var studentId = Encryptor.Decrypt(_studentId);

            var studentcourse = db.StudentCourse.Where(x =>
                                                    x.User.Id == studentId &&
                                                    x.Course.Id == courseId
                                                ).SingleOrDefault();

            if (studentcourse == null) return null; // Content("No se pudo encontrar el alumno indicado.");  //   throw new HttpException(404, "Not Fount"); // Estudiante no pertenece al curso 

            var courseSubject = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course_Id == courseId &&
                                                    x.CourseSubject.Subject_Id == subjectId &&
                                                    x.CourseSubject.Course.StudentCourses.Any(z => z.User.Id == studentId)
                                                )
                                        .Select(x => x.CourseSubject)
                                        .OrderBy(x => x.Course_Id)
                                        .SingleOrDefault();
            if (courseSubject == null) return null; // Content("No se pudo encontrar el profesor indicado.");  // throw new HttpException(404, "Not Fount"); // Exception if couldn't find a teacher

            var subUnit = db.SubUnity.Where(x =>
                                        x.Id == subUnityId &&
                                        x.Unity.Id == unityId &&
                                        x.Unity.CourseSubject.Course_Id == courseSubject.Course_Id &&
                                        x.Unity.CourseSubject.Subject_Id == courseSubject.Subject_Id
                                    ).SingleOrDefault();

            var test = db.Test.Where(x =>
                                       x.Id == testId &&
                                       x.SubUnity.Id == subUnityId
                                   ).SingleOrDefault();

            var answers = db.Answer.Where(x =>
                                        x.Question.Test.Id == testId &&
                                        x.Question.Test.SubUnity.Id == subUnityId &&
                                        x.Question.Test.SubUnity.Unity.Id == unityId &&
                                        x.User.Id == studentId
                                    ).ToList();


            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = db.Users.SingleOrDefault(x => x.Id == teacher),
                CourseSubject = courseSubject,
                Student = studentcourse.User,
                Answers = answers,
                SubUnity = subUnit,
                Test = test
            };
            return vm;
        }

        // GET: Reporte respuestas erroneas por alumno
        public ReportViewModel WrongAnswersForStudentData(List<InputDataViewModel> list)
        {
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _unity = list.First(x => x.Id == "_unity").Value;
            var _subUnity = list.First(x => x.Id == "_subunity").Value;
            var _testId = list.First(x => x.Id == "_testId").Value;
            var _studentId = list.First(x => x.Id == "_studentId").Value;


            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var studentId = Encryptor.Decrypt(_studentId) ?? GlobalParameters.UserId;
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));
            var unityId = Int32.Parse(Encryptor.Decrypt(_unity));
            var subUnityId = Int32.Parse(Encryptor.Decrypt(_subUnity));
            var testId = Int32.Parse(Encryptor.Decrypt(_testId));


            var studentcourse = db.StudentCourse.Where(x =>
                                                    x.User.Id == studentId &&
                                                    x.Course.Year == GlobalParameters.Year &&
                                                    x.Course_Id == courseId
                                                ).SingleOrDefault();

            if (studentcourse == null) return null; // Content("No se pudo encontrar el alumno indicado.");  //   throw new HttpException(404, "Not Fount"); // Estudiante no pertenece al curso 

            //La consulta se muere al hacer consulta anidada la tuve que separar
            //var teacherCourseSubjects = db.TeacherCourseSubject
            //							.Where(x => x.CourseSubject.Course.EducationalEstablishment.Id == establishmentId &&
            //										x.CourseSubject.Course_Id == studentcourse.Course.Id &&
            //										x.CourseSubject.Subject_Id == subjectId &&
            //										x.CourseSubject.Course.StudentCourses.Any(z => z.User.Id == studentId)
            //									);

            //Author : AP
            //Nueva forma
            var teacherCourseSubjects = db.TeacherCourseSubject
                                        .Where(x => x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course_Id == studentcourse.Course.Id &&
                                                    x.CourseSubject.Subject_Id == subjectId
                                                );

            TeacherCourseSubject teacherCourseSubject = null;

            foreach (var _teacherCourseSubject in teacherCourseSubjects)
                if (_teacherCourseSubject.CourseSubject.Course.StudentCourses.Any(p => p.User.Id == studentId))
                {
                    teacherCourseSubject = _teacherCourseSubject;
                    break;
                }
            //Fin

            if (teacherCourseSubject == null) return null; // Content("No se pudo encontrar el profesor indicado.");  // throw new HttpException(404, "Not Fount"); // Exception if couldn't find a teacher

            var subUnit = db.SubUnity.Where(x =>
                                        x.Id == subUnityId &&
                                        x.Unity.Id == unityId &&
                                        x.Unity.CourseSubject.Course_Id == teacherCourseSubject.CourseSubject.Course_Id &&
                                        x.Unity.CourseSubject.Subject_Id == teacherCourseSubject.CourseSubject.Subject_Id
                                    ).SingleOrDefault();

            var test = db.Test.AsNoTracking().Where(x =>
                                       x.Id == testId &&
                                       x.SubUnity.Id == subUnityId
                                   ).SingleOrDefault();

            var answers = db.Answer.Where(x =>
                                        x.Question.Test.Id == testId &&
                                        x.Question.Test.SubUnity.Id == subUnityId &&
                                        x.Question.Test.SubUnity.Unity.Id == unityId &&
                                        x.User.Id == studentId
                                    ).ToList();
            var contents = db.AchievementIndicator.Where(x => x.Test.Id == test.Id && x.Status);
            var ids = db.Question.Where(x => x.Test.Id == testId).Select(x => x.Skill.Id).Distinct();
            var skills = db.Skill.Where(x => ids.Contains(x.Id));

            var questions = db.Question.Where(x => x.Test.Id == testId);

            var isHideAnswers = false;

            if (test.IsPublishedDocumentCorrectionWithRestriction)
            {
                var course = db.Course.AsNoTracking().FirstOrDefault(p => p.Id == test.SubUnity.Unity.CourseSubject.Course_Id);
                isHideAnswers = test.StudentTests.Count(p => p.Status && p.Done) != course.StudentCourses.Count();
            }

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = teacherCourseSubject.User,
                CourseSubject = teacherCourseSubject.CourseSubject,
                Student = studentcourse.User,
                Answers = answers,
                SubUnity = subUnit,
                Test = test,
                IsHideAnswers = isHideAnswers,
                AchievementIndicator = contents,
                Skills = skills,
                Questions = questions,
            };

            return vm;
        }

        // GET: Reporte resultados por alumno
        public ReportViewModel MyResultsData(List<InputDataViewModel> list)
        {
            var _subjecId = list.First(x => x.Id == "_subjecId").Value;
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _studentId = list.First(x => x.Id == "_studentId").Value;

            var subjectId = Int32.Parse(_subjecId);
            var studentId = (_studentId) ?? GlobalParameters.UserId;
            //var courseId = db.StudentCourse.Where(x => x.User.Id == studentId &&
            //                                          x.Course.Year == GlobalParameters.Year)
            //                                      .Select(x => x.Course.Id)
            //                                      .Distinct()
            //                                      .SingleOrDefault();

            var courseId = Int32.Parse(_coursId);

            var teacher = db.TeacherCourseSubject.Where(x => x.CourseSubject.Course_Id == courseId &&
                                                           x.CourseSubject.Subject_Id == subjectId
                                                           )
                                                 .Select(x => x.User)
                                                 .FirstOrDefault();
            if (teacher == null) return null; //return Content("La asignatura actual no tiene un profesor asociado.");

            var studentcourse = db.StudentCourse.Where(x =>
                                                    x.User.Id == studentId &&
                                                    x.Course.Id == courseId
                                                ).SingleOrDefault();
            var answers = db.Answer.Where(x =>
                                       x.Question.Test.SubUnity.Unity.CourseSubject.Subject_Id == subjectId && x.Question.Test.SubUnity.Unity.CourseSubject.Course_Id == courseId
                                   ).ToList();


            if (studentcourse == null) return null;// throw new HttpException(400, "Bad Request"); // Estudiante no pertenece al curso 

            var courseSubject = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher.Id &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course_Id == courseId &&
                                                    x.CourseSubject.Subject_Id == subjectId &&
                                                    x.CourseSubject.Course.StudentCourses.Any(z => z.User.Id == studentcourse.User.Id)
                                                )
                                        .Select(x => x.CourseSubject)
                                        .SingleOrDefault();
            var ids = db.Question.Where(x => x.Test.SubUnity.Unity.CourseSubject.Subject.Id == subjectId && x.Status == true).Select(x => x.Skill.Id).Distinct();
            var skills = db.Skill.Where(x => ids.Contains(x.Id));
            var contents = db.AchievementIndicator.Where(x => x.Test.SubUnity.Unity.CourseSubject.Subject_Id == subjectId && x.Status);

            var tests = db.Test.Where(x => x.SubUnity.Unity.CourseSubject.Subject_Id == subjectId).Select(x => x.Id).Distinct();
            var questions = db.Question.Where(x => x.Status == true && tests.Contains(x.Test.Id) && x.Test.SubUnity.Unity.CourseSubject.Subject_Id == subjectId && x.Test.SubUnity.Unity.CourseSubject.Course_Id == courseId);


            if (courseSubject == null) return null; // throw new HttpException(400, "Bad Request"); // Exception if couldn't find a teacher

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = teacher,
                CourseSubject = courseSubject,
                Student = studentcourse.User,
                Skills = skills,
                AchievementIndicator = contents,
                Tests = tests,
                Answers = answers,
                Questions = questions
            };

            return vm;
        }

        // GET: Alerta calificaciones deficientes
        public ReportViewModel BadQualificationsAlertData(List<InputDataViewModel> list)
        {
            var _periodId = list.First(x => x.Id == "_periodId").Value;
            var _coursId = list.First(x => x.Id == "_coursId").Value;
            var _studentId = list.First(x => x.Id == "_studentId").Value;

            var periodId = Int32.Parse(_periodId);
            var period = db.PeriodDetail.FirstOrDefault(x => x.Id == periodId);
            var courseId = 0;
            string studentId = null;


            if (GlobalParameters.Rol == RoleName.Guardian)
                studentId = (_studentId);
            else
                if (GlobalParameters.Rol == RoleName.Student)
                studentId = GlobalParameters.UserId;

            courseId = Int32.Parse(_coursId);


            var students = new List<User>();
            if (studentId == null)
            {
                students = db.StudentCourse.Where(x => x.Course.Id == courseId)
                                           .Select(x => x.User)
                                           .Distinct()
                                           .ToList();
            }
            else students = db.Users.Where(x => x.Id == studentId).ToList();

            var teacher = db.TeacherCourseSubject.Where(x => x.CourseSubject.Course_Id == courseId &&
                                                          x.CourseSubject.Course.Year == GlobalParameters.Year
                                                          )
                                                .Select(x => x.User)
                                                .FirstOrDefault();
            if (teacher == null) return null; // Content("El curso actual no posee un profesor asociado.");

            var course = db.Course.FirstOrDefault(x => x.Id == courseId);

            var courseSubjects = db.CourseSubject
                                      .Where(x =>
                                                  x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                  x.Course.Year == GlobalParameters.Year &&
                                                  x.Course_Id == courseId
                                              )
                                      .ToList();

            var vm = new ReportViewModel()
            {
                Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId),
                Teacher = teacher,
                CourseSubjects = courseSubjects,
                StudentCourse = students,
                Course = course,
                Period = period
            };

            return vm;
        }



        // GET: reporte de asistencia
        public ReportViewModel AttendanceReportData(List<InputDataViewModel> list)
        {
            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);
            var _coursId = list.First(x => x.Id == "_coursId").Value;

            var courseId = Int32.Parse(_coursId);
            var course = db.Course.Where(x => x.Year == GlobalParameters.Year &&
                                                       x.Id == courseId &&
                                                       x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId
                                            ).FirstOrDefault();
            var absents = db.Absent.Where(x => x.AttendanceDate.Course.Id == course.Id);
            //if (absents == null) return Content("No hay inasistencias registradas.");
            var periodDetails = db.PeriodDetail.Where(x => x.EstablishmentPeriod.Id == Establishment.EstablishmentPeriod.Id).ToList();
            var attendanceDates = db.AttendanceDate.ToList();
            var dayTrips = db.Daytrip.ToList();
            var ids = db.StudentCourse.Where(x => x.Course.Id == courseId).Select(x => x.User_Id).Distinct();
            var students = db.Users.Where(x => ids.Contains(x.Id));
            var vm = new ReportViewModel()
            {
                Establishment = Establishment,
                Absents = absents,
                PeriodDetails = periodDetails,
                Course = course,
                DayTrips = dayTrips,
                AttendanceDates = attendanceDates,
                StudentsC = students
            };
            return vm;
        }

        // GET: reporte de asistencia de estudiantes por curso
        public ReportViewModel AttendanceForStudentData(List<InputDataViewModel> list)
        {
            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);

            var _coursId = list.First(x => x.Id == "_coursId").Value;

            var courseId = Int32.Parse(_coursId);
            var course = db.Course.Where(x => x.Year == GlobalParameters.Year &&
                                                       x.Id == courseId &&
                                                       x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId
                                            ).FirstOrDefault();

            var absents = db.Absent.Where(x => x.AttendanceDate.Course.Id == course.Id);
            if (absents == null) return null; // Content("No hay inasistencias registradas.");

            var periodDetails = db.PeriodDetail.Where(x => x.EstablishmentPeriod.Id == Establishment.EstablishmentPeriod.Id).ToList();

            var attendanceDates = db.AttendanceDate.ToList();

            var dayTrips = db.Daytrip.ToList();

            var vm = new ReportViewModel()
            {
                Establishment = Establishment,
                Absents = absents,
                PeriodDetails = periodDetails,
                Course = course,
                DayTrips = dayTrips,
                AttendanceDates = attendanceDates
            };
            return vm;
        }

        // Reporte de alumnos por curso
        public ReportViewModel StudentCourseData(List<InputDataViewModel> list)
        {
            var Establishment = db.EducationalEstablishment.FirstOrDefault(x => x.Id == GlobalParameters.EducationalEstablishmentId);

            var _coursId = list.First(x => x.Id == "_coursId").Value;

            var courseId = Int32.Parse((_coursId));
            var course = db.Course.Where(x => x.Year == GlobalParameters.Year &&
                                                       x.Id == courseId &&
                                                       x.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId
                                            ).FirstOrDefault();
            var ids = db.StudentCourse.Where(x => x.Course.Id == courseId).Select(x => x.User_Id).Distinct();
            var students = db.Users.Where(x => ids.Contains(x.Id));
            var studentcourse = db.StudentCourse.Where(x => x.Course_Id == courseId);
            var vm = new ReportViewModel()
            {
                Establishment = Establishment,
                Course = course,
                StudentsC = students,
                StudentCourseList = studentcourse
            };
            return vm;
        }



        //[Authorize(Roles = "Administrador")]
        //[Route("Api/Report/DownloadExcelAsistence")]
        //public void DownloadExcelAsistence()
        //{
        //    using (SpreadsheetDocument document = SpreadsheetDocument.Create("Hola", SpreadsheetDocumentType.Workbook))
        //    {
        //        WorkbookPart workbookPart = document.AddWorkbookPart();
        //        workbookPart.Workbook = new Workbook();

        //        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
        //        worksheetPart.Worksheet = new Worksheet(new SheetData());

        //        Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

        //        Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Test Sheet" };

        //        sheets.Append(sheet);

        //        workbookPart.Workbook.Save();
        //    }
        //}


    }
}