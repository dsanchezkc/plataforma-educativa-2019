﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class PeriodController : ApiController
    {
        private InteracticContext db;

        public PeriodController()
        {
            db = new InteracticContext();
            //db.Configuration.ProxyCreationEnabled = false;
        }

        // Carga select utilizado para generacion de informes.
        [Route("api/period/report/select")]
        public IHttpActionResult GetPeriodForSchool()
        {
            var establishmentId = GlobalParameters.EducationalEstablishmentId;

            var period = db.EducationalEstablishment.Where(x => x.Id == establishmentId).Select(x => x.EstablishmentPeriod).SingleOrDefault();

            var detailPeriods = period.PeriodDetails;

            if (detailPeriods == null) return BadRequest();

            var selectOptions = detailPeriods.Select(x => new { x.Id, x.Name }).ToList();
                       
            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add( new GeneralSelectDto()
                {
                    Id= Encryptor.Encrypt( item.Id.ToString()),
                    Name = item.Name
                });
            }
            return Ok(select);
        }
    }
}
