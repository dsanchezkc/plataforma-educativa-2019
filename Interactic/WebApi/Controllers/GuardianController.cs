﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using System.Web.Script.Serialization;
using System.Web;
using WebApi;
using WebApi.Stores;
using Microsoft.AspNet.Identity;
using ViewModels;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class GuardianController : ApiController
	{
		private InteracticContext db;

		public GuardianController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		/// <summary>
		/// Obtiene lista de Usuarios asociados como Apoderados al CurrentUser
		/// </summary>
		/// <returns></returns>
		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Guardian.Where(p => p.Status && p.User_Student_Id == GlobalParameters.UserId).Select(p => p.User).Distinct(), UserCloneHelper.UserMap_5));
		}

		/// <summary>
		/// Obtiene ViewModel GuardianViewModel
		/// </summary>
		/// <param name="id">Id referente al APODERADO</param>
		/// <returns></returns>
		public HttpResponseMessage Get(string id)
		{
			var guardians = db.Guardian.Where(p => p.Status && p.User_Student_Id == GlobalParameters.UserId && p.User_Id == id);
			var viewModel = new GuardianViewModel()
			{
				Guardian = CloneHelper.CloneObjectMap(db.Users.FirstOrDefault(p => p.Id == id), UserCloneHelper.UserMap_5)
			};

			if (guardians != null && guardians.Count() != 0)
			{
				var establishments = guardians.Select(p => p.EducationalEstablishment).Distinct();

				foreach (var establishment in establishments)
					viewModel.EducationalEstablishments.Add(new EducationalEstablishment() { Id = establishment.Id });
			}

			return Request.CreateResponse(HttpStatusCode.OK, viewModel);
		}

		[Authorize(Roles = "Alumno")]
		public HttpResponseMessage Post([FromBody]GuardianViewModel guardianViewModel)
		{
			guardianViewModel = PostSpecial(guardianViewModel);
			return PutSpecial(guardianViewModel);
		}

		private GuardianViewModel PostSpecial(GuardianViewModel guardianViewModel)
		{
			if (!guardianViewModel.Guardian.UserName.IsValidRUN())
				throw new Exception("R.U.N inválido.");

			guardianViewModel.Guardian.UserName = guardianViewModel.Guardian.UserName.WithoutFormatRUN();
			var user = db.Users.FirstOrDefault(p => p.UserName == guardianViewModel.Guardian.UserName);

			if (user == null)
			{
				user = new User()
				{
					Id = guardianViewModel.Guardian.Id = Guid.NewGuid().ToString(),
					UserName = guardianViewModel.Guardian.UserName.WithoutFormatRUN(),
					Name = guardianViewModel.Guardian.Name,
					MotherLastName = guardianViewModel.Guardian.MotherLastName,
					FatherLastName = guardianViewModel.Guardian.FatherLastName,
				};

				var userManager = new ApplicationUserManager(new ApplicationUserStore(InteracticContext.Create()));
				var result = userManager.Create(user, user.UserName);

				if (!result.Succeeded)
					throw new Exception("No es posible registrar.");

				db.SaveChanges();
				user = db.Users.FirstOrDefault(p => p.Id == user.Id);
			}

			guardianViewModel.Guardian = user;

			return guardianViewModel;
		}

		private HttpResponseMessage PutSpecial(GuardianViewModel guardianViewModel)
		{
			var establishtments = guardianViewModel.EducationalEstablishments;

			if (establishtments == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error.");

			var currentGuardians = db.Guardian.Where(p => p.User.Id == guardianViewModel.Guardian.Id && p.Student.Id == GlobalParameters.UserId);
			var currentUser = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);

			//Actualizando asociaciones
			foreach (var guardian in currentGuardians)
				if (!guardianViewModel.EducationalEstablishments.Any(p => p.Id == guardian.EducationalEstablishment_Id))
					guardian.Status = false;
				else
					guardian.Status = true;

			//Agregando
			foreach (var establishment in establishtments)
				if (!currentGuardians.Any(p => p.EducationalEstablishment_Id == establishment.Id))
				{
					db.Guardian.Add(new Guardian()
					{
						EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(p => p.Id == establishment.Id),
						Student = currentUser,
						User = guardianViewModel.Guardian
					});
				}

			db.SaveChanges();

			foreach (var establishment in db.EducationalEstablishment.AsNoTracking())
			{
				var userRolEducationalEstablishment = guardianViewModel.Guardian.UserRoleEducationalEstablishments.FirstOrDefault(p => p.EducationalEstablishment_Id == establishment.Id && p.Role.Name == "Apoderado");
				var count = db.Guardian.Where(p => p.EducationalEstablishment.Id == establishment.Id && p.User.Id == guardianViewModel.Guardian.Id && p.Status).Count();

				if (count != 0 && userRolEducationalEstablishment == null)
					guardianViewModel.Guardian.UserRoleEducationalEstablishments.Add(new UserRoleEducationalEstablishment()
					{
						User = guardianViewModel.Guardian,
						EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(p => p.Id == establishment.Id),
						Role = db.Roles.FirstOrDefault(p => p.Name == "Apoderado")
					});
				else
				if (userRolEducationalEstablishment != null)
					userRolEducationalEstablishment.Status = count != 0;
			}

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Alumno")]
		public HttpResponseMessage Delete(string id)
		{
			var guardian = db.Users.FirstOrDefault(p => p.Id == id);

			if (guardian == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			return PutSpecial(new GuardianViewModel()
			{
				Guardian = guardian,
				EducationalEstablishments = new List<EducationalEstablishment>()
			});
		}

	}
}
