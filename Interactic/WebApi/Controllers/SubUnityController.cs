﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using WebApi.CloneModels;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class SubUnityController : ApiController
    {
        private InteracticContext db;

        public SubUnityController()
        {
            db = new InteracticContext();
        }
		
		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.SubUnity.Where(p => p.Status), SubUnityCloneHelper.SubUnityMap_1));
		}
		
		public HttpResponseMessage Get(int id)
		{
			var subUnity = db.SubUnity.FirstOrDefault(e => e.Id == id);

			if (subUnity != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(subUnity, SubUnityCloneHelper.SubUnityMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");
		}

		[Route("Api/SubUnity/GetSubUnitiesByUnity")]
		public HttpResponseMessage GetSubUnitiesByUnity(int unity_id)
		{
			var subUnities = db.SubUnity.Where(p => p.Status && p.Unity.Id == unity_id);

			if (subUnities != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(subUnities, SubUnityCloneHelper.SubUnityMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");
		}

        [Route("Api/SubUnity/GetTotalQuestionsCount")]
        public HttpResponseMessage GetTotalQuestionsCount(int subunity_id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, db.SpecialQuestion.Where(e => e.SubUnity_Id == subunity_id).Count());
        }

        // Carga select utilizado para generacion de informes.
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		[Route("api/subunity/report/select")]
        public IHttpActionResult GetUnityForSchool(string _coursId, string _subjecId, string _unity, string _teachId = null)
        {
            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = Encryptor.Decrypt(_teachId);

            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));
            var unityId = Int32.Parse(Encryptor.Decrypt(_unity));

            var unityies = db.TeacherCourseSubject                                        
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course.Id == courseId &&
                                                    x.CourseSubject.Subject.Id == subjectId
                                                )
                                        .Select(x => x.CourseSubject.Units)
                                        .SingleOrDefault();
            if (unityies == null) return BadRequest();

            var unity = unityies.Where(x => x.Id == unityId).SingleOrDefault();
            
            if (unity == null) return BadRequest();

            var selectOptions = db.Unity.Where(x=> x.Id == unityId)
                                        .Include(x=> x.SubUnits)
                                        .SingleOrDefault()
                                        .SubUnits
                                        .Select(x => new { x.Id, x.Name })
                                        .ToList();

            if (selectOptions == null) return BadRequest();

            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add( new GeneralSelectDto()
                {
                    Id= Encryptor.Encrypt( item.Id.ToString()),
                    Name = item.Name
                });
            }
            return Ok(select);
        }

        // Carga select utilizado para generacion de informes.
        [Route("api/subunityForStudent/report/select")]
        public IHttpActionResult GetUnityForStudent(string _subjecId, string _unity, string _studentId = null)
        {
            var studentId = Encryptor.Decrypt(_studentId) ?? GlobalParameters.UserId;
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));
            var unityId = Int32.Parse(Encryptor.Decrypt(_unity));

            var unityies = db.CourseSubject
                                          .Where(x =>
                                                      x.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                      x.Course.StudentCourses.Any(z => z.User.Id == studentId) &&
                                                      x.Course.Year == GlobalParameters.Year &&
                                                      x.Subject.Id == subjectId && x.Subject.Status && x.Course.Status && x.Status
                                              )
                                        .Select(x => x.Units)
                                        .SingleOrDefault();

            if (unityies == null) return BadRequest();

            var unity = unityies.Where(x => x.Id == unityId).SingleOrDefault();

            if (unity == null) return BadRequest();

            var selectOptions = db.Unity.Where(x => x.Id == unityId)
                                        .Include(x => x.SubUnits)
                                        .SingleOrDefault()
                                        .SubUnits
                                        .Select(x => new { x.Id, x.Name })
                                        .ToList();

            if (selectOptions == null) return BadRequest();

            var select = new List<GeneralSelectDto>();
            foreach (var item in selectOptions)
            {
                select.Add(new GeneralSelectDto()
                {
                    Id = Encryptor.Encrypt(item.Id.ToString()),
                    Name = item.Name
                });
            }
            return Ok(select);
        }

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Post([FromBody] SubUnity subUnity)
		{
			if (subUnity == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			db.SubUnity.Add(new SubUnity()
			{
				Name = subUnity.Name,
				Unity = db.Unity.FirstOrDefault(p => p.Id == subUnity.Unity.Id)
			});

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Put([FromBody] SubUnity subUnity)
		{
			var item = db.SubUnity.FirstOrDefault(p => p.Id == subUnity.Id);

			if (item == null) return Request.CreateResponse(HttpStatusCode.BadRequest);

			item.Name = subUnity.Name;
			item.Unity = db.Unity.FirstOrDefault(p => p.Id == subUnity.Unity.Id);
			db.SaveChanges();

			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Delete(int id)
		{
			var item = db.SubUnity.FirstOrDefault(p => p.Id == id);

			if (item == null || item.Tests.Count != 0) return Request.CreateResponse(HttpStatusCode.BadRequest);

			db.SubUnity.Remove(item);
			db.SaveChanges();

			return Request.CreateResponse(HttpStatusCode.OK);
		}
	}
}