﻿
using Model;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Web.Script.Serialization;


namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class CriterionController : ApiController
    {
        private InteracticContext db;

        public CriterionController()
        {
            db = new InteracticContext();
        }

        [Authorize(Roles = "Profesor")]
        [HttpPut, Route("Api/Criterion/SetCriterionText")]
        public HttpResponseMessage SetCriterionText(int criterion_id, string text)
        {
            var criterion = db.Criterion.FirstOrDefault(p => p.Id == criterion_id);
            if (criterion != null)
            {
                criterion.Name = text;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Criterio no encontrado.");
            }
        }

        
    }
}