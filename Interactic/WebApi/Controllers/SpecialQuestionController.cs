﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;
using WebApi.CloneModels;
using ViewModels;
using ModelEnum;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class SpecialQuestionController : ApiController
    {
        private InteracticContext db;

        public SpecialQuestionController()
        {
            db = new InteracticContext();
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.SpecialQuestion.Where(p => p.Status), SpecialQuestionCloneHelper.SpecialQUestionMap_1));
        }

        public HttpResponseMessage Get(int id)
        {
            var specialquestion = db.SpecialQuestion.Where(e => e.Id == id && e.Status);
            

            if (specialquestion != null) {
                if (specialquestion.FirstOrDefault().QuestionType == ModelEnum.QuestionType.Alternatives)
                {
                    var _specialquestion = CloneHelper.CloneListMap(specialquestion, QuestionAlternativeCloneHelper.QuestionAlternativeMap_1);
                    return Request.CreateResponse(HttpStatusCode.OK, _specialquestion);
                }
                else if (specialquestion.FirstOrDefault().QuestionType == ModelEnum.QuestionType.PairedWords)
                {
                    var _specialquestion = CloneHelper.CloneListMap(specialquestion, QuestionPairedWordCloneHelper.QuestionPairedWordMap_1);
                    return Request.CreateResponse(HttpStatusCode.OK, _specialquestion);
                }
                else if (specialquestion.FirstOrDefault().QuestionType == ModelEnum.QuestionType.TrueFalse)
                {
                    var _specialquestion = CloneHelper.CloneListMap(specialquestion, QuestionTrueFalseCloneHelper.QuestionTrueFalseMap_1);
                    return Request.CreateResponse(HttpStatusCode.OK, _specialquestion);
                }
                else {
                    var _specialquestion = CloneHelper.CloneListMap(specialquestion, QuestionWrittenAnswerCloneHelper.QuestionWrittenAnswerMap_1);
                    return Request.CreateResponse(HttpStatusCode.OK, _specialquestion);
                }
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Contenido no encontrado.");
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/SpecialQuestion/QuestionWrittenAnswer")]
        [HttpGet]
        public HttpResponseMessage QuestionWrittenAnswer(int id)
        {
            var questionWrittenAnswer = db.QuestionWrittenAnswer.FirstOrDefault(e => e.Id == id && e.Status);

            //Validando acceso
            switch (GlobalParameters.Rol)
            {
                case "Profesor":
                    if (!db.TeacherCourseSubject.AsNoTracking().Any(p =>
                        p.Status &&
                        p.CourseSubject.Course.Year == GlobalParameters.Year &&
                        p.User.Id == GlobalParameters.UserId &&
                        p.Course_Id == questionWrittenAnswer.SpecialTest.CourseSubject.Course_Id &&
                        p.Subject_Id == questionWrittenAnswer.SpecialTest.CourseSubject.Subject_Id))
                        return Request.CreateResponse(HttpStatusCode.Unauthorized);
                    break;

                case "SubAdministrador":
                    if (!db.CourseSubject.AsNoTracking().Any(p =>
                         p.Status &&
                         p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                         p.Course.Year == GlobalParameters.Year &&
                         p.Course_Id == questionWrittenAnswer.SpecialTest.CourseSubject.Course_Id &&
                         p.Subject_Id == questionWrittenAnswer.SpecialTest.CourseSubject.Subject_Id))
                        return Request.CreateResponse(HttpStatusCode.Unauthorized);
                    break;
            }

            if (questionWrittenAnswer != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObject(questionWrittenAnswer));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Pregunta no encontrada.");
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/SpecialQuestion/GetAllQuestions")]
        [HttpGet]
        public HttpResponseMessage GetAllQuestions()
        {
            var questions = db.SpecialQuestion.Where(e => e.Status);
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(questions, SpecialQuestionCloneHelper.SpecialQUestionMap_1));
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Delete(int id)
        {
            var item = db.SpecialQuestion.FirstOrDefault(p => p.Id == id);

            /*

            if (item.QuestionType == QuestionType.Alternatives) {
                var questions = db.QuestionItem.Where(e => e.QuestionAlternative_Id == item.Id);
                foreach (var it in questions)
                {
                    it.Status = false;
                    db.SaveChanges();
                }
            }

            if (item.QuestionType == QuestionType.PairedWords)
            {
                var questions = db.QuestionItem.Where(e => e.QuestionPairedWord_Id == item.Id);
                foreach (var it in questions)
                {
                    it.Status = false;
                    db.SaveChanges();
                }
            }

            if (item.QuestionType == QuestionType.TrueFalse)
            {
                var questions = db.QuestionItem.Where(e => e.QuestionTrueFalse_Id == item.Id);
                foreach (var it in questions)
                {
                    it.Status = false;
                    db.SaveChanges();
                }
            }

            if (item.QuestionType == QuestionType.WrittenAnswer)
            {
                var questions = db.QuestionItem.Where(e => e.QuestionWrittenAnswer_Id == item.Id);
                foreach (var it in questions)
                {
                    it.Status = false;
                    db.SaveChanges();
                }
            } */

            if (item == null ) return Request.CreateResponse(HttpStatusCode.BadRequest);
            item.Status = false;

            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Put([FromBody] SpecialQuestionRequestViewModel sqRVM)
        {

            if (sqRVM.QuestionAlternatives != null)
            {
                var sq = db.QuestionAlternative.FirstOrDefault(e => e.Status && e.Id == sqRVM.Id);

                var local_qa = sqRVM.QuestionAlternatives.FirstOrDefault(p => p.Id == sq.Id);
                sq.Status = local_qa != null;
                if (sq.Status)
                {
                    sq.Order = local_qa.Order;
                    sq.Name = local_qa.Name;
                    sq.Difficulty = local_qa.Difficulty;
                    sq.ComplementaryAnswer = local_qa.ComplementaryAnswer;

                    foreach (var qai in sq.QuestionAlternativeItems)
                    {
                        var local_qai = local_qa.QuestionAlternativeItems.FirstOrDefault(p => p.Id == qai.Id);
                        qai.Status = local_qai != null;

                        if (local_qai != null)
                        {
                            qai.Text = local_qai.Text;
                            qai.Value = local_qai.Value;
                        }
                    }

                    foreach (var local_qai in local_qa.QuestionAlternativeItems)
                        if (local_qai.Id == 0 || !sq.QuestionAlternativeItems.Any(p => p.Id == local_qai.Id))
                            sq.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
                            {
                                QuestionType = QuestionType.Alternatives,
                                Text = local_qai.Text,
                                Value = local_qai.Value
                            });
                }

            }
            if (sqRVM.QuestionTrueFalses != null) {

                var sq = db.QuestionTrueFalse.FirstOrDefault(e => e.Status && e.Id == sqRVM.Id);

                var local_qtf = sqRVM.QuestionTrueFalses.FirstOrDefault(p => p.Id == sq.Id);
                sq.Status = local_qtf != null;

                if (sq.Status)
                {
                    sq.Order = local_qtf.Order;
                    sq.Name = local_qtf.Name;
                    sq.Difficulty = local_qtf.Difficulty;
                    sq.ComplementaryAnswer = local_qtf.ComplementaryAnswer;

                    foreach (var qtfi in sq.QuestionTrueFalseItems)
                    {
                        var local_qtfi = local_qtf.QuestionTrueFalseItems.FirstOrDefault(p => p.Id == qtfi.Id);
                        qtfi.Status = local_qtfi != null;

                        if (local_qtfi != null)
                        {
                            qtfi.Text = local_qtfi.Text;
                            qtfi.Value = local_qtfi.Value;
                        }
                    }

                    foreach (var local_qtfi in local_qtf.QuestionTrueFalseItems)
                        if (local_qtfi.Id == 0 || !sq.QuestionTrueFalseItems.Any(p => p.Id == local_qtfi.Id))
                            sq.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
                            {
                                QuestionType = QuestionType.TrueFalse,
                                Text = local_qtfi.Text,
                                Value = local_qtfi.Value
                            });
                }

            }
            if (sqRVM.QuestionPairedWords != null) {

                var sq = db.QuestionPairedWord.FirstOrDefault(e => e.Status && e.Id == sqRVM.Id);

                var local_qtw = sqRVM.QuestionPairedWords.FirstOrDefault(p => p.Id == sq.Id);
                sq.Status = local_qtw != null;

                if (sq.Status)
                {
                    sq.Order = local_qtw.Order;
                    sq.Name = local_qtw.Name;
                    sq.Difficulty = local_qtw.Difficulty;
                    sq.ComplementaryAnswer = local_qtw.ComplementaryAnswer;

                    foreach (var qtwi in sq.QuestionPairedWordItems)
                    {
                        var local_qtwi = local_qtw.QuestionPairedWordItems.FirstOrDefault(p => p.Id == qtwi.Id);
                        qtwi.Status = local_qtwi != null;

                        if (local_qtwi != null)
                        {
                            qtwi.Column_1_Text = local_qtwi.Column_1_Text;
                            qtwi.Column_1_Alternative = local_qtwi.Column_1_Alternative;
                            qtwi.Column_2_Text = local_qtwi.Column_2_Text;
                            qtwi.Column_2_Index = local_qtwi.Column_2_Index;
                        }
                    }

                    foreach (var local_qtwi in local_qtw.QuestionPairedWordItems)
                        if (local_qtwi.Id == 0 || !sq.QuestionPairedWordItems.Any(p => p.Id == local_qtwi.Id))
                            sq.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
                            {
                                QuestionType = QuestionType.PairedWords,
                                Column_1_Text = local_qtwi.Column_1_Text,
                                Column_1_Alternative = local_qtwi.Column_1_Alternative,
                                Column_2_Text = local_qtwi.Column_2_Text,
                                Column_2_Index = local_qtwi.Column_2_Index
                            });
                }

            }

            if (sqRVM.QuestionWrittenAnswers != null) {

                var sq = db.QuestionWrittenAnswer.FirstOrDefault(e => e.Status && e.Id == sqRVM.Id);

                var local_qwa = sqRVM.QuestionWrittenAnswers.FirstOrDefault();
                sq.Status = local_qwa != null;

                if (local_qwa != null)
                {
                    sq.Order = local_qwa.Order;
                    sq.Name = local_qwa.Name;
                    sq.Difficulty = sqRVM.Difficulty;
                    sq.QuestionWrittenAnswerItem.Answer = local_qwa.QuestionWrittenAnswerItem.Answer ?? "";
                }
            }

            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Post([FromBody] SpecialQuestionRequestViewModel sqRVM)
        {
            if (sqRVM.QuestionAlternatives != null) {
                var specialquestion = new QuestionAlternative()
                {
                    Difficulty = sqRVM.Difficulty,
                    SubUnity = sqRVM.SubUnity_Id != null ? db.SubUnity.FirstOrDefault(e => e.Status && e.Id == sqRVM.SubUnity_Id) : null,
                    Order = '1',
                    QuestionType = QuestionType.Alternatives,
                    Name = sqRVM.QuestionAlternatives.FirstOrDefault().Name,
                    ComplementaryAnswer = sqRVM.QuestionAlternatives.FirstOrDefault().ComplementaryAnswer,
                    QuestionAlternativeItems = new List<QuestionAlternativeItem>()
                };
                if (sqRVM.QuestionAlternatives.FirstOrDefault().QuestionAlternativeItems != null)
                    foreach (var qai in sqRVM.QuestionAlternatives.FirstOrDefault().QuestionAlternativeItems)
                        specialquestion.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
                        {
                            QuestionType = QuestionType.Alternatives,
                            Text = qai.Text,
                            Value = qai.Value
                        });
                db.QuestionAlternative.Add(specialquestion);
            }

            if (sqRVM.QuestionPairedWords != null)
            {
                var specialquestion = new QuestionPairedWord()
                {
                    Difficulty = sqRVM.Difficulty,
                    SubUnity = sqRVM.SubUnity_Id != null ? db.SubUnity.FirstOrDefault(e => e.Status && e.Id == sqRVM.SubUnity_Id) : null,
                    Order = '1',
                    QuestionType = QuestionType.PairedWords,
                    ComplementaryAnswer = sqRVM.QuestionPairedWords.FirstOrDefault().ComplementaryAnswer,
                    Name = sqRVM.QuestionPairedWords.FirstOrDefault().Name,
                    QuestionPairedWordItems = new List<QuestionPairedWordItem>()
                };
                if (sqRVM.QuestionPairedWords.FirstOrDefault().QuestionPairedWordItems != null)
                    foreach (var qtwi in sqRVM.QuestionPairedWords.FirstOrDefault().QuestionPairedWordItems)
                        specialquestion.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
                        {
                            QuestionType = QuestionType.PairedWords,
                            Column_1_Text = qtwi.Column_1_Text,
                            Column_1_Alternative = qtwi.Column_1_Alternative,
                            Column_2_Text = qtwi.Column_2_Text,
                            Column_2_Index = qtwi.Column_2_Index
                        });
                db.QuestionPairedWord.Add(specialquestion);
            }
            if (sqRVM.QuestionTrueFalses != null)
            {
                var specialquestion = new QuestionTrueFalse()
                {
                    Difficulty = sqRVM.Difficulty,
                    SubUnity = sqRVM.SubUnity_Id != null ? db.SubUnity.FirstOrDefault(e => e.Status && e.Id == sqRVM.SubUnity_Id) : null,
                    Order = '1',
                    QuestionType = QuestionType.TrueFalse,
                    Name = sqRVM.QuestionTrueFalses.FirstOrDefault().Name,
                    ComplementaryAnswer = sqRVM.QuestionTrueFalses.FirstOrDefault().ComplementaryAnswer,
                    QuestionTrueFalseItems = new List<QuestionTrueFalseItem>()
                };
                foreach (var qtf in sqRVM.QuestionTrueFalses.FirstOrDefault().QuestionTrueFalseItems)
                    specialquestion.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
                    {
                        QuestionType = QuestionType.TrueFalse,
                        Text = qtf.Text,
                        Value = qtf.Value
                    });
                db.QuestionTrueFalse.Add(specialquestion);
            }
            if (sqRVM.QuestionWrittenAnswers != null)
            {
                var specialquestion = new QuestionWrittenAnswer()
                {
                    Difficulty = sqRVM.Difficulty,
                    SubUnity = sqRVM.SubUnity_Id != null ? db.SubUnity.FirstOrDefault(e => e.Status && e.Id == sqRVM.SubUnity_Id) : null,
                    Order = '1',
                    QuestionType = QuestionType.WrittenAnswer,
                    Name = sqRVM.QuestionWrittenAnswers.FirstOrDefault().Name,
                    QuestionWrittenAnswerItem = new QuestionWrittenAnswerItem()
                    {
                        QuestionType = QuestionType.WrittenAnswer,
                        Answer = sqRVM.QuestionWrittenAnswers.FirstOrDefault().QuestionWrittenAnswerItem.Answer ?? ""
                    }
                };
                db.QuestionWrittenAnswer.Add(specialquestion);

            }
            
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private void AssociateQuestion_TrueFalse(List<QuestionTrueFalse> local_qtfs, TestRequestViewModel test)
        {
            if (local_qtfs == null) return;

            var qtfs = test.QuestionTrueFalses;

            foreach (var qtf in qtfs)
            {
                var local_qtf = local_qtfs.FirstOrDefault(p => p.Id == qtf.Id);
                qtf.Status = local_qtf != null;

                if (qtf.Status)
                {
                    qtf.Order = local_qtf.Order;
                    qtf.Name = local_qtf.Name;
                    qtf.Difficulty = local_qtf.Difficulty;
                    qtf.ComplementaryAnswer = local_qtf.ComplementaryAnswer;

                    foreach (var qtfi in qtf.QuestionTrueFalseItems)
                    {
                        var local_qtfi = local_qtf.QuestionTrueFalseItems.FirstOrDefault(p => p.Id == qtfi.Id);
                        qtfi.Status = local_qtfi != null;

                        if (local_qtfi != null)
                        {
                            qtfi.Text = local_qtfi.Text;
                            qtfi.Value = local_qtfi.Value;
                        }
                    }

                    foreach (var local_qtfi in local_qtf.QuestionTrueFalseItems)
                        if (local_qtfi.Id == 0 || !qtf.QuestionTrueFalseItems.Any(p => p.Id == local_qtfi.Id))
                            qtf.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
                            {
                                QuestionType = QuestionType.TrueFalse,
                                Text = local_qtfi.Text,
                                Value = local_qtfi.Value
                            });
                }
            }

            foreach (var local_qtf in local_qtfs)
                if (local_qtf.Id == 0 || !qtfs.Any(p => p.Id == local_qtf.Id))
                {
                    var qtf = new QuestionTrueFalse()
                    {
                        QuestionType = QuestionType.TrueFalse,
                        Order = local_qtf.Order,
                        Name = local_qtf.Name,
                        Difficulty = local_qtf.Difficulty,
                        ComplementaryAnswer = local_qtf.ComplementaryAnswer,
                        QuestionTrueFalseItems = new List<QuestionTrueFalseItem>()
                    };

                    if (local_qtf.QuestionTrueFalseItems != null)
                        foreach (var local_qtfi in local_qtf.QuestionTrueFalseItems)
                            qtf.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
                            {
                                QuestionType = QuestionType.TrueFalse,
                                Text = local_qtfi.Text,
                                Value = local_qtfi.Value
                            });
                    else
                        continue;
                }
        }

        private void AssociateQuestion_WrittenAnswer(List<QuestionWrittenAnswer> local_qwas, TestRequestViewModel test)
        {
            if (local_qwas == null) return;

            var qwas = test.QuestionWrittenAnswers;

            foreach (var qwa in qwas)
            {
                var local_qwa = local_qwas.FirstOrDefault(p => p.Id == qwa.Id);
                qwa.Status = local_qwa != null;

                if (local_qwa != null)
                {
                    qwa.Order = local_qwa.Order;
                    qwa.Name = local_qwa.Name;
                    qwa.Difficulty = local_qwa.Difficulty;
                    qwa.QuestionWrittenAnswerItem.Answer = local_qwa.QuestionWrittenAnswerItem.Answer ?? "";
                }
            }
            /*
            foreach (var local_qwa in local_qwas)
                if (local_qwa.Id == 0 || !qwas.Any(p => p.Id == local_qwa.Id))
                    test.SpecialQuestions.Add(new QuestionWrittenAnswer()
                    {
                        QuestionType = QuestionType.WrittenAnswer,
                        Order = local_qwa.Order,
                        Name = local_qwa.Name,
                        Difficulty = 1,
                        QuestionWrittenAnswerItem = new QuestionWrittenAnswerItem()
                        {
                            QuestionType = QuestionType.WrittenAnswer,
                            Answer = local_qwa.QuestionWrittenAnswerItem.Answer ?? ""
                        }
                    });
                    */
        }
        private void AssociateQuestion_QuestionPairedWords(List<QuestionPairedWord> local_qtws, TestRequestViewModel test)
        {
            if (local_qtws == null) return;

            var qtws = test.QuestionPairedWords;

            foreach (var qtw in qtws)
            {
                var local_qtw = local_qtws.FirstOrDefault(p => p.Id == qtw.Id);
                qtw.Status = local_qtw != null;

                if (qtw.Status)
                {
                    qtw.Order = local_qtw.Order;
                    qtw.Name = local_qtw.Name;
                    qtw.Difficulty = local_qtw.Difficulty;
                    qtw.ComplementaryAnswer = local_qtw.ComplementaryAnswer;

                    foreach (var qtwi in qtw.QuestionPairedWordItems)
                    {
                        var local_qtwi = local_qtw.QuestionPairedWordItems.FirstOrDefault(p => p.Id == qtwi.Id);
                        qtwi.Status = local_qtwi != null;

                        if (local_qtwi != null)
                        {
                            qtwi.Column_1_Text = local_qtwi.Column_1_Text;
                            qtwi.Column_1_Alternative = local_qtwi.Column_1_Alternative;
                            qtwi.Column_2_Text = local_qtwi.Column_2_Text;
                            qtwi.Column_2_Index = local_qtwi.Column_2_Index;
                        }
                    }

                    foreach (var local_qtwi in local_qtw.QuestionPairedWordItems)
                        if (local_qtwi.Id == 0 || !qtw.QuestionPairedWordItems.Any(p => p.Id == local_qtwi.Id))
                            qtw.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
                            {
                                QuestionType = QuestionType.PairedWords,
                                Column_1_Text = local_qtwi.Column_1_Text,
                                Column_1_Alternative = local_qtwi.Column_1_Alternative,
                                Column_2_Text = local_qtwi.Column_2_Text,
                                Column_2_Index = local_qtwi.Column_2_Index
                            });
                }
            }

            foreach (var local_qtw in local_qtws)
                if (local_qtw.Id == 0 || !qtws.Any(p => p.Id == local_qtw.Id))
                {
                    var qtw = new QuestionPairedWord()
                    {
                        QuestionType = QuestionType.PairedWords,
                        Order = local_qtw.Order,
                        Name = local_qtw.Name,
                        Difficulty = local_qtw.Difficulty,
                        ComplementaryAnswer = local_qtw.ComplementaryAnswer,
                        QuestionPairedWordItems = new List<QuestionPairedWordItem>()
                    };

                    if (local_qtw.QuestionPairedWordItems != null)
                        foreach (var local_qtwi in local_qtw.QuestionPairedWordItems)
                            qtw.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
                            {
                                QuestionType = QuestionType.PairedWords,
                                Column_1_Text = local_qtwi.Column_1_Text,
                                Column_1_Alternative = local_qtwi.Column_1_Alternative,
                                Column_2_Text = local_qtwi.Column_2_Text,
                                Column_2_Index = local_qtwi.Column_2_Index
                            });
                    else
                        continue;

                }
        }

        private void AssociateQuestion_Alternative(List<QuestionAlternative> local_qas, TestRequestViewModel test)
        {
            if (local_qas == null) return;

            var qas = test.QuestionAlternatives;

            foreach (var qa in qas)
            {
                var local_qa = local_qas.FirstOrDefault(p => p.Id == qa.Id);
                qa.Status = local_qa != null;

                if (qa.Status)
                {
                    qa.Order = local_qa.Order;
                    qa.Name = local_qa.Name;
                    qa.Difficulty = local_qa.Difficulty;
                    qa.ComplementaryAnswer = local_qa.ComplementaryAnswer;

                    foreach (var qai in qa.QuestionAlternativeItems)
                    {
                        var local_qai = local_qa.QuestionAlternativeItems.FirstOrDefault(p => p.Id == qai.Id);
                        qai.Status = local_qai != null;

                        if (local_qai != null)
                        {
                            qai.Text = local_qai.Text;
                            qai.Value = local_qai.Value;
                        }
                    }

                    foreach (var local_qai in local_qa.QuestionAlternativeItems)
                        if (local_qai.Id == 0 || !qa.QuestionAlternativeItems.Any(p => p.Id == local_qai.Id))
                            qa.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
                            {
                                QuestionType = QuestionType.Alternatives,
                                Text = local_qai.Text,
                                Value = local_qai.Value
                            });
                }
            }

            foreach (var local_qa in local_qas)
                if (local_qa.Id == 0 || !qas.Any(p => p.Id == local_qa.Id))
                {
                    var qa = new QuestionAlternative()
                    {
                        QuestionType = QuestionType.Alternatives,
                        Order = local_qa.Order,
                        Name = local_qa.Name,
                        Difficulty = local_qa.Difficulty,
                        ComplementaryAnswer = local_qa.ComplementaryAnswer,
                        QuestionAlternativeItems = new List<QuestionAlternativeItem>()
                    };

                    if (local_qa.QuestionAlternativeItems != null)
                        foreach (var local_qai in local_qa.QuestionAlternativeItems)
                            qa.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
                            {
                                QuestionType = QuestionType.Alternatives,
                                Text = local_qai.Text,
                                Value = local_qai.Value
                            });
                    else
                        continue;

                }
        }
    }
}
