﻿
using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using GeneralHelper;
using WebApi.CloneModels;
using System.Web;
using System.Web.Script.Serialization;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class EvaluationRubricController : ApiController
    {
        private InteracticContext db;

        public EvaluationRubricController()
        {
            db = new InteracticContext();
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.EvaluationRubric.Where(x => x.Status), EvaluationRubricCloneHelper.EvaluationRubricMap_0));
        }

        public HttpResponseMessage Get(int id)
        {
            var er = db.EvaluationRubric.FirstOrDefault(e => e.Id == id && e.Status == true);

            if (er != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(er, EvaluationRubricCloneHelper.EvaluationRubricMap_1));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Evaluación no encontrada.");
        }

        [Authorize(Roles = "Profesor")]
        [HttpGet, Route("Api/EvaluationRubric/GetEvaluationsBySubject")]
        public HttpResponseMessage GetEvaluationsBySubject(int subject_id, int course_id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.EvaluationRubric.Where(x => x.Status && x.Subject_Id == subject_id && x.Course_Id == course_id), EvaluationRubricCloneHelper.EvaluationRubricMap_1));
        }

        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Post()
        {
            var form = HttpContext.Current.Request.Form;
            var evaluation = new JavaScriptSerializer().Deserialize<EvaluationRubric>(form["EvaluationRubric"]);
            db.EvaluationRubric.Add(evaluation);
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK, evaluation.Id);
        }

        [Authorize(Roles = "Profesor")]
        [HttpPut]
        [Route("Api/EvaluationRubric/RegisterScoresByStudent")]
        public HttpResponseMessage RegisterScoresByStudent()
        {
            var form = HttpContext.Current.Request.Form;
            var _EvaluationRubric = new JavaScriptSerializer().Deserialize<EvaluationRubric>(form["EvaluationRubric"]);
            var er = db.EvaluationRubric.FirstOrDefault(p => p.Id == _EvaluationRubric.Id);
            foreach (var ers in _EvaluationRubric.EvaluationRubricStudents) {
                var item = db.EvaluationRubricStudent.Where(x=> x.Student_Id == ers.Student_Id && x.EvaluationRubric_Id == ers.EvaluationRubric_Id && x.Criterion_Id == ers.Criterion_Id);
                foreach (var i in item) {
                    db.EvaluationRubricStudent.Remove(i);
                }
            }
            er.EvaluationRubricStudents = _EvaluationRubric.EvaluationRubricStudents;
            db.SaveChanges();
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        
        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Put()
        {
            var form = HttpContext.Current.Request.Form;
            var _evaluation = new JavaScriptSerializer().Deserialize<EvaluationRubric>(form["EvaluationRubric"]);
            var evaluation = db.EvaluationRubric.FirstOrDefault(p =>  p.Status && p.Id == _evaluation.Id);
            evaluation.Name = _evaluation.Name;
            evaluation.Rubric_Id = _evaluation.Rubric_Id;
            evaluation.Description = _evaluation.Description;
            evaluation.Subject_Id = _evaluation.Subject_Id;
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Authorize(Roles = "Profesor")]
        public HttpResponseMessage Delete(int id)
        {
            var er = db.EvaluationRubric.FirstOrDefault(e => e.Id == id);

            if (er != null)
            {
                er.Status = false;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Evaluación no encontrada.");
        }
    }
}
