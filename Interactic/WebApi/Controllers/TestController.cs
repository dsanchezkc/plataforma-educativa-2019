﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using WebApi.CloneModels;
using GeneralHelper;
using System.Web.UI;
using System.Data;
using ClosedXML.Excel;
using ModelEnum;
using ViewModels;
//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Spreadsheet;
//using DocumentFormat.OpenXml;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class TestController : ApiController
    {
        private InteracticContext db;

        public TestController()
        {
            db = new InteracticContext();
           db.Configuration.ProxyCreationEnabled = true;
        }

		public HttpResponseMessage Get(int id)
		{
			var test = db.Test.FirstOrDefault(e => e.Id == id && e.Status);

            if (test != null) {
                if (test.Global == true)
                {
                    switch (GlobalParameters.Rol)
                    {
                        case "Alumno":
                            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(test, TestCloneHelper.TestMap_2));
                        default:
                            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(test, TestCloneHelper.TestMap_4));
                    }
                }
                else {
                    switch (GlobalParameters.Rol)
                    {
                        case "Alumno":
                            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(test, TestCloneHelper.TestMap_2));
                        default:
                            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(test, TestCloneHelper.TestMap_1));
                    }
                }
                
            }
							
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
		}		
        

		/// <summary>
		/// Author AP
		/// Obtiene la lista de Test relacionado al SubUnity
		/// </summary>
		[Route("Api/Test/GetTestsBySubUnity")]
		public object GetTestsBySubUnity(int subunity_id)
		{
			var tests = db.SubUnity.FirstOrDefault(p => p.Id == subunity_id)?.Tests.Where(p => p.Status);

			if (tests != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(tests, TestCloneHelper.TestMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
		}

		

        [Route("Api/Test/GetGlobalTestsBySubUnity")]
        public object GetGlobalTestsBySubUnity(int id, int course_id)
        {
            var testcourse = db.CourseTest.Where(x => x.Course_Id == course_id && x.Test.Subject_Id == id).Select(x => x.Test_Id).ToList();
            var tests = db.Test.Where(x => testcourse.Contains(x.Id) && x.Global == true);
            //var tests = db.Test.Where(x => x.Subject_Id==id && x.Global == true);
            if (tests != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(tests, TestCloneHelper.TestMap_4));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        }


        /// <summary>
        /// Author AP
        /// Entrega el Test asociado a la lista de usuarios con el curso respectivo al Test.
        /// StudentTest no se registra en ningun lado automáticamente, es por eso que se debe consultar el listado 
        /// de usuarios asociados al Curso para poder generar la lista, posteriormente esa lista es registrada en otro proceso.
        /// </summary>
        [Route("Api/Test/GetTestAssociateStudentTests")]
		public HttpResponseMessage GetTestAssociateStudentTests(int test_id)
		{
			var test = db.Test.AsNoTracking().FirstOrDefault(p => p.Id == test_id);
			test.StudentTests.Clear();

			var course = test.SubUnity.Unity.CourseSubject.Course;
			var studentCourses = CloneHelper.CloneListMap(db.StudentCourse.Where(p => p.Course_Id == course.Id).OrderBy(p => p.ListNumber), StudentCourseCloneHelper.StudentCourseMap_1);	

			foreach (var studentCourse in studentCourses)
			{
				studentCourse.User.StudentCourses.Clear();
				studentCourse.User.StudentCourses.Add(studentCourse);

				var studentTest = db.StudentTest.AsNoTracking().FirstOrDefault(p => p.Test.Id == test_id && p.User.Id == studentCourse.User_Id);

				if (studentTest == null)
					test.StudentTests.Add(new StudentTest());			
				else
					test.StudentTests.Add(studentTest);

				test.StudentTests.Last().User = studentCourse.User;
			}

            

			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(test, TestCloneHelper.TestMap_3));
		}

		[Authorize(Roles = "Profesor, Administrador, SubAdministrador")]
		public HttpResponseMessage Post()
		{
			var form = HttpContext.Current.Request.Form;
			var files = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files : null;
			var test = new JavaScriptSerializer().Deserialize<Test>(form["Test"]);


			for (var i = 0; files != null && i < files.Count; i++)
			{
				var key = files.Keys[i];
				var file = files[key];

				if (file != null && file.ContentLength != 0)
				{
					if (file.ContentType.ToLower() != "application/pdf")
						return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

					string nameDocument = Path.GetFileName(file.FileName);
					//string physicalPath = HttpContext.Current.Server.MapPath("~/images/" + ImageName);

					
					var path = @"C:\EvaluandomeResources\Pdfs";
					Directory.CreateDirectory(path);
					var physicalPath = path + @"\" + FileHelper.ChangeNameFile(path, Path.GetFileName(file.FileName));
					file.SaveAs(physicalPath);

					switch (key)
					{
						case "pdf":
							test.NameDocument = nameDocument;
							test.PathDocument = physicalPath;
							break;

						case "pdf_correction":
							test.NameDocumentCorrection = nameDocument;
							test.PathDocumentCorrection = physicalPath;
							break;
					}
					
				}
			}

			test.Questions = new List<Question>();
			((List<Question>)test.Questions).AddRange(test.AchievementIndicator.SelectMany(p => p.AchievementAlternatives.Select(q => q.Question)));

			foreach (var question in test.Questions)
				if (question.Skill != null)
					question.Skill = db.Skill.FirstOrDefault(p => p.Id == question.Skill.Id);

            if (test.SubUnity != null) {
                test.SubUnity = db.SubUnity.FirstOrDefault(p => p.Id == test.SubUnity.Id);
            }

            if (test.Subject != null)
            {
                test.Subject = db.Subject.FirstOrDefault(p => p.Id == test.Subject.Id);
            }

            db.Test.Add(test);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK, test.Id);
		}

		[Authorize(Roles = "Profesor, Administrador, SubAdministrador")]
		public HttpResponseMessage Put()
		{
			var form = HttpContext.Current.Request.Form;
			var files = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files : null;
			var _test = new JavaScriptSerializer().Deserialize<Test>(form["Test"]);
			var test = db.Test.FirstOrDefault(p => p.Id == _test.Id);

			for (var i = 0; files != null && i < files.Count; i++)
			{
				var key = files.Keys[i];
				var file = files[key];

				if (file != null && file.ContentLength != 0)
				{
					if (file.ContentType.ToLower() != "application/pdf")
						return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

					string nameDocument = Path.GetFileName(file.FileName);
					//string physicalPath = HttpContext.Current.Server.MapPath("~/images/" + ImageName);

					
					var path = @"C:\EvaluandomeResources\Pdfs";
					Directory.CreateDirectory(path);
					var physicalPath = path + @"\" + FileHelper.ChangeNameFile(path, Path.GetFileName(file.FileName));
					file.SaveAs(physicalPath);

					switch (key)
					{
						case "pdf":
							test.NameDocument = nameDocument;
							test.PathDocument = physicalPath;
							break;

						case "pdf_correction":
							test.NameDocumentCorrection = nameDocument;
							test.PathDocumentCorrection = physicalPath;
							break;
					}
					
				}
			}

			test.Title = _test.Title;
			test.NumberAlternatives = _test.NumberAlternatives;
			test.SubUnity = db.SubUnity.FirstOrDefault(p => p.Id == _test.SubUnity.Id);
			test.IsPublishedDocumentCorrectionWithRestriction = _test.IsPublishedDocumentCorrectionWithRestriction;

			/***************** Actualizando Question *****************/
			var questions = _test.AchievementIndicator.SelectMany(p => p.AchievementAlternatives.Select(q => q.Question)).ToList();


			foreach (var question in test.Questions)
			{
				var isContain = questions.Any(p => p.Order == question.Order);
				question.Status = isContain;
			}

			foreach (var _question in questions)
			{
				var question = test.Questions.FirstOrDefault(p => p.Order == _question.Order);

				if (question != null)
				{
					question.Choice = _question.Choice;
					question.Order = _question.Order;

					if (_question.Skill == null)
						question.Skill = null;
					else
						question.Skill = db.Skill.FirstOrDefault(p => p.Id == _question.Skill.Id);
					db.SaveChanges();
				}
				else
				{
					_question.Test = test;
					_question.Skill = db.Skill.FirstOrDefault(p => p.Id == _question.Skill.Id);
					test.Questions.Add(_question);
				}
			}

			var _questions = test.Questions.Where(p => !p.Status);

			if (_questions != null)
			{
				foreach (var item in _questions)
					db.AchievementAlternative.RemoveRange(db.AchievementAlternative.Where(p => p.Question.Id == item.Id));
			}
			/*********************************************************/
			/*************** Actualizando Achievements ****************/

			foreach (var indicator in _test.AchievementIndicator)
			{
				foreach (var alternative in indicator.AchievementAlternatives)
				{
					alternative.AchievementIndicator = null;
					alternative.Question = test.Questions.FirstOrDefault(p => p.Order == alternative.Question.Order);
				}				
			}

			//Actualizando las preguntas por Indicador
			foreach (var item in test.AchievementIndicator)
			{
				db.AchievementAlternative.RemoveRange(item.AchievementAlternatives);

				var _item = _test.AchievementIndicator.FirstOrDefault(p => p.Id == item.Id);

				if (_item != null)
				{
					item.Name = _item.Name;
					item.AchievementAlternatives.AddRange(_item.AchievementAlternatives);
				}				
			}
			//Añadiendo los nuevos
			test.AchievementIndicator.AddRange(_test.AchievementIndicator.Where(p => p.Id == 0));
			//Borrando todos los que no tengan ninguna pregunta asociada
			db.AchievementIndicator.RemoveRange(test.AchievementIndicator.Where(p => p.AchievementAlternatives.Count == 0));


			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		/// <summary>
		/// Author AP
		/// Registra la configuración de los Alumnos relacionado con el Test
		/// </summary>
		[Authorize(Roles = "Profesor, Administrador, SubAdministrador")]
		[HttpPost, Route("Api/Test/RegisterConfigurationTestAndUsers")]
		public HttpResponseMessage RegisterConfigurationTestAndUsers([FromBody] Test _test)
		{
			var test = db.Test.FirstOrDefault(p => p.Id == _test.Id);
			test.Timer = (_test.Timer <= 0) ? 0 : _test.Timer;

			foreach (var _studentTest in _test.StudentTests)
			{
				var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == _studentTest.User.Id && p.Test.Id == _test.Id);

				if (studentTest == null)
					db.StudentTest.Add(new StudentTest()
					{
						Test = test,
						User = db.Users.FirstOrDefault(p => p.Id == _studentTest.User.Id),
						Active = _studentTest.Active
					});
				else
					studentTest.Active = _studentTest.Active;				
			}

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

        
        [Authorize(Roles = "Profesor, Administrador, SubAdministrador")]
		public HttpResponseMessage Delete(int id)
		{
			var test = db.Test.FirstOrDefault(e => e.Id == id);

			if (test != null)
			{
				test.Status = false;
				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
		}
        

        [Authorize(Roles = "Profesor, Administrador, SubAdministrador")]
        [HttpPost, Route("Api/Test/RestartTest")]
        public HttpResponseMessage RestartTest(int test)
        {
            var testStudent = db.StudentTest.FirstOrDefault(e => e.Id == test);
            var answers = db.Answer.Where(e => e.Question.Test.Id == testStudent.Test.Id && e.User_Id == testStudent.User.Id);

            if (testStudent != null)
            {
                testStudent.Done = false;
                if (answers != null)
                {
                    foreach (var answer in answers)
                    {
                        db.Answer.Remove(answer);
                    }
                }
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }           
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        }

        //[HttpPost]
        //[Authorize(Roles = "Administrador")]
        //public void DownloadExcelAsistence()
        //{
        //    using (SpreadsheetDocument document = SpreadsheetDocument.Create("Hola", SpreadsheetDocumentType.Workbook))
        //    {
        //        WorkbookPart workbookPart = document.AddWorkbookPart();
        //        workbookPart.Workbook = new Workbook();

        //        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
        //        worksheetPart.Worksheet = new Worksheet(new SheetData());

        //        Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

        //        Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Test Sheet" };

        //        sheets.Append(sheet);

        //        workbookPart.Workbook.Save();
        //    }
        //}

        //[HttpPost]
        //[Authorize(Roles = "Administrador")]
        //public void DownloadExcelAsistence()
        //{
        //    using (SpreadsheetDocument document = SpreadsheetDocument.Create("Hola", SpreadsheetDocumentType.Workbook))
        //    {
        //        WorkbookPart workbookPart = document.AddWorkbookPart();
        //        workbookPart.Workbook = new Workbook();

        //        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
        //        worksheetPart.Worksheet = new Worksheet(new SheetData());

        //        Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

        //        Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Test Sheet" };

        //        sheets.Append(sheet);

        //        workbookPart.Workbook.Save();
        //    }
        //}



        [HttpGet]
		[Route("Api/Test/DownloadPDF")]
		public HttpResponseMessage DownloadPDF(int test_id)
		{
			var test = db.Test.FirstOrDefault(p => p.Id == test_id);

			if (test == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");

			try
			{
				var binary = FileHelper.GetBinaryFile(test.PathDocument);
				string base64PDF = Convert.ToBase64String(binary, 0, binary.Length);

				return Request.CreateResponse(HttpStatusCode.OK, base64PDF);
			}
			catch (Exception)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, "Aún no ha sido subo el test.");
			}
		}

		[HttpGet]
		[Route("Api/Test/DownloadPDFCorrection")]
		public HttpResponseMessage DownloadPDFCorrection(int test_id)
		{
			var test = db.Test.AsNoTracking().FirstOrDefault(p => p.Id == test_id);

			if (test == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");

			if (GlobalParameters.Rol=="Alumno" && test.IsPublishedDocumentCorrectionWithRestriction)
			{
				var course = db.Course.AsNoTracking().FirstOrDefault(p => p.Id == test.SubUnity.Unity.CourseSubject.Course_Id);
				if (test.StudentTests.Count(p => p.Status && p.Done) != course.StudentCourses.Count())
					return Request.CreateResponse(HttpStatusCode.Unauthorized, "Acceso denegado - Temporal.");
			}

			try
			{
				var binary = FileHelper.GetBinaryFile(test.PathDocumentCorrection);
				string base64PDF = Convert.ToBase64String(binary, 0, binary.Length);

				return Request.CreateResponse(HttpStatusCode.OK, base64PDF);
			}
			catch (Exception)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, "Aún no ha sido suba la corrección.");
			}
		}


        [Route("api/test/testForEducationalEstablishment/count")]
        public IHttpActionResult GetStudentForEducationalEstablishment()
        {
            var count = db.Test.Where(x => x.SubUnity.Unity.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId 
            && x.Status && x.SubUnity.Status).ToList().Count;

            return Ok(count);
        }



        /// <summary>
        /// Author JI
        /// Carga datos a tabla de filtros para generar informe rendimiento por test.
        /// </summary>
        [Route("api/test/report/GetCalificationsByTestAndStudent")]
        public IHttpActionResult GetCalificationsByTestAndStudent(int testId, string studentId)
        {
            var studentCourse = db.StudentTest.Where(x => x.User.Id == studentId && x.Test.Id == testId && x.Test.Status);

            if (studentCourse == null)
                return Ok("El estudiante no ha realizado el test");

            var countCorrectAnswers = db.Answer.Where(x => x.Question.Test.Id == testId && 
                          x.User_Id == studentId && x.Choice == x.Question.Choice).ToList().Count();

            var countBadAnswers = db.Answer.Where(x => x.Question.Test.Id == testId &&
                         x.User_Id == studentId && x.Choice != x.Question.Choice).ToList().Count();

            var countQuestions = db.Question.Where(x => x.Test.Id == testId && x.Status).ToList().Count();

            var calification = countQuestions > 0 ? ((double)countCorrectAnswers / (double)countQuestions) * 100 : 0;

           // calification = countBadAnswers > 0 && countCorrectAnswers == 0 ? -1 : calification;

            return Ok(calification);
        }
 
        [Route("api/test/report/GetCalificationsByTest")]
        public IHttpActionResult GetCalificationsByTest(int testId)
        {
            var anomStudentCourse = db.StudentTest.Where(x => x.Test.Id == testId && x.Test.Status).Select(x => new { x.User, x.Done}).ToList();
            var anomCourse = db.Test.Where(x => x.Id == testId).Select(x => x.SubUnity.Unity.CourseSubject.Course).SingleOrDefault();
            List<ReportTableDto> userCalifications = new List<ReportTableDto>();
            foreach (var anom in anomStudentCourse)
            {
                var countCorrectAnswers = db.Answer.Where(x => x.Question.Test.Id == testId &&
                              x.User_Id == anom.User.Id && x.Choice == x.Question.Choice).ToList().Count();

                var countQuestions = db.Question.Where(x => x.Test.Id == testId && x.Status).ToList().Count();
                var calification = countQuestions > 0 ? ((double)countCorrectAnswers / (double)countQuestions) * 100 : 0;
                var studentList = db.StudentCourse.Where(x => x.User_Id == anom.User.Id && x.Course.Id == anomCourse.Id).Select(x => x.ListNumber).SingleOrDefault();
                ReportTableDto userCalification = new ReportTableDto() {Id = anom.User.Id, List = studentList, Name = anom.User.Name, LastNameP = anom.User.FatherLastName, LastNameM = anom.User.MotherLastName,
                    Calification = calification, isDone = anom.Done };

                userCalifications.Add(userCalification);
            }

            return Ok(userCalifications);
        }


        /// <summary>
        /// Author JI
        /// Carga datos a tabla de filtros para generar informe rendimiento por test.
        /// </summary>
        [Route("api/test/report/table")]
        public IHttpActionResult GetStudentByCourse(string _coursId, string _subjecId, string _unity, string _subunity, string _teachId = null)
        {
            string teacher = "";
            if (_teachId == null)
                teacher = GlobalParameters.UserId;
            else
                teacher = Encryptor.Decrypt(_teachId);

            var courseId = Int32.Parse(Encryptor.Decrypt(_coursId));
            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));
            var unityId = Int32.Parse(Encryptor.Decrypt(_unity));
            var subunityId = Int32.Parse(Encryptor.Decrypt(_subunity));

            var CourseSubject = db.TeacherCourseSubject
                                        .Where(x => x.User.Id == teacher &&
                                                    x.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
                                                    x.CourseSubject.Course.Id == courseId &&
                                                    x.CourseSubject.Subject.Id == subjectId
                                                )
                                        .Select(x => x.CourseSubject)
                                        .SingleOrDefault();

            if (CourseSubject == null) return BadRequest();

            var tests = db.Test.Where(x => x.Status).Include(x => x.Questions).Include(x => x.StudentTests).Where(x => x.SubUnity.Id == subunityId &&
                                                     x.SubUnity.Unity.Id == unityId &&
                                                     x.SubUnity.Unity.CourseSubject.Course_Id == CourseSubject.Course_Id &&
                                                     x.SubUnity.Unity.CourseSubject.Subject_Id == CourseSubject.Subject_Id
                                                ).ToList();

            if (tests == null) return BadRequest();



            var select = new List<ReportTableDto>();
            foreach (var item in tests)
            {
                var totalStudents = 0;
                if (item.StudentTests != null) totalStudents = item.StudentTests.Count;

                var totalQuestions = 0;
                if (item.Questions != null) totalQuestions = item.Questions.Where(p => p.Status).Count();

                select.Add(new ReportTableDto()
                {
                    Test_Id = item.Id,
                    Id = Encryptor.Encrypt(item.Id.ToString()),
                    Name = item.Title,
                    TotalQuestions = totalQuestions,
                    TotalStudents = totalStudents
                });
            }
            return Ok(select);
        }

        /// <summary>
        /// Author JI
        /// Carga select utilizado para generacion de informes.
        /// </summary>
        [Route("api/testForStudent/report/table")]
        public IHttpActionResult GetUnityForStudent(string _subjecId, string _unity, string _subUnity, string _studentId = null)
        {
            var studentId = Encryptor.Decrypt(_studentId) ?? GlobalParameters.UserId;

            var subjectId = Int32.Parse(Encryptor.Decrypt(_subjecId));
            var unityId = Int32.Parse(Encryptor.Decrypt(_unity));
            var subunityId = Int32.Parse(Encryptor.Decrypt(_subUnity));

            var tests = db.Test.Where(x => x.SubUnity.Id == subunityId &&
                                           x.SubUnity.Unity.Id == unityId &&
                                           x.SubUnity.Unity.CourseSubject.Subject_Id == subjectId &&
                                           x.Status
                                      )
                                     .ToList();

            if (tests == null) return BadRequest();


            var select = new List<ReportTableDto>();
            foreach (var item in tests)
            {
                var performance = 0;
                foreach (var question in item.Questions.Where(p => p.Status))
                {
                    foreach (var answer in question.Answers.Where(x => x.User.Id == studentId))
                    {
                        if (question.Choice == answer.Choice)
                        {
                            performance++;
                        }
                    }
                }
                performance = (performance * 100) / item.Questions.Where(p => p.Status).Count();
                var date = DateTime.Now.ToString("dd/MM/yyyy");
                if (performance != 0)
                {
                    select.Add(new ReportTableDto()
                    {
                        Id = Encryptor.Encrypt(item.Id.ToString()),
                        Name = item.Title,
                        Date = date,
                        Performance = performance,
                        Test_Id = item.Id
                    });
                }
            }
            return Ok(select);
        }


        //[Authorize(Roles = "Alumno")]
        //[Route("Api/Test/GetComplementaryAnswers")]
        //[HttpGet]
        //public HttpResponseMessage GetComplementaryAnswers(int test_id)
        //{
        //    var studentTest = db.StudentTest.FirstOrDefault(p => p.Test.Status && p.Test.Id == test_id && p.User.Id == GlobalParameters.UserId);

        //    if (studentTest == null || !studentTest.Done)
        //        return Request.CreateResponse(HttpStatusCode.Unauthorized, "Acceso denegado a respuestas complementarias.");

        //    var listAnswer = new List<object>();

        //    foreach (var question in studentTest.Test.Questions)
        //    {
        //        switch (question.QuestionType)
        //        {
        //            case QuestionType.WrittenAnswer:
        //                var questionWrittenAnswerItem = ((QuestionWrittenAnswer)question).QuestionWrittenAnswerItem;

        //                if (questionWrittenAnswerItem == null || questionWrittenAnswerItem.Answer == null || questionWrittenAnswerItem.Answer.Trim() == "")
        //                    break;

        //                listAnswer.Add(new { Id = questionWrittenAnswerItem.Id, Type = question.QuestionType, Answer = questionWrittenAnswerItem.Answer });
        //                break;

        //            case QuestionType.Alternatives:
        //            case QuestionType.PairedWords:
        //            case QuestionType.TrueFalse:
        //                if (question.ComplementaryAnswer == null || question.ComplementaryAnswer.Trim() == "")
        //                    break;

        //                listAnswer.Add(new { Id = question.Id, Type = question.QuestionType, Answer = question.ComplementaryAnswer });
        //                break;
        //        }
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, listAnswer);
        //}


        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //[Route("Api/Test/GetAnswerTestsBySubUnityAndUser")]
        //public HttpResponseMessage GetAnswerTestsBySubUnityAndUser(int subunity_id, string user_id)
        //{
        //    IQueryable<CourseSubject> courseSubjects = null;
        //    var subUnity = db.SubUnity.FirstOrDefault(p => p.Status && p.Id == subunity_id);
        //    var user = db.Users.FirstOrDefault(p => p.Status && p.Id == user_id);

        //    if (subUnity == null || user == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound);

        //    switch (GlobalParameters.Rol)
        //    {
        //        case "Profesor":
        //            courseSubjects = db.TeacherCourseSubject.AsNoTracking().Where(p =>
        //                p.Status &&
        //                p.User.Id == GlobalParameters.UserId &&
        //                p.CourseSubject.Course_Id == subUnity.Unity.CourseSubject.Course_Id &&
        //                p.CourseSubject.Subject_Id == subUnity.Unity.CourseSubject.Subject_Id &&
        //                p.CourseSubject.Course.Year == GlobalParameters.Year).Select(p => p.CourseSubject);
        //            break;

        //        case "Administrador":
        //        case "SubAdministrador":
        //            courseSubjects = db.CourseSubject.AsNoTracking().Where(p =>
        //                p.Status &&
        //                p.Course_Id == subUnity.Unity.CourseSubject.Course_Id &&
        //                p.Subject_Id == subUnity.Unity.CourseSubject.Subject_Id &&
        //                p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
        //                p.Course.Year == GlobalParameters.Year);
        //            break;
        //    }

        //    var studentAnswers = new List<StudentAnswer>();

        //    foreach (var courseSubject in courseSubjects)
        //        foreach (var studentAnswer in db.StudentAnswer.AsNoTracking().Where(p => p.Status &&
        //        p.QuestionItem.QuestionType == QuestionType.WrittenAnswer &&
        //        p.StudentTest.Test.SubUnity.Unity.CourseSubject.Course_Id == courseSubject.Course_Id &&
        //        p.StudentTest.Test.SubUnity.Unity.CourseSubject.Subject_Id == courseSubject.Subject_Id &&
        //        p.StudentTest.Test.Status))
        //        {
        //            var action = db.Action.AsNoTracking().FirstOrDefault(p => p.Status && p.Task_Id == studentAnswer.StudentTest.Test.Id);

        //            if (action == null || !action.Activity.Status || !action.Activity.SubUnity.Status) continue;

        //            studentAnswers.Add(studentAnswer);
        //        }

        //    var activities = db.Activity.AsNoTracking().Where(p => p.Status && p.SubUnity.Id == subunity_id).SelectMany(p => p.Actions);

        //    return GetPendingTestResponseViewModel(studentAnswers.Where(p => p.StudentTest.User.Id == user_id && activities.Any(q => q.Task_Id == p.StudentTest.Test.Id)));
        //}


        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //[Route("Api/Test/GetPendingTests")]
        //public HttpResponseMessage GetPendingTests()
        //{
        //    return GetPendingTestResponseViewModel(_GetPendingTests());
        //}

        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //[Route("Api/Test/GetPendingTestsCount")]
        //public HttpResponseMessage GetPendingTestsCount()
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, _GetPendingTests().Count());
        //}

        //private List<StudentAnswer> _GetPendingTests()
        //{
        //    IQueryable<CourseSubject> courseSubjects = null;

        //    switch (GlobalParameters.Rol)
        //    {
        //        case "Profesor":
        //            courseSubjects = db.TeacherCourseSubject.AsNoTracking().Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year).Select(p => p.CourseSubject);
        //            break;

        //        case "Administrador":
        //        case "SubAdministrador":
        //            courseSubjects = db.CourseSubject.AsNoTracking().Where(p => p.Status && p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Course.Year == GlobalParameters.Year);
        //            break;
        //    }

        //    var studentAnswers = new List<StudentAnswer>();

        //    foreach (var courseSubject in courseSubjects)
        //    {
        //        foreach (var studentAnswer in db.StudentAnswer.AsNoTracking().Where(p => p.Status &&
        //                                                                        p.QuestionItem.QuestionType == QuestionType.WrittenAnswer && p.Qualification <= 0 &&
        //                                                                        p.StudentTest.Test.SubUnity.Unity.CourseSubject.Course_Id == courseSubject.Course_Id &&
        //                                                                        p.StudentTest.Test.SubUnity.Unity.CourseSubject.Subject_Id == courseSubject.Subject_Id &&
        //                                                                        p.StudentTest.Test.Status))
        //        {
        //            var action = db.Action.AsNoTracking().FirstOrDefault(p => p.Status && p.Task_Id == studentAnswer.StudentTest.Test.Id);

        //            if (action == null || !action.Activity.Status || !action.Activity.SubUnity.Status) continue;
        //            studentAnswers.Add(studentAnswer);
        //        }
        //    }

        //    return studentAnswers;
        //}

        //private HttpResponseMessage GetPendingTestResponseViewModel(IEnumerable<StudentAnswer> studentAnswers)
        //{
        //    var pendingTestRVMs = new List<PendingTestResponseViewModel>();

        //    ///La carga de información es extremadamente grande. Se usan los parametros del header para enviar solamente 
        //    ///una parte de todo.
        //    foreach (var studentAnswer in studentAnswers.Skip(GlobalParameters.ResultPag * GlobalParameters.ResultLimit).Take(GlobalParameters.ResultLimit))
        //    {
        //        PendingTestResponseViewModel pendingTestResponseViewModel = null;
        //        pendingTestRVMs.Add(pendingTestResponseViewModel = new PendingTestResponseViewModel()
        //        {
        //            SubUnity = db.Action.FirstOrDefault(p => p.Status && p.Task_Id == studentAnswer.StudentTest.Test.Id).Activity?.SubUnity,
        //            CourseSubject = studentAnswer.StudentTest.Test.CourseSubject,
        //            QuestionWrittenAnswer = (studentAnswer.QuestionItem as QuestionWrittenAnswerItem)?.QuestionWrittenAnswer,
        //            StudentAnswer = studentAnswer
        //        });

        //        //Eliminando Pregunta - Puede contener un peso demasiado elevado para enviar.
        //        //Solamente enviar cuando se carga en un formulario directo.
        //        if (pendingTestResponseViewModel.QuestionWrittenAnswer != null)
        //            pendingTestResponseViewModel.QuestionWrittenAnswer.Name = "";

        //    }

        //    ///Enviamos la nueva información.
        //    ResponseParameters.ResultLimit = GlobalParameters.ResultLimit;
        //    ResponseParameters.ResultTotal = studentAnswers.Count();
        //    ResponseParameters.ResultPag = GlobalParameters.ResultPag;

        //    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(pendingTestRVMs, PendingTestResponseViewModelCloneHelper.PendingTestResponseViewModelMap_1));
        //}

        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //[Route("Api/Test/RegisterQualification")]
        //[HttpPost]
        //public HttpResponseMessage RegisterQualification([FromBody] StudentAnswer _studentAnswer)
        //{

        //    var studentAnswer = db.StudentAnswer.FirstOrDefault(p => p.QuestionItem_Id == _studentAnswer.QuestionItem_Id &&
        //    p.Test_Id == _studentAnswer.Test_Id && p.User_Id == _studentAnswer.User_Id);

        //    if (studentAnswer == null)
        //        return Request.CreateResponse(HttpStatusCode.NotFound);

        //    //Validando acceso
        //    switch (GlobalParameters.Rol)
        //    {
        //        case "Profesor":
        //            if (!db.TeacherCourseSubject.AsNoTracking().Any(p =>
        //                p.Status &&
        //                p.CourseSubject.Course.Year == GlobalParameters.Year &&
        //                p.User.Id == GlobalParameters.UserId &&
        //                p.Course_Id == studentAnswer.StudentTest.Test.SubUnity.Unity.CourseSubject.Course_Id &&
        //                p.Subject_Id == studentAnswer.StudentTest.Test.SubUnity.Unity.CourseSubject.Subject_Id))
        //                return Request.CreateResponse(HttpStatusCode.Unauthorized);
        //            break;

        //        case "SubAdministrador":
        //            if (!db.CourseSubject.AsNoTracking().Any(p =>
        //                 p.Status &&
        //                 p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId &&
        //                 p.Course.Year == GlobalParameters.Year &&
        //                 p.Course_Id == studentAnswer.StudentTest.Test.SubUnity.Unity.CourseSubject.Course_Id &&
        //                 p.Subject_Id == studentAnswer.StudentTest.Test.SubUnity.Unity.CourseSubject.Subject_Id))
        //                return Request.CreateResponse(HttpStatusCode.Unauthorized);
        //            break;
        //    }

        //    if (_studentAnswer.Qualification < 0)
        //        _studentAnswer.Qualification = 0;

        //    if (_studentAnswer.Qualification > 100)
        //        _studentAnswer.Qualification = 100;

        //    studentAnswer.Qualification = _studentAnswer.Qualification;
        //    db.SaveChanges();

        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}

        //[Authorize(Roles = "Alumno")]
        //[HttpPost]
        //[Route("Api/Test/RegisterStudentQuestions")]
        //public HttpResponseMessage RegisterStudentQuestions([FromBody] TestAnswersRequestViewModel testARVM)
        //{
        //    var studentTest = db.StudentTest.FirstOrDefault(p => p.Test.Status && p.Test.Id == testARVM.Test_Id && p.User.Id == GlobalParameters.UserId);

        //    if (studentTest == null)
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, "Test no asociado.");

        //    var test = studentTest.Test;
        //    var user = studentTest.User;

        //    if (studentTest.Done)
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, "Este Test ya se encuentra respondido.");

        //    if (test.Timer != 0 && DateTime.Now > studentTest.Start.AddMinutes(test.Timer + 1))
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, "Tiempo terminado. No es posible registrar.");

        //    db.StudentAnswer.RemoveRange(db.StudentAnswer.Where(p => p.StudentTest.User.Id == studentTest.User.Id && p.StudentTest.Test.Id == studentTest.Test.Id));

        //    try
        //    {
        //        //Preguntas de desarrollo
        //        foreach (var qwai in testARVM.QuestionWrittenAnswerItems)
        //        {
        //            studentTest.StudentAnswers.Add(new StudentAnswer()
        //            {
        //                QuestionItem = db.QuestionWrittenAnswerItem.FirstOrDefault(p => p.QuestionWrittenAnswer.Test.Id == test.Id && p.Id == qwai.Id),
        //                Answer = qwai.Answer,
        //            });
        //        }

        //        //Preguntas de alternativa
        //        foreach (var qai in testARVM.QuestionAlternativeItems)
        //        {
        //            studentTest.StudentAnswers.Add(new StudentAnswer()
        //            {
        //                QuestionItem = db.QuestionAlternativeItem.FirstOrDefault(p => p.QuestionAlternative.Test.Id == test.Id && p.Id == qai.Id),
        //                Answer = qai.Value.ToString()
        //            });
        //        }

        //        //Preguntas de terminos pariados
        //        foreach (var qpwi in testARVM.QuestionPairedWordItems)
        //        {
        //            studentTest.StudentAnswers.Add(new StudentAnswer()
        //            {
        //                QuestionItem = db.QuestionPairedWordItem.FirstOrDefault(p => p.QuestionPairedWord.Test.Id == test.Id && p.Id == qpwi.Id),
        //                Answer = qpwi.Column_1_Alternative.ToString()
        //            });
        //        }

        //        //Preguntas de verdadero/falso
        //        foreach (var qtfi in testARVM.QuestionTrueFalseItems)
        //        {
        //            studentTest.StudentAnswers.Add(new StudentAnswer()
        //            {
        //                QuestionItem = db.QuestionTrueFalseItem.FirstOrDefault(p => p.QuestionTrueFalse.Test.Id == test.Id && p.Id == qtfi.Id),
        //                Answer = qtfi.Value.ToString()
        //            });
        //        }

        //        studentTest.Done = true;
        //        db.SaveChanges();
        //    }
        //    catch (Exception)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, "El test no ha podido ser procesado.");
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, GetQualification(studentTest));
        //}


        ///// <summary>
        ///// La nota puede ser como mínimo un 1 y como máximo un 100.
        ///// -1 indica que no se puede calcular la nota(Por ejemplo el test tenga preguntas con desarrollo).
        ///// -2 indica que se ha intentado revisar las respuestas de desarrollo pero aún no han sido evaluadas por el profesor.
        ///// </summary>
        //public static double GetQualification(StudentTest studentTest, bool includeWrittenAnswer = false)
        //{
        //    var test = studentTest.Test;
        //    var studentAnswer = studentTest.StudentAnswers;

        //    if (!includeWrittenAnswer)
        //        if (test.Questions.Any(p => p.Status && p.QuestionType == QuestionType.WrittenAnswer))
        //            return -1;

        //    var count = 0.0;
        //    var countQuestionItem = 0;

        //    foreach (var question in test.Questions.Where(p => p.Status))
        //    {
        //        switch (question.QuestionType)
        //        {
        //            case QuestionType.TrueFalse:
        //                var questionTrueFalseItems = (question as QuestionTrueFalse).QuestionTrueFalseItems.Where(p => p.Status);
        //                countQuestionItem += questionTrueFalseItems.Count();

        //                foreach (var questionItem in questionTrueFalseItems)
        //                {
        //                    var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
        //                    if (item == null) continue;
        //                    count += questionItem.Value.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
        //                }
        //                break;

        //            case QuestionType.Alternatives:
        //                var questionAlternativeItems = (question as QuestionAlternative).QuestionAlternativeItems.Where(p => p.Status);
        //                var questionAlternativeItem = questionAlternativeItems.FirstOrDefault(p => p.Value);
        //                if (questionAlternativeItem == null) continue;
        //                count += studentAnswer.Any(p => p.QuestionItem_Id == questionAlternativeItem.Id) ? 1 : 0;
        //                countQuestionItem += 1;
        //                break;

        //            case QuestionType.PairedWords:
        //                var questionPairedWordItems = (question as QuestionPairedWord).QuestionPairedWordItems.Where(p => p.Status);
        //                countQuestionItem += questionPairedWordItems.Count();

        //                foreach (var questionItem in questionPairedWordItems)
        //                {
        //                    var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
        //                    if (item == null) continue;
        //                    count += questionItem.Column_1_Alternative.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
        //                }
        //                break;

        //            case QuestionType.WrittenAnswer:
        //                var writtenAnswerItem = (question as QuestionWrittenAnswer).QuestionWrittenAnswerItem;
        //                var qualification = studentAnswer.FirstOrDefault(p => p.Status && p.QuestionItem_Id == writtenAnswerItem.Id)?.Qualification;
        //                countQuestionItem++;

        //                if (qualification.HasValue && qualification.Value == 0)
        //                    return -2;

        //                if (!qualification.HasValue)
        //                    continue;

        //                count += (double)qualification.Value / 100.0;
        //                break;
        //        }
        //    }

        //    if (countQuestionItem == 0)
        //        return 0;

        //    return Math.Round((((double)count / (double)countQuestionItem) * 99) + 1, 1);
        //}

        //public static QualificationInfoViewModel GetQualificationWithInfo(StudentTest studentTest)
        //{
        //    var qualificationInfo = new QualificationInfoViewModel();

        //    if (studentTest == null)
        //        return qualificationInfo;

        //    var test = studentTest.Test;
        //    var studentAnswer = studentTest.StudentAnswers;
        //    var value = 0.0;

        //    foreach (var question in test.Questions.Where(p => p.Status))
        //    {
        //        switch (question.QuestionType)
        //        {
        //            case QuestionType.TrueFalse:
        //                var questionTrueFalseItems = (question as QuestionTrueFalse).QuestionTrueFalseItems.Where(p => p.Status);

        //                foreach (var questionItem in questionTrueFalseItems)
        //                {
        //                    var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
        //                    if (item == null)
        //                    {
        //                        qualificationInfo.TrueFalse.AddQualification(0, false);
        //                        continue;
        //                    }
        //                    value = questionItem.Value.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
        //                    qualificationInfo.TrueFalse.AddQualification(value, value == 1);
        //                }
        //                break;

        //            case QuestionType.Alternatives:
        //                var questionAlternativeItems = (question as QuestionAlternative).QuestionAlternativeItems.Where(p => p.Status);
        //                var questionAlternativeItem = questionAlternativeItems.FirstOrDefault(p => p.Value);
        //                if (questionAlternativeItem == null)
        //                {
        //                    qualificationInfo.Alternatives.AddQualification(0, false);
        //                    continue;
        //                }
        //                value = studentAnswer.Any(p => p.QuestionItem_Id == questionAlternativeItem.Id) ? 1 : 0;
        //                qualificationInfo.Alternatives.AddQualification(value, value == 1);
        //                break;

        //            case QuestionType.PairedWords:
        //                var questionPairedWordItems = (question as QuestionPairedWord).QuestionPairedWordItems.Where(p => p.Status);

        //                foreach (var questionItem in questionPairedWordItems)
        //                {
        //                    var item = studentAnswer.FirstOrDefault(p => p.QuestionItem_Id == questionItem.Id);
        //                    if (item == null)
        //                    {
        //                        qualificationInfo.PairedWords.AddQualification(0, false);
        //                        continue;
        //                    }
        //                    value = questionItem.Column_1_Alternative.ToString().ToLower() == item.Answer.ToLower() ? 1 : 0;
        //                    qualificationInfo.PairedWords.AddQualification(value, value == 1);
        //                }
        //                break;

        //            case QuestionType.WrittenAnswer:
        //                var writtenAnswerItem = (question as QuestionWrittenAnswer).QuestionWrittenAnswerItem;
        //                var qualification = studentAnswer.FirstOrDefault(p => p.Status && p.QuestionItem_Id == writtenAnswerItem.Id)?.Qualification;

        //                if (qualification.HasValue && qualification.Value == 0)
        //                {
        //                    qualificationInfo.IsWrittenAnswerPending = true;
        //                    continue;
        //                }

        //                if (!qualification.HasValue)
        //                {
        //                    qualificationInfo.WrittenAnswer.AddQualification(0);
        //                    continue;
        //                }

        //                value = (double)qualification.Value / 100.0;
        //                qualificationInfo.WrittenAnswer.AddQualification(value);
        //                break;
        //        }
        //    }

        //    return qualificationInfo;
        //}


        ///// <summary>
        ///// Valida si es posible manipular el test
        ///// </summary>
        ///// <returns></returns>
        //private bool ValidateTest(Test test)
        //{
        //    if (test == null)
        //        return false;

        //    var activity = db.Action.FirstOrDefault(p => p.Status && p.Task_Id == test.Id)?.Activity;

        //    if (activity == null)
        //        return false;

        //    var course = activity.SubUnity.Unity.CourseSubject.Course;

        //    //Verificamos si el usuario que está pidiendo el Test tiene acceso a el
        //    IQueryable<Course> courses_currentUser = null;
        //    switch (GlobalParameters.Rol)
        //    {
        //        case "Administrador":
        //        case "SubAdministrador":
        //            courses_currentUser = db.Course.AsNoTracking().Where(p => p.Status && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year);
        //            break;

        //        case "Profesor":
        //            courses_currentUser = db.TeacherCourseSubject.AsNoTracking().Where(p => p.User.Id == GlobalParameters.UserId && p.Status && p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.CourseSubject.Course.Year == GlobalParameters.Year).Select(p => p.CourseSubject.Course).Distinct();
        //            break;

        //        case "Alumno":
        //            var user = db.Users.AsNoTracking().FirstOrDefault(p => p.Id == GlobalParameters.UserId);
        //            courses_currentUser = user.StudentCourses.Where(p => p.Status && p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Course.Year == GlobalParameters.Year).Select(q => q.Course).Distinct().AsQueryable();
        //            break;
        //    }

        //    if (courses_currentUser == null)
        //        return false;

        //    if (!courses_currentUser.Any(p => p.Id == course.Id))
        //        return false;

        //    return true;
        //}

        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //[Route("Api/Test/GetTest_Edit")]
        //public HttpResponseMessage GetTest_Edit(int id)
        //{
        //    var test = db.Test.FirstOrDefault(p => p.Status && p.Id == id);

        //    if (!ValidateTest(test))
        //        return Request.CreateResponse(HttpStatusCode.Unauthorized);


        //    var _test = CloneHelper.CloneObject(test) as Test;


        //    if (_test == null)
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError);

        //    var questionWrittenAnswers = CloneHelper.CloneListMap(test.Questions.OfType<QuestionWrittenAnswer>(), QuestionWrittenAnswerCloneHelper.QuestionWrittenAnswerMap_1);
        //    _test.Questions.AddRange(questionWrittenAnswers);

        //    var questionTrueFalses = CloneHelper.CloneListMap(test.Questions.OfType<QuestionTrueFalse>(), QuestionTrueFalseCloneHelper.QuestionTrueFalseMap_1);
        //    _test.Questions.AddRange(questionTrueFalses);

        //    var questionPairedWords = CloneHelper.CloneListMap(test.Questions.OfType<QuestionPairedWord>(), QuestionPairedWordCloneHelper.QuestionPairedWordMap_1);
        //    foreach (var qpw in questionPairedWords)
        //        qpw.QuestionPairedWordItems = qpw.QuestionPairedWordItems.OrderBy(p => p.Column_2_Index).ToList();
        //    _test.Questions.AddRange(questionPairedWords);

        //    var questionAlternatives = CloneHelper.CloneListMap(test.Questions.OfType<QuestionAlternative>(), QuestionAlternativeCloneHelper.QuestionAlternativeMap_1);
        //    _test.Questions.AddRange(questionAlternatives);

        //    _test.Questions = _test.Questions.OrderBy(p => p.Order).ToList();

        //    return Request.CreateResponse(HttpStatusCode.OK, _test);
        //}

        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
        //public HttpResponseMessage Get(int id)
        //{
        //    var test = db.Test.FirstOrDefault(p => p.Status && p.Id == id);

        //    if (!ValidateTest(test))
        //        return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Acceso denegado" });

        //    //Iniciando la evaluación
        //    var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == GlobalParameters.UserId && p.Test.Id == test.Id);
        //    if (studentTest == null && GlobalParameters.Rol == "Alumno")
        //        db.StudentTest.Add(studentTest = new StudentTest()
        //        {
        //            User = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId),
        //            Test = test,
        //            Start = DateTime.Now,
        //            Attempts = 0
        //        });

        //    object info = new { CurrentTime = test.Timer * 60, AttemptsAvailable = 0 };

        //    if (studentTest != null && GlobalParameters.Rol == "Alumno")
        //    {
        //        if (test.Attempts <= studentTest.Attempts)
        //            return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });

        //        var timer = test.Timer * 60 - (DateTime.Now - studentTest.Start).TotalSeconds;
        //        if ((timer <= 0 && test.Timer > 0) || studentTest.Done)
        //        {
        //            studentTest.Attempts++;
        //            studentTest.Start = DateTime.Now;
        //            timer = test.Timer * 60 - (DateTime.Now - studentTest.Start).TotalSeconds;
        //        }

        //        if (test.Attempts <= studentTest.Attempts)
        //        {
        //            db.SaveChanges();
        //            return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });
        //        }

        //        studentTest.Done = false;
        //        db.SaveChanges();

        //        info = new { CurrentTime = timer < 0 ? 0 : timer, AttemptsAvailable = test.Attempts - studentTest.Attempts <= 0 ? 0 : test.Attempts - studentTest.Attempts };
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, new { Error = "", Info = info, Test = TestCloneHelper.GetTestWithoutAnswers(test) });
        //}

        //[Authorize(Roles = "Alumno")]
        //[Route("Api/Test/GetInfo")]
        //public HttpResponseMessage GetInfo(int id)
        //{
        //    var test = db.Test.FirstOrDefault(p => p.Status && p.Id == id);

        //    if (!ValidateTest(test))
        //        return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Acceso denegado" });

        //    var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == GlobalParameters.UserId && p.Test.Id == test.Id);

        //    var timer = 0.0;

        //    if (studentTest != null)
        //    {
        //        if (test.Attempts <= studentTest.Attempts)
        //            return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });

        //        timer = test.Timer * 60 - (DateTime.Now - studentTest.Start).TotalSeconds;

        //        if (studentTest.Done)
        //            timer = -1;

        //        if (studentTest.Done && studentTest.Attempts + 1 >= test.Attempts)
        //            return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });

        //        if (timer <= 0 && test.Timer > 0)
        //            studentTest.Attempts++;

        //        if (test.Timer == 0 && studentTest.Done)
        //            studentTest.Attempts++;

        //        if (test.Attempts <= studentTest.Attempts)
        //            return Request.CreateResponse(HttpStatusCode.OK, new { Error = "Ya has ocupado todos los intentos posibles." });
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK,
        //        new
        //        {
        //            Error = "",
        //            Info = new
        //            {
        //                Timer = test.Timer * 60,
        //                CurrentAttempts = studentTest != null ? test.Attempts - studentTest.Attempts : test.Attempts,
        //                Attempts = test.Attempts,
        //                CurrentTimer = timer,
        //                IsFirtTime = studentTest == null
        //            }
        //        });
        //}


        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //public HttpResponseMessage Post([FromBody] TestRequestViewModel testRVM)
        //{
        //    var activity = db.Activity.FirstOrDefault(p => p.Id == testRVM.Activity_Id);

        //    if (activity == null)
        //        return Request.CreateResponse(HttpStatusCode.BadRequest);

        //    //Verificando por si ya existe otro informe con Nota Final
        //    if (testRVM.Test.IsTestReport)
        //    {
        //        var actions = activity.SubUnity.Activities.Where(p => p.Status).SelectMany(p => p.Actions.Where(q => q.Status && q.Type == ((int)ActionType.Test)));

        //        foreach (var action in actions)
        //        {
        //            var _test = db.Test.AsNoTracking().FirstOrDefault(p => p.Status && p.Id == action.Task_Id && p.IsTestReport);

        //            if (_test != null)
        //                return Request.CreateResponse(HttpStatusCode.BadRequest, "El Test " + _test.Title + ", ya está registrado como Test Final, solamente puede existir un test final por Sub-Unidad.");
        //        }
        //    }

        //    var test = new Test()
        //    {
        //        Title = testRVM.Test.Title,
        //        Timer = testRVM.Test.Timer < 0 ? 0 : testRVM.Test.Timer,
        //        Attempts = testRVM.Test.Attempts < 1 ? 1 : testRVM.Test.Attempts,
        //        IsTestReport = testRVM.Test.IsTestReport,
        //        CourseSubject = activity.SubUnity.Unity.CourseSubject,
        //        SubUnity = activity.SubUnity,
        //        Questions = new List<Question>()
        //    };

        //    if (testRVM.QuestionWrittenAnswers.Count == 0 && testRVM.QuestionTrueFalses.Count == 0 &&
        //        testRVM.QuestionPairedWords.Count == 0 && testRVM.QuestionAlternatives.Count == 0)
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, "Al menos debe existir una pregunta");

        //    //Registrando Respuesta Escrita
        //    AssociateQuestion_WrittenAnswer(testRVM.QuestionWrittenAnswers, test);

        //    //Registrando Verdadero/Falso
        //    AssociateQuestion_TrueFalse(testRVM.QuestionTrueFalses, test);

        //    //Registrando Terminos pariados
        //    AssociateQuestion_QuestionPairedWords(testRVM.QuestionPairedWords, test);

        //    //Registrando Alternativas
        //    AssociateQuestion_Alternative(testRVM.QuestionAlternatives, test);

        //    db.Test.Add(test);
        //    db.SaveChanges();

        //    activity.Actions.Add(new Model.Action()
        //    {
        //        Activity = activity,
        //        Task_Id = test.Id,
        //        Order = activity.Actions.Count == 0 ? 0 : activity.Actions.Max(p => p.Order) + 1,
        //        Type = (int)ActionType.Test
        //    });

        //    db.SaveChanges();

        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}

        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //public HttpResponseMessage Put([FromBody] TestRequestViewModel testRVM)
        //{
        //    var test = db.Test.FirstOrDefault(p => p.Id == testRVM.Test.Id);

        //    if (test == null)
        //        return Request.CreateResponse(HttpStatusCode.BadRequest);

        //    test.Title = testRVM.Test.Title;
        //    test.Timer = testRVM.Test.Timer < 0 ? 0 : testRVM.Test.Timer;
        //    test.Attempts = testRVM.Test.Attempts < 1 ? 1 : testRVM.Test.Attempts;
        //    test.IsTestReport = testRVM.Test.IsTestReport;

        //    //Verificando por si ya existe otro informe con Nota Final
        //    if (test.IsTestReport)
        //    {
        //        var action = db.Action.FirstOrDefault(p => p.Status && p.Task_Id == test.Id);
        //        if (action == null)
        //            return Request.CreateResponse(HttpStatusCode.BadRequest);

        //        foreach (var _action in action.Activity.SubUnity.Activities.Where(p => p.Status).SelectMany(p => p.Actions.Where(q => q.Status && q.Type == ((int)ActionType.Test) && q.Task_Id != test.Id)))
        //        {
        //            var _test = db.Test.AsNoTracking().FirstOrDefault(p => p.Status && p.Id == _action.Task_Id && p.IsTestReport);

        //            if (_test != null)
        //                return Request.CreateResponse(HttpStatusCode.BadRequest, "El Test " + _test.Title + ", ya está registrado como Test Final, solamente puede existir un test final por Sub-Unidad.");
        //        }
        //    }

        //    if (testRVM.QuestionWrittenAnswers.Count == 0 && testRVM.QuestionTrueFalses.Count == 0 &&
        //        testRVM.QuestionPairedWords.Count == 0 && testRVM.QuestionAlternatives.Count == 0)
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, "Al menos debe existir una pregunta");

        //    //Registrando Respuesta Escrita
        //    AssociateQuestion_WrittenAnswer(testRVM.QuestionWrittenAnswers, test);

        //    //Registrando Verdadero/Falso
        //    AssociateQuestion_TrueFalse(testRVM.QuestionTrueFalses, test);

        //    //Registrando Terminos pariados
        //    AssociateQuestion_QuestionPairedWords(testRVM.QuestionPairedWords, test);

        //    //Registrando Alternativas
        //    AssociateQuestion_Alternative(testRVM.QuestionAlternatives, test);

        //    db.SaveChanges();
        //    return Request.CreateResponse(HttpStatusCode.OK);
        //}

        //[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        //public HttpResponseMessage Delete(int id)
        //{
        //    var test = db.Test.FirstOrDefault(e => e.Id == id);

        //    if (test != null)
        //    {
        //        test.Status = false;

        //        var action = db.Action.FirstOrDefault(p => p.Task_Id == test.Id && p.Type == ((int)ActionType.Test));
        //        if (action != null)
        //            action.Status = false;

        //        db.SaveChanges();
        //        return Request.CreateResponse(HttpStatusCode.OK);
        //    }
        //    else
        //        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        //}

        //private void AssociateQuestion_TrueFalse(List<QuestionTrueFalse> local_qtfs, Test test)
        //{
        //    if (local_qtfs == null) return;

        //    var qtfs = test.Questions.OfType<QuestionTrueFalse>();

        //    foreach (var qtf in qtfs)
        //    {
        //        var local_qtf = local_qtfs.FirstOrDefault(p => p.Id == qtf.Id);
        //        qtf.Status = local_qtf != null;

        //        if (qtf.Status)
        //        {
        //            qtf.Order = local_qtf.Order;
        //            qtf.Name = local_qtf.Name;
        //            qtf.ComplementaryAnswer = local_qtf.ComplementaryAnswer;

        //            foreach (var qtfi in qtf.QuestionTrueFalseItems)
        //            {
        //                var local_qtfi = local_qtf.QuestionTrueFalseItems.FirstOrDefault(p => p.Id == qtfi.Id);
        //                qtfi.Status = local_qtfi != null;

        //                if (local_qtfi != null)
        //                {
        //                    qtfi.Text = local_qtfi.Text;
        //                    qtfi.Value = local_qtfi.Value;
        //                }
        //            }

        //            foreach (var local_qtfi in local_qtf.QuestionTrueFalseItems)
        //                if (local_qtfi.Id == 0 || !qtf.QuestionTrueFalseItems.Any(p => p.Id == local_qtfi.Id))
        //                    qtf.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
        //                    {
        //                        QuestionType = QuestionType.TrueFalse,
        //                        Text = local_qtfi.Text,
        //                        Value = local_qtfi.Value
        //                    });
        //        }
        //    }

        //    foreach (var local_qtf in local_qtfs)
        //        if (local_qtf.Id == 0 || !qtfs.Any(p => p.Id == local_qtf.Id))
        //        {
        //            var qtf = new QuestionTrueFalse()
        //            {
        //                QuestionType = QuestionType.TrueFalse,
        //                Order = local_qtf.Order,
        //                Name = local_qtf.Name,
        //                ComplementaryAnswer = local_qtf.ComplementaryAnswer,
        //                QuestionTrueFalseItems = new List<QuestionTrueFalseItem>()
        //            };

        //            if (local_qtf.QuestionTrueFalseItems != null)
        //                foreach (var local_qtfi in local_qtf.QuestionTrueFalseItems)
        //                    qtf.QuestionTrueFalseItems.Add(new QuestionTrueFalseItem()
        //                    {
        //                        QuestionType = QuestionType.TrueFalse,
        //                        Text = local_qtfi.Text,
        //                        Value = local_qtfi.Value
        //                    });
        //            else
        //                continue;

        //            test.Questions.Add(qtf);
        //        }
        //}

        //private void AssociateQuestion_WrittenAnswer(List<QuestionWrittenAnswer> local_qwas, Test test)
        //{
        //    if (local_qwas == null) return;

        //    var qwas = test.Questions.OfType<QuestionWrittenAnswer>();

        //    foreach (var qwa in qwas)
        //    {
        //        var local_qwa = local_qwas.FirstOrDefault(p => p.Id == qwa.Id);
        //        qwa.Status = local_qwa != null;

        //        if (local_qwa != null)
        //        {
        //            qwa.Order = local_qwa.Order;
        //            qwa.Name = local_qwa.Name;
        //            qwa.QuestionWrittenAnswerItem.Answer = local_qwa.QuestionWrittenAnswerItem.Answer ?? "";
        //        }
        //    }

        //    foreach (var local_qwa in local_qwas)
        //        if (local_qwa.Id == 0 || !qwas.Any(p => p.Id == local_qwa.Id))
        //            test.Questions.Add(new QuestionWrittenAnswer()
        //            {
        //                QuestionType = QuestionType.WrittenAnswer,
        //                Order = local_qwa.Order,
        //                Name = local_qwa.Name,
        //                QuestionWrittenAnswerItem = new QuestionWrittenAnswerItem()
        //                {
        //                    QuestionType = QuestionType.WrittenAnswer,
        //                    Answer = local_qwa.QuestionWrittenAnswerItem.Answer ?? ""
        //                }
        //            });
        //}

        //private void AssociateQuestion_QuestionPairedWords(List<QuestionPairedWord> local_qtws, Test test)
        //{
        //    if (local_qtws == null) return;

        //    var qtws = test.Questions.OfType<QuestionPairedWord>();

        //    foreach (var qtw in qtws)
        //    {
        //        var local_qtw = local_qtws.FirstOrDefault(p => p.Id == qtw.Id);
        //        qtw.Status = local_qtw != null;

        //        if (qtw.Status)
        //        {
        //            qtw.Order = local_qtw.Order;
        //            qtw.Name = local_qtw.Name;
        //            qtw.ComplementaryAnswer = local_qtw.ComplementaryAnswer;

        //            foreach (var qtwi in qtw.QuestionPairedWordItems)
        //            {
        //                var local_qtwi = local_qtw.QuestionPairedWordItems.FirstOrDefault(p => p.Id == qtwi.Id);
        //                qtwi.Status = local_qtwi != null;

        //                if (local_qtwi != null)
        //                {
        //                    qtwi.Column_1_Text = local_qtwi.Column_1_Text;
        //                    qtwi.Column_1_Alternative = local_qtwi.Column_1_Alternative;
        //                    qtwi.Column_2_Text = local_qtwi.Column_2_Text;
        //                    qtwi.Column_2_Index = local_qtwi.Column_2_Index;
        //                }
        //            }

        //            foreach (var local_qtwi in local_qtw.QuestionPairedWordItems)
        //                if (local_qtwi.Id == 0 || !qtw.QuestionPairedWordItems.Any(p => p.Id == local_qtwi.Id))
        //                    qtw.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
        //                    {
        //                        QuestionType = QuestionType.PairedWords,
        //                        Column_1_Text = local_qtwi.Column_1_Text,
        //                        Column_1_Alternative = local_qtwi.Column_1_Alternative,
        //                        Column_2_Text = local_qtwi.Column_2_Text,
        //                        Column_2_Index = local_qtwi.Column_2_Index
        //                    });
        //        }
        //    }

        //    foreach (var local_qtw in local_qtws)
        //        if (local_qtw.Id == 0 || !qtws.Any(p => p.Id == local_qtw.Id))
        //        {
        //            var qtw = new QuestionPairedWord()
        //            {
        //                QuestionType = QuestionType.PairedWords,
        //                Order = local_qtw.Order,
        //                Name = local_qtw.Name,
        //                ComplementaryAnswer = local_qtw.ComplementaryAnswer,
        //                QuestionPairedWordItems = new List<QuestionPairedWordItem>()
        //            };

        //            if (local_qtw.QuestionPairedWordItems != null)
        //                foreach (var local_qtwi in local_qtw.QuestionPairedWordItems)
        //                    qtw.QuestionPairedWordItems.Add(new QuestionPairedWordItem()
        //                    {
        //                        QuestionType = QuestionType.PairedWords,
        //                        Column_1_Text = local_qtwi.Column_1_Text,
        //                        Column_1_Alternative = local_qtwi.Column_1_Alternative,
        //                        Column_2_Text = local_qtwi.Column_2_Text,
        //                        Column_2_Index = local_qtwi.Column_2_Index
        //                    });
        //            else
        //                continue;

        //            test.Questions.Add(qtw);
        //        }
        //}

        //private void AssociateQuestion_Alternative(List<QuestionAlternative> local_qas, Test test)
        //{
        //    if (local_qas == null) return;

        //    var qas = test.Questions.OfType<QuestionAlternative>();

        //    foreach (var qa in qas)
        //    {
        //        var local_qa = local_qas.FirstOrDefault(p => p.Id == qa.Id);
        //        qa.Status = local_qa != null;

        //        if (qa.Status)
        //        {
        //            qa.Order = local_qa.Order;
        //            qa.Name = local_qa.Name;
        //            qa.ComplementaryAnswer = local_qa.ComplementaryAnswer;

        //            foreach (var qai in qa.QuestionAlternativeItems)
        //            {
        //                var local_qai = local_qa.QuestionAlternativeItems.FirstOrDefault(p => p.Id == qai.Id);
        //                qai.Status = local_qai != null;

        //                if (local_qai != null)
        //                {
        //                    qai.Text = local_qai.Text;
        //                    qai.Value = local_qai.Value;
        //                }
        //            }

        //            foreach (var local_qai in local_qa.QuestionAlternativeItems)
        //                if (local_qai.Id == 0 || !qa.QuestionAlternativeItems.Any(p => p.Id == local_qai.Id))
        //                    qa.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
        //                    {
        //                        QuestionType = QuestionType.Alternatives,
        //                        Text = local_qai.Text,
        //                        Value = local_qai.Value
        //                    });
        //        }
        //    }

        //    foreach (var local_qa in local_qas)
        //        if (local_qa.Id == 0 || !qas.Any(p => p.Id == local_qa.Id))
        //        {
        //            var qa = new QuestionAlternative()
        //            {
        //                QuestionType = QuestionType.Alternatives,
        //                Order = local_qa.Order,
        //                Name = local_qa.Name,
        //                ComplementaryAnswer = local_qa.ComplementaryAnswer,
        //                QuestionAlternativeItems = new List<QuestionAlternativeItem>()
        //            };

        //            if (local_qa.QuestionAlternativeItems != null)
        //                foreach (var local_qai in local_qa.QuestionAlternativeItems)
        //                    qa.QuestionAlternativeItems.Add(new QuestionAlternativeItem()
        //                    {
        //                        QuestionType = QuestionType.Alternatives,
        //                        Text = local_qai.Text,
        //                        Value = local_qai.Value
        //                    });
        //            else
        //                continue;

        //            test.Questions.Add(qa);
        //        }
        //}

    }
}
