﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using ViewModels;
using System.Web;
using System.Web.Script.Serialization;
using Generic;
using System.IO;
using Enums;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class TopicController : ApiController
	{
		private InteracticContext db;

		public TopicController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

        /*
		public static bool TopicAccess(Model.Action action)
		{
			var db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;

			switch (GlobalParameters.Rol)
			{
				case "Administrador":
					return true;

				case "SubAdministrador":
					return db.EducationalEstablishment.Any(p =>p.Id == GlobalParameters.EducationalEstablishmentId &&  p.Courses.Any(q => q.Id == action.Activity.SubUnity.Unity.CourseSubject.Course_Id));

				case "Profesor":
					return action.Activity.SubUnity.Unity.CourseSubject.TeacherCourseSubjects.Any(p => p.User_Id == GlobalParameters.UserId);

				case "Alumno":
					return action.Activity.SubUnity.Unity.CourseSubject.Course.StudentCourses.Any(p => p.Status && p.User_Id == GlobalParameters.UserId);
			}

			return false;
		}
        */

		public HttpResponseMessage Get(int id)
		{
			var topic = db.Topic.FirstOrDefault(e => e.Status && e.Id == id);

			if (topic != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(topic, TopicCloneHelper.TopicMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Tema no encontrado.");
		}

		[Route("Api/Topic/GetTopicWithPosts")]
		public HttpResponseMessage GetTopicWithPosts(int id)
		{
			var topic = db.Topic.FirstOrDefault(e => e.Status && e.Id == id);

			if (topic != null )
			{
				var _topic = CloneHelper.CloneObjectMap(topic, TopicCloneHelper.TopicMap_2);
				_topic.Posts = _topic.Posts.OrderBy(p => p.EditedAt).ToList();
				return Request.CreateResponse(HttpStatusCode.OK, _topic);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Tema no encontrado.");
		}

		[Route("Api/Topic/GetTopicsResponseViewModelByForum")]
		public HttpResponseMessage GetTopicsResponseViewModelByForum(int forum_id)
		{
			var forum = db.Forum.FirstOrDefault(p => p.Id == forum_id);

			if (forum == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Foro no encontrado.");

			if (forum != null)
			{
				var topicsResponse = new List<TopicResponseViewModel>();

				foreach (var topic in forum.Topics.Where(p => p.Status).OrderByDescending(p => p.EditedAt))
				{
					topicsResponse.Add(new TopicResponseViewModel()
					{
						Topic = CloneHelper.CloneObjectMap(topic, TopicCloneHelper.TopicMap_1),
						Answers = topic.Posts.Where(p => p.Status).Count(),
						LastUpdate = topic.Posts.Where(p => p.Status).Count() != 0 ? topic.Posts.Where(p => p.Status).Max(p => p.CreatedAt) : default(DateTime)
					});
				}

				return Request.CreateResponse(HttpStatusCode.OK, topicsResponse);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Tema no encontrado.");
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
		public HttpResponseMessage Post()
		{
			var _topic = Serializer.Deseralize<Topic>(HttpContext.Current.Request.Form["Topic"]);

			if (_topic.Description != null || _topic.Description != "")
				_topic.Description = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(_topic.Description));

			var files = HttpContext.Current.Request.Files;

			if (_topic == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			var topic = new Topic()
			{
				Title = _topic.Title,
				Description = _topic.Description,
				AllowPosting = _topic.AllowPosting,
				Author = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId),
				Forum = db.Forum.FirstOrDefault(p => p.Id == _topic.Forum.Id),
				TopicFiles = new List<TopicFile>()
			};

			if (GlobalParameters.Rol == "Alumno" && topic.Forum.Type != ForumType.Social)
				return Request.CreateResponse(HttpStatusCode.Unauthorized);

			db.Topic.Add(topic);
			db.SaveChanges();

			for (var i = 0; i < files.Count; i++)
			{
				var file = files[i];

				if (file != null && file.ContentLength != 0)
				{
					var relativePath = PathHelper.ForumTopicPath + topic.Id;
					var path = PathHelper.GetFullPath(relativePath);

					try
					{
						Directory.CreateDirectory(path);
						var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
						path = Path.Combine(path, fileName);
						file.SaveAs(path);

						topic.TopicFiles.Add(new TopicFile()
						{
							DocumentName = Path.GetFileName(file.FileName),
							DocumentPath = Path.Combine(relativePath, fileName),
							DocumentType = file.ContentType.ToLower()
						});
					}
					catch (Exception)
					{
						continue;
					}
				}
			}
			
			db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
		public HttpResponseMessage Put()
		{
			var _topic = Serializer.Deseralize<Topic>(HttpContext.Current.Request.Form["Topic"]);

			if (_topic.Description != null || _topic.Description != "")
				_topic.Description = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(_topic.Description));

			var files = HttpContext.Current.Request.Files;

			if (_topic == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			var topic = db.Topic.FirstOrDefault(p => p.Status && p.Id == _topic.Id);

			if (topic == null)
				return Request.CreateResponse(HttpStatusCode.NotFound);		

			if (GlobalParameters.Rol == "Alumno" && topic.Forum.Type != ForumType.Social)
				return Request.CreateResponse(HttpStatusCode.Unauthorized);

			topic.Title = _topic.Title;
			topic.Description = _topic.Description;
			topic.AllowPosting = _topic.AllowPosting;

			foreach (var topicfile_local in _topic.TopicFiles.Where(p => !p.Status))
			{
				var topicFile = topic.TopicFiles.FirstOrDefault(p => p.Id == topicfile_local.Id);

				if (topicFile != null)
					topicFile.Status = false;
			}

			for (var i = 0; i < files.Count; i++)
			{
				var file = files[i];

				if (file != null && file.ContentLength != 0)
				{
					var relativePath = PathHelper.ForumTopicPath + topic.Id;
					var path = PathHelper.GetFullPath(relativePath);

					try
					{
						Directory.CreateDirectory(path);
						var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
						path = Path.Combine(path, fileName);
						file.SaveAs(path);

						topic.TopicFiles.Add(new TopicFile()
						{
							DocumentName = Path.GetFileName(file.FileName),
							DocumentPath = Path.Combine(relativePath, fileName),
							DocumentType = file.ContentType.ToLower()
						});
					}
					catch (Exception)
					{
						continue;
					}
				}
			}

			db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Delete(int id)
		{
			var topic = db.Topic.FirstOrDefault(e => e.Id == id);

			if (topic != null)
			{
				topic.Status = false;
				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Tema no encontrado.");
		}
	}
}
