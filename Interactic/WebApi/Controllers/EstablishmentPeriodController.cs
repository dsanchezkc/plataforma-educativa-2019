﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Data.Entity;
using System.Web.Http;
using System.Net.Http;
using System.Net;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class EstablishmentPeriodController : ApiController
	{
		private InteracticContext db;

		public EstablishmentPeriodController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = false;
		}

		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, db.EstablishmentPeriod);
		}
	}
}