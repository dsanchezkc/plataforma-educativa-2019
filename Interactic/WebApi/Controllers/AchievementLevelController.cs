﻿
using Model;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class AchievementLevelController : ApiController
    {
        private InteracticContext db;

        public AchievementLevelController()
        {
            db = new InteracticContext();
        }

        [Authorize(Roles = "Profesor")]
        [HttpPut, Route("Api/AchievementLevel/SetAchievementLevelText")]
        public HttpResponseMessage SetAchievementLevelText(int achievementlevel_id, string text)
        {
            var achievement = db.AchievementLevel.FirstOrDefault(p => p.Id == achievementlevel_id);
            if (achievement != null)
            {
                achievement.Name = text;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Nivel de logro no encontrado.");
            }
        }
    }
}