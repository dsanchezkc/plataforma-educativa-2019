﻿using System.Web;
using Model;
using System.Linq;
using System.Data.Entity;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using Helper;
using WebApi.CloneModels;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class DimensionController : ApiController
    {
        private InteracticContext db;

        public DimensionController()
        {
            db = new InteracticContext();
        }

        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Dimension, DimensionCloneHelper.DimensionMap_1));
        }

        public HttpResponseMessage Get(int id)
        {
            var dim = db.Dimension.FirstOrDefault(e => e.Id == id);

            if (dim != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(dim, DimensionCloneHelper.DimensionMap_1));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Dimension no encontrada.");
        }
    }
}