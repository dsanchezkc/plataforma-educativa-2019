﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class RoleController : ApiController
	{
		private InteracticContext db;

		public RoleController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.Roles, 1));
		}
		
		[Authorize(Roles = "Administrador, SubAdministrador")]
		[Route("Api/Role/GetRolesFiltererAdminAndSubAdmin")]
		public HttpResponseMessage GetRolesFiltererAdminAndSubAdmin()
		{
			switch (GlobalParameters.Rol)
			{
				case "Administrador":
					return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.Roles, 1));

				case "SubAdministrador":
					return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(db.Roles.Where(p => p.Name != "Administrador" && p.Name != "SubAdministrador"), 1));
			}

			return Request.CreateResponse(HttpStatusCode.NotFound);
		}


		public HttpResponseMessage Get(string id)
		{
			var roles = db.Roles.FirstOrDefault(e => e.Id == id);

			if (roles != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObject(roles, 1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Rol no encontrado.");
		}

		[Route("Api/Role/GetRolesByEducationalEstablishmentFilteredByUser")]
		public HttpResponseMessage GetEducationalEstablishmentsByCommuneFilteredByUser(int educationalestablishment_id)
		{
			var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
			var isSuperAdmin = user.Roles.FirstOrDefault(p =>p.Status && p.Role.Name == "Administrador") != null;

			var roles = user.UserRoleEducationalEstablishments.Where(p => p.Status && p.User.Id == GlobalParameters.UserId && p.EducationalEstablishment.Id == educationalestablishment_id).Select(p => p.Role);

			if (isSuperAdmin)
			{
				var result = new List<Role>();
				result.AddRange(roles);

				if (roles.FirstOrDefault(p => p.Name == "Administrador") == null)
					result.Add(db.Roles.FirstOrDefault(p => p.Name == "Administrador"));

				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(result, 1));
			}
			else
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(roles, 1));
		}
	}
}
