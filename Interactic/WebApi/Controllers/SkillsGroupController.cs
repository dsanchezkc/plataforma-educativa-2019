﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using System.Collections.ObjectModel;
using WebApi.CloneModels;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class SkillsGroupController : ApiController
    {
        private InteracticContext db;

        public SkillsGroupController()
        {
            db = new InteracticContext();
        }

		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.SkillsGroup.AsNoTracking().Where(p => p.Status), SkillsGroupCloneHelper.SkillsGroupMap_1));
		}

		public HttpResponseMessage Get(int id)
		{
			var skillsGroup = db.SkillsGroup.AsNoTracking().FirstOrDefault(e => e.Status && e.Id == id);

			if (skillsGroup != null)
				return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(skillsGroup, SkillsGroupCloneHelper.SkillsGroupMap_1));
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Grupo de habilidades no encontrado.");
		}

        [Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Post([FromBody] SkillsGroup _skillsGroup)
		{
			if(_skillsGroup == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			var skillGroup = new SkillsGroup()
			{
				Name = _skillsGroup.Name,
				Skills = new List<Skill>()
			};

			if (_skillsGroup.Skills != null)
				foreach (var skill in _skillsGroup.Skills)
					skillGroup.Skills.Add(new Skill() { Name = skill.Name });

			db.SkillsGroup.Add(skillGroup);
			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Put([FromBody] SkillsGroup _skillsGroup)
		{
			if (_skillsGroup == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			var skillGroup = db.SkillsGroup.FirstOrDefault(p => p.Id == _skillsGroup.Id);

			if (skillGroup == null)
				return Request.CreateResponse(HttpStatusCode.NotFound);

			skillGroup.Name = _skillsGroup.Name;

			if (_skillsGroup.Skills == null)
				_skillsGroup.Skills = new List<Skill>();

			foreach (var skill in skillGroup.Skills)
			{
				var _skill = _skillsGroup.Skills.FirstOrDefault(p => p.Id == skill.Id);
				skill.Status = _skill != null;
				skill.Name = _skill != null ? _skill.Name : skill.Name;
			}

			foreach (var _skill in _skillsGroup.Skills)
				if (!skillGroup.Skills.Any(p => p.Id == _skill.Id))
					skillGroup.Skills.Add(new Skill() { Name = _skill.Name });

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Delete(int id)
		{
			var skillsGroup = db.SkillsGroup.FirstOrDefault(e => e.Id == id);

			if (skillsGroup != null)
			{
				skillsGroup.Status = false;
				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Grupo de habilidades no encontrado.");
		}
	}
}
