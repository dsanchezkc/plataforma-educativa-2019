﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Helper;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using WebApi.CloneModels;
using WebApi;
using WebApi.Stores;
using Microsoft.AspNet.Identity;
using Extensions;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class StudentCourseController : ApiController
	{
		private InteracticContext db;

		public StudentCourseController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}		

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		[Route("Api/StudentCourse/GetStudentCoursesByCourse")]
		public HttpResponseMessage GetStudentCoursesByCourse(int course_id)
		{
			var course = db.Course.FirstOrDefault(p => p.Id == course_id);

			if (course == null)
				Request.CreateResponse(HttpStatusCode.BadRequest, "Curso no encontrado");

			var studentCourses = CloneHelper.CloneListMap(course.StudentCourses, StudentCourseCloneHelper.StudentCourseMap_1);

			return Request.CreateResponse(HttpStatusCode.OK, studentCourses.OrderBy(p => p.ListNumber));
		}		
	}
}
