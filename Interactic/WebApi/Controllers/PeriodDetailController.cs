﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class PeriodDetailController : ApiController
    {
        private InteracticContext db;

        public PeriodDetailController()
        {
            db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
        }

		/// <summary>
		/// Author AP
		/// Entrega el listado de periodos asociados al establecimiento logeado.
		/// </summary>
		/// <returns></returns>
		[Route("Api/PeriodDetail/GetPeriodDetailsFilteredByCurrentEstablishment")]
		public HttpResponseMessage GetPeriodDetailsFilteredByCurrentEstablishment()
		{
			var periodDetails = db.EducationalEstablishment.FirstOrDefault(p => p.Status && p.Id == GlobalParameters.EducationalEstablishmentId).EstablishmentPeriod.PeriodDetails.OrderBy(p => p.Id);
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneList(periodDetails));
		}		
    }
}
