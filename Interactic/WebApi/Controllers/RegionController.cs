﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Data.Entity;
using System.Web.Http;
using System.Net;
using System.Net.Http;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class RegionController : ApiController
	{
		private InteracticContext db;

		public RegionController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = false;
		}

		[AllowAnonymous]
		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, db.Region);
		}
	}
}