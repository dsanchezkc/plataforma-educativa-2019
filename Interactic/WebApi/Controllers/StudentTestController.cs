﻿using Model;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dtos;
using Helper;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using WebApi.CloneModels;
using GeneralHelper;

namespace WebApiEvaluandome.Controllers
{
	[Authorize(Roles ="Alumno")]
	public class StudentTestController : ApiController
	{
		private InteracticContext db;

		public StudentTestController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		/// <summary>
		/// Author AP
		/// Obtiene la lista de Test relacionado al usuario
		/// </summary>
		[Route("Api/StudentTest/GetStudentTestsFilteredByUser")]
		public HttpResponseMessage GetStudentTestsFilteredByUser()
		{
			switch (GlobalParameters.Rol)
			{
				case "Alumno":
					var studentTests = CloneHelper.CloneListMap(db.StudentTest.Where(p => p.User.Id == GlobalParameters.UserId && p.Active).OrderByDescending(p => p.CreatedAt), StudentTestCloneHelper.StudentTestMap_1);

					foreach (var studentTest in studentTests)
						studentTest.User.Answers.RemoveRange(studentTest.User.Answers.Where(p => p.Question.Test.Id != studentTest.Test.Id));

					return Request.CreateResponse(HttpStatusCode.OK, studentTests);
				default:
					return Request.CreateResponse(HttpStatusCode.NotFound);
			}
		}

		/// <summary>
		/// Author AP
		/// Obtiene la lista de Test relacionado al usuario y la subunitidad
		/// </summary>
		[Route("Api/StudentTest/GetStudentTestsBySubUnityFilteredByUser")]
		public HttpResponseMessage GetStudentTestsBySubUnityFilteredByUser(int subunity_id)
		{
			switch (GlobalParameters.Rol)
			{
				case "Alumno":
					var studentTests = CloneHelper.CloneListMap(db.StudentTest.Where(p => p.User.Id == GlobalParameters.UserId && p.Active && p.Test.SubUnity.Id == subunity_id && p.Done).OrderByDescending(p => p.CreatedAt), StudentTestCloneHelper.StudentTestMap_1);

					foreach (var studentTest in studentTests)
						studentTest.User.Answers.RemoveRange(studentTest.User.Answers.Where(p => p.Question.Test.Id != studentTest.Test.Id));

					return Request.CreateResponse(HttpStatusCode.OK, studentTests);
				default:
					return Request.CreateResponse(HttpStatusCode.NotFound);
			}
		}

		/// <summary>
		/// Author AP
		/// Obtiene el Test realizado por el Alumno + sus respuestas.
		/// </summary>
		[Authorize(Roles = "Alumno")]
		[Route("Api/StudentTest/GetStudentTestByTestFilteredByUser")]
		public HttpResponseMessage GetStudentTestByTestFilteredByUser(int test_id)
		{
			var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == GlobalParameters.UserId && p.Active && p.Test.Id == test_id && p.Test.Status );
			var _studentTest = CloneHelper.CloneObjectMap(studentTest, StudentTestCloneHelper.StudentTestMap_1);
			
			if (_studentTest == null)
				return Request.CreateResponse(HttpStatusCode.NotFound);

			_studentTest.User.Answers.RemoveRange(_studentTest.User.Answers.Where(p => p.Question.Test.Id != _studentTest.Test.Id));

			var questions_order = _studentTest.Test.Questions.Where(p => p.Status).OrderBy(p => p.Order).ToList();
			_studentTest.Test.Questions.Clear();
			_studentTest.Test.Questions.AddRange(questions_order);

			var answers_order = _studentTest.User.Answers.OrderBy(p => p.Question.Order).ToList();
			_studentTest.User.Answers.Clear();
			_studentTest.User.Answers.AddRange(answers_order);			

			var countSuccess = 0;
			var countError = 0;
			var countSkip = 0;

			foreach (var question in _studentTest.Test.Questions.Where(p => p.Status))
			{
				var studentChoice = _studentTest.User.Answers.FirstOrDefault(p => p.Question_Id == question.Id)?.Choice;

				if (studentChoice != null)
					if (question.Choice == studentChoice)
						countSuccess++;
					else
					if (studentChoice == "-")
						countSkip++;
					else
						countError++;
			}

			//Si el test es restrictivo - No es posible ver las respuesta hasta que todos hayan respondido.
			if (_studentTest.Test.IsPublishedDocumentCorrectionWithRestriction)
			{
				var course = db.Course.AsNoTracking().FirstOrDefault(p => p.Id == studentTest.Test.SubUnity.Unity.CourseSubject.Course_Id);
				if (studentTest.Test.StudentTests.Count(p => p.Status && p.Done) != course.StudentCourses.Count())
					foreach (var question in _studentTest.Test.Questions.Where(p => p.Status))
						question.Choice = "-";
			}

			return Request.CreateResponse(HttpStatusCode.OK, new { Info = new { Success = countSuccess, Skip = countSkip, Error = countError }, StudentTest = _studentTest });
		}

		/// <summary>
		/// Author AP
		/// Obtiene la lista de Test no realizados aún
		/// </summary>
		[Route("Api/StudentTest/GetTestsYetToBeDone")]
		public HttpResponseMessage GetTestsYetToBeDone()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.StudentTest.Where(p => p.User.Id == GlobalParameters.UserId && p.Active && !p.Done && p.Test.Status), StudentTestCloneHelper.StudentTestMap_2));
		}

		[Route("Api/StudentTest/GetStudentTestFilteredByUser")]
		public object GetStudentTestFilteredByUser(int test_id)
		{
			var studentTest = db.StudentTest.FirstOrDefault(e => e.Test.Id == test_id && e.User.Id == GlobalParameters.UserId && e.Status && e.Active);
			var _studentTest = CloneHelper.CloneObjectMap(studentTest, StudentTestCloneHelper.StudentTestMap_3);

			var questions_order = _studentTest.Test.Questions.OrderBy(p => p.Order).ToList();
			_studentTest.Test.Questions.Clear();
			_studentTest.Test.Questions.AddRange(questions_order);
            if (studentTest != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _studentTest);
            }
            else {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


        public object GetSpecialTestsBySubUnity(int subunity_id)
        {
            var tests = db.SpecialTest.Where(p => p.Status && p.SubUnity.Id == subunity_id);

            if (tests != null)
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(tests, TestCloneHelper.TestMap_1));
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Test no encontrado.");
        }

        /// <summary>
        /// Author AP
        /// Registrar Test realizado por un alumno
        /// </summary>
        [HttpPost, Route("Api/StudentTest/RegisterTest")]
		public HttpResponseMessage RegisterTest([FromBody] StudentTest _studentTest)
		{
			var correct_Count = 0;

			var studentTest = db.StudentTest.FirstOrDefault(p => p.User.Id == GlobalParameters.UserId && p.Test.Id == _studentTest.Test.Id);

			if (!studentTest.Active || studentTest.Done)
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Test no se encuentra en estado para ser registrado");

			foreach (var question in studentTest.Test.Questions.Where(p => p.Status))
			{
				var answer = _studentTest.User.Answers.FirstOrDefault(p => p.Question.Id == question.Id);

				if (answer.Choice == null || answer.Choice == "" || !"ABCDE".Contains(answer.Choice))
					answer.Choice = "-";

				if (answer == null)
					studentTest.User.Answers.Add(new Answer() { Question = question, Choice = " " });
				else
				{
					studentTest.User.Answers.Add(new Answer() { Question = question, Choice = answer.Choice });

					if (question.Choice == answer.Choice)
						correct_Count++;
				}
			}

			studentTest.Done = true;
			db.SaveChanges();

			var performance = (float)correct_Count / (float)studentTest.Test.Questions.Where(p => p.Status).Count();

			return Request.CreateResponse(HttpStatusCode.OK, performance);
		}		
	}
}