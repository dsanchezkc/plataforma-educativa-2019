﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Data.Entity;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using Helper;
using Generic;
using ViewModels;
using GeneralHelper;
using System.IO;
using System.Collections.ObjectModel;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class MessageController : ApiController
	{
		private InteracticContext db;

		public MessageController()
		{
			db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		[Route("Api/Message/GetListReceiver")]
		public HttpResponseMessage GetListReceiver(string search)
		{
			if (search == null)
				return Request.CreateResponse(HttpStatusCode.OK, new List<string>());

			search = search.ToLower();

			var result_users = db.Users.Where(p => (p.UserName.ToLower().Contains(search) || p.Name.ToLower().Contains(search)) && p.UserRoleEducationalEstablishments.Any(q => q.Status && (q.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId || q.Role.Name == "Administrador")))
								 .Select(p => "[" + p.UserName.ToLower() + "] " + p.FatherLastName.Trim().ToLower() + " " + p.Name.Trim().ToLower());

			var result_Courses = db.EducationalEstablishment.FirstOrDefault(p => p.Status && p.Id == GlobalParameters.EducationalEstablishmentId)?.Courses.Where(q => q.Status && q.Name.ToLower().Contains(search)).Select(p => p.Name.ToLower());

			var result = new List<string>();

			if (result_Courses != null)
				result.AddRange(result_Courses);

			if (result_users != null)
				result.AddRange(result_users);

			return Request.CreateResponse(HttpStatusCode.OK, result);
		}

		/// <summary>
		/// @Author - AP
		/// Recibe mensaje que fue escrito - Realiza el proceso de inserción y almacenado de archivo de adjuntos
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		[Route("Api/Message/Write")]
		public HttpResponseMessage Write()
		{
			if (!HttpContext.Current.Request.Form.AllKeys.Any(p => p == "message"))
				return Request.CreateResponse(HttpStatusCode.BadRequest, "El formulario no contiene una Key específica");

			var content = Serializer.Deseralize<MessageRequestViewModel>(HttpContext.Current.Request.Form["message"]);
			var files = HttpContext.Current.Request.Files;

			if (content.To.Count == 0)
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Debe existir al menos un destinatario.");

			if (content.Subject == null || content.Subject.Trim() == "")
				content.Subject = "Sin asunto";

			if (content.Content == null || content.Content.Trim() == "")
				content.Content = "";

			var toUsers = GetUsersByRunOrCourse(content.To);
			var message = new Message()
			{
				FromUser = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId),
				Body = content.Content,
				MessageAttacheds = new Collection<MessageAttached>()
			};

			for (var i = 0; i < files.Count; i++)
			{
				var file = files[i];

				if (file != null && file.ContentLength != 0)
				{
					var path = @"C:\EvaluandomeResources\Email\" + GlobalParameters.UserId;

					try
					{
						Directory.CreateDirectory(path);
						path = Path.Combine(path, Guid.NewGuid().ToString() + Path.GetExtension(file.FileName));
						file.SaveAs(path);
					}
					catch (Exception)
					{
						continue;
					}

					message.MessageAttacheds.Add(new MessageAttached()
					{
						Attached = new Attached()
						{
							DocumentName = Path.GetFileName(file.FileName),
							DocumentPath = path,
							DocumentType = file.ContentType.ToLower()
						}
					});
				}
			}

			var currentUser = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
			ConversationMessage conversationMessage = null;
			//Registrandos a los distintos destinatarios.
			foreach (var user in toUsers)
			{
				var _conversationMessage = new ConversationMessage()
				{
					Conversation = new Conversation()
					{
						IsVisible = true,
						ToUser = user,
						Readed = false,
						Subject = content.Subject,
					},
					Message = message
				};

				db.ConversationMessage.Add(_conversationMessage);

				if (user.Id == currentUser.Id)
					conversationMessage = _conversationMessage;
			}

			//Registrando el autor del mensaje.
			
			if (conversationMessage==null)
			{
				db.ConversationMessage.Add(conversationMessage = new ConversationMessage()
				{
					Conversation = new Conversation()
					{
						IsVisible = false,
						ToUser = currentUser,
						Readed = false,
						Subject = content.Subject,
					},
					Message = message
				});				
			}

			db.AuthorMessage.Add(new AuthorMessage()
			{
				User = currentUser,
				ConversationMessage = conversationMessage
			});

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		/// <summary>
		/// @Author - AP
		/// Recibe una respuesta.
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		[Route("Api/Message/Answer")]
		public HttpResponseMessage Answer()
		{
			if (!HttpContext.Current.Request.Form.AllKeys.Any(p => p == "answer"))
				return Request.CreateResponse(HttpStatusCode.BadRequest, "El formulario no contiene una Key específica");

			var content = Serializer.Deseralize<MessageAnswerRequestViewModel>(HttpContext.Current.Request.Form["answer"]);
			var files = HttpContext.Current.Request.Files;
			Message message = null;

			message = db.Message.FirstOrDefault(p => p.Id == content.Message_Id);

			if (message == null)
				return Request.CreateResponse(HttpStatusCode.NotFound, "Mensaje no encontrado.");

			var firstMensaje = message;

			while (firstMensaje.MessageParent != null)
				firstMensaje = firstMensaje.MessageParent;

			var conversationMessage = db.ConversationMessage.FirstOrDefault(p => p.Message.Id == firstMensaje.Id && p.Conversation.ToUser.Id == GlobalParameters.UserId);
			var conversation = conversationMessage?.Conversation;

			if (conversationMessage == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Conversación no encontrada para este usuario.");

			var child = message;

			while (child.MessageChild != null)
				child = child.MessageChild;

			var currentUser = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);

			var newMessage = new Message()
			{
				FromUser = currentUser,
				Body = content.Body,
				MessageAttacheds = new Collection<MessageAttached>()
			};

			child.MessageChild = newMessage;
			newMessage.MessageParent = child;
			message = newMessage;

			//Haciendo visible la conversación al creador para la bandeja de entrada.
			var conversationAuthor = db.ConversationMessage.FirstOrDefault(p => p.Message.Id == firstMensaje.Id && !p.IsForward && p.Conversation.ToUser.Id == firstMensaje.FromUser.Id);
			conversationAuthor.Conversation.IsVisible = true;

			//Buscamos si el usuario quien responde ya se encuentra registrado como que alguna vez respondió el mensaje
			var authorMessage = db.AuthorMessage.FirstOrDefault(p => p.User.Id == GlobalParameters.UserId && p.ConversationMessage.Id == conversationMessage.Id);

			if (authorMessage == null)
				db.AuthorMessage.Add(new AuthorMessage()
				{
					User = currentUser,
					ConversationMessage = conversationMessage
				});

			foreach (var item in firstMensaje.ConversationMessages)
				if (!item.IsForward && item.Conversation.ToUser != currentUser)
					item.Conversation.Readed = false;
			                                       

			for (var i = 0; i < files.Count; i++)
			{
				var file = files[i];

				if (file != null && file.ContentLength != 0)
				{
					var path = @"C:\EvaluandomeResources\Email\" + GlobalParameters.UserId;

					try
					{
						Directory.CreateDirectory(path);
						path = Path.Combine(path, Guid.NewGuid().ToString() + Path.GetExtension(file.FileName));
						file.SaveAs(path);
					}
					catch (Exception)
					{
						continue;
					}

					message.MessageAttacheds.Add(new MessageAttached()
					{
						Attached = new Attached()
						{
							DocumentName = Path.GetFileName(file.FileName),
							DocumentPath = path,
							DocumentType = file.ContentType.ToLower()
						}
					});
				}
			}

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		/// <summary>
		/// @Author - AP
		/// Reenvío de un mensaje.
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		[Route("Api/Message/Forward")]
		public HttpResponseMessage Forward()
		{
			if (!HttpContext.Current.Request.Form.AllKeys.Any(p => p == "forward"))
				return Request.CreateResponse(HttpStatusCode.BadRequest, "El formulario no contiene una Key específica");

			var content = Serializer.Deseralize<MessageForwardRequestViewModel>(HttpContext.Current.Request.Form["forward"]);
			var files = HttpContext.Current.Request.Files;
			Conversation conversation = null;

			conversation = db.Conversation.FirstOrDefault(p => p.Id == content.Conversation_Id && p.ToUser.Id == GlobalParameters.UserId);

			if (conversation == null)
				return Request.CreateResponse(HttpStatusCode.NotFound, "Conversación no encontrada.");

			var currentUser = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);

			var toUsers = GetUsersByRunOrCourse(content.ToUsers);
			var message = new Message()
			{
				FromUser = currentUser,
				Body = content.Body,
				MessageAttacheds = new Collection<MessageAttached>()
			};

			//Registrando mensaje en el usuario que lo envío
			conversation.ConversationMessages.Add(new ConversationMessage()
			{
				IsForward = true,
				Message = message
			});

			ConversationMessage conversationMessage = null;
			//Registrandos a los distintos destinatarios.
			foreach (var user in toUsers)
			{
				var _conversationMessage = new ConversationMessage()
				{
					Conversation = new Conversation()
					{
						IsVisible = true,
						ToUser = user,
						Readed = false,
						Subject = conversation.Subject,
					},
					Message = message
				};

				db.ConversationMessage.Add(_conversationMessage);

				if (user.Id == currentUser.Id)
					conversationMessage = _conversationMessage;
			}

			//Registrando el autor del mensaje.

			if (conversationMessage == null)
			{
				db.ConversationMessage.Add(conversationMessage = new ConversationMessage()
				{
					Conversation = new Conversation()
					{
						IsVisible = false,
						ToUser = currentUser,
						Readed = false,
						Subject = conversation.Subject,
					},
					Message = message
				});
			}

			db.AuthorMessage.Add(new AuthorMessage()
			{
				User = currentUser,
				ConversationMessage = conversationMessage
			});

			for (var i = 0; i < files.Count; i++)
			{
				var file = files[i];

				if (file != null && file.ContentLength != 0)
				{
					var path = @"C:\EvaluandomeResources\Email\" + GlobalParameters.UserId;

					try
					{
						Directory.CreateDirectory(path);
						path = Path.Combine(path, Guid.NewGuid().ToString() + Path.GetExtension(file.FileName));
						file.SaveAs(path);
					}
					catch (Exception)
					{
						continue;
					}

					message.MessageAttacheds.Add(new MessageAttached()
					{
						Attached = new Attached()
						{
							DocumentName = Path.GetFileName(file.FileName),
							DocumentPath = path,
							DocumentType = file.ContentType.ToLower()
						}
					});
				}
			}

			//Transfiriendo y validando archivos adjuntos existentes.
			var _message = conversation.ConversationMessages.FirstOrDefault().Message;
			var attacheds = new List<Attached>();
			attacheds.AddRange(_message.MessageAttacheds.Select(p => p.Attached));

			while (_message.MessageChild != null)
			{
				_message = _message.MessageChild;
				attacheds.AddRange(_message.MessageAttacheds.Select(p => p.Attached));
			}

			attacheds = attacheds.Distinct().ToList();

			for (var i = 0; i < content.Attacheds.Count; i++)
			{
				var attached_id = content.Attacheds[i];
				Attached attached = attacheds.FirstOrDefault(p => p.Id == attached_id);
				if (attached != null)
					message.MessageAttacheds.Add(new MessageAttached()
					{
						Attached = attached
					});
			}

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}	

		[HttpGet]
		[Route("Api/Message/GetMessages")]
		public HttpResponseMessage GetMessages()
		{
			var conversations = db.Conversation.Where(p => p.IsVisible && p.ToUser.Id == GlobalParameters.UserId).OrderByDescending(p => p.EditedAt).Take(10);
			return Request.CreateResponse(HttpStatusCode.OK, GenerateConversationEmailResponseViewModel(conversations));
		}

		[HttpGet]
		[Route("Api/Message/LoadMessages")]
		public HttpResponseMessage LoadMessages(int conversation_reference, int count)
		{
			var conversations = db.Conversation.Where(p => p.IsVisible && p.Id < conversation_reference && p.ToUser.Id == GlobalParameters.UserId).OrderByDescending(p => p.EditedAt).Take(count);
			return Request.CreateResponse(HttpStatusCode.OK, GenerateConversationEmailResponseViewModel(conversations));
		}

		[HttpGet]
		[Route("Api/Message/LoadSent")]
		public HttpResponseMessage LoadSent(int conversation_reference, int count)
		{
			var conversations = db.AuthorMessage.Where(p => p.ConversationMessage.Conversation.Id < conversation_reference && p.User.Id == GlobalParameters.UserId).Select(p => p.ConversationMessage.Conversation).Distinct().OrderByDescending(p => p.CreatedAt).Take(count);
			return Request.CreateResponse(HttpStatusCode.OK, GenerateConversationEmailResponseViewModel(conversations));
		}

		[HttpGet]
		[Route("Api/Message/Sent")]
		public HttpResponseMessage Sent()
		{
			var conversations = db.AuthorMessage.Where(p => p.User.Id == GlobalParameters.UserId).Select(p => p.ConversationMessage.Conversation).Distinct().OrderByDescending(p => p.CreatedAt).Take(10);
			return Request.CreateResponse(HttpStatusCode.OK, GenerateConversationEmailResponseViewModel(conversations));
		}

		[HttpGet]
		[Route("Api/Message/GetNotifications")]
		public HttpResponseMessage GetNotifications()
		{
			var conversations = db.Conversation.Where(p => p.IsVisible && p.ToUser.Id == GlobalParameters.UserId).OrderByDescending(p => p.EditedAt);

			var resume = new NotificationResumeResponseViewModel()
			{
				UnreadCount = conversations.Count(p => !p.Readed)
			};

			foreach (var conversation in conversations.Take(3))
			{
				var firstMessage = conversation.ConversationMessages.FirstOrDefault().Message;

				while (firstMessage.MessageChild != null)
					firstMessage = firstMessage.MessageChild;

				resume.Notifications.Add(new NotificationResponseViewModel()
				{
					Conversation_Id = conversation.Id,
					Date = firstMessage.EditedAt,
					Name = firstMessage.FromUser.Name+" "+firstMessage.FromUser.FatherLastName.ToUpper().ElementAt(0)+".",
					Body = firstMessage.Body
				});
			}

			return Request.CreateResponse(HttpStatusCode.OK, resume);
		}



		[HttpGet]
		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno, Apoderado")]
		[Route("Api/Message/Download")]
		public HttpResponseMessage Download(int attached_id, int message_id)
		{
			var message = db.Message.FirstOrDefault(p => p.Id == message_id);

			if (message == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error.");

			var attached = message.MessageAttacheds.FirstOrDefault(p => p.Attached.Id == attached_id)?.Attached;

			if (attached == null)
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Archivo no encontrado.");

			while (message.MessageParent != null)
				message = message.MessageParent;

			var conversation = message.ConversationMessages.FirstOrDefault(p => p.Conversation.ToUser.Id == GlobalParameters.UserId);

			if (conversation == null)
				return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Acceso denegado.");

			try
			{
				var binary = FileHelper.GetBinaryFile(attached.DocumentPath);
				return Request.CreateResponse(HttpStatusCode.OK, Convert.ToBase64String(binary, 0, binary.Length));
			}
			catch (Exception)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, "Aún no ha sido subo el archivo.");
			}
		}

		[HttpGet]
		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno, Apoderado")]
		[Route("Api/Message/Readed")]
		public HttpResponseMessage Readed(int conversation_id)
		{
			var conversation = db.Conversation.FirstOrDefault(p => p.Id == conversation_id && p.ToUser.Id == GlobalParameters.UserId);

			if (conversation == null)
				return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Acceso denegado.");

			conversation.Readed = true;

			db.BaseSaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		private IEnumerable<ConversationEmailResponseViewModel> GenerateConversationEmailResponseViewModel(IEnumerable<Conversation> conversations)
		{
			var conv_Special = new List<ConversationEmailResponseViewModel>();

			foreach (var conversation in conversations)
			{
				var viewModel = new ConversationEmailResponseViewModel() { Id = conversation.Id, Readed = conversation.Readed, Subject = conversation.Subject, Date = conversation.EditedAt };
				conv_Special.Add(viewModel);

				foreach (var cm in conversation.ConversationMessages)
				{
					var toUserNames = cm.Message.ConversationMessages.Where(p => !p.IsForward).Select(p => "[" + p.Conversation.ToUser.UserName + "]" + " " + p.Conversation.ToUser.Name + " " + p.Conversation.ToUser.FatherLastName).ToList();
					var message = cm.Message;

					while (message != null)
					{
						viewModel.Messages.Add(new MessageEmail(message.FromUser)
						{
							Id = message.Id,
							IsForward = cm.IsForward,
							ToUsers = toUserNames,
							Date = message.CreatedAt,
							Body = message.Body,
							Attacheds = AttachedEmail.Cast(message.MessageAttacheds.Select(p => p.Attached))
						});

						//Si es un mensaje de reenvío - solamente agregamos el primer mensaje
						if (cm.IsForward) break;

						message = message.MessageChild;
					}
				}
				viewModel.Messages = viewModel.Messages.OrderBy(p => p.Date).ToList();
			}
			return conv_Special.OrderByDescending(p => p.Date);
		}

		/// <summary>
		/// Relaciona la lista de "to" con R.U.Ns y nombres de cursos para entregar los usuarios relacionados
		/// </summary>
		/// <param name="to">lista de asociación</param>
		/// <returns></returns>
		private IEnumerable<User> GetUsersByRunOrCourse(List<string> to)
		{
			var toList = new List<User>();

			foreach (var item in to)
			{
				int pFrom = item.IndexOf("[");
				int pTo = item.LastIndexOf("]");

				if (pFrom == -1 || pTo == -1) //Significa que es un curso
				{
					var students = db.Course.FirstOrDefault(p => p.Name.ToLower() == item)?.StudentCourses.Select(p => p.User);

					if (students != null)
						toList.AddRange(students);
				}
				else
				if (pFrom == 0 && pTo != 0)  //Significa que es un Run
				{
					var run = item.Substring(1, pTo - 1);
					var user = db.Users.FirstOrDefault(p => p.UserName == run);

					if (user != null)
						toList.Add(user);
				}
			}

			return toList.Distinct();
		}
	}
}