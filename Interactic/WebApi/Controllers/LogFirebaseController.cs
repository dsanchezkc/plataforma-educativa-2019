﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using Model;
using System.Web.Http.Cors;
using Helper;
using WebApi.CloneModels;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;

namespace WebApiEvaluandome.Controllers
{
    [Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
    public class LogFirebaseController : ApiController
    {
        private InteracticContext db;

        public LogFirebaseController()
        {
            db = new InteracticContext();
			db.Configuration.ProxyCreationEnabled = true;
        }

        public HttpResponseMessage Get()
        {
            var logFirebase = db.LogFirebase.Where(x => x.Status);
			return Request.CreateResponse(HttpStatusCode.OK, logFirebase);
        }

        public HttpResponseMessage Get(int id)
        {    
            var logFirebase = db.LogFirebase.FirstOrDefault(e => e.Id == id);

			if (logFirebase != null)
				return Request.CreateResponse(HttpStatusCode.OK, logFirebase);
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "LogFirebase no encontrado.");
        }

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]		
		public HttpResponseMessage Delete(int id)
        {
            var logFirebase = db.LogFirebase.FirstOrDefault(e => e.Id == id);

            if (logFirebase != null)
            {
                db.LogFirebase.Remove(logFirebase);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            	return Request.CreateErrorResponse(HttpStatusCode.NotFound, "LogFirebase no encontrado.");
        }

		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		public HttpResponseMessage Put([FromBody] LogFirebase logFirebase)
        {
			var logFirebaseEntity = db.LogFirebase.FirstOrDefault(e => e.Id == logFirebase.Id);

			if (logFirebaseEntity != null)
            {
                logFirebaseEntity.Token_Extern = logFirebase.Token_Extern;
                logFirebaseEntity.User = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
				db.SaveChanges();
				return Request.CreateResponse(HttpStatusCode.OK);
            }

			return Request.CreateErrorResponse(HttpStatusCode.NotFound, "LogFirebase no encontrado.");
        }

        public HttpResponseMessage Post([FromBody] LogFirebase logFirebase)
        {
            if (logFirebase != null)
            {

                var exists = db.LogFirebase.FirstOrDefault(x => x.Token_Extern == logFirebase.Token_Extern && x.User.Id == logFirebase.User_Id);

                if (exists != null) { return Request.CreateResponse(HttpStatusCode.OK); }

                logFirebase.User = db.Users.FirstOrDefault(p => p.Id == logFirebase.User_Id);
       
                db.LogFirebase.Add(logFirebase);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        private List<String> getTokenByUser(string userName) {
            var tokensByUser = new List<String>();

            var tokens = db.LogFirebase.Where(p => p.User.UserName == userName);

            foreach (LogFirebase token in tokens) {
                tokensByUser.Add(token.Token_Extern);
            }


            return tokensByUser;
        }


      
        //EL USERNAME DEBE TENER EL FORMATO [123123123] ...entre corchetes
        //o el nombre de un curso sin corchetes
        [HttpPost]
        [Route("Api/LogFirebase/sendNotification")]
        public  HttpResponseMessage sendNotification(string message, string userName)
        {
            var users = GetUserOrCourse(userName);
            foreach (var user in users) {
                var tokens = getTokenByUser(user);
                foreach (var token in tokens)
                {
                    notify(message, token);
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private async void notify(string message, string token)
        {
            try
            {

                var url = "https://fcm.googleapis.com/fcm/send"; //url de api google
                WebRequest tRequest = WebRequest.Create(url);
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = token,
                    notification = new
                    {
                        body = message,
                        title = "Evaluándome",
                        sound = "Enabled"
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add("Authorization:key=AIzaSyBI3dVmqUGzukOmmKaUlY98YDekOfG5y7s"); //SERVER KEY FROM CONSOLEFIREBASE
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }

        /// <summary>
		/// Relaciona la lista de "to" con R.U.Ns y nombres de cursos para entregar los usuarios relacionados
		/// </summary>
		/// <param name="to">lista de asociación</param>
		/// <returns></returns>
		private IEnumerable<string> GetUserOrCourse(string to)
        {
            var toList = new List<string>();

                int pFrom = to.IndexOf("[");
                int pTo = to.LastIndexOf("]");

                if (pFrom == -1 || pTo == -1) //Significa que es un curso
                {
                    var students = db.Course.FirstOrDefault(p => p.Name.ToLower() == to)?.StudentCourses.Select(p => p.User.UserName);

                    if (students != null)
                        toList.AddRange(students);
                }
                else
                if (pFrom == 0 && pTo != 0)  //Significa que es un Run
                {
                    var run = to.Substring(1, pTo - 1);
                    var user = db.Users.FirstOrDefault(p => p.UserName == run);

                    if (user != null)
                        toList.Add(user.UserName);
                }

            return toList.Distinct();
        }
    }
}
