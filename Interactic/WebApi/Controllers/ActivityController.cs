﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Helper;
using Dtos;
using WebApi.CloneModels;
using Extensions;
using GeneralHelper;
using ViewModels;

namespace WebApiEvaluandome.Controllers
{
    [Authorize]
    public class ActivityController : ApiController
    {
        private InteracticContext db;

        public ActivityController()
        {
            db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = true;
        }

        [HttpGet]
        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/Activity/OrderActivity")]
        public HttpResponseMessage OrderActivity(int activity_id, int order)
        {
            var activity = db.Activity.FirstOrDefault(p => p.Status && p.Id == activity_id);

            if (activity == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error.");

            var orderList = new List<Model.Activity>();
            var count = 0;
            foreach (var _activity in activity.SubUnity.Activities.Where(p => p.Status).OrderBy(p => p.Order))
            {
                if (_activity.Id != activity.Id)
                    orderList.Add(_activity);

                _activity.Order = count++;
            }

            if (order == activity.Order)
                return Request.CreateResponse(HttpStatusCode.OK);

            var isChanged = false;

            var orderMax = orderList.Max(p => p.Order);

            for (var i = 0; i < orderList.Count; i++)
                if (order == orderList[i].Order)
                {
                    if (order == 0)
                        orderList.Insert(0, activity);
                    else
                    if (order == orderMax && activity.Order <= orderMax)
                        orderList.Add(activity);
                    else
                        orderList.Insert(order > activity.Order ? i + 1 : i, activity);

                    isChanged = true;
                    break;
                }

            if (isChanged)
            {
                for (var i = 0; i < orderList.Count; i++)
                    orderList[i].Order = i;

                db.SaveChanges();
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }


        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Post([FromBody] ActivityRequestViewModel activityRequest)
        {
            if (activityRequest != null)
            {
                var subUnity = db.SubUnity.FirstOrDefault(p => p.Id == activityRequest.SubUnity_Id);

                var activity = new Activity()
                {
                    Name = activityRequest.Name,
                    SubUnity = db.SubUnity.FirstOrDefault(p => p.Id == activityRequest.SubUnity_Id),
                    Order = subUnity.Activities.Count == 0 ? 0 : subUnity.Activities.Max(p => p.Order) + 1
                };

                db.Activity.Add(activity);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(activity, ActivityCloneHelper.ActivityMap_1));
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Put([FromBody] ActivityRequestViewModel activityRequest)
        {
            if (activityRequest != null)
            {
                var activity = db.Activity.FirstOrDefault(p => p.Id == activityRequest.Activity_Id);

                if (activity != null)
                {
                    activity.Name = activityRequest.Name;

                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(activity, ActivityCloneHelper.ActivityMap_1));
                }
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        public HttpResponseMessage Delete(int id)
        {
            var activity = db.Activity.FirstOrDefault(e => e.Id == id);

            if (activity != null)
            {
                activity.Status = false;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Actividad no encontrada.");
        }
    }
}
