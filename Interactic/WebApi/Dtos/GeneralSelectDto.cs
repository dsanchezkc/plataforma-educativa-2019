﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dtos
{
    public class GeneralSelectDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}