﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dtos
{
    public class ReportTableDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LastNameP { get; set; }
        public string LastNameM { get; set; }
        public int Average { get; set; }
        public int MissingTests { get; set; }
        public int TotalQuestions { get; set; }
        public int TotalStudents { get; set; }
        public int Performance { get; set; }
        public double Calification { get; set; }
        public string Date { get; set; }
        public bool isDone { get; set; }
        public int List { get; set; }
		public int Test_Id { get; set; }
    }
}