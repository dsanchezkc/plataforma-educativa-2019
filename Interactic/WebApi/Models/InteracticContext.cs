﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using WebApi.Migrations;

namespace Model
{
	public class InteracticContext : IdentityDbContext<User, Role, string, IdentityUserLogin, UserRoleEducationalEstablishment, IdentityUserClaim>
	{
        public DbSet<Activity> Activity { set; get; }
        public DbSet<Action> Action { set; get; }
        public DbSet<AchievementIndicator> AchievementIndicator { set; get; }
        public DbSet<AchievementAlternative> AchievementAlternative { set; get; }
        public DbSet<Answer> Answer { set; get; }
        public DbSet<Absent> Absent { set; get; }
        public DbSet<Attached> Attached { set; get; }
        public DbSet<AttendanceDate> AttendanceDate { set; get; }
        public DbSet<AuthorMessage> AuthorMessage { set; get; }
        public DbSet<BaseSubject> BaseSubject { set; get; }
        public DbSet<Commune> Commune { set; get; }
        public DbSet<Conversation> Conversation { set; get; }
        public DbSet<ConversationMessage> ConversationMessage { set; get; }
        public DbSet<Course> Course { set; get; }
        public DbSet<CourseSubject> CourseSubject { set; get; }
        public DbSet<CourseSubSubject> CourseSubSubject { set; get; }
        public DbSet<DayTrip> Daytrip { set; get; }
        public DbSet<EducationalEstablishment> EducationalEstablishment { set; get; }
        public DbSet<EstablishmentPeriod> EstablishmentPeriod { set; get; }
        public DbSet<Event> Event { set; get; }
        public DbSet<EventFile> EventFile { set; get; }
        public DbSet<Forum> Forum { set; get; }
        public DbSet<Guardian> Guardian { set; get; }
        public DbSet<LogFirebase> LogFirebase { set; get; }
        public DbSet<Message> Message { set; get; }
        public DbSet<MessageAttached> MessageAttached { set; get; }
        public DbSet<Note> Note { set; get; }
        public DbSet<Observation> Observation { set; get; }
        public DbSet<PeriodDetail> PeriodDetail { set; get; }
        public DbSet<Post> Post { set; get; }
        public DbSet<PostFile> PostFile { set; get; }
        //public DbSet<PostQualification> PostQualification { set; get; }
        public DbSet<Province> Province { set; get; }
        public DbSet<Question> Question { set; get; }
        public DbSet<QuestionAlternative> QuestionAlternative { set; get; }
        public DbSet<QuestionAlternativeItem> QuestionAlternativeItem { set; get; }
        public DbSet<QuestionItem> QuestionItem { set; get; }
        public DbSet<QuestionPairedWord> QuestionPairedWord { set; get; }
        public DbSet<QuestionPairedWordItem> QuestionPairedWordItem { set; get; }
        public DbSet<QuestionTrueFalse> QuestionTrueFalse { set; get; }
        public DbSet<QuestionTrueFalseItem> QuestionTrueFalseItem { set; get; }
        public DbSet<QuestionWrittenAnswer> QuestionWrittenAnswer { set; get; }
        public DbSet<QuestionWrittenAnswerItem> QuestionWrittenAnswerItem { set; get; }
        public DbSet<Region> Region { set; get; }
        public DbSet<Skill> Skill { set; get; }
        public DbSet<SkillsGroup> SkillsGroup { set; get; }
        public DbSet<SpecialQuestion> SpecialQuestion { set; get; }
        public DbSet<SpecialTest> SpecialTest { set; get; }
        public DbSet<StudentAnswer> StudentAnswer { set; get; }
        public DbSet<StudentCourse> StudentCourse { set; get; }
        public DbSet<StudentTest> StudentTest { set; get; }
        public DbSet<StudentSpecialTest> StudentSpecialTest { set; get; }
        public DbSet<StudentQualification> StudentQualification { set; get; }
        public DbSet<Subject> Subject { set; get; }
        public DbSet<SubSubject> SubSubject { set; get; }
        public DbSet<SubUnity> SubUnity { set; get; }
        public DbSet<TeacherCourseSubject> TeacherCourseSubject { set; get; }
        public DbSet<Test> Test { set; get; }
        public DbSet<Topic> Topic { set; get; }
        public DbSet<TopicFile> TopicFile { set; get; }
        public DbSet<Unity> Unity { set; get; }
        public DbSet<UserRoleEducationalEstablishment> UserRoleEducationalEstablishment { set; get; }
        public DbSet<CourseTest> CourseTest { set; get; }
        public DbSet<Rubric> Rubric { set; get; }
        public DbSet<Criterion> Criterion { set; get; }
        public DbSet<AchievementLevel> AchievementLevel { set; get; }
        public DbSet<DescriptionRubricItem> DescriptionRubricItem { set; get; }
        public DbSet<Dimension> Dimension { set; get; }
        public DbSet<RubricDimension> RubricDimension { set; get; }
        public DbSet<EvaluationRubric> EvaluationRubric { set; get; }
        public DbSet<EvaluationRubricStudent> EvaluationRubricStudent { set; get; }
        public DbSet<SpecialQuestionSpecialTest> SpecialQuestionSpecialTest { set; get; }
        public DbSet<Viewed> Viewed { set; get; }
		
        public InteracticContext() : base("InteracticContext")
        {}

        public static InteracticContext Create()
        {
            return new InteracticContext();
        }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /*********************** Restricciones ***********************/
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);

            /******************* Cambios de nombres ********************/
            modelBuilder.Entity<User>().ToTable("User");

            modelBuilder.Entity<Role>().ToTable("Role");

            modelBuilder.Entity<UserRoleEducationalEstablishment>().ToTable("UserRolEducationalEstablishment")
                                                                   .Property(p => p.UserId).HasColumnName("User_Id");

            modelBuilder.Entity<UserRoleEducationalEstablishment>().Property(p => p.RoleId).HasColumnName("Role_Id");

            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");

            /******************** Enlazando relaciones *******************/
            modelBuilder.Entity<Absent>().HasKey(p => new { p.AttendanceDate_Id, p.User_Id });
            modelBuilder.Entity<Absent>().HasRequired(p => p.User).WithMany(p => p.Absents).HasForeignKey(p => p.User_Id);
            modelBuilder.Entity<Absent>().HasRequired(p => p.AttendanceDate).WithMany(p => p.Absents).HasForeignKey(p => p.AttendanceDate_Id);

            modelBuilder.Entity<AchievementAlternative>().HasRequired(p => p.AchievementIndicator).WithMany(p => p.AchievementAlternatives);

            modelBuilder.Entity<AchievementLevel>().HasRequired(p => p.Rubric).WithMany(p => p.AchievementLevels);


            modelBuilder.Entity<AchievementIndicator>().HasRequired(p => p.Test).WithMany(p => p.AchievementIndicator);

            modelBuilder.Entity<Answer>().HasKey(p => new { p.Question_Id, p.User_Id });
            modelBuilder.Entity<Answer>().HasRequired(p => p.User).WithMany(p => p.Answers).HasForeignKey(p => p.User_Id);
            modelBuilder.Entity<Answer>().HasRequired(p => p.Question).WithMany(p => p.Answers).HasForeignKey(p => p.Question_Id);

            modelBuilder.Entity<AttendanceDate>().HasRequired(p => p.PeriodDetail).WithMany(p => p.AttendanceDates);
            modelBuilder.Entity<AttendanceDate>().HasRequired(p => p.BaseSubject).WithMany(p => p.AttendanceDates);
            modelBuilder.Entity<AttendanceDate>().HasRequired(p => p.Daytrip).WithMany(p => p.AttendanceDates);
            modelBuilder.Entity<AttendanceDate>().HasRequired(p => p.Course).WithMany(p => p.AttendanceDates);

            modelBuilder.Entity<AuthorMessage>().HasRequired(p => p.User).WithMany(p => p.AuthorMessages);
            modelBuilder.Entity<AuthorMessage>().HasRequired(p => p.ConversationMessage).WithMany(p => p.AuthorMessages);

            modelBuilder.Entity<Commune>().HasRequired(p => p.Province).WithMany(p => p.Communes);

            modelBuilder.Entity<Conversation>().HasRequired(p => p.ToUser).WithMany(p => p.Conversations);

            modelBuilder.Entity<ConversationMessage>().HasRequired(p => p.Conversation).WithMany(p => p.ConversationMessages);
            modelBuilder.Entity<ConversationMessage>().HasRequired(p => p.Message).WithMany(p => p.ConversationMessages);

            modelBuilder.Entity<Criterion>().HasRequired(p => p.Rubric).WithMany(p => p.Criterions);

            modelBuilder.Entity<Course>().HasRequired(p => p.EducationalEstablishment).WithMany(p => p.Courses);

            modelBuilder.Entity<CourseSubject>().HasKey(p => new { p.Subject_Id, p.Course_Id });
            modelBuilder.Entity<CourseSubject>().HasRequired(p => p.Subject).WithMany(p => p.CourseSubjects).HasForeignKey(p => p.Subject_Id);
            modelBuilder.Entity<CourseSubject>().HasRequired(p => p.Course).WithMany(p => p.CourseSubjects).HasForeignKey(p => p.Course_Id);

            //modelBuilder.Entity<CourseTest>().HasKey(p => new { p.Test_Id, p.Course_Id });
            modelBuilder.Entity<CourseTest>().HasRequired(p => p.Test).WithMany(p => p.CourseTests).HasForeignKey(p => p.Test_Id);
            modelBuilder.Entity<CourseTest>().HasRequired(p => p.Course).WithMany(p => p.CourseTests).HasForeignKey(p => p.Course_Id);

            modelBuilder.Entity<CourseSubSubject>().HasRequired(p => p.CourseSubject).WithMany(p => p.CourseSubSubjects);
            modelBuilder.Entity<CourseSubSubject>().HasRequired(p => p.SubSubject).WithMany(p => p.CourseSubSubjects);

            modelBuilder.Entity<DescriptionRubricItem>().HasRequired(p => p.AchievementLevel).WithMany(p => p.DescriptionRubricItems);
            modelBuilder.Entity<DescriptionRubricItem>().HasRequired(p => p.Criterion).WithMany(p => p.DescriptionRubricItems);
            modelBuilder.Entity<DescriptionRubricItem>().HasRequired(p => p.Rubric).WithMany(p => p.DescriptionRubricItems);

            modelBuilder.Entity<EducationalEstablishment>().HasRequired(p => p.Commune).WithMany(p => p.EducationalEstablishments);
            modelBuilder.Entity<EducationalEstablishment>().HasRequired(p => p.EstablishmentPeriod).WithMany(p => p.EducationalEstablishments);


            modelBuilder.Entity<PeriodDetail>().HasRequired(p => p.EstablishmentPeriod).WithMany(p => p.PeriodDetails);

            modelBuilder.Entity<Event>().HasRequired(p => p.Course).WithMany(p => p.Events);

            modelBuilder.Entity<EventFile>().HasRequired(p => p.Event).WithMany(p => p.EventFiles);

            modelBuilder.Entity<Forum>().HasRequired(p => p.SubUnity).WithMany(p => p.Forums);

            modelBuilder.Entity<EvaluationRubricStudent>().HasRequired(p => p.Criterion).WithMany(p => p.EvaluationRubricStudents);
            modelBuilder.Entity<EvaluationRubricStudent>().HasRequired(p => p.EvaluationRubric).WithMany(p => p.EvaluationRubricStudents);
            modelBuilder.Entity<EvaluationRubricStudent>().HasRequired(p => p.Student).WithMany(p => p.EvaluationRubricStudents);

            modelBuilder.Entity<Guardian>().HasKey(p => new { p.User_Id, p.User_Student_Id, p.EducationalEstablishment_Id });
            modelBuilder.Entity<Guardian>().HasRequired(p => p.User).WithMany(p => p.Guardians);
            modelBuilder.Entity<Guardian>().HasRequired(p => p.EducationalEstablishment).WithMany(p => p.Guardians);

            modelBuilder.Entity<LogFirebase>().HasKey(p => new { p.User_Id, p.Token_Extern });
            modelBuilder.Entity<LogFirebase>().HasRequired(p => p.User).WithMany(p => p.LogFirebase).HasForeignKey(p => p.User_Id);

            modelBuilder.Entity<Message>().HasRequired(p => p.FromUser).WithMany(p => p.Messages);
            modelBuilder.Entity<Message>().HasOptional(p => p.MessageChild).WithMany(p => p.MessageChildren);
            modelBuilder.Entity<Message>().HasOptional(p => p.MessageParent).WithMany(p => p.MessageParents);

            modelBuilder.Entity<MessageAttached>().HasRequired(p => p.Message).WithMany(p => p.MessageAttacheds);
            modelBuilder.Entity<MessageAttached>().HasRequired(p => p.Attached).WithMany(p => p.MessageAttacheds);

            modelBuilder.Entity<Note>().HasRequired(p => p.SubUnity).WithMany(p => p.Notes);

            modelBuilder.Entity<Observation>().HasRequired(p => p.User).WithMany(p => p.Observations);
            modelBuilder.Entity<Observation>().HasRequired(p => p.BaseSubject).WithMany(p => p.Observations);
            modelBuilder.Entity<Observation>().HasRequired(p => p.Course).WithMany(p => p.Observations);
            modelBuilder.Entity<Observation>().HasRequired(p => p.PeriodDetail).WithMany(p => p.Observations);
            modelBuilder.Entity<Observation>().HasRequired(p => p.Teacher);

            modelBuilder.Entity<PeriodDetail>().HasRequired(p => p.EstablishmentPeriod).WithMany(p => p.PeriodDetails);

            modelBuilder.Entity<Post>().HasRequired(p => p.Topic).WithMany(p => p.Posts);
            modelBuilder.Entity<Post>().HasRequired(p => p.Author).WithMany(p => p.Posts);

            modelBuilder.Entity<PostFile>().HasRequired(p => p.Post).WithMany(p => p.PostFiles);

            //modelBuilder.Entity<PostQualification>().HasRequired(p => p.Post).WithMany(p => p.PostQualifications);
            //modelBuilder.Entity<PostQualification>().HasRequired(p => p.User).WithMany(p => p.PostQualifications);

            modelBuilder.Entity<Province>().HasRequired(p => p.Region).WithMany(p => p.Provinces);

            modelBuilder.Entity<Question>().HasRequired(p => p.Test).WithMany(p => p.Questions);

            modelBuilder.Entity<SpecialQuestion>().HasRequired(p => p.SpecialTest).WithMany(p => p.SpecialQuestions);

            modelBuilder.Entity<QuestionAlternativeItem>().HasRequired(p => p.QuestionAlternative).WithMany(p => p.QuestionAlternativeItems);

            modelBuilder.Entity<QuestionPairedWordItem>().HasRequired(p => p.QuestionPairedWord).WithMany(p => p.QuestionPairedWordItems);

            modelBuilder.Entity<QuestionTrueFalseItem>().HasRequired(p => p.QuestionTrueFalse).WithMany(p => p.QuestionTrueFalseItems);

            modelBuilder.Entity<QuestionWrittenAnswer>().HasOptional(p => p.QuestionWrittenAnswerItem).WithOptionalPrincipal(p => p.QuestionWrittenAnswer);

            modelBuilder.Entity<Skill>().HasRequired(p => p.SkillsGroup).WithMany(p => p.Skills);

            modelBuilder.Entity<Rubric>().HasRequired(p => p.Subject).WithMany(p => p.Rubrics);

            modelBuilder.Entity<RubricDimension>().HasRequired(p => p.Rubric).WithMany(p => p.RubricDimensions);
            modelBuilder.Entity<RubricDimension>().HasRequired(p => p.Dimension).WithMany(p => p.RubricDimensions);

            modelBuilder.Entity<SpecialQuestionSpecialTest>().HasKey(p => new { p.SpecialQuestion_Id, p.SpecialTest_Id });

            modelBuilder.Entity<StudentAnswer>().HasKey(p => new { p.QuestionItem_Id, p.User_Id, p.SpecialTest_Id });
            modelBuilder.Entity<StudentAnswer>().HasRequired(p => p.StudentSpecialTest).WithMany(p => p.StudentAnswers);
            modelBuilder.Entity<StudentAnswer>().HasRequired(p => p.QuestionItem).WithMany(p => p.StudentAnswers);

            modelBuilder.Entity<StudentCourse>().HasKey(p => new { p.User_Id, p.Course_Id });
            modelBuilder.Entity<StudentCourse>().HasRequired(p => p.User).WithMany(p => p.StudentCourses).HasForeignKey(p => p.User_Id);
            modelBuilder.Entity<StudentCourse>().HasRequired(p => p.Course).WithMany(p => p.StudentCourses).HasForeignKey(p => p.Course_Id);

            modelBuilder.Entity<StudentQualification>().HasRequired(c => c.BaseSubject).WithMany(p => p.StudentQualifications);
            modelBuilder.Entity<StudentQualification>().HasRequired(p => p.PeriodDetail).WithMany(p => p.StudentQualifications);
            modelBuilder.Entity<StudentQualification>().HasRequired(p => p.Student).WithMany(p => p.StudentQualifications);

            modelBuilder.Entity<StudentTest>().HasRequired(p => p.User).WithMany(p => p.UserTests);
            modelBuilder.Entity<StudentTest>().HasRequired(p => p.Test).WithMany(p => p.StudentTests);

            modelBuilder.Entity<StudentSpecialTest>().HasKey(p => new { p.User_Id, p.SpecialTest_Id });
            modelBuilder.Entity<StudentSpecialTest>().HasRequired(p => p.User).WithMany(p => p.UserSpecialTests);
            modelBuilder.Entity<StudentSpecialTest>().HasRequired(p => p.SpecialTest).WithMany(p => p.StudentSpecialTests);

            modelBuilder.Entity<SpecialQuestion>().HasRequired(p => p.SubUnity).WithMany(p => p.SpecialQuestions);

            modelBuilder.Entity<SpecialTest>().HasRequired(p => p.CourseSubject).WithMany(p => p.SpecialTests);

            modelBuilder.Entity<SubUnity>().HasRequired(p => p.Unity).WithMany(p => p.SubUnits);

            modelBuilder.Entity<SubSubject>().HasRequired(p => p.Subject).WithMany(p => p.SubSubjects);

            modelBuilder.Entity<TeacherCourseSubject>().HasRequired(p => p.CourseSubject).WithMany(p => p.TeacherCourseSubjects);
            modelBuilder.Entity<TeacherCourseSubject>().HasRequired(p => p.User).WithMany(p => p.TeacherCourseSubjects);

            modelBuilder.Entity<Topic>().HasRequired(p => p.Forum).WithMany(p => p.Topics);
            modelBuilder.Entity<Topic>().HasRequired(p => p.Author).WithMany(p => p.Topics);

            modelBuilder.Entity<TopicFile>().HasRequired(p => p.Topic).WithMany(p => p.TopicFiles);

            modelBuilder.Entity<Unity>().HasRequired(p => p.CourseSubject).WithMany(p => p.Units);

            modelBuilder.Entity<UserRoleEducationalEstablishment>().HasKey(p => new { p.UserId, p.RoleId, p.EducationalEstablishment_Id });
            modelBuilder.Entity<UserRoleEducationalEstablishment>().HasRequired(p => p.EducationalEstablishment).WithMany(p => p.UserRoleEducationalEstablishments);
            modelBuilder.Entity<UserRoleEducationalEstablishment>().HasRequired(p => p.User).WithMany(p => p.UserRoleEducationalEstablishments);
            modelBuilder.Entity<UserRoleEducationalEstablishment>().HasRequired(p => p.Role).WithMany(p => p.UserRoleEducationalEstablishments);
        }

		public static void Init()
		{
			//Database.SetInitializer(new MigrateDatabaseToLatestVersion<InteracticContext, Configuration>());
			//Create().Database.Initialize(true);
		}

		public override int SaveChanges()
		{
			var entries = this.ChangeTracker.Entries();

			foreach (var entry in entries)
			{
				try
				{
					var _entry = entry.Entity as ModelBase;

					if (_entry == null) continue;

					switch (entry.State)
					{
						case EntityState.Added:
							_entry.CreatedAt = _entry.EditedAt = DateTime.Now;
							break;

						case EntityState.Modified:
							_entry.EditedAt = DateTime.Now;
							break;
					}
				}
                catch (DbEntityValidationException ex)
                {
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join("; ", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }

            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

		/// <summary>
		/// Usar si se desea saltar el tema de registrar CreatedAt/EditedAt
		/// En ocaciones se ocupa el valor de EditedAt, pero no lo quieres cambiar por un cambio mínimo.
		/// </summary>
		/// <returns></returns>
		public int BaseSaveChanges()
		{
			return base.SaveChanges();
		}
	}
}