﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Model;
using System.Web;
using Extensions;
using Helper;

namespace WebApi.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async System.Threading.Tasks.Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
			//context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

			var idEducationalEstablishment = context.OwinContext.Get<string>("idEducationalEstablishment");
			var role = context.OwinContext.Get<string>("role");
			var year = context.OwinContext.Get<string>("year");

			if (year == "")
				year = DateTime.Now.Year.ToString();

			UserRoleEducationalEstablishment userTypeEducationalEstablishment = null;

			var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
			User user = await userManager.FindAsync(context.UserName.WithoutFormatRUN(), context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

			var db = InteracticContext.Create();
			if (role != "" && year != "" && idEducationalEstablishment != "")
			{
				var _idEducationalEstablishment = int.Parse(idEducationalEstablishment);

				if (role == "Administrador")
				{
					var ure = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.Role.Name == role && p.Status);

					if (ure == null)
					{
						context.SetError("invalid_grant", "Usuario incorrecto.");
						return;
					}

					userTypeEducationalEstablishment = new UserRoleEducationalEstablishment()
					{
						EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(p => p.Id == _idEducationalEstablishment),
						Role = db.Roles.FirstOrDefault(p => p.Name == role),
						User = user
					};

					userTypeEducationalEstablishment.EducationalEstablishment_Id = userTypeEducationalEstablishment.EducationalEstablishment.Id;
					userTypeEducationalEstablishment.RoleId = userTypeEducationalEstablishment.Role.Id;
					userTypeEducationalEstablishment.UserId = userTypeEducationalEstablishment.User.Id;

					if (userTypeEducationalEstablishment.EducationalEstablishment == null)
					{
						context.SetError("invalid_grant", "Usuario incorrecto.");
						return;
					}
				}
				else
					userTypeEducationalEstablishment = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.Role.Name == role && p.EducationalEstablishment_Id == _idEducationalEstablishment && p.Status);
			}
			else
			{
				userTypeEducationalEstablishment = user.UserRoleEducationalEstablishments.Where(p => p.Status).OrderBy(p => p.Role.Rank).FirstOrDefault();
				role = userTypeEducationalEstablishment.Role.Name;
				idEducationalEstablishment = userTypeEducationalEstablishment.EducationalEstablishment_Id.ToString();
			}

			if (userTypeEducationalEstablishment == null)
			{
				context.SetError("invalid_grant", "Usuario incorrecto.");
				return;
			}

			ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, OAuthDefaults.AuthenticationType);

			Claim claim;
			do
			{
				claim = oAuthIdentity.Claims.FirstOrDefault(p => p.Type == ClaimTypes.Role);

				if (claim != null) 
					oAuthIdentity.TryRemoveClaim(claim);
			}
			while (claim != null);
			

			oAuthIdentity.AddClaim(new Claim(ClaimTypes.Role, role));
			oAuthIdentity.AddClaim(new Claim("interactic-idEducationalEstablishment", idEducationalEstablishment));
			oAuthIdentity.AddClaim(new Claim("interactic-year", year));

			ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager, CookieAuthenticationDefaults.AuthenticationType);

			AuthenticationProperties properties = CreateProperties(user, userTypeEducationalEstablishment, year);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override System.Threading.Tasks.Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return System.Threading.Tasks.Task.FromResult<object>(null);
        }

        public override System.Threading.Tasks.Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

			string idEducationalEstablishment = context.Parameters.Where(f => f.Key == "idEducationalEstablishment").Select(f => f.Value).SingleOrDefault()[0];
			context.OwinContext.Set<string>("idEducationalEstablishment", idEducationalEstablishment);
			
			string role = context.Parameters.Where(f => f.Key == "role").Select(f => f.Value).SingleOrDefault()[0];
			context.OwinContext.Set<string>("role", role);

			string year = context.Parameters.Where(f => f.Key == "year").Select(f => f.Value).SingleOrDefault()[0];
			context.OwinContext.Set<string>("year", year);

			return System.Threading.Tasks.Task.FromResult<object>(null);
        }

        public override System.Threading.Tasks.Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return System.Threading.Tasks.Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
			IDictionary<string, string> data = new Dictionary<string, string>
			{
				{ "userName", userName }			
			};

            return new AuthenticationProperties(data);
        }

		public static AuthenticationProperties CreateProperties(User user, UserRoleEducationalEstablishment userTypeEducationalEstablishment, string year = "")
		{
			IDictionary<string, string> data = new Dictionary<string, string>
			{
				{ "userName", user.UserName },
				{ "nameUser", user.Name},
				{ "pathImageUser", user.ImagePath?? "" },

				{ "role", userTypeEducationalEstablishment.Role.Name },

				{ "idEducationalEstablishment", userTypeEducationalEstablishment.EducationalEstablishment_Id.ToString() },
				{ "nameEducationalEstablishment", userTypeEducationalEstablishment.EducationalEstablishment.Name},
				{ "pathImageEducationalEstablishment", userTypeEducationalEstablishment.EducationalEstablishment.PathImage??"" },

				{ "year", year }
			};

			return new AuthenticationProperties(data);
		}


	}
}