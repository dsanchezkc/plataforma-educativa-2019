﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Providers
{
	public class CustomOAuthGrantResourceOwnerCredentialsContext : OAuthGrantResourceOwnerCredentialsContext
	{
		public string prueba { set; get; }

		public CustomOAuthGrantResourceOwnerCredentialsContext(IOwinContext context, OAuthAuthorizationServerOptions options, string clientId, string userName, string password, IList<string> scope) : base(context, options, clientId, userName, password, scope)
		{ }
	}
}