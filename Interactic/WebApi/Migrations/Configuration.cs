namespace WebApi.Migrations
{
	using Microsoft.AspNet.Identity;
	using Microsoft.AspNet.Identity.EntityFramework;
	using Model;
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
	using WebApi.Stores;

	internal sealed class Configuration : DbMigrationsConfiguration<InteracticContext>
    {
        public Configuration()
        {
			AutomaticMigrationsEnabled = true;
			ContextKey = "WebApi.Models.ApplicationDbContext";
        }

        protected override void Seed(InteracticContext context)
        {
			Seed_RegionsProvincesCommunes(context);
			Seed_EstablishmentPeriod(context);
			Seed_FirstEducationalEstablishment(context);
			Seed_AccountTypes(context);
			Seed_FirstAdmin(context);
		}

		private void Seed_EstablishmentPeriod(Model.InteracticContext context)
		{
			var result = context.EstablishmentPeriod.FirstOrDefault();

			if (result != null) return;

			context.EstablishmentPeriod.Add(new EstablishmentPeriod()
			{
				Name = "Semestre",
				PeriodDetails = new List<PeriodDetail>()
				{
					new PeriodDetail(){ Name = "Semestre 1" },
					new PeriodDetail(){ Name = "Semestre 2" }
				}
			});

			context.EstablishmentPeriod.Add(new EstablishmentPeriod()
			{
				Name = "Trimestre",
				PeriodDetails = new List<PeriodDetail>()
				{
					new PeriodDetail(){ Name = "Trimistre 1" },
					new PeriodDetail(){ Name = "Trimistre 2" },
					new PeriodDetail(){ Name = "Trimistre 3" }
				}
			});

			context.SaveChanges();
		}

		private void Seed_FirstEducationalEstablishment(Model.InteracticContext context)
		{
			var result = context.EducationalEstablishment.FirstOrDefault();

			if (result != null) return;

			context.EducationalEstablishment.Add(
					new EducationalEstablishment()
					{
						Name = "Interactic",
						Address = "Default",
						Commune = context.Commune.FirstOrDefault(),
						EstablishmentPeriod = context.EstablishmentPeriod.FirstOrDefault()
					});

			context.SaveChanges();
		}

		private void Seed_FirstAdmin(Model.InteracticContext context)
		{
			if (context.Users.FirstOrDefault(p => p.UserName == "1007") != null)
				return;

			var userManager = new ApplicationUserManager(new ApplicationUserStore(context));

			var user = new User() { Id = Guid.NewGuid().ToString(), UserName = "1007", Email = "admin@interactic.cl", Name = "Administrador", FatherLastName = "Administrador", MotherLastName = "Administrador", Address = "Base de datos" };
			var result = userManager.Create(user, "asp.net");
		
			var _user = context.Users.FirstOrDefault(p => p.UserName == "1007");
			var rol = context.Roles.OfType<Role>().FirstOrDefault(p => p.Name == "Administrador");
			var establishment = context.EducationalEstablishment.FirstOrDefault();

			context.UserRoleEducationalEstablishment.Add(new UserRoleEducationalEstablishment() { User = _user, Role = rol, EducationalEstablishment_Id = establishment.Id });
			context.SaveChanges();
		}

		private void Seed_AccountTypes(InteracticContext context)
		{
			var roleStore = new ApplicationAccountTypeStore(context);
			var roleManager = new ApplicationRoleManager(roleStore);

			if (!roleManager.RoleExists("Administrador"))
				roleManager.Create(new Role() {Id=Guid.NewGuid().ToString() ,Name = "Administrador", Rank = 1 });
		
			if (!roleManager.RoleExists("SubAdministrador"))
				roleManager.Create(new Role() {Id=Guid.NewGuid().ToString(), Name = "SubAdministrador", Rank = 2 });			

			if (!roleManager.RoleExists("Profesor"))
				roleManager.Create(new Role() {Id=Guid.NewGuid().ToString(), Name = "Profesor", Rank = 3 });

			if (!roleManager.RoleExists("Alumno"))
				roleManager.Create(new Role() {Id=Guid.NewGuid().ToString(), Name = "Alumno", Rank = 4 });
		}

		#region Ingreso de Regions, Provincias y Comunas
		private void Seed_RegionsProvincesCommunes(InteracticContext context)
		{
			#region AddRegiones
			AddRegion(1, "Tarapac�", context);
			AddRegion(2, "Antofagasta", context);
			AddRegion(3, "Atacama", context);
			AddRegion(4, "Coquimbo", context);
			AddRegion(5, "Valpara�so", context);
			AddRegion(6, "Regi�n del Libertador Gral. Bernardo O�Higgins", context);
			AddRegion(7, "Regi�n del Maule", context);
			AddRegion(8, "Regi�n del Biob�o", context);
			AddRegion(9, "Regi�n de la Araucan�a", context);
			AddRegion(10, "Regi�n de Los Lagos", context);
			AddRegion(11, "Regi�n Ais�n del Gral. Carlos Ib��ez del Campo", context);
			AddRegion(12, "Regi�n de Magallanes y de la Ant�rtica Chilena", context);
			AddRegion(13, "Regi�n Metropolitana de Santiago", context);
			AddRegion(14, "Regi�n de Los R�os", context);
			AddRegion(15, "Arica y Parinacota", context);
			#endregion

			#region AddProvincias
			AddProvince(11, "Iquique", 1, context);
			AddProvince(14, "Tamarugal", 1, context);
			AddProvince(21, "Antofagasta", 2, context);
			AddProvince(22, "El Loa", 2, context);
			AddProvince(23, "Tocopilla", 2, context);
			AddProvince(31, "Copiap�", 3, context);
			AddProvince(32, "Cha�aral", 3, context);
			AddProvince(33, "Huasco", 3, context);
			AddProvince(41, "Elqui", 4, context);
			AddProvince(42, "Choapa", 4, context);
			AddProvince(43, "Limar�", 4, context);
			AddProvince(51, "Valpara�so", 5, context);
			AddProvince(52, "Isla de Pascua", 5, context);
			AddProvince(53, "Los Andes", 5, context);
			AddProvince(54, "Petorca", 5, context);
			AddProvince(55, "Quillota", 5, context);
			AddProvince(56, "San Antonio", 5, context);
			AddProvince(57, "San Felipe de Aconcagua", 5, context);
			AddProvince(58, "Marga Marga", 5, context);
			AddProvince(61, "Cachapoal", 6, context);
			AddProvince(62, "Cardenal Caro", 6, context);
			AddProvince(63, "Colchagua", 6, context);
			AddProvince(71, "Talca", 7, context);
			AddProvince(72, "Cauquenes", 7, context);
			AddProvince(73, "Curic�", 7, context);
			AddProvince(74, "Linares", 7, context);
			AddProvince(81, "Concepci�n", 8, context);
			AddProvince(82, "Arauco", 8, context);
			AddProvince(83, "Biob�o", 8, context);
			AddProvince(84, "�uble", 8, context);
			AddProvince(91, "Caut�n", 9, context);
			AddProvince(92, "Malleco", 9, context);
			AddProvince(101, "Llanquihue", 10, context);
			AddProvince(102, "Chilo�", 10, context);
			AddProvince(103, "Osorno", 10, context);
			AddProvince(104, "Palena", 10, context);
			AddProvince(111, "Coihaique", 11, context);
			AddProvince(112, "Ais�n", 11, context);
			AddProvince(113, "Capit�n Prat", 11, context);
			AddProvince(114, "General Carrera", 11, context);
			AddProvince(121, "Magallanes", 12, context);
			AddProvince(122, "Ant�rtica Chilena", 12, context);
			AddProvince(123, "Tierra del Fuego", 12, context);
			AddProvince(124, "�ltima Esperanza", 12, context);
			AddProvince(131, "Santiago", 13, context);
			AddProvince(132, "Cordillera", 13, context);
			AddProvince(133, "Chacabuco", 13, context);
			AddProvince(134, "Maipo", 13, context);
			AddProvince(135, "Melipilla", 13, context);
			AddProvince(136, "Talagante", 13, context);
			AddProvince(141, "Valdivia", 14, context);
			AddProvince(142, "Ranco", 14, context);
			AddProvince(151, "Arica", 15, context);
			AddProvince(152, "Parinacota", 15, context);
			#endregion

			#region AddComunas
			AddCommune(1101, "Iquique", 11, context);
			AddCommune(1107, "Alto Hospicio", 11, context);
			AddCommune(1401, "Pozo Almonte", 14, context);
			AddCommune(1402, "Cami�a", 14, context);
			AddCommune(1403, "Colchane", 14, context);
			AddCommune(1404, "Huara", 14, context);
			AddCommune(1405, "Pica", 14, context);
			AddCommune(2101, "Antofagasta", 21, context);
			AddCommune(2102, "Mejillones", 21, context);
			AddCommune(2103, "Sierra Gorda", 21, context);
			AddCommune(2104, "Taltal", 21, context);
			AddCommune(2201, "Calama", 22, context);
			AddCommune(2202, "Ollag�e", 22, context);
			AddCommune(2203, "San Pedro de Atacama", 22, context);
			AddCommune(2301, "Tocopilla", 23, context);
			AddCommune(2302, "Mar�a Elena", 23, context);
			AddCommune(3101, "Copiap�", 31, context);
			AddCommune(3102, "Caldera", 31, context);
			AddCommune(3103, "Tierra Amarilla", 31, context);
			AddCommune(3201, "Cha�aral", 32, context);
			AddCommune(3202, "Diego de Almagro", 32, context);
			AddCommune(3301, "Vallenar", 33, context);
			AddCommune(3302, "Alto del Carmen", 33, context);
			AddCommune(3303, "Freirina", 33, context);
			AddCommune(3304, "Huasco", 33, context);
			AddCommune(4101, "La Serena", 41, context);
			AddCommune(4102, "Coquimbo", 41, context);
			AddCommune(4103, "Andacollo", 41, context);
			AddCommune(4104, "La Higuera", 41, context);
			AddCommune(4105, "Paiguano", 41, context);
			AddCommune(4106, "Vicu�a", 41, context);
			AddCommune(4201, "Illapel", 42, context);
			AddCommune(4202, "Canela", 42, context);
			AddCommune(4203, "Los Vilos", 42, context);
			AddCommune(4204, "Salamanca", 42, context);
			AddCommune(4301, "Ovalle", 43, context);
			AddCommune(4302, "Combarbal�", 43, context);
			AddCommune(4303, "Monte Patria", 43, context);
			AddCommune(4304, "Punitaqui", 43, context);
			AddCommune(4305, "R�o Hurtado", 43, context);
			AddCommune(5101, "Valpara�so", 51, context);
			AddCommune(5102, "Casablanca", 51, context);
			AddCommune(5103, "Conc�n", 51, context);
			AddCommune(5104, "Juan Fern�ndez", 51, context);
			AddCommune(5105, "Puchuncav�", 51, context);
			AddCommune(5107, "Quintero", 51, context);
			AddCommune(5109, "Vi�a del Mar", 51, context);
			AddCommune(5201, "Isla de Pascua", 52, context);
			AddCommune(5301, "Los Andes", 53, context);
			AddCommune(5302, "Calle Larga", 53, context);
			AddCommune(5303, "Rinconada", 53, context);
			AddCommune(5304, "San Esteban", 53, context);
			AddCommune(5401, "La Ligua", 54, context);
			AddCommune(5402, "Cabildo", 54, context);
			AddCommune(5403, "Papudo", 54, context);
			AddCommune(5404, "Petorca", 54, context);
			AddCommune(5405, "Zapallar", 54, context);
			AddCommune(5501, "Quillota", 55, context);
			AddCommune(5502, "Calera", 55, context);
			AddCommune(5503, "Hijuelas", 55, context);
			AddCommune(5504, "La Cruz", 55, context);
			AddCommune(5506, "Nogales", 55, context);
			AddCommune(5601, "San Antonio", 56, context);
			AddCommune(5602, "Algarrobo", 56, context);
			AddCommune(5603, "Cartagena", 56, context);
			AddCommune(5604, "El Quisco", 56, context);
			AddCommune(5605, "El Tabo", 56, context);
			AddCommune(5606, "Santo Domingo", 56, context);
			AddCommune(5701, "San Felipe", 57, context);
			AddCommune(5702, "Catemu", 57, context);
			AddCommune(5703, "Llaillay", 57, context);
			AddCommune(5704, "Panquehue", 57, context);
			AddCommune(5705, "Putaendo", 57, context);
			AddCommune(5706, "Santa Mar�a", 57, context);
			AddCommune(5801, "Quilpu�", 58, context);
			AddCommune(5802, "Limache", 58, context);
			AddCommune(5803, "Olmu�", 58, context);
			AddCommune(5804, "Villa Alemana", 58, context);
			AddCommune(6101, "Rancagua", 61, context);
			AddCommune(6102, "Codegua", 61, context);
			AddCommune(6103, "Coinco", 61, context);
			AddCommune(6104, "Coltauco", 61, context);
			AddCommune(6105, "Do�ihue", 61, context);
			AddCommune(6106, "Graneros", 61, context);
			AddCommune(6107, "Las Cabras", 61, context);
			AddCommune(6108, "Machal�", 61, context);
			AddCommune(6109, "Malloa", 61, context);
			AddCommune(6110, "Mostazal", 61, context);
			AddCommune(6111, "Olivar", 61, context);
			AddCommune(6112, "Peumo", 61, context);
			AddCommune(6113, "Pichidegua", 61, context);
			AddCommune(6114, "Quinta de Tilcoco", 61, context);
			AddCommune(6115, "Rengo", 61, context);
			AddCommune(6116, "Requ�noa", 61, context);
			AddCommune(6117, "San Vicente", 61, context);
			AddCommune(6201, "Pichilemu", 62, context);
			AddCommune(6202, "La Estrella", 62, context);
			AddCommune(6203, "Litueche", 62, context);
			AddCommune(6204, "Marchihue", 62, context);
			AddCommune(6205, "Navidad", 62, context);
			AddCommune(6206, "Paredones", 62, context);
			AddCommune(6301, "San Fernando", 63, context);
			AddCommune(6302, "Ch�pica", 63, context);
			AddCommune(6303, "Chimbarongo", 63, context);
			AddCommune(6304, "Lolol", 63, context);
			AddCommune(6305, "Nancagua", 63, context);
			AddCommune(6306, "Palmilla", 63, context);
			AddCommune(6307, "Peralillo", 63, context);
			AddCommune(6308, "Placilla", 63, context);
			AddCommune(6309, "Pumanque", 63, context);
			AddCommune(6310, "Santa Cruz", 63, context);
			AddCommune(7101, "Talca", 71, context);
			AddCommune(7102, "Constituci�n", 71, context);
			AddCommune(7103, "Curepto", 71, context);
			AddCommune(7104, "Empedrado", 71, context);
			AddCommune(7105, "Maule", 71, context);
			AddCommune(7106, "Pelarco", 71, context);
			AddCommune(7107, "Pencahue", 71, context);
			AddCommune(7108, "R�o Claro", 71, context);
			AddCommune(7109, "San Clemente", 71, context);
			AddCommune(7110, "San Rafael", 71, context);
			AddCommune(7201, "Cauquenes", 72, context);
			AddCommune(7202, "Chanco", 72, context);
			AddCommune(7203, "Pelluhue", 72, context);
			AddCommune(7301, "Curic�", 73, context);
			AddCommune(7302, "Huala��", 73, context);
			AddCommune(7303, "Licant�n", 73, context);
			AddCommune(7304, "Molina", 73, context);
			AddCommune(7305, "Rauco", 73, context);
			AddCommune(7306, "Romeral", 73, context);
			AddCommune(7307, "Sagrada Familia", 73, context);
			AddCommune(7308, "Teno", 73, context);
			AddCommune(7309, "Vichuqu�n", 73, context);
			AddCommune(7401, "Linares", 74, context);
			AddCommune(7402, "Colb�n", 74, context);
			AddCommune(7403, "Longav�", 74, context);
			AddCommune(7404, "Parral", 74, context);
			AddCommune(7405, "Retiro", 74, context);
			AddCommune(7406, "San Javier", 74, context);
			AddCommune(7407, "Villa Alegre", 74, context);
			AddCommune(7408, "Yerbas Buenas", 74, context);
			AddCommune(8101, "Concepci�n", 81, context);
			AddCommune(8102, "Coronel", 81, context);
			AddCommune(8103, "Chiguayante", 81, context);
			AddCommune(8104, "Florida", 81, context);
			AddCommune(8105, "Hualqui", 81, context);
			AddCommune(8106, "Lota", 81, context);
			AddCommune(8107, "Penco", 81, context);
			AddCommune(8108, "San Pedro de la Paz", 81, context);
			AddCommune(8109, "Santa Juana", 81, context);
			AddCommune(8110, "Talcahuano", 81, context);
			AddCommune(8111, "Tom�", 81, context);
			AddCommune(8112, "Hualp�n", 81, context);
			AddCommune(8201, "Lebu", 82, context);
			AddCommune(8202, "Arauco", 82, context);
			AddCommune(8203, "Ca�ete", 82, context);
			AddCommune(8204, "Contulmo", 82, context);
			AddCommune(8205, "Curanilahue", 82, context);
			AddCommune(8206, "Los �lamos", 82, context);
			AddCommune(8207, "Tir�a", 82, context);
			AddCommune(8301, "Los �ngeles", 83, context);
			AddCommune(8302, "Antuco", 83, context);
			AddCommune(8303, "Cabrero", 83, context);
			AddCommune(8304, "Laja", 83, context);
			AddCommune(8305, "Mulch�n", 83, context);
			AddCommune(8306, "Nacimiento", 83, context);
			AddCommune(8307, "Negrete", 83, context);
			AddCommune(8308, "Quilaco", 83, context);
			AddCommune(8309, "Quilleco", 83, context);
			AddCommune(8310, "San Rosendo", 83, context);
			AddCommune(8311, "Santa B�rbara", 83, context);
			AddCommune(8312, "Tucapel", 83, context);
			AddCommune(8313, "Yumbel", 83, context);
			AddCommune(8314, "Alto Biob�o", 83, context);
			AddCommune(8401, "Chill�n", 84, context);
			AddCommune(8402, "Bulnes", 84, context);
			AddCommune(8403, "Cobquecura", 84, context);
			AddCommune(8404, "Coelemu", 84, context);
			AddCommune(8405, "Coihueco", 84, context);
			AddCommune(8406, "Chill�n Viejo", 84, context);
			AddCommune(8407, "El Carmen", 84, context);
			AddCommune(8408, "Ninhue", 84, context);
			AddCommune(8409, "�iqu�n", 84, context);
			AddCommune(8410, "Pemuco", 84, context);
			AddCommune(8411, "Pinto", 84, context);
			AddCommune(8412, "Portezuelo", 84, context);
			AddCommune(8413, "Quill�n", 84, context);
			AddCommune(8414, "Quirihue", 84, context);
			AddCommune(8415, "R�nquil", 84, context);
			AddCommune(8416, "San Carlos", 84, context);
			AddCommune(8417, "San Fabi�n", 84, context);
			AddCommune(8418, "San Ignacio", 84, context);
			AddCommune(8419, "San Nicol�s", 84, context);
			AddCommune(8420, "Treguaco", 84, context);
			AddCommune(8421, "Yungay", 84, context);
			AddCommune(9101, "Temuco", 91, context);
			AddCommune(9102, "Carahue", 91, context);
			AddCommune(9103, "Cunco", 91, context);
			AddCommune(9104, "Curarrehue", 91, context);
			AddCommune(9105, "Freire", 91, context);
			AddCommune(9106, "Galvarino", 91, context);
			AddCommune(9107, "Gorbea", 91, context);
			AddCommune(9108, "Lautaro", 91, context);
			AddCommune(9109, "Loncoche", 91, context);
			AddCommune(9110, "Melipeuco", 91, context);
			AddCommune(9111, "Nueva Imperial", 91, context);
			AddCommune(9112, "Padre las Casas", 91, context);
			AddCommune(9113, "Perquenco", 91, context);
			AddCommune(9114, "Pitrufqu�n", 91, context);
			AddCommune(9115, "Puc�n", 91, context);
			AddCommune(9116, "Saavedra", 91, context);
			AddCommune(9117, "Teodoro Schmidt", 91, context);
			AddCommune(9118, "Tolt�n", 91, context);
			AddCommune(9119, "Vilc�n", 91, context);
			AddCommune(9120, "Villarrica", 91, context);
			AddCommune(9121, "Cholchol", 91, context);
			AddCommune(9201, "Angol", 92, context);
			AddCommune(9202, "Collipulli", 92, context);
			AddCommune(9203, "Curacaut�n", 92, context);
			AddCommune(9204, "Ercilla", 92, context);
			AddCommune(9205, "Lonquimay", 92, context);
			AddCommune(9206, "Los Sauces", 92, context);
			AddCommune(9207, "Lumaco", 92, context);
			AddCommune(9208, "Pur�n", 92, context);
			AddCommune(9209, "Renaico", 92, context);
			AddCommune(9210, "Traigu�n", 92, context);
			AddCommune(9211, "Victoria", 92, context);
			AddCommune(10101, "Puerto Montt", 101, context);
			AddCommune(10102, "Calbuco", 101, context);
			AddCommune(10103, "Cocham�", 101, context);
			AddCommune(10104, "Fresia", 101, context);
			AddCommune(10105, "Frutillar", 101, context);
			AddCommune(10106, "Los Muermos", 101, context);
			AddCommune(10107, "Llanquihue", 101, context);
			AddCommune(10108, "Maull�n", 101, context);
			AddCommune(10109, "Puerto Varas", 101, context);
			AddCommune(10201, "Castro", 102, context);
			AddCommune(10202, "Ancud", 102, context);
			AddCommune(10203, "Chonchi", 102, context);
			AddCommune(10204, "Curaco de V�lez", 102, context);
			AddCommune(10205, "Dalcahue", 102, context);
			AddCommune(10206, "Puqueld�n", 102, context);
			AddCommune(10207, "Queil�n", 102, context);
			AddCommune(10208, "Quell�n", 102, context);
			AddCommune(10209, "Quemchi", 102, context);
			AddCommune(10210, "Quinchao", 102, context);
			AddCommune(10301, "Osorno", 103, context);
			AddCommune(10302, "Puerto Octay", 103, context);
			AddCommune(10303, "Purranque", 103, context);
			AddCommune(10304, "Puyehue", 103, context);
			AddCommune(10305, "R�o Negro", 103, context);
			AddCommune(10306, "San Juan de la Costa", 103, context);
			AddCommune(10307, "San Pablo", 103, context);
			AddCommune(10401, "Chait�n", 104, context);
			AddCommune(10402, "Futaleuf�", 104, context);
			AddCommune(10403, "Hualaihu�", 104, context);
			AddCommune(10404, "Palena", 104, context);
			AddCommune(11101, "Coihaique", 111, context);
			AddCommune(11102, "Lago Verde", 111, context);
			AddCommune(11201, "Ais�n", 112, context);
			AddCommune(11202, "Cisnes", 112, context);
			AddCommune(11203, "Guaitecas", 112, context);
			AddCommune(11301, "Cochrane", 113, context);
			AddCommune(11302, "O�Higgins", 113, context);
			AddCommune(11303, "Tortel", 113, context);
			AddCommune(11401, "Chile Chico", 114, context);
			AddCommune(11402, "R�o Ib��ez", 114, context);
			AddCommune(12101, "Punta Arenas", 121, context);
			AddCommune(12102, "Laguna Blanca", 121, context);
			AddCommune(12103, "R�o Verde", 121, context);
			AddCommune(12104, "San Gregorio", 121, context);
			AddCommune(12201, "Cabo de Hornos", 122, context);
			AddCommune(12202, "Ant�rtica", 122, context);
			AddCommune(12301, "Porvenir", 123, context);
			AddCommune(12302, "Primavera", 123, context);
			AddCommune(12303, "Timaukel", 123, context);
			AddCommune(12401, "Natales", 124, context);
			AddCommune(12402, "Torres del Paine", 124, context);
			AddCommune(13101, "Santiago", 131, context);
			AddCommune(13102, "Cerrillos", 131, context);
			AddCommune(13103, "Cerro Navia", 131, context);
			AddCommune(13104, "Conchal�", 131, context);
			AddCommune(13105, "El Bosque", 131, context);
			AddCommune(13106, "Estaci�n Central", 131, context);
			AddCommune(13107, "Huechuraba", 131, context);
			AddCommune(13108, "Independencia", 131, context);
			AddCommune(13109, "La Cisterna", 131, context);
			AddCommune(13110, "La Florida", 131, context);
			AddCommune(13111, "La Granja", 131, context);
			AddCommune(13112, "La Pintana", 131, context);
			AddCommune(13113, "La Reina", 131, context);
			AddCommune(13114, "Las Condes", 131, context);
			AddCommune(13115, "Lo Barnechea", 131, context);
			AddCommune(13116, "Lo Espejo", 131, context);
			AddCommune(13117, "Lo Prado", 131, context);
			AddCommune(13118, "Macul", 131, context);
			AddCommune(13119, "Maip�", 131, context);
			AddCommune(13120, "�u�oa", 131, context);
			AddCommune(13121, "Pedro Aguirre Cerda", 131, context);
			AddCommune(13122, "Pe�alol�n", 131, context);
			AddCommune(13123, "Providencia", 131, context);
			AddCommune(13124, "Pudahuel", 131, context);
			AddCommune(13125, "Quilicura", 131, context);
			AddCommune(13126, "Quinta Normal", 131, context);
			AddCommune(13127, "Recoleta", 131, context);
			AddCommune(13128, "Renca", 131, context);
			AddCommune(13129, "San Joaqu�n", 131, context);
			AddCommune(13130, "San Miguel", 131, context);
			AddCommune(13131, "San Ram�n", 131, context);
			AddCommune(13132, "Vitacura", 131, context);
			AddCommune(13201, "Puente Alto", 132, context);
			AddCommune(13202, "Pirque", 132, context);
			AddCommune(13203, "San Jos� de Maipo", 132, context);
			AddCommune(13301, "Colina", 133, context);
			AddCommune(13302, "Lampa", 133, context);
			AddCommune(13303, "Tiltil", 133, context);
			AddCommune(13401, "San Bernardo", 134, context);
			AddCommune(13402, "Buin", 134, context);
			AddCommune(13403, "Calera de Tango", 134, context);
			AddCommune(13404, "Paine", 134, context);
			AddCommune(13501, "Melipilla", 135, context);
			AddCommune(13502, "Alhu�", 135, context);
			AddCommune(13503, "Curacav�", 135, context);
			AddCommune(13504, "Mar�a Pinto", 135, context);
			AddCommune(13505, "San Pedro", 135, context);
			AddCommune(13601, "Talagante", 136, context);
			AddCommune(13602, "El Monte", 136, context);
			AddCommune(13603, "Isla de Maipo", 136, context);
			AddCommune(13604, "Padre Hurtado", 136, context);
			AddCommune(13605, "Pe�aflor", 136, context);
			AddCommune(14101, "Valdivia", 141, context);
			AddCommune(14102, "Corral", 141, context);
			AddCommune(14103, "Lanco", 141, context);
			AddCommune(14104, "Los Lagos", 141, context);
			AddCommune(14105, "M�fil", 141, context);
			AddCommune(14106, "Mariquina", 141, context);
			AddCommune(14107, "Paillaco", 141, context);
			AddCommune(14108, "Panguipulli", 141, context);
			AddCommune(14201, "La Uni�n", 142, context);
			AddCommune(14202, "Futrono", 142, context);
			AddCommune(14203, "Lago Ranco", 142, context);
			AddCommune(14204, "R�o Bueno", 142, context);
			AddCommune(15101, "Arica", 151, context);
			AddCommune(15102, "Camarones", 151, context);
			AddCommune(15201, "Putre", 152, context);
			AddCommune(15202, "General Lagos", 152, context);
			#endregion

			context.SaveChanges();
		}

		Dictionary<int, Region> Regions = new Dictionary<int, Region>();
		Dictionary<int, Province> Provinces = new Dictionary<int, Province>();

		private void AddRegion(int Id, string Name, InteracticContext context)
		{
			var region = new Region() { Name = Name, Provinces = new List<Province>() };
			var result = context.Region.FirstOrDefault(p => p.Name == Name);

			if (result == null)
				context.Region.Add(region);
			else
				region = result;

			Regions.Add(Id, region);
		}

		private void AddProvince(int Id, string Name, int Region_Id, InteracticContext context)
		{
			var province = new Province()
			{
				Name = Name,
				Communes = new List<Commune>(),
				Region = Regions[Region_Id]
			};

			var result = context.Province.FirstOrDefault(p => p.Name == Name);

			if (result == null)
				context.Province.Add(province);
			else
				province = result;

			Provinces.Add(Id, province);
		}

		private void AddCommune(int Id, string Name, int Province_Id, InteracticContext context)
		{
			var commune = new Commune()
			{
				Name = Name,
				Province = Provinces[Province_Id]
			};

			var result = context.Commune.FirstOrDefault(p => p.Name == Name);

			if (result == null)
				context.Commune.Add(commune);
		}
		#endregion

	}
}
