namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models14 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Question", "ComplementaryAnswer", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Question", "ComplementaryAnswer");
        }
    }
}
