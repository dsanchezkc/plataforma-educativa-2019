namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models9 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Post", "Author_Id", "dbo.User");
            DropIndex("dbo.Post", new[] { "Author_Id" });
            CreateTable(
                "dbo.PostQualification",
                c => new
                    {
                        User_Id = c.String(nullable: false, maxLength: 128),
                        Post_Id = c.Int(nullable: false),
                        Qualification = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Post_Id })
                .ForeignKey("dbo.Post", t => t.Post_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Post_Id);
            
            DropColumn("dbo.Post", "Author_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Post", "Author_Id", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.PostQualification", "User_Id", "dbo.User");
            DropForeignKey("dbo.PostQualification", "Post_Id", "dbo.Post");
            DropIndex("dbo.PostQualification", new[] { "Post_Id" });
            DropIndex("dbo.PostQualification", new[] { "User_Id" });
            DropTable("dbo.PostQualification");
            CreateIndex("dbo.Post", "Author_Id");
            AddForeignKey("dbo.Post", "Author_Id", "dbo.User", "Id");
        }
    }
}
