namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models19 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Test", "SubUnity_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Test", "SubUnity_Id");
            AddForeignKey("dbo.Test", "SubUnity_Id", "dbo.SubUnity", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Test", "SubUnity_Id", "dbo.SubUnity");
            DropIndex("dbo.Test", new[] { "SubUnity_Id" });
            DropColumn("dbo.Test", "SubUnity_Id");
        }
    }
}
