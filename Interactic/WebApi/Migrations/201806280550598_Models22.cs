namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models22 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UploadFileStudentItem", "Student_Id", "dbo.User");
            DropForeignKey("dbo.UploadFileStudentItem", "UploadFileStudent_Id", "dbo.UploadFileStudent");
            DropIndex("dbo.UploadFileStudentItem", new[] { "Student_Id" });
            DropIndex("dbo.UploadFileStudentItem", new[] { "UploadFileStudent_Id" });
            CreateTable(
                "dbo.UploadFileStudentQualification",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Qualification = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Student_Id = c.String(nullable: false, maxLength: 128),
                        UploadFileStudent_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Student_Id)
                .ForeignKey("dbo.UploadFileStudent", t => t.UploadFileStudent_Id)
                .Index(t => t.Student_Id)
                .Index(t => t.UploadFileStudent_Id);
            
            AddColumn("dbo.UploadFileStudentItem", "UploadFileStudentQualification_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.UploadFileStudentItem", "UploadFileStudentQualification_Id");
            AddForeignKey("dbo.UploadFileStudentItem", "UploadFileStudentQualification_Id", "dbo.UploadFileStudentQualification", "Id");
            DropColumn("dbo.UploadFileStudentItem", "Student_Id");
            DropColumn("dbo.UploadFileStudentItem", "UploadFileStudent_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UploadFileStudentItem", "UploadFileStudent_Id", c => c.Int(nullable: false));
            AddColumn("dbo.UploadFileStudentItem", "Student_Id", c => c.String(nullable: false, maxLength: 128));
            DropForeignKey("dbo.UploadFileStudentItem", "UploadFileStudentQualification_Id", "dbo.UploadFileStudentQualification");
            DropForeignKey("dbo.UploadFileStudentQualification", "UploadFileStudent_Id", "dbo.UploadFileStudent");
            DropForeignKey("dbo.UploadFileStudentQualification", "Student_Id", "dbo.User");
            DropIndex("dbo.UploadFileStudentItem", new[] { "UploadFileStudentQualification_Id" });
            DropIndex("dbo.UploadFileStudentQualification", new[] { "UploadFileStudent_Id" });
            DropIndex("dbo.UploadFileStudentQualification", new[] { "Student_Id" });
            DropColumn("dbo.UploadFileStudentItem", "UploadFileStudentQualification_Id");
            DropTable("dbo.UploadFileStudentQualification");
            CreateIndex("dbo.UploadFileStudentItem", "UploadFileStudent_Id");
            CreateIndex("dbo.UploadFileStudentItem", "Student_Id");
            AddForeignKey("dbo.UploadFileStudentItem", "UploadFileStudent_Id", "dbo.UploadFileStudent", "Id");
            AddForeignKey("dbo.UploadFileStudentItem", "Student_Id", "dbo.User", "Id");
        }
    }
}
