namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models18 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubUnity", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SubUnity", "Order");
        }
    }
}
