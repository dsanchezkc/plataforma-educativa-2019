namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models20 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UnityViewed",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Timer = c.Int(nullable: false),
                        SessionCode = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Unity_Id = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Unity", t => t.Unity_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.Unity_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UnityViewed", "User_Id", "dbo.User");
            DropForeignKey("dbo.UnityViewed", "Unity_Id", "dbo.Unity");
            DropIndex("dbo.UnityViewed", new[] { "User_Id" });
            DropIndex("dbo.UnityViewed", new[] { "Unity_Id" });
            DropTable("dbo.UnityViewed");
        }
    }
}
