namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models17 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Action", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Action", "Order");
        }
    }
}
