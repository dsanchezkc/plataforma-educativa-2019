namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Forum", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Forum", "Type");
        }
    }
}
