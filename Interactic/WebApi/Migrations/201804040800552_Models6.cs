namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topic", "Author_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Topic", "Author_Id");
            AddForeignKey("dbo.Topic", "Author_Id", "dbo.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Topic", "Author_Id", "dbo.User");
            DropIndex("dbo.Topic", new[] { "Author_Id" });
            DropColumn("dbo.Topic", "Author_Id");
        }
    }
}
