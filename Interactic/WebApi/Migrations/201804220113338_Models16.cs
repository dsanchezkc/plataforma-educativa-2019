namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models16 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Activity", "Order", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Activity", "Order");
        }
    }
}
