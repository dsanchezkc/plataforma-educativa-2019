namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models15 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Viewed",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModelBase_Type = c.String(),
                        ModelBase_Id = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Viewed", "User_Id", "dbo.User");
            DropIndex("dbo.Viewed", new[] { "User_Id" });
            DropTable("dbo.Viewed");
        }
    }
}
