namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Topic", "AllowPosting", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Topic", "AllowPosting");
        }
    }
}
