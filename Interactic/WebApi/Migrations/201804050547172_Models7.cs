namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Post", "Author_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Post", "Author_Id");
            AddForeignKey("dbo.Post", "Author_Id", "dbo.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Post", "Author_Id", "dbo.User");
            DropIndex("dbo.Post", new[] { "Author_Id" });
            DropColumn("dbo.Post", "Author_Id");
        }
    }
}
