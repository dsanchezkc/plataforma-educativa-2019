namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Publication", newName: "Forum");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Forum", newName: "Publication");
        }
    }
}
