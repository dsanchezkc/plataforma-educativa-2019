namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Action",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Task_Id = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Activity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Activity", t => t.Activity_Id)
                .Index(t => t.Activity_Id);
            
            CreateTable(
                "dbo.Activity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        SubUnity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SubUnity", t => t.SubUnity_Id)
                .Index(t => t.SubUnity_Id);
            
            CreateTable(
                "dbo.SubUnity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Start = c.DateTime(nullable: false),
                        Ends = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Unity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Unity", t => t.Unity_Id)
                .Index(t => t.Unity_Id);
            
            CreateTable(
                "dbo.Unity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Year = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        CourseSubject_Subject_Id = c.Int(nullable: false),
                        CourseSubject_Course_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CourseSubject", t => new { t.CourseSubject_Subject_Id, t.CourseSubject_Course_Id })
                .Index(t => new { t.CourseSubject_Subject_Id, t.CourseSubject_Course_Id });
            
            CreateTable(
                "dbo.CourseSubject",
                c => new
                    {
                        Subject_Id = c.Int(nullable: false),
                        Course_Id = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Subject_Id, t.Course_Id })
                .ForeignKey("dbo.Course", t => t.Course_Id)
                .ForeignKey("dbo.Subject", t => t.Subject_Id)
                .Index(t => t.Subject_Id)
                .Index(t => t.Course_Id);
            
            CreateTable(
                "dbo.Course",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Year = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        EducationalEstablishment_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EducationalEstablishment", t => t.EducationalEstablishment_Id)
                .Index(t => t.EducationalEstablishment_Id);
            
            CreateTable(
                "dbo.EducationalEstablishment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        NameImage = c.String(),
                        PathImage = c.String(),
                        Address = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Commune_Id = c.Int(nullable: false),
                        EstablishmentPeriod_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Commune", t => t.Commune_Id)
                .ForeignKey("dbo.EstablishmentPeriod", t => t.EstablishmentPeriod_Id)
                .Index(t => t.Commune_Id)
                .Index(t => t.EstablishmentPeriod_Id);
            
            CreateTable(
                "dbo.Commune",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Province_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Province", t => t.Province_Id)
                .Index(t => t.Province_Id);
            
            CreateTable(
                "dbo.Province",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Region_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Region", t => t.Region_Id)
                .Index(t => t.Region_Id);
            
            CreateTable(
                "dbo.Region",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EstablishmentPeriod",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PeriodDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        EstablishmentPeriod_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EstablishmentPeriod", t => t.EstablishmentPeriod_Id)
                .Index(t => t.EstablishmentPeriod_Id);
            
            CreateTable(
                "dbo.UserRolEducationalEstablishment",
                c => new
                    {
                        User_Id = c.String(nullable: false, maxLength: 128),
                        Role_Id = c.String(nullable: false, maxLength: 128),
                        EducationalEstablishment_Id = c.Int(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Role_Id, t.EducationalEstablishment_Id })
                .ForeignKey("dbo.EducationalEstablishment", t => t.EducationalEstablishment_Id)
                .ForeignKey("dbo.Role", t => t.Role_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Role_Id)
                .Index(t => t.EducationalEstablishment_Id);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Rank = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                        MotherLastName = c.String(nullable: false),
                        FatherLastName = c.String(nullable: false),
                        Address = c.String(),
                        ImagePath = c.String(),
                        Status = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.StudentCourse",
                c => new
                    {
                        User_Id = c.String(nullable: false, maxLength: 128),
                        Course_Id = c.Int(nullable: false),
                        EstablishmentRegisterNumber = c.Int(nullable: false),
                        ListNumber = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Course_Id })
                .ForeignKey("dbo.Course", t => t.Course_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Course_Id);
            
            CreateTable(
                "dbo.StudentTest",
                c => new
                    {
                        User_Id = c.String(nullable: false, maxLength: 128),
                        Test_Id = c.Int(nullable: false),
                        Attempts = c.Int(nullable: false),
                        IsAnswered = c.Boolean(nullable: false),
                        Start = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Test_Id })
                .ForeignKey("dbo.Test", t => t.Test_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Test_Id);
            
            CreateTable(
                "dbo.StudentAnswer",
                c => new
                    {
                        QuestionItem_Id = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                        Test_Id = c.Int(nullable: false),
                        Answer = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuestionItem_Id, t.User_Id, t.Test_Id })
                .ForeignKey("dbo.QuestionItem", t => t.QuestionItem_Id)
                .ForeignKey("dbo.StudentTest", t => new { t.User_Id, t.Test_Id })
                .Index(t => t.QuestionItem_Id)
                .Index(t => new { t.User_Id, t.Test_Id });
            
            CreateTable(
                "dbo.QuestionItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false),
                        Value = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Column_1_Text = c.String(),
                        Column_1_Alternative = c.Int(),
                        Column_2_Text = c.String(),
                        Column_2_Index = c.Int(),
                        Answer = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        QuestionPairedWord_Id = c.Int(),
                        QuestionTrueFalse_Id = c.Int(),
                        QuestionWrittenAnswer_Id = c.Int(),
                        QuestionAlternative_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Question", t => t.QuestionPairedWord_Id)
                .ForeignKey("dbo.Question", t => t.QuestionTrueFalse_Id)
                .ForeignKey("dbo.Question", t => t.QuestionWrittenAnswer_Id)
                .ForeignKey("dbo.Question", t => t.QuestionAlternative_Id)
                .Index(t => t.QuestionPairedWord_Id)
                .Index(t => t.QuestionTrueFalse_Id)
                .Index(t => t.QuestionWrittenAnswer_Id)
                .Index(t => t.QuestionAlternative_Id);
            
            CreateTable(
                "dbo.Question",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Order = c.Int(nullable: false),
                        QuestionType = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Test_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Test", t => t.Test_Id)
                .Index(t => t.Test_Id);
            
            CreateTable(
                "dbo.Test",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Attempts = c.Int(nullable: false),
                        Timer = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        IsTestReport = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TeacherCourseSubject",
                c => new
                    {
                        Subject_Id = c.Int(nullable: false),
                        Course_Id = c.Int(nullable: false),
                        User_Id = c.String(nullable: false, maxLength: 128),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.Subject_Id, t.Course_Id, t.User_Id })
                .ForeignKey("dbo.CourseSubject", t => new { t.Subject_Id, t.Course_Id })
                .ForeignKey("dbo.User", t => t.User_Id)
                .Index(t => new { t.Subject_Id, t.Course_Id })
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Subject",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        TypeQualification = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ActionFile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentPath = c.String(nullable: false),
                        DocumentName = c.String(nullable: false),
                        DocumentType = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        ActionFileGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ActionFileGroup", t => t.ActionFileGroup_Id)
                .Index(t => t.ActionFileGroup_Id);
            
            CreateTable(
                "dbo.ActionFileGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ActionLesson",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Link",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Publication",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActionFile", "ActionFileGroup_Id", "dbo.ActionFileGroup");
            DropForeignKey("dbo.Action", "Activity_Id", "dbo.Activity");
            DropForeignKey("dbo.Activity", "SubUnity_Id", "dbo.SubUnity");
            DropForeignKey("dbo.SubUnity", "Unity_Id", "dbo.Unity");
            DropForeignKey("dbo.Unity", new[] { "CourseSubject_Subject_Id", "CourseSubject_Course_Id" }, "dbo.CourseSubject");
            DropForeignKey("dbo.CourseSubject", "Subject_Id", "dbo.Subject");
            DropForeignKey("dbo.CourseSubject", "Course_Id", "dbo.Course");
            DropForeignKey("dbo.Course", "EducationalEstablishment_Id", "dbo.EducationalEstablishment");
            DropForeignKey("dbo.TeacherCourseSubject", "User_Id", "dbo.User");
            DropForeignKey("dbo.TeacherCourseSubject", new[] { "Subject_Id", "Course_Id" }, "dbo.CourseSubject");
            DropForeignKey("dbo.StudentTest", "User_Id", "dbo.User");
            DropForeignKey("dbo.StudentTest", "Test_Id", "dbo.Test");
            DropForeignKey("dbo.StudentAnswer", new[] { "User_Id", "Test_Id" }, "dbo.StudentTest");
            DropForeignKey("dbo.StudentAnswer", "QuestionItem_Id", "dbo.QuestionItem");
            DropForeignKey("dbo.QuestionItem", "QuestionAlternative_Id", "dbo.Question");
            DropForeignKey("dbo.QuestionItem", "QuestionWrittenAnswer_Id", "dbo.Question");
            DropForeignKey("dbo.QuestionItem", "QuestionTrueFalse_Id", "dbo.Question");
            DropForeignKey("dbo.QuestionItem", "QuestionPairedWord_Id", "dbo.Question");
            DropForeignKey("dbo.Question", "Test_Id", "dbo.Test");
            DropForeignKey("dbo.StudentCourse", "User_Id", "dbo.User");
            DropForeignKey("dbo.StudentCourse", "Course_Id", "dbo.Course");
            DropForeignKey("dbo.UserRolEducationalEstablishment", "User_Id", "dbo.User");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.User");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.User");
            DropForeignKey("dbo.UserRolEducationalEstablishment", "Role_Id", "dbo.Role");
            DropForeignKey("dbo.UserRolEducationalEstablishment", "EducationalEstablishment_Id", "dbo.EducationalEstablishment");
            DropForeignKey("dbo.EducationalEstablishment", "EstablishmentPeriod_Id", "dbo.EstablishmentPeriod");
            DropForeignKey("dbo.PeriodDetail", "EstablishmentPeriod_Id", "dbo.EstablishmentPeriod");
            DropForeignKey("dbo.EducationalEstablishment", "Commune_Id", "dbo.Commune");
            DropForeignKey("dbo.Commune", "Province_Id", "dbo.Province");
            DropForeignKey("dbo.Province", "Region_Id", "dbo.Region");
            DropIndex("dbo.ActionFile", new[] { "ActionFileGroup_Id" });
            DropIndex("dbo.TeacherCourseSubject", new[] { "User_Id" });
            DropIndex("dbo.TeacherCourseSubject", new[] { "Subject_Id", "Course_Id" });
            DropIndex("dbo.Question", new[] { "Test_Id" });
            DropIndex("dbo.QuestionItem", new[] { "QuestionAlternative_Id" });
            DropIndex("dbo.QuestionItem", new[] { "QuestionWrittenAnswer_Id" });
            DropIndex("dbo.QuestionItem", new[] { "QuestionTrueFalse_Id" });
            DropIndex("dbo.QuestionItem", new[] { "QuestionPairedWord_Id" });
            DropIndex("dbo.StudentAnswer", new[] { "User_Id", "Test_Id" });
            DropIndex("dbo.StudentAnswer", new[] { "QuestionItem_Id" });
            DropIndex("dbo.StudentTest", new[] { "Test_Id" });
            DropIndex("dbo.StudentTest", new[] { "User_Id" });
            DropIndex("dbo.StudentCourse", new[] { "Course_Id" });
            DropIndex("dbo.StudentCourse", new[] { "User_Id" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.User", "UserNameIndex");
            DropIndex("dbo.Role", "RoleNameIndex");
            DropIndex("dbo.UserRolEducationalEstablishment", new[] { "EducationalEstablishment_Id" });
            DropIndex("dbo.UserRolEducationalEstablishment", new[] { "Role_Id" });
            DropIndex("dbo.UserRolEducationalEstablishment", new[] { "User_Id" });
            DropIndex("dbo.PeriodDetail", new[] { "EstablishmentPeriod_Id" });
            DropIndex("dbo.Province", new[] { "Region_Id" });
            DropIndex("dbo.Commune", new[] { "Province_Id" });
            DropIndex("dbo.EducationalEstablishment", new[] { "EstablishmentPeriod_Id" });
            DropIndex("dbo.EducationalEstablishment", new[] { "Commune_Id" });
            DropIndex("dbo.Course", new[] { "EducationalEstablishment_Id" });
            DropIndex("dbo.CourseSubject", new[] { "Course_Id" });
            DropIndex("dbo.CourseSubject", new[] { "Subject_Id" });
            DropIndex("dbo.Unity", new[] { "CourseSubject_Subject_Id", "CourseSubject_Course_Id" });
            DropIndex("dbo.SubUnity", new[] { "Unity_Id" });
            DropIndex("dbo.Activity", new[] { "SubUnity_Id" });
            DropIndex("dbo.Action", new[] { "Activity_Id" });
            DropTable("dbo.Publication");
            DropTable("dbo.Link");
            DropTable("dbo.ActionLesson");
            DropTable("dbo.ActionFileGroup");
            DropTable("dbo.ActionFile");
            DropTable("dbo.Subject");
            DropTable("dbo.TeacherCourseSubject");
            DropTable("dbo.Test");
            DropTable("dbo.Question");
            DropTable("dbo.QuestionItem");
            DropTable("dbo.StudentAnswer");
            DropTable("dbo.StudentTest");
            DropTable("dbo.StudentCourse");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.User");
            DropTable("dbo.Role");
            DropTable("dbo.UserRolEducationalEstablishment");
            DropTable("dbo.PeriodDetail");
            DropTable("dbo.EstablishmentPeriod");
            DropTable("dbo.Region");
            DropTable("dbo.Province");
            DropTable("dbo.Commune");
            DropTable("dbo.EducationalEstablishment");
            DropTable("dbo.Course");
            DropTable("dbo.CourseSubject");
            DropTable("dbo.Unity");
            DropTable("dbo.SubUnity");
            DropTable("dbo.Activity");
            DropTable("dbo.Action");
        }
    }
}
