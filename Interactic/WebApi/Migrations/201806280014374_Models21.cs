namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models21 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UploadFileStudentItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentPath = c.String(nullable: false),
                        DocumentName = c.String(nullable: false),
                        DocumentType = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Student_Id = c.String(nullable: false, maxLength: 128),
                        UploadFileStudent_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Student_Id)
                .ForeignKey("dbo.UploadFileStudent", t => t.UploadFileStudent_Id)
                .Index(t => t.Student_Id)
                .Index(t => t.UploadFileStudent_Id);
            
            CreateTable(
                "dbo.UploadFileStudent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UploadFileStudentItem", "UploadFileStudent_Id", "dbo.UploadFileStudent");
            DropForeignKey("dbo.UploadFileStudentItem", "Student_Id", "dbo.User");
            DropIndex("dbo.UploadFileStudentItem", new[] { "UploadFileStudent_Id" });
            DropIndex("dbo.UploadFileStudentItem", new[] { "Student_Id" });
            DropTable("dbo.UploadFileStudent");
            DropTable("dbo.UploadFileStudentItem");
        }
    }
}
