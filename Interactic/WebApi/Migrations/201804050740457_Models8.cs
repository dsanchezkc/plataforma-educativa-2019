namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models8 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostFile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentPath = c.String(nullable: false),
                        DocumentName = c.String(nullable: false),
                        DocumentType = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Post_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Post", t => t.Post_Id)
                .Index(t => t.Post_Id);
            
            CreateTable(
                "dbo.TopicFile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentPath = c.String(nullable: false),
                        DocumentName = c.String(nullable: false),
                        DocumentType = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EditedAt = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Topic_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Topic", t => t.Topic_Id)
                .Index(t => t.Topic_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TopicFile", "Topic_Id", "dbo.Topic");
            DropForeignKey("dbo.PostFile", "Post_Id", "dbo.Post");
            DropIndex("dbo.TopicFile", new[] { "Topic_Id" });
            DropIndex("dbo.PostFile", new[] { "Post_Id" });
            DropTable("dbo.TopicFile");
            DropTable("dbo.PostFile");
        }
    }
}
