namespace WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Models1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentAnswer", "Qualification", c => c.Int(nullable: false));
            AddColumn("dbo.QuestionItem", "QuestionType", c => c.Int(nullable: false));
            AddColumn("dbo.Test", "CourseSubject_Subject_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Test", "CourseSubject_Course_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Test", new[] { "CourseSubject_Subject_Id", "CourseSubject_Course_Id" });
            AddForeignKey("dbo.Test", new[] { "CourseSubject_Subject_Id", "CourseSubject_Course_Id" }, "dbo.CourseSubject", new[] { "Subject_Id", "Course_Id" });
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Test", new[] { "CourseSubject_Subject_Id", "CourseSubject_Course_Id" }, "dbo.CourseSubject");
            DropIndex("dbo.Test", new[] { "CourseSubject_Subject_Id", "CourseSubject_Course_Id" });
            DropColumn("dbo.Test", "CourseSubject_Course_Id");
            DropColumn("dbo.Test", "CourseSubject_Subject_Id");
            DropColumn("dbo.QuestionItem", "QuestionType");
            DropColumn("dbo.StudentAnswer", "Qualification");
        }
    }
}
