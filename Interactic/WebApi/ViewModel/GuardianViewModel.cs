﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ViewModels
{
	public class GuardianViewModel
	{
		/// <summary>
		/// Usuario Apoderado
		/// </summary>
		public User Guardian;
		/// <summary>
		/// Lista de establecimientos a los que se asocia el apoderado
		/// </summary>
		public List<EducationalEstablishment> EducationalEstablishments;

		public GuardianViewModel()
		{
			EducationalEstablishments = new List<EducationalEstablishment>();
		}
	}
}