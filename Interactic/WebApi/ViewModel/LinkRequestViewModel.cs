﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ViewModels
{
	/********************************** Request **************************************/

	/// <summary>
	/// Estructura de mensaje desde el cliente al servidor
	/// </summary>
	public class LinkRequestViewModel
	{
		public int Activity_Id;
		public int Link_Id;
		public string Name;
		public string Url;
	}	
}