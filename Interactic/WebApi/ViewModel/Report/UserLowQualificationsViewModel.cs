﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.CloneModels;

namespace ViewModels.Report
{
	public class UserLowQualificationsViewModel
	{
		public User User;		
		public List<SubjectLowQualificationsViewModel> SubjectLowQualifications;

		public UserLowQualificationsViewModel()
		{
			SubjectLowQualifications = new List<SubjectLowQualificationsViewModel>();
		}
	}
}