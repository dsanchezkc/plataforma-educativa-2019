﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.CloneModels;

namespace ViewModels.Report
{
	public class UnityViewedAverageViewModel
	{
		public User User;
		public List<UnityViewedTime> UnityViewedTimes;

		public UnityViewedAverageViewModel()
		{
			UnityViewedTimes = new List<UnityViewedTime>();
		}

		public void AddTime(Unity unity, double time)
		{
			UnityViewedTimes.Add(new UnityViewedTime() { Time = time, Unity = unity });
		}

		public double GetTime(int unity_id)
		{
			var uvt = UnityViewedTimes.FirstOrDefault(p => p.Unity.Id == unity_id);
			return uvt == null ? -1 : uvt.Time;
		}
	}

	public class UnityViewedTime
	{
		public double Time;
		public Unity Unity;

		public string GetTimeFormat()
		{
			if (Time == 0)
				return "-";

			if (Time <= 60)
				return Math.Round(Time, 1) + " min";

			var hour = Time / 60;
			var minutes = Time - ((int)hour * 60);

			return (int)hour + "hr " + Math.Round(minutes) + "m";
		}
	}
}