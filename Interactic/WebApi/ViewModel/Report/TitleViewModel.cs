﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ViewModels.Report
{
	public class TitleViewModel
	{
		public string Student;
		public string Course;
		public string Subject;
		public string Establishment;
		public string Unity;
		public string SubUnity;

		public int NStudents;
		public int NUnits;
		public int NSubUnits;
	}
}