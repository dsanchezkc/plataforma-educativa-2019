﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.CloneModels;

namespace ViewModels
{
	public class QualificationInfoViewModel
	{
		public double Qualification
		{
			get
			{
				var qualification = (WrittenAnswer.Qualification + TrueFalse.Qualification + PairedWords.Qualification + Alternatives.Qualification) /
						(double)(WrittenAnswer.Count + TrueFalse.Count + PairedWords.Count + Alternatives.Count);				

				return Math.Round((qualification * 99) + 1, 1);
			} }

		public QualificationType WrittenAnswer;
		public QualificationType TrueFalse;
		public QualificationType PairedWords;
		public QualificationType Alternatives;

		/// <summary>
		/// Indica si hay una respuesta escrita pendiente, en este caso la nota va a cambiar cuando el profesor
		/// evalue.
		/// </summary>
		public bool IsWrittenAnswerPending = false;

		public bool IsCalculable { get { return (WrittenAnswer.Count + TrueFalse.Count + PairedWords.Count + Alternatives.Count) != 0;  } }

		public QualificationInfoViewModel()
		{
			WrittenAnswer = new QualificationType();
			TrueFalse = new QualificationType();
			PairedWords = new QualificationType();
			Alternatives = new QualificationType();
		}
	}

	public class QualificationType
	{
		//Calificación acumulada de un tipo
		public double Qualification { private set; get; }
		//Cuantas calificaciones acumuladas son
		public int Count { get { return Success + Errors; } }

		public int Success { private set; get; }
		public int Errors { private set; get; }

		public void AddQualification(double qualification = 0, bool success = true)
		{
			Qualification += qualification;

			if (success)
				Success++;
			else
				Errors++;
		}

		public double Average
		{
			get
			{
				if (Count == 0) return -1;
				return Qualification / (double)Count;
			}
		}
	}
}