﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.CloneModels;

namespace ViewModels.Report
{
	public class SubjectLowQualificationsViewModel
	{
		public Subject Subject;
		public QualificationType Qualification;
		public List<UnityQualificationViewModel> UnityQualifications;
		public List<UnityViewedAverageViewModel> UnityViewedAverages;

		public SubjectLowQualificationsViewModel()
		{
			Qualification = new QualificationType();
			UnityQualifications = new List<UnityQualificationViewModel>();
			UnityViewedAverages = new List<UnityViewedAverageViewModel>();
		}		

		public double Average
		{
			get
			{
				return Math.Round(Qualification.Average, 1);
			}
		}

		public void AddQualification(int unity_id, string qualification, bool includeQualification = true)
		{
			UnityQualifications.Add(new UnityQualificationViewModel() { Id = unity_id, Qualification = qualification, IncludeQualification = includeQualification });
			if (includeQualification)
				Qualification.AddQualification(double.Parse(qualification));
		}

		public double GetQualification(int unity_id)
		{
			var qualifications = 0.0;
			var count = 0.0;
			foreach (var uq in UnityQualifications.Where(p => p.IncludeQualification && p.Id == unity_id))
			{
				qualifications += double.Parse(uq.Qualification);
				count++;
			}

			if (count == 0) return -1;
			return Math.Round(qualifications / count, 1);
		}

		public double GetQualificationAll()
		{
			var qualifications = 0.0;
			var count = 0.0;
			foreach (var uq in UnityQualifications.Where(p => p.IncludeQualification))
			{
				qualifications += double.Parse(uq.Qualification);
				count++;
			}

			if (count == 0) return -1;
			return Math.Round(qualifications / count, 1);
		}
	}
}