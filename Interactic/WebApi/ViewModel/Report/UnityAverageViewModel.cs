﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.CloneModels;

namespace ViewModels.Report
{
	public class UnityAverageViewModel
	{
		public Unity unity;
		public QualificationType Qualification;
		public double AverageAchievements;
		public List<UnityQualificationViewModel> UnityQualifications;
		public List<AchievementViewModel> Achievements;

		public double Average
		{
			get
			{
				return Math.Round(Qualification.Average, 1);
			}
		}

		public UnityAverageViewModel()
		{
			Qualification = new QualificationType();
			UnityQualifications = new List<UnityQualificationViewModel>();
			Achievements = new List<AchievementViewModel>();
		}

		public void AddQualification(int unity_id,string qualification, bool includeQualification = true)
		{
			UnityQualifications.Add(new UnityQualificationViewModel() { Id  = unity_id, Qualification = qualification, IncludeQualification = includeQualification });
			if (includeQualification)
				Qualification.AddQualification(double.Parse(qualification));
		}

		public void AddAchievement(int unity_id, string qualification, bool includeQualification = true)
		{
			Achievements.Add(new AchievementViewModel() { Unity_Id = unity_id, Percentage = qualification, IncludeQualification = includeQualification });
		}

		public double GetQualification(int unity_id)
		{
			var qualifications = 0.0;
			var count = 0.0;
			foreach (var uq in UnityQualifications.Where(p => p.IncludeQualification && p.Id == unity_id))
			{
				qualifications += double.Parse(uq.Qualification);
				count++;
			}

			if (count == 0) return -1;
			return Math.Round(qualifications / count, 1);
		}

		public double GetPercentageAchievement(int unity_id)
		{
			var percentages = 0.0;
			var count = 0.0;
			foreach (var uq in Achievements.Where(p => p.IncludeQualification && p.Unity_Id == unity_id))
			{
				percentages += double.Parse(uq.Percentage);
				count++;
			}

			if (count == 0) return -1;
			return Math.Round(percentages / count, 1);
		}
	}

	public class AchievementViewModel
	{
		public int Unity_Id;
		public string Percentage;
		public bool IncludeQualification;
	}
}