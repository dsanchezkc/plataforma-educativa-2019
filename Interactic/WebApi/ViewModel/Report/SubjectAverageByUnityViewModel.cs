﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.CloneModels;

namespace ViewModels.Report
{
	public class SubjectAverageByUnityInfoViewModel
	{
		public Subject Subject;
		public QualificationType Qualification;
		public double AverageAchievements;
		public List<UnityQualificationViewModel> UnityQualifications;

		public double Average
		{
			get
			{
				return Math.Round(Qualification.Average, 1);
			}
		}

		public SubjectAverageByUnityInfoViewModel()
		{
			Qualification = new QualificationType();
			UnityQualifications = new List<UnityQualificationViewModel>();
		}

		public void AddQualification(string qualification, bool includeQualification = true)
		{
			UnityQualifications.Add(new UnityQualificationViewModel() { Qualification = qualification });
			if (includeQualification)
				Qualification.AddQualification(double.Parse(qualification));
		}
	}

	public class UnityQualificationViewModel
	{
		public int Id;
		public string Qualification;
		public bool IncludeQualification;
	}
}