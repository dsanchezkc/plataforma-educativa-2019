﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ViewModels
{
	/********************************** Request **************************************/

	/// <summary>
	/// Estructura de mensaje desde el cliente para enviar un mensaje
	/// </summary>
	public class MessageRequestViewModel
	{
		public List<string> To { set; get; }
		public string Subject { set; get; }
		public string Content { set; get; }
	}

	/// <summary>
	/// Estructura de mensaje para responder un mensaje o reenviar
	/// </summary>
	public class MessageAnswerRequestViewModel
	{
		/// <summary>
		/// Message Id al que se hace referencia.
		/// </summary>
		public int Message_Id;		

		/// <summary>
		/// Contenido del mensaje
		/// </summary>
		public string Body { set; get; }
	}

	/// <summary>
	/// Estructura de mensaje para responder un mensaje o reenviar
	/// </summary>
	public class MessageForwardRequestViewModel
	{
		/// <summary>
		/// Conversation Id al que se hace referencia.
		/// </summary>
		public int Conversation_Id;

		/// <summary>
		/// Contenido del mensaje
		/// </summary>
		public string Body { set; get; }

		/// <summary>
		/// Si es un reenvío se debe completar el campo ToUser de MessageEmail.
		/// </summary>
		public List<string> ToUsers { set; get; }

		/// <summary>
		/// Si es un reenvío se debe completar los archivos que serán agregados.
		/// </summary>
		public List<int> Attacheds { set; get; }
	}

	/********************************** Response ***********************************/

	/// <summary>
	/// Estructura de respuesta por lado del servidor de un Hilo.
	/// Contiene la información de toda la conversión de un mensaje.
	/// </summary>
	public class ConversationEmailResponseViewModel
	{
		public int Id { set; get; }
		public string Subject { set; get; }
		public bool Readed { set; get; }
		public DateTime Date { set; get; }
		public List<MessageEmail> Messages { set; get; }
		
		public ConversationEmailResponseViewModel()
		{
			Messages = new List<MessageEmail>();
		}
	}

	/// <summary>
	/// Notificaciónes de mensajes
	/// Contiene la información de toda la conversión de un mensaje.
	/// </summary>
	public class NotificationResumeResponseViewModel
	{
		public int UnreadCount { set; get; }
		public List<NotificationResponseViewModel> Notifications { set; get; }

		public NotificationResumeResponseViewModel()
		{
			Notifications = new List<NotificationResponseViewModel>();
		}
	}

	public class NotificationResponseViewModel
	{
		public int Conversation_Id { set; get; }
		public DateTime Date { set; get; }
		public string Name { set; get; }
		public string Body { set; get; }
	}

	/// <summary>
	/// Mensaje asociado a una conversación
	/// </summary>
	public class MessageEmail
	{
		/// <summary>
		/// Identificador del mensaje (Modelo - Message)
		/// </summary>
		public int Id { set; get; }

		/// <summary>
		/// Lista de UserNames
		/// </summary>
		public List<string> ToUsers { set; get; }

		/// <summary>
		/// Información de quien envió el mensaje
		/// </summary>
		public UserEmail FromUser { set; get; }

		/// <summary>
		/// Fecha de publicacióon
		/// </summary>
		public DateTime Date { set; get; }

		/// <summary>
		/// Contenido mensaje
		/// </summary>
		public string Body { set; get; }

		/// <summary>
		/// Archivos adjuntos
		/// </summary>
		public List<AttachedEmail> Attacheds { set; get; }

		/// <summary>
		/// Indica si este mensaje es un reenvío
		/// </summary>
		public bool IsForward { set; get; }

		public MessageEmail(User fromUser)
		{
			Attacheds = new List<AttachedEmail>();
			FromUser = new UserEmail(fromUser);
		}
	}

	/// <summary>
	/// Información del Usuario de quien envió el mensaje
	/// </summary>
	public class UserEmail
	{
		public string UserName { set; get; }
		public string FirstName { set; get; }
		public string LastName { set; get; }

		public UserEmail(User user)
		{
			UserName = user.UserName;
			FirstName = user.Name;
			LastName = user.FatherLastName;
		}
	}

	/// <summary>
	/// Información el archivo adjunto
	/// </summary>
	public class AttachedEmail
	{
		public int Id { set; get; }

		/// <summary>
		/// Nombre del documento más su extención
		/// </summary>
		public string Name { set; get; }
		
		/// <summary>
		/// Información del peso incluido su unidad de medida
		/// </summary>
		public string Weight { set; get; }

		/// <summary>
		/// Tipo del archivo
		/// </summary>
		public string Type { set; get; }

		public static List<AttachedEmail> Cast(IEnumerable<Attached> attacheds)
		{
			var list = new List<AttachedEmail>();

			foreach (var item in attacheds)
			{
				list.Add(new AttachedEmail()
				{
					Name = item.DocumentName,
					Id = item.Id,
					Weight = (new FileInfo(item.DocumentPath).Length/1024)+"KB",
					Type = item.DocumentType
				});
			}

			return list;
		}
	}
}