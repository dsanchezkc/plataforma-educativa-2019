﻿using Model;
using ModelEnum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ViewModels
{
    /********************************** Request **************************************/

    /// <summary>
    /// Estructura de mensaje desde el cliente al servidor
    /// </summary>
    /// 

    public class SpecialQuestionRequestViewModel
    {
        public int? Id;
        public int Order;
        public int QuestionType;
        public string Name;
        public string ComplementaryAnswer;
        public int? Difficulty;
        public int? SubUnity_Id;

        public List<QuestionTrueFalse> QuestionTrueFalses;
        public List<QuestionWrittenAnswer> QuestionWrittenAnswers;
        public List<QuestionPairedWord> QuestionPairedWords;
        public List<QuestionAlternative> QuestionAlternatives;

        public SpecialQuestionRequestViewModel()
        {
            QuestionTrueFalses = new List<QuestionTrueFalse>();
            QuestionWrittenAnswers = new List<QuestionWrittenAnswer>();
            QuestionPairedWords = new List<QuestionPairedWord>();
            QuestionAlternatives = new List<QuestionAlternative>();
        }
    }
}