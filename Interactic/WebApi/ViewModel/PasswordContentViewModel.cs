﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModels
{
    public class PasswordContentViewModel
	{
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}