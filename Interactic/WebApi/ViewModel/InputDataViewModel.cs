﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModels
{
    public class InputDataViewModel
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }
}