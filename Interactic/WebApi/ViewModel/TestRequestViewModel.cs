﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ViewModels
{
	/********************************** Request **************************************/

	/// <summary>
	/// Estructura de mensaje desde el cliente al servidor
	/// </summary>
    /// 

	public class TestRequestViewModel
	{
		public string SubUnity_Id;
        public string Subject_Id;
        public int? Id;
        public string Course_Id;
        public bool Global;
        public bool Random;
        public SpecialTest SpecialTest;

		public List<QuestionTrueFalse> QuestionTrueFalses;
		public List<QuestionWrittenAnswer> QuestionWrittenAnswers;
		public List<QuestionPairedWord> QuestionPairedWords;
		public List<QuestionAlternative> QuestionAlternatives;
        public List<RandomQuestionSet> RandomQuestionSet;

        public TestRequestViewModel()
		{
			QuestionTrueFalses = new List<QuestionTrueFalse>();
			QuestionWrittenAnswers = new List<QuestionWrittenAnswer>();
			QuestionPairedWords = new List<QuestionPairedWord>();
			QuestionAlternatives = new List<QuestionAlternative>();
        }
	}

	public class TestAnswersRequestViewModel
	{
		public int Test_Id;

		public List<QuestionWrittenAnswerItem> QuestionWrittenAnswerItems;
		public List<QuestionTrueFalseItem> QuestionTrueFalseItems;
		public List<QuestionPairedWordItem> QuestionPairedWordItems;
		public List<QuestionAlternativeItem> QuestionAlternativeItems;

		public TestAnswersRequestViewModel()
		{
			QuestionWrittenAnswerItems = new List<QuestionWrittenAnswerItem>();
			QuestionTrueFalseItems = new List<QuestionTrueFalseItem>();
			QuestionPairedWordItems = new List<QuestionPairedWordItem>();
			QuestionAlternativeItems = new List<QuestionAlternativeItem>();
		}
	}

    public class RandomQuestionSet
    {
        public int Subunity_Id;
        public int Count;
    }
}