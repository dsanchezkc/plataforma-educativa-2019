﻿using Enums;
using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ViewModels
{
	/********************************** Request **************************************/

	/// <summary>
	/// Estructura de mensaje desde el cliente al servidor
	/// </summary>
	public class ForumRequestViewModel
	{
		public int Activity_Id;		
		public int Publication_Id;
		public ForumType Type;
		public string Description;
		public string Name;
        public int SubUnity_Id;
        /// <summary>
        /// Indica si de debe registrar esta publicación con una actividad o no.
        /// Se supone que habrán publicaciones que vienen directo del foro, no del panel de Unidad.
        /// </summary>
        public bool IsAsociated;
	}	
}