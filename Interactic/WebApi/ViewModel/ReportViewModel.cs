﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModels
{
    public class ReportViewModel
    {
        public User Teacher { get; set; }
        public User Student { get; set; }
        public Course Course { get; set; }
        public PeriodDetail Period { get; set; }
        public Test Test { get; set; }
        public EducationalEstablishment Establishment { get; set; }
        public SubUnity SubUnity { get; set; }
        public CourseSubject CourseSubject { get; set; }
        public IEnumerable<DayTrip> DayTrips { get; set; }
        public IEnumerable<Absent> Absents { get; set; }
        public IEnumerable<User> StudentCourse { get; set; }
        public IEnumerable<CourseSubject> CourseSubjects { get; set; }
        public IEnumerable<Subject> Subjects { get; set; }
        public IEnumerable<Answer> Answers { get; set; }
        public IEnumerable<PeriodDetail> PeriodDetails { get; set; }
        public IEnumerable<Observation> Observations { get; set; }
        public IEnumerable<AttendanceDate> AttendanceDates { get; set; }
        public IEnumerable<Event> Events { get; set; }
        public IEnumerable<AchievementIndicator> AchievementIndicator { get; set; }
        public IEnumerable<AchievementAlternative> AchievementAlternative { get; set; }
        public IEnumerable<Skill> Skills { get; set; }

		public bool IsHideAnswers { get; set; }
		public int CurrentYear { get; set; }
        public int Month { get; set; }

        public IQueryable<string> Students { get; internal set; }
        public List<string> StudentsCourse { get; internal set; }
        public IQueryable<User> StudentsC { get; internal set; }
        public IQueryable<Question> Questions { get; internal set; }
        public IQueryable<StudentCourse> StudentCourseList { get; internal set; }
        public IQueryable<int> Tests { get; internal set; }
    }
}