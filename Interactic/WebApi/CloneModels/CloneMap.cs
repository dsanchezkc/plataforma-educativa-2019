﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace WebApi.CloneModels
{
	public class CloneMap<Parent> : ICloneMap
	{
		public Expression<Func<Parent, object>> Property { private set; get; }
		public List<ICloneMap> SubProperties;
		public bool ExcludeAllPrimitiveProperties {private set; get; }

		public CloneMap(Expression<Func<Parent, object>> property = null, bool excludeAllPrimitiveProperties = false)
		{
			ExcludeAllPrimitiveProperties = excludeAllPrimitiveProperties;
			Property = property;
			SubProperties = new List<ICloneMap>();
		}

		public string GetPropertyName()
		{
			MemberExpression body = Property.Body as MemberExpression;

			if (body == null)
			{
				UnaryExpression ubody = (UnaryExpression)Property.Body;
				body = ubody.Operand as MemberExpression;
			}

			return body.Member.Name;
		}

		public ICloneMap GetCloneMap(string property)
		{
			for (var i = 0; i < SubProperties.Count; i++)
				if (SubProperties[i].GetPropertyName() == property)
					return SubProperties[i];

			return null;
		}

		public bool IsExcludeAllPrimitiveProperties()
		{
			return ExcludeAllPrimitiveProperties;
		}
	}
}