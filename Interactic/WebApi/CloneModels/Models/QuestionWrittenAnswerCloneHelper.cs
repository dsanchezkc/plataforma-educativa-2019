﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class QuestionWrittenAnswerCloneHelper
	{
		public static CloneMap<QuestionWrittenAnswer> QuestionWrittenAnswerMap_1
		{
			get
			{
				return new CloneMap<QuestionWrittenAnswer>()
				{
					SubProperties =
					{
						new CloneMap<QuestionWrittenAnswer>(p => p.QuestionWrittenAnswerItem)
                    }
				};
			}
		}
	}
}