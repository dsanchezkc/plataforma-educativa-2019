﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class SpecialTestCloneHelper
    {
        public static CloneMap<SpecialTest> TestMap_1
        {
            get
            {
                return new CloneMap<SpecialTest>()
                {
                    SubProperties =
                    {
                        new CloneMap<SpecialTest>(p => p.SpecialQuestions),
                        new CloneMap<SpecialTest>(p => p.CourseSubject)
                        {
                            SubProperties =
                            {
                                new CloneMap<CourseSubject>(p => p.Course),
                                new CloneMap<CourseSubject>(p => p.Subject)
                            }
                        }
                    }
                };
            }
        }

        public static CloneMap<SpecialTest> TestMap_3
        {
            get
            {
                return new CloneMap<SpecialTest>()
                {
                    SubProperties =
                    {
                        new CloneMap<SpecialTest>(p => p.StudentSpecialTests)
                        {
                            SubProperties =
                            {
                                new CloneMap<StudentTest>(p => p.User, excludeAllPrimitiveProperties : true)
                                {
                                    SubProperties =
                                    {
                                        new CloneMap<User>(p => p.Id ),
                                        new CloneMap<User>(p => p.UserName ),
                                        new CloneMap<User>(p => p.Name ),
                                        new CloneMap<User>(p => p.FatherLastName ),
                                        new CloneMap<User>(p => p.MotherLastName ),
                                        new CloneMap<User>(p => p.Status ),

                                        new CloneMap<User>(p => p.StudentCourses)
                                    }
                                }
                            }
                        }
                    }
                };
            }
        }


        public static SpecialTest GetTestWithoutAnswers(SpecialTest test)
        {
            var _test = CloneHelper.CloneObject(test) as SpecialTest;

            var questionWrittenAnswers = CloneHelper.CloneListMap(test.SpecialQuestions.OfType<QuestionWrittenAnswer>(), QuestionWrittenAnswerCloneHelper.QuestionWrittenAnswerMap_1);
            //Borrando respuestas
            foreach (var qwa in questionWrittenAnswers)
            {
                qwa.QuestionWrittenAnswerItem.Answer = "";
                qwa.ComplementaryAnswer = "";
            }
            _test.SpecialQuestions.AddRange(questionWrittenAnswers);

            var questionTrueFalses = CloneHelper.CloneListMap(test.SpecialQuestions.OfType<QuestionTrueFalse>(), QuestionTrueFalseCloneHelper.QuestionTrueFalseMap_1);
            //Borrando respuestas
            foreach (var qtf in questionTrueFalses)
            {
                qtf.ComplementaryAnswer = "";
                foreach (var qtfi in qtf.QuestionTrueFalseItems)
                    qtfi.Value = false;
            }
            _test.SpecialQuestions.AddRange(questionTrueFalses);

            var questionPairedWords = CloneHelper.CloneListMap(test.SpecialQuestions.OfType<QuestionPairedWord>(), QuestionPairedWordCloneHelper.QuestionPairedWordMap_1);
            foreach (var qpw in questionPairedWords)
            {
                qpw.QuestionPairedWordItems = qpw.QuestionPairedWordItems.OrderBy(p => p.Column_2_Index).ToList();

                if (qpw != null)
                    qpw.ComplementaryAnswer = "";
                //Borrando respuestas
                foreach (var qpwi in qpw.QuestionPairedWordItems)
                    qpwi.Column_1_Alternative = 0;
            }
            _test.SpecialQuestions.AddRange(questionPairedWords);

            var questionAlternatives = CloneHelper.CloneListMap(test.SpecialQuestions.OfType<QuestionAlternative>(), QuestionAlternativeCloneHelper.QuestionAlternativeMap_1);
            //Borrando respuestas
            foreach (var qa in questionAlternatives)
            {
                qa.ComplementaryAnswer = "";
                foreach (var qai in qa.QuestionAlternativeItems)
                    qai.Value = false;
            }
            _test.SpecialQuestions.AddRange(questionAlternatives);
            _test.SpecialQuestions = _test.SpecialQuestions.OrderBy(p => p.Order).ToList();

            return _test;
        }
    }
}