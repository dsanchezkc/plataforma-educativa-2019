﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class TopicCloneHelper
	{
		public static CloneMap<Topic> TopicMap_1
		{
			get
			{
				return new CloneMap<Topic>()
				{
					SubProperties =
					{
						new CloneMap<Topic>(p => p.Author, excludeAllPrimitiveProperties : true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.UserName),
								new CloneMap<User>(p => p.Name),
								new CloneMap<User>(p => p.FatherLastName),
								new CloneMap<User>(p => p.MotherLastName),
							}
						},
						new CloneMap<Topic>(p => p.TopicFiles)
					}
				};
			}
		}

		public static CloneMap<Topic> TopicMap_2
		{
			get
			{
				return new CloneMap<Topic>()
				{
					SubProperties =
					{
						new CloneMap<Topic>(p => p.Author, excludeAllPrimitiveProperties : true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.UserName),
								new CloneMap<User>(p => p.Name),
								new CloneMap<User>(p => p.FatherLastName),
								new CloneMap<User>(p => p.MotherLastName),
								new CloneMap<User>(p => p.ImagePath)
							}
						},
                        new CloneMap<Topic>(p => p.Posts)
						{
							SubProperties =
							{
								new CloneMap<Post>(p => p.Author, excludeAllPrimitiveProperties:true)
								{
									SubProperties =
									{
										new CloneMap<User>(p => p.Id),
										new CloneMap<User>(p => p.UserName),
										new CloneMap<User>(p => p.Name),
										new CloneMap<User>(p => p.FatherLastName),
										new CloneMap<User>(p => p.MotherLastName),
										new CloneMap<User>(p => p.ImagePath)
									}
								},
                                /*
								new CloneMap<Post>(p => p.PostQualifications)
								{
									SubProperties =
									{
										new CloneMap<PostQualification>(p => p.User, excludeAllPrimitiveProperties:true)
										{
											SubProperties =
											{
												new CloneMap<User>(p => p.Id),
												new CloneMap<User>(p => p.UserName),
												new CloneMap<User>(p => p.Name),
												new CloneMap<User>(p => p.FatherLastName),
												new CloneMap<User>(p => p.MotherLastName)
											}
										}

									}
								},*/
								new CloneMap<Post>(p => p.PostFiles)
							}
						},
						new CloneMap<Topic>(p => p.TopicFiles)
					}
				};
			}
		}
	}
}