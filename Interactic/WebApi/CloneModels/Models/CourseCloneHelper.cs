﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class CourseCloneHelper
	{
		public static CloneMap<Course> CourseMap_1
		{
			get
			{
				return new CloneMap<Course>()
				{
					SubProperties =
					{
						new CloneMap<Course>( p => p.EducationalEstablishment)
					}
				};
			}
		}

		public static CloneMap<Course> CourseMap_2
		{
			get
			{
				return new CloneMap<Course>()
				{
					SubProperties =
					{
						new CloneMap<Course>( p => p.EducationalEstablishment),
						new CloneMap<Course>( p => p.CourseSubjects)
						{
							SubProperties =
							{
								new CloneMap<CourseSubject>( p => p.Subject)
								{
									SubProperties =
									{
										new CloneMap<Subject>( p => p.SubSubjects)
									}
								},
								new CloneMap<CourseSubject>( p => p.CourseSubSubjects)
							}
						}
					}
				};
			}
		}

		public static CloneMap<Course> CourseMap_3
		{
			get
			{
				return new CloneMap<Course>()
				{
					SubProperties =
					{
						new CloneMap<Course>( p => p.StudentCourses)
						{
							SubProperties =
							{
								new CloneMap<StudentCourse>( p => p.User, excludeAllPrimitiveProperties : true)
								{
									SubProperties =
									{
										new CloneMap<User>(p => p.Id ),
										new CloneMap<User>(p => p.UserName ),
										new CloneMap<User>(p => p.Name ),
										new CloneMap<User>(p => p.FatherLastName ),
										new CloneMap<User>(p => p.MotherLastName ),
										new CloneMap<User>(p => p.Status ),

										new CloneMap<User>( p => p.StudentQualifications)
										{
											SubProperties =
											{
												new CloneMap<StudentQualification>( p => p.BaseSubject),
												new CloneMap<StudentQualification>( p => p.PeriodDetail)
											}
										}
									}
								}
							}
						}
					}
				};
			}
		}
	}
}