﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class NoteCloneHelper
	{
		public static CloneMap<Note> NoteMap_1
		{
			get
			{
				return new CloneMap<Note>()
				{
					SubProperties =
					{
						new CloneMap<Note>( p => p.SubUnity)
						{
							SubProperties=
							{
								new CloneMap<SubUnity>( p => p.Unity)
								{
									SubProperties=
									{
										new CloneMap<Unity>( p => p.CourseSubject)
									}
								}
							}
						}
					}
				};
			}
		}
	}
}