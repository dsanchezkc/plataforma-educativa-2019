﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class EvaluationRubricCloneHelper
    {
        public static CloneMap<EvaluationRubric> EvaluationRubricMap_0
        {
            get
            {
                return new CloneMap<EvaluationRubric>()
                {
                    SubProperties =
                    {
                        new CloneMap<EvaluationRubric>( p => p.Id),
                        new CloneMap<EvaluationRubric>( p => p.Name),
                    }
                };
            }
        }
        public static CloneMap<EvaluationRubric> EvaluationRubricMap_1
        {
            get
            {
                return new CloneMap<EvaluationRubric>()
                {
                    SubProperties =
                    {
                        new CloneMap<EvaluationRubric>( p => p.Id),
                        new CloneMap<EvaluationRubric>( p => p.Name),
                        new CloneMap<EvaluationRubric>( p => p.Subject)
                        {
                            SubProperties =
                            {
                                new CloneMap<Subject>( p => p.Id),
                                new CloneMap<Subject>( p => p.Name),
                            }
                        },
                        new CloneMap<EvaluationRubric>( p => p.Rubric)
                        {
                            SubProperties =
                            {
                                new CloneMap<Rubric>( p => p.Id),
                                new CloneMap<Rubric>( p => p.Name),

                            }
                        },


                    }
                };
            }
        }

    }
}