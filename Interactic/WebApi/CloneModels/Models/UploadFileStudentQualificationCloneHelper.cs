﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class UploadFileStudentQualificationCloneHelper
	{
		public static CloneMap<UploadFileStudentQualification> UploadFileStudentQualificationMap_1
		{
			get
			{
				return new CloneMap<UploadFileStudentQualification>()
				{
					SubProperties =
					{
						new CloneMap<UploadFileStudentQualification>(p => p.UploadFileStudent),

						new CloneMap<UploadFileStudentQualification>(p => p.Student, excludeAllPrimitiveProperties: true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.Id ),
								new CloneMap<User>(p => p.UserName ),
								new CloneMap<User>(p => p.Name ),
								new CloneMap<User>(p => p.FatherLastName ),
								new CloneMap<User>(p => p.MotherLastName ),
								new CloneMap<User>(p => p.Address ),
								new CloneMap<User>(p => p.Email ),
								new CloneMap<User>(p => p.Status ),
							}
						},
						new CloneMap<UploadFileStudentQualification>( p => p.UploadFileStudentItems, excludeAllPrimitiveProperties :true)
						{
							SubProperties =
							{
								new CloneMap<UploadFileStudentItem>(p => p.Id),
								new CloneMap<UploadFileStudentItem>(p => p.DocumentName),
								new CloneMap<UploadFileStudentItem>(p => p.DocumentType),
							}
						}
					}
				};
			}
		}
	}
}