﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class GuardianCloneHelper
	{
		public static CloneMap<Guardian> GuardianMap_1
		{
			get
			{
				return new CloneMap<Guardian>()
				{
					SubProperties =
					{
						new CloneMap<Guardian>( p => p.EducationalEstablishment, true)
						{
							SubProperties =
							{
								new CloneMap<EducationalEstablishment>(p => p.Name)
							}
						},
						new CloneMap<Guardian>( p => p.User, true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.Name),
								new CloneMap<User>(p => p.FatherLastName)
							}
						}
					}
				};
			}
		}		
	}
}