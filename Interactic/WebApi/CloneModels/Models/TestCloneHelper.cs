﻿using GeneralHelper;
using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class TestCloneHelper
	{
		public static CloneMap<Test> TestMap_1
		{
			get
			{
				return new CloneMap<Test>()
				{
					SubProperties =
					{
						new CloneMap<Test>( p => p.SubUnity)
						{
							SubProperties =
							{
								new CloneMap<SubUnity>( p => p.Unity)
								{
									SubProperties=
									{
										new CloneMap<Unity>( p => p.CourseSubject)
										{
											SubProperties =
											{
												new CloneMap<CourseSubject>( p => p.Subject),
												new CloneMap<CourseSubject>( p => p.Course)
											}
										}
									}
								}
							}
						},
						new CloneMap<Test>(p => p.AchievementIndicator)
						{
							SubProperties =
							{
								new CloneMap<AchievementIndicator>(p => p.AchievementAlternatives)
								{
									SubProperties =
									{
										new CloneMap<AchievementAlternative>(p => p.AchievementIndicator),
										new CloneMap<AchievementAlternative>(p => p.Question)
									}
								}
							}
						},
						new CloneMap<Test>(p => p.Questions)
						{
							SubProperties=
							{
								new CloneMap<Question>(p => p.Skill)
							}
						}
                        ,
                        new CloneMap<Test>(p => p.CourseTests)
                        {
                            SubProperties=
                            {
                                new CloneMap<CourseTest>(p => p.Course_Id),
                                new CloneMap<CourseTest>(p => p.Test_Id)
                            }
                        }
                    }
				};
			}
		}

		public static CloneMap<Test> TestMap_2
		{
			get
			{
				return new CloneMap<Test>()
				{
					SubProperties =
					{
						new CloneMap<Test>(p => p.Questions)
					}
				};
			}
		}

		public static CloneMap<Test> TestMap_3
		{
			get
			{
				return new CloneMap<Test>()
				{
					SubProperties =
					{
						new CloneMap<Test>(p => p.StudentTests)
						{
							SubProperties =
							{
								new CloneMap<StudentTest>(p => p.User, excludeAllPrimitiveProperties : true)
								{
									SubProperties =
									{
										new CloneMap<User>(p => p.Id ),
										new CloneMap<User>(p => p.UserName ),
										new CloneMap<User>(p => p.Name ),
										new CloneMap<User>(p => p.FatherLastName ),
										new CloneMap<User>(p => p.MotherLastName ),
										new CloneMap<User>(p => p.Status ),

										new CloneMap<User>(p => p.StudentCourses)
									}
								}
							}
						}
					}
				};
			}
		}

        public static CloneMap<Test> TestMap_4
        {
            get
            {
                return new CloneMap<Test>()
                {
                    SubProperties =
                    {
                        new CloneMap<Test>(p => p.AchievementIndicator)
                        {
                            SubProperties =
                            {
                                new CloneMap<AchievementIndicator>(p => p.AchievementAlternatives)
                                {
                                    SubProperties =
                                    {
                                        new CloneMap<AchievementAlternative>(p => p.AchievementIndicator),
                                        new CloneMap<AchievementAlternative>(p => p.Question)
                                    }
                                }
                            }
                        },
                        new CloneMap<Test>(p => p.Questions)
                        {
                            SubProperties=
                            {
                                new CloneMap<Question>(p => p.Skill)
                            }
                        }
                        ,
                        new CloneMap<Test>(p => p.CourseTests)
                        {
                            SubProperties=
                            {
                                new CloneMap<CourseTest>(p => p.Course_Id),
                                new CloneMap<CourseTest>(p => p.Test_Id)
                            }
                        }
                    }
                };
            }
        }

        /*
        public static Test GetTestWithoutAnswers(SpecialTest test)
        {
            var _test = CloneHelper.CloneObject(test) as Test;

            var questionWrittenAnswers = CloneHelper.CloneListMap(test.Questions.OfType<QuestionWrittenAnswer>(), QuestionWrittenAnswerCloneHelper.QuestionWrittenAnswerMap_1);
            //Borrando respuestas
            foreach (var qwa in questionWrittenAnswers)
            {
                qwa.QuestionWrittenAnswerItem.Answer = "";
                qwa.ComplementaryAnswer = "";
            }
            _test.Questions.AddRange(questionWrittenAnswers);

            var questionTrueFalses = CloneHelper.CloneListMap(test.Questions.OfType<QuestionTrueFalse>(), QuestionTrueFalseCloneHelper.QuestionTrueFalseMap_1);
            //Borrando respuestas
            foreach (var qtf in questionTrueFalses)
            {
                qtf.ComplementaryAnswer = "";
                foreach (var qtfi in qtf.QuestionTrueFalseItems)
                    qtfi.Value = false;
            }
            _test.Questions.AddRange(questionTrueFalses);

            var questionPairedWords = CloneHelper.CloneListMap(test.Questions.OfType<QuestionPairedWord>(), QuestionPairedWordCloneHelper.QuestionPairedWordMap_1);
            foreach (var qpw in questionPairedWords)
            {
                qpw.QuestionPairedWordItems = qpw.QuestionPairedWordItems.OrderBy(p => p.Column_2_Index).ToList();

                if (qpw != null)
                    qpw.ComplementaryAnswer = "";
                //Borrando respuestas
                foreach (var qpwi in qpw.QuestionPairedWordItems)
                    qpwi.Column_1_Alternative = 0;
            }
            _test.Questions.AddRange(questionPairedWords);

            var questionAlternatives = CloneHelper.CloneListMap(test.Questions.OfType<QuestionAlternative>(), QuestionAlternativeCloneHelper.QuestionAlternativeMap_1);
            //Borrando respuestas
            foreach (var qa in questionAlternatives)
            {
                qa.ComplementaryAnswer = "";
                foreach (var qai in qa.QuestionAlternativeItems)
                    qai.Value = false;
            }
            _test.Questions.AddRange(questionAlternatives);
            _test.Questions = _test.Questions.OrderBy(p => p.Order).ToList();

            return _test;
        }
        */
    }
}