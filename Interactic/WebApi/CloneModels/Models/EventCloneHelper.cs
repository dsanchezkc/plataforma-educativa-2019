﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class EventCloneHelper
	{
		public static CloneMap<Event> EventMap_1
		{
			get
			{
				return new CloneMap<Event>()
				{
					SubProperties =
					{
						new CloneMap<Event>(p => p.User, excludeAllPrimitiveProperties : true )
						{
							SubProperties=
							{
								new CloneMap<User>(p => p.Id ),
								new CloneMap<User>(p => p.UserName ),
								new CloneMap<User>(p => p.Name ),
								new CloneMap<User>(p => p.FatherLastName ),
								new CloneMap<User>(p => p.MotherLastName ),
								new CloneMap<User>(p => p.Status )
							}
						},
						new CloneMap<Event>(p => p.Course ),
                        new CloneMap<Event>(p => p.EventFiles)
                        {
                            SubProperties=
                            {
                                new CloneMap<EventFile>(p => p.Id )
                            }
                        }
                    }
				};
			}
		}
	}
}