﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class PostCloneHelper
	{
		public static CloneMap<Post> PostMap_1
		{
			get
			{
				return new CloneMap<Post>()
				{
					SubProperties =
					{
						new CloneMap<Post>(p => p.Author, excludeAllPrimitiveProperties:true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.Id),
								new CloneMap<User>(p => p.UserName),
								new CloneMap<User>(p => p.Name),
								new CloneMap<User>(p => p.FatherLastName),
								new CloneMap<User>(p => p.MotherLastName),
								new CloneMap<User>(p => p.ImagePath)
							}
						},
                        /*
						new CloneMap<Post>(p => p.PostQualifications)
						{
							SubProperties =
							{
								new CloneMap<PostQualification>(p => p.User, excludeAllPrimitiveProperties:true)
								{
									SubProperties =
									{
										new CloneMap<User>(p => p.Id),
										new CloneMap<User>(p => p.UserName),
										new CloneMap<User>(p => p.Name),
										new CloneMap<User>(p => p.FatherLastName),
										new CloneMap<User>(p => p.MotherLastName)
									}
								}
						}
						},
                        
						new CloneMap<Post>(p => p.PostFiles)
                        */
					}
                };
			}
		}
	}
}