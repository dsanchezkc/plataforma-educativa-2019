﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Helper;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using WebApi.CloneModels;
using WebApi;
using WebApi.Stores;
using Microsoft.AspNet.Identity;
using Extensions;
using GeneralHelper;
using ViewModels;

namespace WebApiEvaluandome.Controllers
{
	[Authorize]
	public class UserController : ApiController
	{
		private EvaluandomeContext db;

		public UserController()
		{
			db = new EvaluandomeContext();
			db.Configuration.ProxyCreationEnabled = true;
		}

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Get()
		{
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(db.Users, UserCloneHelper.UserMap_1));
		}

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Get(string id)
		{
			var user = db.Users.FirstOrDefault(e => e.Id == id);

			if (user != null)
			{
				var clone = CloneHelper.CloneObjectMap(user, UserCloneHelper.UserMap_1);

				clone.UserName = user.UserName;
				clone.UserRoleEducationalEstablishments.RemoveRange(clone.UserRoleEducationalEstablishments.Where(p => !p.Status || (p.EducationalEstablishment_Id != GlobalParameters.EducationalEstablishmentId && p.Role.Name != "Administrador" && p.Role.Name != "SubAdministrador")));
				clone.StudentCourses.RemoveRange(clone.StudentCourses.Where(p => p.Course.EducationalEstablishment.Id != GlobalParameters.EducationalEstablishmentId || p.Course.Year != GlobalParameters.Year));
				clone.TeacherCourseSubjects.RemoveRange(clone.TeacherCourseSubjects.Where(p => p.CourseSubject.Course.EducationalEstablishment.Id != GlobalParameters.EducationalEstablishmentId || p.CourseSubject.Course.Year != GlobalParameters.Year));

				return Request.CreateResponse(HttpStatusCode.OK, clone);
			}
			else
				return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Usuario no encontrado.");
		}

        [Authorize(Roles = "Administrador, SubAdministrador")]
        [Route("Api/User/GetUsersByRole")]
        public HttpResponseMessage GetUsersByRole(string role_id)
        {
            if (GlobalParameters.Rol == "SubAdministrador")
                if (db.Roles.FirstOrDefault(p => p.Id == role_id && (p.Name == "Administrador" || p.Name == "SubAdministrador")) != null)
                    return Request.CreateResponse(HttpStatusCode.OK, new List<User>());
           
            //var users = db.UserRoleEducationalEstablishment.Where(p => (p.User.UserName.Contains(text) || p.User.Name.Contains(text) || p.User.FatherLastName.Contains(text)) && p.RoleId == role_id && (p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId || p.Role.Name == "Administrador") && p.Status).Select(q => q.User).Distinct();

            var   users= db.UserRoleEducationalEstablishment.Where(p => p.RoleId == role_id && (p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId || p.Role.Name == "Administrador") && p.Status).Select(q => q.User).Distinct();
            return Request.CreateCloneResponse(users, UserCloneHelper.UserMap_1);
		}

        [Authorize(Roles = "Administrador, SubAdministrador")]
        [Route("Api/User/GetUsersByText")]
        public HttpResponseMessage GetUsersByText(string role_id, string text)
        {
            if (GlobalParameters.Rol == "SubAdministrador")
                if (db.Roles.FirstOrDefault(p => p.Id == role_id && (p.Name == "Administrador" || p.Name == "SubAdministrador")) != null)
                    return Request.CreateResponse(HttpStatusCode.OK, new List<User>());
            object users = null;
            if (text == "" || text == null)
            {
                users = db.UserRoleEducationalEstablishment.Where(p => p.RoleId == role_id && (p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId || p.Role.Name == "Administrador") && p.Status).Select(q => q.User).Distinct();
            }
            else {
                users = db.UserRoleEducationalEstablishment.Where(p => (p.User.UserName.Contains(text) || p.User.Name.Contains(text) || p.User.FatherLastName.Contains(text) || p.User.MotherLastName.Contains(text)) && p.RoleId == role_id && (p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId || p.Role.Name == "Administrador") && p.Status).Select(q => q.User).Distinct();
            }
            return Request.CreateCloneResponse(users, UserCloneHelper.UserMap_1);
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        [Route("Api/User/GetUsersByName")]
        public HttpResponseMessage GetUsersByName(string name)
        {
            var users = db.UserRoleEducationalEstablishment.Where(p => p.User.Name.Contains(name) && (p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId) && p.Status).Select(q => q.User).Distinct();

            return Request.CreateCloneResponse(users, UserCloneHelper.UserMap_1);
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        [Route("Api/User/GetUsersBySurname")]
        public HttpResponseMessage GetUsersBySurname(string surname)
        {
            var users = db.UserRoleEducationalEstablishment.Where(p => p.User.FatherLastName.Contains(surname) && (p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId) && p.Status).Select(q => q.User).Distinct();

            return Request.CreateCloneResponse(users, UserCloneHelper.UserMap_1);
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
        [Route("Api/User/GetUsersByRun")]
        public HttpResponseMessage GetUsersByRun(string rut)
        {
            var users = db.UserRoleEducationalEstablishment.Where(p => p.User.UserName.Contains(rut) && (p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId) && p.Status).Select(q => q.User).Distinct();

            return Request.CreateCloneResponse(users, UserCloneHelper.UserMap_1);
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
		[Route("Api/User/GetUserByCurrentEstablishment")]
		public HttpResponseMessage GetUserByCurrentEstablishment(string username)
		{
			if (!username.IsValidRUN())
				Request.CreateResponse(HttpStatusCode.BadRequest, "R.U.N invalido");

			username = username.WithoutFormatRUN();
			var user = db.Users.FirstOrDefault(p => p.UserName == username);

			return user == null ? Request.CreateResponse(HttpStatusCode.OK, user) : Get(user.Id);			
		}

		[Authorize(Roles = "Administrador, SubAdministrador,Alumno")]
		[Route("Api/User/GetUserWithSimpleInformation")]
		public HttpResponseMessage GetUserWithSimpleInformation(string username)
		{
			if (!username.IsValidRUN())
				Request.CreateResponse(HttpStatusCode.BadRequest, "R.U.N invalido");

			username = username.WithoutFormatRUN();
			var user = db.Users.FirstOrDefault(p => p.UserName == username);

			if (user == null)
				return Request.CreateResponse(HttpStatusCode.OK, user);

			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(user, UserCloneHelper.UserMap_5));
		}

		[Route("Api/User/GetCurrentUser")]
		public HttpResponseMessage GetCurrentUser()
		{
			var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);
			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneObjectMap(user, UserCloneHelper.UserMap_4));
		}


		[Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
		[Route("Api/User/GetUsersByCourse")]
		public HttpResponseMessage GetUsersByCourse(int course_id)
		{
			var course = db.Course.FirstOrDefault(p => p.Id == course_id);

			if (course == null)
				Request.CreateResponse(HttpStatusCode.BadRequest, "Curso no encontrado");

			return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(course.StudentCourses.OrderBy(x=> x.ListNumber).Select(p => p.User), UserCloneHelper.UserMap_2));
		}

        [Authorize(Roles = "Administrador, SubAdministrador, Profesor")]
        [Route("Api/User/GetUsersByCourseWithListNumber")]
        public HttpResponseMessage GetUsersByCourseWhithListNumber(int course_id)
        {
            var course = db.Course.FirstOrDefault(p => p.Id == course_id);

            if (course == null)
                Request.CreateResponse(HttpStatusCode.BadRequest, "Curso no encontrado");

            return Request.CreateResponse(HttpStatusCode.OK, CloneHelper.CloneListMap(course.StudentCourses.OrderBy(x => x.ListNumber), StudentCourseCloneHelper.StudentCourseMap_1));
        }

        [Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Post()
		{
			var user = PostSpecial(new JavaScriptSerializer().Deserialize<User>(HttpContext.Current.Request.Form["User"]));
			return PutSpecial(user);
		}

		private User PostSpecial(User _user)
		{
			if (!_user.UserName.IsValidRUN())
				throw new Exception("R.U.N inválido.");

			var user = new User()
			{
				Id = _user.Id = Guid.NewGuid().ToString(),
				UserName = _user.UserName.WithoutFormatRUN(),
				Name = _user.Name,
				MotherLastName = _user.MotherLastName,
				FatherLastName = _user.FatherLastName,
				Address = _user.Address
			};

			var userManager = new ApplicationUserManager(new ApplicationUserStore(EvaluandomeContext.Create()));
			var result = userManager.Create(user, user.UserName);
			db.SaveChanges();
			return _user;
		}

		[HttpPost]
		[Route("Api/User/UpdateInformationCurrentUser")]
		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
		public HttpResponseMessage UpdateInformationCurrentUser()
		{
			var file = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files[0] : null;
			var _user = new JavaScriptSerializer().Deserialize<User>(HttpContext.Current.Request.Form["User"]);

			var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);

			user.Name = _user.Name;
			user.Address = _user.Address;
			user.MotherLastName = _user.MotherLastName;
			user.FatherLastName = _user.FatherLastName;

			if (file != null && file.ContentLength != 0)
			{
				if (file.ContentType.ToLower() != "image/png" && file.ContentType.ToLower() != "image/jpeg" && file.ContentType.ToLower() != "image/pjpeg")
					return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

				var path = @"C:\EvaluandomeResources\Images\Avatars\" + user.Id;
				var physicalPath = ImageHelper.SaveImage(file, path, resolutionMax: 500, autoSizeSymmetric: true);
				user.ImagePath = physicalPath == null ? "" : physicalPath;				
			}

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[HttpPut]
		[Route("Api/User/UpdatePasswordCurrentUser")]
		[Authorize(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
		public HttpResponseMessage UpdatePasswordCurrentUser([FromBody]PasswordContentViewModel password)
		{
			if (password == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);

			if (password.NewPassword == null || password.NewPassword.Length < 6)
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Contraseña nueva debe contener al menos 6 carácteres.");

			if (password.OldPassword == null || password.OldPassword.Length < 6)
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Contraseña actual incorrecta.");


			var userManager = new ApplicationUserManager(new ApplicationUserStore(EvaluandomeContext.Create()));
			var result = userManager.ChangePassword(user.Id, password.OldPassword, password.NewPassword);

			if (result.Succeeded)
				return Request.CreateResponse(HttpStatusCode.OK);
			else
			{
				var error = "";
				foreach (var _error in result.Errors)
					error += _error + " ";

				return Request.CreateResponse(HttpStatusCode.BadRequest, error);
			}
		}

		[HttpGet]
		[Authorize(Roles = "Administrador, SubAdministrador ,Profesor, Alumno")]
		[Route("Api/User/DownloadAvatar")]
		public HttpResponseMessage DownloadAvatar()
		{
			var user = db.Users.FirstOrDefault(p => p.Id == GlobalParameters.UserId);

			try
			{
				var binary = FileHelper.GetBinaryFile(user.ImagePath);
				return Request.CreateResponse(HttpStatusCode.OK, "data:image/" + Path.GetExtension(user.ImagePath).Substring(1) + ";base64," + Convert.ToBase64String(binary, 0, binary.Length));
			}
			catch (Exception)
			{
				return Request.CreateResponse(HttpStatusCode.NotFound, "Error.");
			}
		}

		[Authorize(Roles = "Administrador, SubAdministrador")]
		public HttpResponseMessage Put()
		{
			return PutSpecial(new JavaScriptSerializer().Deserialize<User>(HttpContext.Current.Request.Form["User"]));
		}

		private HttpResponseMessage PutSpecial(User _user)
		{
			var file = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files[0] : null;
			
			var user = db.Users.FirstOrDefault(p => p.Id == _user.Id);

			user.Name = _user.Name;
			user.Address = _user.Address;
			user.MotherLastName = _user.MotherLastName;
			user.FatherLastName = _user.FatherLastName;

			if (file != null && file.ContentLength != 0)
			{
				if (file.ContentType.ToLower() != "image/png" && file.ContentType.ToLower() != "image/jpeg" && file.ContentType.ToLower() != "image/pjpeg")
					return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);

				var path = @"C:\EvaluandomeResources\Images\Avatars\" + user.Id;
				var physicalPath = ImageHelper.SaveImage(file, path, resolutionMax: 500, autoSizeSymmetric: true);
				user.ImagePath = physicalPath ?? "";
			}

			AssociateStudent(user, _user);
			AssociateTeacher(user, _user);

			switch (GlobalParameters.Rol)
			{
				case "Administrador":
					AssociateAdministrator(user, _user);
					AssociateSubAdministrator(user, _user);
				break;
				default:break;
			}

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}

		[HttpPost]
		[Authorize(Roles = "Administrador, SubAdministrador")]
		[Route("Api/User/RegisterStudents_Excel")]
		public HttpResponseMessage RegisterStudents_Excel()
		{
			var form = HttpContext.Current.Request.Form;
			var file = (HttpContext.Current.Request.Files.Count > 0) ? HttpContext.Current.Request.Files[0] : null;
			var course_id = int.Parse(form["course_id"]);
			var students = new List<User>();
			var errorList = new List<string>();

			if (file != null && file.ContentLength != 0)
			{
				if (file.ContentType.ToLower() != "application/vnd.ms-excel" && file.ContentType.ToLower() != "application/excel" && file.ContentType.ToLower() != "application/x-excel" && file.ContentType.ToLower() != "application/x-msexcel" && file.ContentType.ToLower() != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
					return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType, file.ContentType);

				string documentName = Path.GetFileName(file.FileName);
				var path = @"C:\EvaluandomeResources\Temporal\" + GlobalParameters.UserId;
				Directory.CreateDirectory(path);
				var physicalPath = path + @"\" + Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
				file.SaveAs(physicalPath);

				try
				{

					var content = ExcelHelper.GetDataSetFromExcelFile(physicalPath);

					if (content.Tables.Count == 0)
						return Request.CreateResponse(HttpStatusCode.BadRequest, "Nombre hoja excel no permitido.");

					var rows_Count = content.Tables[0].Rows.Count;

					for (var i = 0; i < rows_Count; i++)
					{
						try
						{
							var column = content.Tables[0].Rows[i];
							var student = new User()
							{
								UserName = column[0].ToString()?.Trim().WithoutFormatRUN(),
								FatherLastName = column[1].ToString()?.Trim(),
								MotherLastName = column[2].ToString()?.Trim(),
								Name = column[3].ToString()?.Trim(),
								StudentCourses = new List<StudentCourse>()
						{
							new StudentCourse()
							{
								Course =  new Course()
								{
									Id = course_id
								},
								ListNumber = int.Parse(column[4].ToString()),
								EstablishmentRegisterNumber = int.Parse(column[5].ToString())
							}
						},
								UserRoleEducationalEstablishments = new List<UserRoleEducationalEstablishment>()
							};

							students.Add(student);
						}
						catch (Exception e)
						{

						}
					}

					var success_count = 0;

					foreach (var _user in students)
					{
						try
						{
							if (!_user.UserName.IsValidRUN())
								continue;

							var user = db.Users.FirstOrDefault(p => p.UserName == _user.UserName);

							if (user == null)
							{
								PostSpecial(_user);
								user = db.Users.FirstOrDefault(p => p.UserName == _user.UserName);

								if (user == null)
									continue;
							}
							else
							{
								var studentCourse = user.StudentCourses.FirstOrDefault(p => p.User_Id == user.Id && p.Course_Id == course_id);

								if (studentCourse == null)
									_user.StudentCourses.AddRange(user.StudentCourses.Where(p => p.Status));
								else
									_user.StudentCourses.AddRange(user.StudentCourses.Where(p => p.Status && !(p.User_Id == studentCourse.User_Id && p.Course_Id == studentCourse.Course_Id)));

								user.Name = _user.Name?.Trim();
								user.FatherLastName = _user.FatherLastName?.Trim();
								user.MotherLastName = _user.MotherLastName?.Trim();
							}

							_user.Id = user.Id;
							AssociateStudent(user, _user);
							db.SaveChanges();
							success_count++;
						}
						catch (Exception e)
						{
							errorList.Add(_user.UserName + ": " + e.Message);
						}

					}

				}
				catch (Exception e)
				{
					return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message+"\n"+e.InnerException?.Message );

				}
			}

			return Request.CreateResponse(HttpStatusCode.OK, "Registro realizado en " + (students.Count - errorList.Count) + " de " + students.Count + " alumnos.");
		}

		private void AssociateStudent(User user, User _user)
		{
			foreach (var studentCourse in user.StudentCourses)
			{
				if (studentCourse.Course.EducationalEstablishment.Id != GlobalParameters.EducationalEstablishmentId || studentCourse.Course.Year != GlobalParameters.Year)
					continue;

				StudentCourse _studentCourse = null;

				if ((_studentCourse = _user.StudentCourses.FirstOrDefault(p => p.Course.Id == studentCourse.Course.Id && _user.Id == studentCourse.User_Id)) == null)
					studentCourse.Status = false;
				else
				{
					studentCourse.Status = true;

					if (studentCourse.ListNumber != _studentCourse.ListNumber)
						studentCourse.ListNumber = db.StudentCourse.FirstOrDefault(p => p.Course.Id == _studentCourse.Course.Id && p.ListNumber == _studentCourse.ListNumber) != null ? -1 : _studentCourse.ListNumber;

					if (studentCourse.EstablishmentRegisterNumber != _studentCourse.EstablishmentRegisterNumber)
						studentCourse.EstablishmentRegisterNumber = db.StudentCourse.FirstOrDefault(p => p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.EstablishmentRegisterNumber == _studentCourse.EstablishmentRegisterNumber) != null ? -1 : _studentCourse.EstablishmentRegisterNumber;
				}
			}

			//Agregar
			foreach (var _studentCourse in _user.StudentCourses)
				if (user.StudentCourses.FirstOrDefault(p => p.Course_Id == _studentCourse.Course.Id && p.User_Id == _user.Id) == null)
					user.StudentCourses.Add(new StudentCourse()
					{
						Course = db.Course.FirstOrDefault(p => p.Status && p.Id == _studentCourse.Course.Id && p.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Year == GlobalParameters.Year),
						User = user,
						ListNumber = db.StudentCourse.FirstOrDefault(p => p.Course.Id == _studentCourse.Course.Id && p.ListNumber == _studentCourse.ListNumber) != null ? -1 : _studentCourse.ListNumber,
						EstablishmentRegisterNumber = db.StudentCourse.FirstOrDefault(p => p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.EstablishmentRegisterNumber == _studentCourse.EstablishmentRegisterNumber) != null ? -1 : _studentCourse.EstablishmentRegisterNumber
					});

			var userRolEducationalEstablishment = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId && p.Role.Name == "Alumno");
			var studentCourses_Count = user.StudentCourses.Where(p => p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Status).Count();

			if (studentCourses_Count != 0 && userRolEducationalEstablishment == null)
				user.UserRoleEducationalEstablishments.Add(new UserRoleEducationalEstablishment()
				{
					User = user,
					EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(p => p.Id == GlobalParameters.EducationalEstablishmentId),
					Role = db.Roles.FirstOrDefault(p => p.Name == "Alumno")
				});
			else
			if (userRolEducationalEstablishment != null)
				userRolEducationalEstablishment.Status = studentCourses_Count != 0;
		}

		private void AssociateTeacher(User user, User user_local = null)
		{
			foreach (var tcs in user.TeacherCourseSubjects)
			{
				if (tcs.CourseSubject.Course.EducationalEstablishment.Id != GlobalParameters.EducationalEstablishmentId || tcs.CourseSubject.Course.Year != GlobalParameters.Year)
					continue;

				TeacherCourseSubject tcs_local = null;

				if ((tcs_local = user_local.TeacherCourseSubjects.FirstOrDefault(p => p.CourseSubject.Course.Id == tcs.CourseSubject.Course.Id && p.CourseSubject.Subject.Id == tcs.CourseSubject.Subject.Id && user_local.Id == tcs.User_Id)) == null)
					tcs.Status = false;
				else
					tcs.Status = true;
			}

			//Agregar
			foreach (var tcs_local in user_local.TeacherCourseSubjects)
				if (user.TeacherCourseSubjects.FirstOrDefault(p => p.Course_Id == tcs_local.CourseSubject.Course.Id && p.Subject_Id == tcs_local.CourseSubject.Subject.Id && p.User_Id == user_local.Id) == null)
					user.TeacherCourseSubjects.Add(new TeacherCourseSubject()
					{
						User = user,
						CourseSubject = db.CourseSubject.FirstOrDefault(p => p.Course_Id == tcs_local.CourseSubject.Course.Id && p.Subject_Id == tcs_local.CourseSubject.Subject.Id && p.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Course.Year == GlobalParameters.Year)
					});

			var userRolEducationalEstablishment = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId && p.Role.Name == "Profesor");
			var tcs_Count = user.TeacherCourseSubjects.Where(p => p.CourseSubject.Course.EducationalEstablishment.Id == GlobalParameters.EducationalEstablishmentId && p.Status).Count();

			if (tcs_Count != 0 && userRolEducationalEstablishment == null)
				user.UserRoleEducationalEstablishments.Add(new UserRoleEducationalEstablishment()
				{
					User = user,
					EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(p => p.Id == GlobalParameters.EducationalEstablishmentId),
					Role = db.Roles.FirstOrDefault(p => p.Name == "Profesor")
				});
			else
			if (userRolEducationalEstablishment != null)
				userRolEducationalEstablishment.Status = tcs_Count != 0;
		}

		private void AssociateSubAdministrator(User user, User user_local = null)
		{
			var userRoleEducationalEstablishments_local = user_local.UserRoleEducationalEstablishments.Where(p => p.EducationalEstablishment != null);

			foreach (var ure in user.UserRoleEducationalEstablishments)
			{
				UserRoleEducationalEstablishment ure_local = null;

				if ((ure_local = userRoleEducationalEstablishments_local.FirstOrDefault(p => p.EducationalEstablishment.Id == ure.EducationalEstablishment.Id && p.Role.Name == ure.Role.Name && user_local.Id == user.Id)) == null)
				{
					if (ure.Role.Name == "SubAdministrador")
						ure.Status = false;
				}
				else
					if (ure.Role.Name == "SubAdministrador")
					ure.Status = true;
			}

			//Agregar
			foreach (var ure_local in userRoleEducationalEstablishments_local)
				if (user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.User.Id == user_local.Id && p.EducationalEstablishment_Id == ure_local.EducationalEstablishment.Id && p.Role.Name == ure_local.Role.Name) == null)
					if (ure_local.Role.Name == "SubAdministrador")
						user.UserRoleEducationalEstablishments.Add(new UserRoleEducationalEstablishment()
						{
							User = user,
							EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(p => p.Id == ure_local.EducationalEstablishment.Id),
							Role = db.Roles.FirstOrDefault(p => p.Name == ure_local.Role.Name)
						});
		}

		private void AssociateAdministrator(User user, User user_local = null)
		{
			var ure_local = user_local.UserRoleEducationalEstablishments.FirstOrDefault(p => p.EducationalEstablishment != null && p.Role.Name == "Administrador");
			var ure = user.UserRoleEducationalEstablishments.FirstOrDefault(p => p.Role.Name == "Administrador");

			//Editar
			if (ure != null)
				ure.Status = ure_local != null;

			if (ure_local != null && ure == null)
				user.UserRoleEducationalEstablishments.Add(new UserRoleEducationalEstablishment()
				{
					User = user,
					EducationalEstablishment = db.EducationalEstablishment.FirstOrDefault(),
					Role = db.Roles.FirstOrDefault(p => p.Name == "Administrador")
				});
		}

		/// <summary>
		/// Author AP
		/// Desactiva un usuario asociado a un establecimiento - Si es Admin seguirá estando asociado.
		/// Para desactivarlo, dirigirse a edición y desclickear Role : Administrador.
		/// </summary>
		[Authorize(Roles = "Administrador")]
		public HttpResponseMessage Delete(string id)
		{
			var item = db.Users.FirstOrDefault(p => p.Id == id);

			if (item == null)
				return Request.CreateResponse(HttpStatusCode.BadRequest);

			foreach (var ure in item.UserRoleEducationalEstablishments)
				if (ure.EducationalEstablishment_Id == GlobalParameters.EducationalEstablishmentId && ure.Role.Name != "Administrador")
					ure.Status = false;

			db.SaveChanges();
			return Request.CreateResponse(HttpStatusCode.OK);
		}
	}
}
