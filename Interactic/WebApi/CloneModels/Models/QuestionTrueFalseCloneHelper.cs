﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class QuestionTrueFalseCloneHelper
	{
		public static CloneMap<QuestionTrueFalse> QuestionTrueFalseMap_1
		{
			get
			{
				return new CloneMap<QuestionTrueFalse>()
				{
					SubProperties =
					{
						new CloneMap<QuestionTrueFalse>( p => p.QuestionTrueFalseItems)
					}
				};
			}
		}
	}
}