﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class SkillsGroupCloneHelper
	{
		public static CloneMap<SkillsGroup> SkillsGroupMap_1
		{
			get
			{
				return new CloneMap<SkillsGroup>()
				{
					SubProperties =
					{
						new CloneMap<SkillsGroup>( p => p.Skills)
					}
				};
			}
		}
	}
}