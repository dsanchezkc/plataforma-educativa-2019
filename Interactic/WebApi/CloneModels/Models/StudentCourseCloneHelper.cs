﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class StudentCourseCloneHelper
	{
		public static CloneMap<StudentCourse> StudentCourseMap_1
		{
			get
			{
				return new CloneMap<StudentCourse>()
				{
                    SubProperties =
					{
                        new CloneMap<StudentCourse>(p => p.ListNumber ),
                        new CloneMap<StudentCourse>( p => p.User, excludeAllPrimitiveProperties : true)
						{

                            SubProperties =
							{
								new CloneMap<User>(p => p.Id ),
								new CloneMap<User>(p => p.UserName ),
								new CloneMap<User>(p => p.Name ),
								new CloneMap<User>(p => p.FatherLastName ),
								new CloneMap<User>(p => p.MotherLastName ),
								new CloneMap<User>(p => p.Status ),
                            }
						}
					}
				};
			}
		}
	}
}