﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class ActivityCloneHelper
	{
		public static CloneMap<Activity> ActivityMap_1
		{
			get
			{
				return new CloneMap<Activity>()
				{
					SubProperties =
					{
						new CloneMap<Activity>( p => p.SubUnity),
					}
				};
			}
		}		
	}
}