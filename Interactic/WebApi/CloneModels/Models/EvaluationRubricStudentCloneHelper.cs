﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class EvaluationRubricStudentCloneHelper
    {
        public static CloneMap<EvaluationRubricStudent> EvaluationRubricStudentMap_0
        {
            get
            {
                return new CloneMap<EvaluationRubricStudent>()
                {
                    SubProperties =
                    {
                        new CloneMap<EvaluationRubricStudent>( p => p.Id),
                        new CloneMap<EvaluationRubricStudent>( p => p.Criterion_Id),
                        new CloneMap<EvaluationRubricStudent>( p => p.EvaluationRubric_Id),
                        new CloneMap<EvaluationRubricStudent>( p => p.Student_Id),
                        new CloneMap<EvaluationRubricStudent>( p => p.Score),
                    }
                };
            }
        }
    }
}