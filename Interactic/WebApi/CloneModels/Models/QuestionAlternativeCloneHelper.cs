﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class QuestionAlternativeCloneHelper
	{
		public static CloneMap<QuestionAlternative> QuestionAlternativeMap_1
		{
			get
			{
				return new CloneMap<QuestionAlternative>()
				{
					SubProperties =
					{
						new CloneMap<QuestionAlternative>( p => p.QuestionAlternativeItems),
                    }
				};
			}
		}
	}
}