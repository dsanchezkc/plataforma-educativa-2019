﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class AttendanceDateCloneHelper
	{
		public static CloneMap<AttendanceDate> AttendanceDateMap_1
		{
			get
			{
				return new CloneMap<AttendanceDate>()
				{
					SubProperties =
					{
						new CloneMap<AttendanceDate>( p => p.BaseSubject),
						new CloneMap<AttendanceDate>( p => p.PeriodDetail),
						new CloneMap<AttendanceDate>( p => p.Daytrip),
						new CloneMap<AttendanceDate>( p => p.Course),
						new CloneMap<AttendanceDate>( p => p.Absents)
					}
				};
			}
		}		
	}
}