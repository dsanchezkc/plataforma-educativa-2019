﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class RubricCloneHelper
    {
        public static CloneMap<Rubric> RubricMap_3
        {
            get
            {
                return new CloneMap<Rubric>()
                {
                    SubProperties =
                    {
                        new CloneMap<Rubric>( p => p.Id),
                        new CloneMap<Rubric>( p => p.Name),
                    }
                };
            }
        }
        public static CloneMap<Rubric> RubricMap_1
        {
            get
            {
                return new CloneMap<Rubric>()
                {
                    SubProperties =
                    {
                        new CloneMap<Rubric>( p => p.Id),
                        new CloneMap<Rubric>( p => p.Name),
                        new CloneMap<Rubric>( p => p.AchievementLevels)
                        {
                            SubProperties =
                            {
                                new CloneMap<AchievementLevel>( p => p.Name),
                                new CloneMap<AchievementLevel>( p => p.Rubric),
                                new CloneMap<AchievementLevel>( p => p.Dimension_Id),
                            }
                        },
                        new CloneMap<Rubric>( p => p.Criterions)
                        {
                            SubProperties =
                            {
                                new CloneMap<Criterion>( p => p.Name),
                                new CloneMap<Criterion>( p => p.Rubric),
                                new CloneMap<Criterion>( p => p.Dimension_Id)
                            }
                        },

                        new CloneMap<Rubric>( p => p.DescriptionRubricItems)
                        {
                            SubProperties =
                            {
                                new CloneMap<DescriptionRubricItem>( p => p.Text),
                                new CloneMap<DescriptionRubricItem>( p => p.Criterion_Id),
                                new CloneMap<DescriptionRubricItem>( p => p.AchievementLevel_Id),
                                new CloneMap<DescriptionRubricItem>( p => p.Dimension_Id),
                                new CloneMap<DescriptionRubricItem>( p => p.Score)
                            }
                        },
                        new CloneMap<Rubric>( p => p.RubricDimensions)
                        {
                            SubProperties =
                            {
                                new CloneMap<RubricDimension>( p => p.Rubric_Id),
                                new CloneMap<RubricDimension>( p => p.Dimension)
                                {
                                    SubProperties =
                                    {
                                        new CloneMap<Dimension>( p => p.Name),
                                    }
                                },
                            }
                        },
                    }
                };
            }
        }

        public static CloneMap<Rubric> RubricMap_2
        {
            get
            {
                return new CloneMap<Rubric>()
                {
                    SubProperties =
                    {
                        new CloneMap<Rubric>( p => p.AchievementLevels)
                        {
                            SubProperties =
                            {
                                new CloneMap<AchievementLevel>( p => p.Id),
                                new CloneMap<AchievementLevel>( p => p.Name),
                                new CloneMap<AchievementLevel>( p => p.Rubric),
                                new CloneMap<AchievementLevel>( p => p.Dimension_Id)
                            }
                        },
                        new CloneMap<Rubric>( p => p.Criterions)
                        {
                            SubProperties =
                            {
                                new CloneMap<Criterion>( p => p.Id),
                                new CloneMap<Criterion>( p => p.Name),
                                new CloneMap<Criterion>( p => p.Rubric),
                                new CloneMap<Criterion>( p => p.Dimension_Id)
                            }
                        },

                        new CloneMap<Rubric>( p => p.DescriptionRubricItems)
                        {
                            SubProperties =
                            {
                                new CloneMap<DescriptionRubricItem>( p => p.Text),
                                new CloneMap<DescriptionRubricItem>( p => p.Criterion_Id),
                                new CloneMap<DescriptionRubricItem>( p => p.AchievementLevel_Id),
                                new CloneMap<DescriptionRubricItem>( p => p.Dimension_Id),
                                new CloneMap<DescriptionRubricItem>( p => p.Score)
                            }
                        },
                        new CloneMap<Rubric>( p => p.RubricDimensions)
                        {
                            SubProperties =
                            {
                                new CloneMap<RubricDimension>( p => p.Rubric_Id),
                                new CloneMap<RubricDimension>( p => p.Dimension)
                                {
                                    SubProperties =
                                    {
                                        new CloneMap<Dimension>( p => p.Name),
                                    }
                                },
                            }
                        },

                    }
                };
            }
        }
    }
}