﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class DimensionCloneHelper
    {
        public static CloneMap<Dimension> DimensionMap_1
        {
            get
            {
                return new CloneMap<Dimension>()
                {
                    SubProperties =
                    {
                        new CloneMap<Dimension>(p => p.Name)
                    }
                };
            }
        }
    }
}