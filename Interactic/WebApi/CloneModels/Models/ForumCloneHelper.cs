﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class ForumCloneHelper
    {
        public static CloneMap<Forum> Map
        {
            get
            {
                return new CloneMap<Forum>()
                {
                    SubProperties =
                    {
                        new CloneMap<Forum>( p => p.Id)
                    }
                };
            }
        }
    }
}