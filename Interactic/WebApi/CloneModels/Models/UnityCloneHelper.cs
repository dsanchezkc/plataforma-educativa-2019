﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class UnityCloneHelper
	{
		public static CloneMap<Unity> UnityMap_1
		{
			get
			{
				return new CloneMap<Unity>()
				{
					SubProperties =
					{
						new CloneMap<Unity>( p => p.CourseSubject),
						new CloneMap<Unity>( p => p.SubUnits)
					}
				};
			}
		}

		public static CloneMap<Unity> UnityMap_2
		{
			get
			{
				return new CloneMap<Unity>()
				{
					SubProperties =
					{
						new CloneMap<Unity>( p => p.CourseSubject)
						{
							SubProperties =
							{
								new CloneMap<CourseSubject>( p => p.Course),
								new CloneMap<CourseSubject>( p => p.Subject)
							}
						},
						new CloneMap<Unity>( p => p.SubUnits)
						{
							SubProperties=
							{
								new CloneMap<SubUnity>( p => p.Unity)
							}
						}
					}
				};
			}
		}
	}
}