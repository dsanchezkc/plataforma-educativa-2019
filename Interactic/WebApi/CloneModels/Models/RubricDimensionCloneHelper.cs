﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class RubricDimensionCloneHelper
    {
        public static CloneMap<RubricDimension> RubricDimensionMap_1
        {
            get
            {
                return new CloneMap<RubricDimension>()
                {
                    SubProperties =
                    {
                        new CloneMap<RubricDimension>(p => p.Dimension_Id),
                        new CloneMap<RubricDimension>(p => p.Rubric_Id)
                    }
                };
            }
        }
    }
}