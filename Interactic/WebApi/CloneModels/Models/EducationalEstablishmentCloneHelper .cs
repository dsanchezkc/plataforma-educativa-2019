﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class EducationalEstablishmentCloneHelper
	{
		public static CloneMap<EducationalEstablishment> EducationalEstablishment_1
		{
			get
			{
				return new CloneMap<EducationalEstablishment>()
				{
					SubProperties =
					{
						new CloneMap<EducationalEstablishment>( p => p.Commune)
						{
							SubProperties =
							{
								new CloneMap<Commune>( p => p.Province)
								{
									SubProperties =
									{
										new CloneMap<Province>( p => p.Region)
									}
								}
							}
						},
						new CloneMap<EducationalEstablishment>( p => p.EstablishmentPeriod)
						{
							SubProperties=
							{
								new CloneMap<EstablishmentPeriod>( p => p.PeriodDetails)
							}
						}
					}
				};
			}
		}		
	}
}