﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class SubjectCloneHelper
	{
		public static CloneMap<Subject> SubjectMap_1
		{
			get
			{
				return new CloneMap<Subject>()
				{
					SubProperties =
					{
						new CloneMap<Subject>( p => p.CourseSubjects)
						{
							SubProperties =
							{
								new CloneMap<CourseSubject>( p => p.Course)
							}
						},
						new CloneMap<Subject>( p => p.SubSubjects)
					}
				};
			}
		}

		public static CloneMap<Subject> SubjectMap_2
		{
			get
			{
				return new CloneMap<Subject>()
				{
					SubProperties =
					{
						new CloneMap<Subject>( p => p.SubSubjects)
					}
				};
			}
		}

		public static CloneMap<Subject> SubjectMap_3
		{
			get
			{
				return new CloneMap<Subject>()
				{
					SubProperties =
					{
						new CloneMap<Subject>( p => p.SubSubjects)
						{
							SubProperties =
							{
								new CloneMap<SubSubject>( p => p.Observations)
							}
						},
						new CloneMap<Subject>( p => p.Observations)
					}
				};
			}
		}
	}
}