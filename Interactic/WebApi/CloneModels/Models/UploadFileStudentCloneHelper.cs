﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class UploadFileStudentCloneHelper
	{
		public static CloneMap<UploadFileStudent> UploadFileStudentMap_1
		{
			get
			{
				return new CloneMap<UploadFileStudent>()
				{
					SubProperties =
					{
						new CloneMap<UploadFileStudent>( p => p.UploadFileStudentQualifications)
						{
							SubProperties =
							{
								new CloneMap<UploadFileStudentQualification>( p => p.UploadFileStudentItems, excludeAllPrimitiveProperties :true)
								{
									SubProperties =
									{
										new CloneMap<UploadFileStudentItem>(p => p.Id),
										new CloneMap<UploadFileStudentItem>(p => p.DocumentName),
										new CloneMap<UploadFileStudentItem>(p => p.DocumentType),
									}
								}
							}
						}
					}
				};
			}
		}

		public static CloneMap<UploadFileStudent> UploadFileStudentMap_2
		{
			get
			{
				return new CloneMap<UploadFileStudent>();
			}
		}
	}
}