﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class StudentTestCloneHelper
	{
		public static CloneMap<StudentTest> StudentTestMap_1
		{
			get
			{
				return new CloneMap<StudentTest>()
				{
					SubProperties =
					{
						new CloneMap<StudentTest>( p => p.Test)
						{
							SubProperties =
							{
								new CloneMap<Test>(p => p.Questions)
							}
						},
						new CloneMap<StudentTest>( p => p.User, excludeAllPrimitiveProperties : true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.Id ),
								new CloneMap<User>(p => p.UserName ),
								new CloneMap<User>(p => p.Name ),
								new CloneMap<User>(p => p.FatherLastName ),
								new CloneMap<User>(p => p.MotherLastName ),
								new CloneMap<User>(p => p.Status ),

								new CloneMap<User>( p => p.Answers)
								{
									SubProperties =
									{
										new CloneMap<Answer>( p => p.Question)
										{
											SubProperties =
											{
												new CloneMap<Question>( p => p.Test)
											}
										}
									}
								}
							}
						}
					}
				};
			}
		}

		public static CloneMap<StudentTest> StudentTestMap_2
		{
			get
			{
				return new CloneMap<StudentTest>()
				{
					SubProperties =
					{
						new CloneMap<StudentTest>( p => p.Test)
						{
							SubProperties =
							{
								new CloneMap<Test>( p => p.SubUnity)
								{
									SubProperties =
									{
										new CloneMap<SubUnity>( p => p.Unity)
										{
											SubProperties=
											{
												new CloneMap<Unity>( p => p.CourseSubject)
												{
													SubProperties =
													{
														new CloneMap<CourseSubject>( p => p.Subject),
														new CloneMap<CourseSubject>( p => p.Course)
													}
												}
											}
										}
									}
								}
							}
						},
						new CloneMap<StudentTest>( p => p.User, excludeAllPrimitiveProperties : true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.Id ),
								new CloneMap<User>(p => p.UserName ),
								new CloneMap<User>(p => p.Name ),
								new CloneMap<User>(p => p.FatherLastName ),
								new CloneMap<User>(p => p.MotherLastName ),
								new CloneMap<User>(p => p.Status )
							}
						}
					}
				};
			}
		}

        public static CloneMap<StudentTest> StudentTestMap_3
		{
			get
			{
				return new CloneMap<StudentTest>()
				{
					SubProperties =
					{
						new CloneMap<StudentTest>( p => p.Test)
						{
							SubProperties =
							{
								new CloneMap<Test>(p => p.Questions)
							}
						},
						new CloneMap<StudentTest>( p => p.User, excludeAllPrimitiveProperties : true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.Id ),
								new CloneMap<User>(p => p.UserName ),
								new CloneMap<User>(p => p.Name ),
								new CloneMap<User>(p => p.FatherLastName ),
								new CloneMap<User>(p => p.MotherLastName ),
								new CloneMap<User>(p => p.Status )
							}
						}
					}
				};
			}
		}

		public static CloneMap<StudentTest> StudentTestMap_4
		{
			get
			{
				return new CloneMap<StudentTest>()
				{
					SubProperties =
					{
						new CloneMap<StudentTest>( p => p.Test),
						new CloneMap<StudentTest>( p => p.User, excludeAllPrimitiveProperties : true)
						{
							SubProperties =
							{
								new CloneMap<User>(p => p.Id ),
								new CloneMap<User>(p => p.UserName ),
								new CloneMap<User>(p => p.Name ),
								new CloneMap<User>(p => p.FatherLastName ),
								new CloneMap<User>(p => p.MotherLastName ),
								new CloneMap<User>(p => p.Status )
							}
						}
					}
				};
			}
		}
	}
}