﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class SpecialQuestionCloneHelper
    {
        public static CloneMap<SpecialQuestion> SpecialQUestionMap_1
        {
            get
            {
                return new CloneMap<SpecialQuestion>()
                {
                    SubProperties =
                    {
                        new CloneMap<SpecialQuestion>( p => p.Id),
                        new CloneMap<SpecialQuestion>( p => p.Name)
                    }
                };
            }
        }
        public static CloneMap<SpecialQuestion> SpecialQUestionMap_2
        {
            get
            {
                return new CloneMap<SpecialQuestion>()
                {
                    SubProperties =
                    {
                        new CloneMap<SpecialQuestion>( p => p.Id),
                        new CloneMap<SpecialQuestion>( p => p.Name)
                    }
                };
            }
        }
    }
}