﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class UserCloneHelper
	{
		public static CloneMap<User> UserMap_1
		{
			get
			{
				return new CloneMap<User>(excludeAllPrimitiveProperties : true)
				{
					SubProperties =
					{
						new CloneMap<User>(p => p.Id ),
						new CloneMap<User>(p => p.UserName ),
						new CloneMap<User>(p => p.Name ),
						new CloneMap<User>(p => p.FatherLastName ),
						new CloneMap<User>(p => p.MotherLastName ),
						new CloneMap<User>(p => p.Address ),
						new CloneMap<User>(p => p.Status ),

						new CloneMap<User>(p => p.UserRoleEducationalEstablishments )
						{
							SubProperties =
							{
								new CloneMap<UserRoleEducationalEstablishment>(p => p.Role),
								new CloneMap<UserRoleEducationalEstablishment>(p => p.EducationalEstablishment)
								{
									SubProperties =
									{
										new CloneMap<EducationalEstablishment>(p => p.Commune)
									}
								}
							}
						},
						new CloneMap<User>(p => p.StudentCourses )
						{
							SubProperties =
							{
								new CloneMap<StudentCourse>(p => p.Course)
								{
									SubProperties =
									{
										new CloneMap<Course>(p => p.EducationalEstablishment)
									}
								}
							}
						},
						new CloneMap<User>(p => p.TeacherCourseSubjects )
						{
							SubProperties =
							{
								new CloneMap<TeacherCourseSubject>(p => p.CourseSubject)
								{
									SubProperties =
									{
										new CloneMap<CourseSubject>(p => p.Subject ),
										new CloneMap<CourseSubject>(p => p.Course )
										{
											SubProperties =
											{
												new CloneMap<Course>(p => p.EducationalEstablishment )
											}
										}
									}
								}
							}
						}
					}
				};
			}
		}

		public static CloneMap<User> UserMap_2
		{
			get
			{
				return new CloneMap<User>(excludeAllPrimitiveProperties: true)
				{
					SubProperties =
					{
						new CloneMap<User>(p => p.Id ),
						new CloneMap<User>(p => p.UserName ),
						new CloneMap<User>(p => p.Name ),
						new CloneMap<User>(p => p.FatherLastName ),
						new CloneMap<User>(p => p.MotherLastName ),
						new CloneMap<User>(p => p.Address ),
						new CloneMap<User>(p => p.Status ),
					}
				};
			}
		}

		public static CloneMap<User> UserMap_3
		{
			get
			{
				return new CloneMap<User>(excludeAllPrimitiveProperties : true)
				{
					SubProperties =
					{
						new CloneMap<User>(p => p.Id ),
						new CloneMap<User>(p => p.UserName ),
						new CloneMap<User>(p => p.Name ),
						new CloneMap<User>(p => p.FatherLastName ),
						new CloneMap<User>(p => p.MotherLastName ),
						new CloneMap<User>(p => p.Address ),
						new CloneMap<User>(p => p.Status ),

						new CloneMap<User>(p => p.Observations)
					}
				};
			}
		}


		public static CloneMap<User> UserMap_4
		{
			get
			{
				return new CloneMap<User>(excludeAllPrimitiveProperties: true)
				{
					SubProperties =
					{
						new CloneMap<User>(p => p.Id ),
						new CloneMap<User>(p => p.UserName ),
						new CloneMap<User>(p => p.Name ),
						new CloneMap<User>(p => p.FatherLastName ),
						new CloneMap<User>(p => p.MotherLastName ),
						new CloneMap<User>(p => p.Address ),
						new CloneMap<User>(p => p.ImagePath ),
						new CloneMap<User>(p => p.Status ),
					}
				};
			}
		}

		public static CloneMap<User> UserMap_5
		{
			get
			{
				return new CloneMap<User>(excludeAllPrimitiveProperties: true)
				{
					SubProperties =
					{
						new CloneMap<User>(p => p.Id ),
						new CloneMap<User>(p => p.UserName ),
						new CloneMap<User>(p => p.Name ),
						new CloneMap<User>(p => p.FatherLastName ),
						new CloneMap<User>(p => p.MotherLastName )						
					}
				};
			}
		}
	}
}