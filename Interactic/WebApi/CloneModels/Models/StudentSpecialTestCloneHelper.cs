﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
    public static class StudentSpecialTestCloneHelper
    {
        public static CloneMap<StudentSpecialTest> StudentTestMap_1
        {
            get
            {
                return new CloneMap<StudentSpecialTest>()
                {
                    SubProperties =
                    {
                        new CloneMap<StudentSpecialTest>( p => p.SpecialTest)
                        {
                            SubProperties =
                            {
                                new CloneMap<SpecialTest>(p => p.SpecialQuestions)
                            }
                        },
                        new CloneMap<StudentSpecialTest>( p => p.User, excludeAllPrimitiveProperties : true)
                        {
                            SubProperties =
                            {
                                new CloneMap<User>(p => p.Id ),
                                new CloneMap<User>(p => p.UserName ),
                                new CloneMap<User>(p => p.Name ),
                                new CloneMap<User>(p => p.FatherLastName ),
                                new CloneMap<User>(p => p.MotherLastName ),
                                new CloneMap<User>(p => p.Status ),

                                new CloneMap<User>( p => p.Answers)
                                {
                                    SubProperties =
                                    {
                                        new CloneMap<Answer>( p => p.Question)
                                        {
                                            SubProperties =
                                            {
                                                new CloneMap<Question>( p => p.Test)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                };
            }
        }

        public static CloneMap<StudentSpecialTest> StudentTestMap_2
        {
            get
            {
                return new CloneMap<StudentSpecialTest>()
                {
                    SubProperties =
                    {
                        new CloneMap<StudentSpecialTest>( p => p.SpecialTest)
                        {
                            SubProperties =
                            {
                                new CloneMap<Test>( p => p.SubUnity)
                                {
                                    SubProperties =
                                    {
                                        new CloneMap<SubUnity>( p => p.Unity)
                                        {
                                            SubProperties=
                                            {
                                                new CloneMap<Unity>( p => p.CourseSubject)
                                                {
                                                    SubProperties =
                                                    {
                                                        new CloneMap<CourseSubject>( p => p.Subject),
                                                        new CloneMap<CourseSubject>( p => p.Course)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        new CloneMap<StudentSpecialTest>( p => p.User, excludeAllPrimitiveProperties : true)
                        {
                            SubProperties =
                            {
                                new CloneMap<User>(p => p.Id ),
                                new CloneMap<User>(p => p.UserName ),
                                new CloneMap<User>(p => p.Name ),
                                new CloneMap<User>(p => p.FatherLastName ),
                                new CloneMap<User>(p => p.MotherLastName ),
                                new CloneMap<User>(p => p.Status )
                            }
                        }
                    }
                };
            }
        }

        public static CloneMap<StudentSpecialTest> StudentTestMap_3
        {
            get
            {
                return new CloneMap<StudentSpecialTest>()
                {
                    SubProperties =
                    {
                        new CloneMap<StudentSpecialTest>( p => p.SpecialTest)
                        {
                            SubProperties =
                            {
                                new CloneMap<SpecialTest>(p => p.SpecialQuestions)
                                {
                                    SubProperties =
                                    {
                                        new CloneMap<QuestionAlternativeItem>(p => p.QuestionAlternative)
                                        {
                                            SubProperties =
                                            {
                                                new CloneMap<QuestionAlternativeItem>(p => p.Text),
                                                new CloneMap<QuestionAlternativeItem>(p => p.Value),
                                                new CloneMap<QuestionAlternativeItem>(p => p.QuestionType)
                                            }
                                        }
                                    }
                                }
                            }
                            
                        },
                        new CloneMap<StudentSpecialTest>( p => p.User, excludeAllPrimitiveProperties : true)
                        {
                            SubProperties =
                            {
                                new CloneMap<User>(p => p.Id ),
                                new CloneMap<User>(p => p.UserName ),
                                new CloneMap<User>(p => p.Name ),
                                new CloneMap<User>(p => p.FatherLastName ),
                                new CloneMap<User>(p => p.MotherLastName ),
                                new CloneMap<User>(p => p.Status )
                            }
                        }
                    }
                };
            }
        }

        public static CloneMap<StudentSpecialTest> StudentTestMap_4
        {
            get
            {
                return new CloneMap<StudentSpecialTest>()
                {
                    SubProperties =
                    {
                        new CloneMap<StudentSpecialTest>( p => p.SpecialTest),
                        new CloneMap<StudentSpecialTest>( p => p.User, excludeAllPrimitiveProperties : true)
                        {
                            SubProperties =
                            {
                                new CloneMap<User>(p => p.Id ),
                                new CloneMap<User>(p => p.UserName ),
                                new CloneMap<User>(p => p.Name ),
                                new CloneMap<User>(p => p.FatherLastName ),
                                new CloneMap<User>(p => p.MotherLastName ),
                                new CloneMap<User>(p => p.Status )
                            }
                        }
                    }
                };
            }
        }
    }
}