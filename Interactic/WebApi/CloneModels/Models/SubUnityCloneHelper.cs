﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class SubUnityCloneHelper
	{
		public static CloneMap<SubUnity> SubUnityMap_1
		{
			get
			{
				return new CloneMap<SubUnity>()
				{
					SubProperties =
					{
						new CloneMap<SubUnity>( p => p.Unity)
						{
							SubProperties=
							{
								new CloneMap<Unity>( p => p.CourseSubject)
								{
									SubProperties=
									{
										new CloneMap<CourseSubject>( p => p.Course),
										new CloneMap<CourseSubject>( p => p.Subject)
									}
								}
							}
						}
					}
				};
			}
		}		
	}
}