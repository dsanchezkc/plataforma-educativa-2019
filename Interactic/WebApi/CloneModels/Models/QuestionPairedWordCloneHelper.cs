﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public static class QuestionPairedWordCloneHelper
	{
		public static CloneMap<QuestionPairedWord> QuestionPairedWordMap_1
		{
			get
			{
				return new CloneMap<QuestionPairedWord>()
				{
					SubProperties =
					{
						new CloneMap<QuestionPairedWord>( p => p.QuestionPairedWordItems)
					}
				};
			}
		}
	}
}