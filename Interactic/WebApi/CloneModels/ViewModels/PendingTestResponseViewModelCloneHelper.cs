﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViewModels;

namespace WebApi.CloneModels
{
	public static class PendingTestResponseViewModelCloneHelper
	{
		public static CloneMap<PendingTestResponseViewModel> PendingTestResponseViewModelMap_1
		{
			get
			{
				return new CloneMap<PendingTestResponseViewModel>()
				{
					SubProperties =
					{
						new CloneMap<PendingTestResponseViewModel>( p => p.StudentAnswer)
						{
							SubProperties =
							{
								new CloneMap<StudentAnswer>(p => p.StudentSpecialTest)
								{
									SubProperties =
									{
										new CloneMap<StudentSpecialTest>(p => p.User, excludeAllPrimitiveProperties : true)
										{
											SubProperties =
											{
												new CloneMap<User>(p => p.UserName),
												new CloneMap<User>(p => p.Name ),
												new CloneMap<User>(p => p.FatherLastName )
											}
										}										
									}
								}
							}
						},
						new CloneMap<PendingTestResponseViewModel>(p => p.QuestionWrittenAnswer)
						{
							SubProperties =
							{
								new CloneMap<QuestionWrittenAnswer>(p => p.SpecialTest )
							}
						},
						new CloneMap<PendingTestResponseViewModel>(p => p.CourseSubject)
						{
							SubProperties =
							{
								new CloneMap<CourseSubject>(p => p.Subject ),
								new CloneMap<CourseSubject>(p => p.Course )
							}
						},
						new CloneMap<PendingTestResponseViewModel>( p => p.SubUnity)
						{
							SubProperties =
							{
								new CloneMap<SubUnity>(p => p.Unity)								
							}
						}
					}
				};
			}
		}
	}
}