﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.CloneModels
{
	public interface ICloneMap
	{
		bool IsExcludeAllPrimitiveProperties();
		string GetPropertyName();
		ICloneMap GetCloneMap(string property);
	}
}