﻿using Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace WebApi.CloneModels
{
	public static class  HttpRequestMessageExtension
	{
		public static HttpResponseMessage CreateCloneResponse(this HttpRequestMessage httpRequestMessage, object items, ICloneMap iCloneMap)
		{
			var request = new HttpRequestMessage();
			request.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = new HttpConfiguration();
			return request.CreateResponse(HttpStatusCode.OK, new CloneResponse(items, iCloneMap));
		}
	}

	public class CloneResponse
	{
		public object data { get; private set; }
		public ICloneMap iCloneMap { get; private set; }
		public long count { get { return Count(); } }

		public CloneResponse(object data, ICloneMap iCloneMap)
		{
			this.data = data;
			this.iCloneMap = iCloneMap;
		}		

		public object Result(int resultPag, int resultLimit)
		{
			var _items = data as IEnumerable;
			
			if (_items == null)
				return CloneHelper.CloneObjectMap(data, iCloneMap);
			else
			{
				var method = typeof(CloneHelper).GetMethod("CloneListMapResponse", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
				MethodInfo generic = method.MakeGenericMethod(data.GetType().GenericTypeArguments[0]);
				return generic.Invoke(null, new object[] { data, iCloneMap, resultPag, resultLimit });
			}
		}

		private long Count()
		{
			var count = 0;
			var items = data as IEnumerable;

			if (items != null)
				foreach (var item in items)
					count++;
			else
				if (data != null)
					count = 1;

			return count;
		}
	}	
}