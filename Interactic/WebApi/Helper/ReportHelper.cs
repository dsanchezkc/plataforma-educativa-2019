﻿using GeneralHelper;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helper
{
    public class ReportHelper
    {
        public static User detectTeacher(string _teacher)
        {
            var db = new InteracticContext();
            db.Configuration.ProxyCreationEnabled = false;

            var teacherId = GlobalParameters.TeacherId;  // Global teacher id from actual user
            if (_teacher != null)
            {
                teacherId = Encryptor.Decrypt(_teacher);
            }
            var teacher = db.Users.SingleOrDefault(x => x.Id == teacherId);
            if (teacher == null) throw new HttpException(404, "Not Fount"); // Exception if couldn't find a teacher

            return teacher;
        }
    }
}