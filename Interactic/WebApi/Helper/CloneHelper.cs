﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;
using WebApi.CloneModels;

namespace Helper
{
	public static class CloneHelper
	{
		/// <summary>
		/// Sólo usar con la BD - Bd tiene un comportamiento distinto a los objetos normales
		/// </summary>
		/// 
		public static IEnumerable<T> CloneList<T>(IEnumerable<T> list, int depth = 1)
		{
			return (IEnumerable<T>)_CloneList(list, depth + 1);
		}

		private static IEnumerable<object> _CloneList(object list, int depth = 1)
		{
			if (depth <= 0) return null;

			Type argumentType = null;

			if (list.GetType().BaseType.GenericTypeArguments.Length == 0)
				argumentType = list.GetType().GenericTypeArguments[0];
			else
				argumentType = list.GetType().BaseType.GenericTypeArguments[0];

			var constructedListType = typeof(Collection<>).MakeGenericType(argumentType);
			var content = Activator.CreateInstance(constructedListType) as IList;

			foreach (var item in list as IEnumerable)
			{
				if ((depth - 1) >= 1)
					content.Add(CloneObject(item, depth - 1));
			}

			return (IEnumerable<object>)content;
		}

		public static object CloneObject(object item, int depth = 1)
		{
			if (depth <= 0) return null;

			var itemType = item.GetType();

			if (itemType.Module.ScopeName == "EntityProxyModule")
				itemType = item.GetType().BaseType;

			var newItem = Activator.CreateInstance(itemType);

			var properties = itemType.GetProperties();

			for (int i = 0; i < properties.Length; i++)
			{
				var property = properties[i];

				if (PrimitiveTypes.IsPrimitive(property.PropertyType))
					property.SetValue(newItem, property.GetValue(item));
				else
				{
					MethodInfo method = null;

					if (property.PropertyType.GetInterface("IEnumerable") != null)
					{
						method = typeof(CloneHelper).GetMethod("_CloneList", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
						try
						{
							property.SetValue(newItem, method.Invoke(null, new object[] { property.GetValue(item), depth }));
						}
						catch
						{

						}
					}
					else
					{
						method = typeof(CloneHelper).GetMethod("CloneObject");
						property.SetValue(newItem, method.Invoke(null, new object[] { property.GetValue(item), depth - 1 }));
					}
				}
			}

			return newItem;
		}

		public static T CloneObjectMap<T>(T item, ICloneMap map)
		{
			return (T)_CloneObjectMap(item, map);
		}

		private static object _CloneObjectMap(object item, ICloneMap map, bool onlyStatusTrue = true)
		{
			if (map == null) return null;

			var itemType = item.GetType();

			if (itemType.Module.ScopeName =="EntityProxyModule")
				itemType = item.GetType().BaseType;

			var newItem = Activator.CreateInstance(itemType);

			var properties = itemType.GetProperties();

			for (int i = 0; i < properties.Length; i++)
			{
				var property = properties[i];

				if (PrimitiveTypes.IsPrimitive(property.PropertyType))
				{
					if (map.IsExcludeAllPrimitiveProperties() && map.GetCloneMap(property.Name) == null)
						continue;

					property.SetValue(newItem, property.GetValue(item));
				}
				else
				{
					var cloneMap = map.GetCloneMap(property.Name);

					MethodInfo method = null;

					if (property.PropertyType.GetInterface("IEnumerable") != null)
					{
						method = typeof(CloneHelper).GetMethod("_CloneListMap", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
						try
						{
							property.SetValue(newItem, method.Invoke(null, new object[] { property.GetValue(item), cloneMap, onlyStatusTrue }));
						}
						catch
						{

						}
					}
					else
					{
						method = typeof(CloneHelper).GetMethod("_CloneObjectMap", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
						property.SetValue(newItem, method.Invoke(null, new object[] { property.GetValue(item), cloneMap, onlyStatusTrue }));
					}
				}
			}

			return newItem;
		}

		public static IEnumerable<T> CloneListMap<T>(IEnumerable<T> list, ICloneMap map)
		{
			return (IEnumerable<T>)_CloneListMap(list, map);
		}

		private static IEnumerable<T> CloneListMapResponse<T>(IEnumerable<T> list, ICloneMap map, int resultPag, int resultLimit)
		{
			return (IEnumerable<T>)_CloneListMap(list.Skip(resultPag * resultLimit).Take(resultLimit), map);
		}

		private static IEnumerable<object> _CloneListMap(object list, ICloneMap map, bool onlyStatusTrue = true)
		{
			Type argumentType = null;

			if (list == null) return null;

			if (list.GetType().BaseType.GenericTypeArguments.Length == 0)
				argumentType = list.GetType().GenericTypeArguments[0];
			else
				argumentType = list.GetType().BaseType.GenericTypeArguments[0];

			var constructedListType = typeof(Collection<>).MakeGenericType(argumentType);
			var content = Activator.CreateInstance(constructedListType) as IList;

			if (map != null)
				foreach (var item in list as IEnumerable)
					if (onlyStatusTrue)
					{
						if (GetStatus(item))
							content.Add(_CloneObjectMap(item, map));
					}
					else
						content.Add(_CloneObjectMap(item, map));

			return (IEnumerable<object>)content;
		}

		private static bool GetStatus(object item)
		{
			var itemType = item.GetType().BaseType;
			var property = itemType.GetProperties().FirstOrDefault(p => p.Name == "Status");

			if (property == null) return true;
			return (bool)property.GetValue(item);
		}
	}

	public static class PrimitiveTypes
	{
		private static readonly Type[] List;

		static PrimitiveTypes()
		{
			var types = new[]
						   {
							  typeof (Enum),
							  typeof (String),
							  typeof (Char),
							  typeof (Guid),

							  typeof (Boolean),
							  typeof (Byte),
							  typeof (Int16),
							  typeof (Int32),
							  typeof (Int64),
							  typeof (Single),
							  typeof (Double),
							  typeof (Decimal),

							  typeof (SByte),
							  typeof (UInt16),
							  typeof (UInt32),
							  typeof (UInt64),

							  typeof (DateTime),
							  typeof (DateTimeOffset),
							  typeof (TimeSpan),
							};


			var nullTypes = from t in types
							where t.IsValueType
							select typeof(Nullable<>).MakeGenericType(t);

			List = types.Concat(nullTypes).ToArray();
		}

		public static bool IsPrimitive(Type type)
		{
			if (List.Any(x => x.IsAssignableFrom(type)))
				return true;

			var nut = Nullable.GetUnderlyingType(type);
			return nut != null && nut.IsEnum;
		}
	}
}