﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace Helper
{
    public class GlobalParameters
    {        
        public static int EducationalEstablishmentId { get { return int.Parse(GetValueIdentity("interactic-idEducationalEstablishment")); } }

		public static int Year { get { return int.Parse(GetValueIdentity("interactic-year")); } }

        public static string UserId { get { return HttpContext.Current.User.Identity.GetUserId(); } }

        public static string Rol { get { return GetValueIdentity(ClaimTypes.Role); } }

        // Filtrar si el usuario es profesor setear teacherID
        public static string TeacherId = UserId;
        // Filtrar si el usuario es estudiante setear StudentId
        public static string StudentId = UserId;

        private static string GetValueIdentity(string type)
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            IEnumerable<Claim> claims = identity.Claims;

            var result = claims.FirstOrDefault(p => p.Type == type);

            return result.Value;
        }

		/**************** Configuración Petición *******************************/
		public static int ResultLimit
		{
			get
			{
				try
				{
					IEnumerable<string> headerValues = HttpContext.Current.Request.Headers.GetValues("Pagination-Result-Limit");
					var limit = headerValues?.FirstOrDefault();
					return limit != null? int.Parse(limit) : int.MaxValue;
				}
				catch (Exception)
				{
					return int.MaxValue;
				}			
			}
		}

		public static int ResultPag
		{
			get
			{
				try
				{
					IEnumerable<string> headerValues = HttpContext.Current.Request.Headers.GetValues("Pagination-Result-Pag");
					var pag = headerValues?.FirstOrDefault();

					return pag != null ? int.Parse(pag) : 0;
				}
				catch (Exception)
				{
						return 0;
				}
			}
		}		
	}
}