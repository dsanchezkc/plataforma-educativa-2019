﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace Helper
{
    public static class ResponseParameters
    {
		/// <summary>
		/// Cantidad máxima que se puede enviar
		/// </summary>
		public static long ResultLimit
		{
			set
			{
				HttpContext.Current.Response.Headers.Add("Pagination-Result-Limit", value.ToString());
			}
		}

		/// <summary>
		/// Cantidad de elementos que son enviados
		/// </summary>
		public static long ResultTotal
		{
			set
			{
				HttpContext.Current.Response.Headers.Add("Pagination-Result-Total", value.ToString());
			}
		}

		/// <summary>
		/// Indica el número de paginación (Información necesaria cuando se trabaja con Tabla)
		/// </summary>
		public static long ResultPag
		{
			set
			{
				HttpContext.Current.Response.Headers.Add("Pagination-Result-Pag", value.ToString());
			}
		}
	}
}