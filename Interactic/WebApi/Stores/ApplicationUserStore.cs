﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebApi.Stores
{
	public class ApplicationUserStore : UserStore<User, Role, string, IdentityUserLogin, UserRoleEducationalEstablishment, IdentityUserClaim>, IUserStore<User>
	{
        private InteracticContext context;

		public ApplicationUserStore(InteracticContext context)
		: base(context)
		{
			this.context = context;
		}



	}
}