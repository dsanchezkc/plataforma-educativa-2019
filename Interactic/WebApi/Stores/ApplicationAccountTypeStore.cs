﻿using Microsoft.AspNet.Identity.EntityFramework;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Stores
{
	public class ApplicationAccountTypeStore : RoleStore<Role, string, UserRoleEducationalEstablishment>
	{
		public ApplicationAccountTypeStore(InteracticContext context) : base(context)
		{
		}
	}
}