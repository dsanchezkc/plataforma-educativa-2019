﻿using Helper;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using WebApi.CloneModels;

namespace WebApi
{
    public class FilterConfig
    {
		public static void Register(HttpConfiguration config)
		{
			config.Filters.Add(new AuthorizeAttribute());
			config.Filters.Add(new ContentFilter());
		}

		public class ContentFilter : ActionFilterAttribute
		{
			public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
			{
				var objectContent = actionExecutedContext?.Response?.Content as ObjectContent;

				if (objectContent == null) return;
				
				var cloneResponse = objectContent.Value as CloneResponse;

				if (cloneResponse == null) return;

				HttpContext.Current.Response.Headers.Add("Pagination-Result-Limit", GlobalParameters.ResultLimit.ToString());
				HttpContext.Current.Response.Headers.Add("Pagination-Result-Total", cloneResponse.count.ToString());
				HttpContext.Current.Response.Headers.Add("Pagination-Result-Pag", GlobalParameters.ResultPag.ToString());

				var httpRequestMessage = new HttpRequestMessage();
				httpRequestMessage.Properties[System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey] = new HttpConfiguration();
				actionExecutedContext.Response = httpRequestMessage.CreateResponse(HttpStatusCode.OK, cloneResponse.Result(GlobalParameters.ResultPag, GlobalParameters.ResultLimit));
			}
		}
	}
}
