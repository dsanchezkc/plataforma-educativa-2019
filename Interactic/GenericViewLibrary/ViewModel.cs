﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Generic
{
	public abstract class ViewModel
	{
		public abstract string IdAssociated();

		public virtual object ContentAssociated()
		{
			return new object();
		}
	}
}