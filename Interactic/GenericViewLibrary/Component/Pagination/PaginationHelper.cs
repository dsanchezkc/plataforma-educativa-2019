﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Component.Pagination
{
	public static class PaginationHelper
	{
		public static Segment<T> GenerateSegment<T>(int pag, int size, int countResult, IEnumerable<T> list)
		{
			pag = pag < 0 ? 0 : pag;
			var count = countResult <= 0 ? list.Count() : countResult;
			var pages = (int)Math.Ceiling((float)count / (float)size);
			pag = pag >= pages ? pages - 1 : pag;

			return new Segment<T>()
			{
				newList = countResult <= 0 ? list.Skip(pag * size).Take(size) : list,
				pagination = new GenericPagination(pag, pages)
			};
		}
	}

	public class Segment<T>
	{
		public IEnumerable<T> newList;
		public GenericPagination pagination;
	}
}