﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Component.Pagination
{
	public class GenericPagination
	{
		public int pag { get; }
		public int pages { get; }

		public int startPag { get; }
		public int endPag { get; }

		public int backPage { get; }
		public int nextPage { get; }

		public GenericPagination(int pag, int pages)
		{
			this.pag = pag;
			this.pages = pages;

			var min = (pag - 2 < 0) ? 0 : pag - 2;
			backPage = (pag - 1 < 0) ? 0 : pag - 1;
			nextPage = (pag + 1 >= pages) ? pages - 1 : pag + 1;

			if (min + 5 >= pages)
			{
				min = pages - 5;
				min = min < 0 ? 0 : min;
			}

			var max = min + 4;
			max = max > pages ? pages - 1 : max;			

			startPag = min;
			endPag = max;
		}
	}

	public class Pagination
	{
		/// <summary>
		/// Página a mostrar
		/// </summary>
		public int pag { set; get; }
		/// <summary>
		/// Cantidad máxima de elementos a enviar
		/// </summary>
		public int itemsMax { set; get; }
		/// <summary>
		/// Cantidad de resultados obtenidos
		/// </summary>
		public int countResult { set; get; }

		public Pagination()
		{
			itemsMax = 5;
		}
	}
}