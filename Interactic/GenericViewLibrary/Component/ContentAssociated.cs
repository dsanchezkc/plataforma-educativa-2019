﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Generic
{
	public class ContentAssociated
	{
		public string id;
		public string contentValue;
		public List<GroupInformation> group;

		public ContentAssociated()
		{
			group = new List<GroupInformation>();
			contentValue = "";
		}
	}
}