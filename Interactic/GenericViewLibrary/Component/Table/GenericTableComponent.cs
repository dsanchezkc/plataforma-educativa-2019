﻿using Component.Pagination;
using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Component.Table
{
	public class GenericTableComponent
	{
		public object dataQuery;
		public GenericPagination pagination;
		public GenericTable genericTable;
		public int groupCode;
		public string queryProcess;
		
		public static GenericTableComponent Generate<T>(IEnumerable<T> list, string queryProcess, Component.Pagination.Pagination pagination, object dataQuery) where T : ViewModel
		{
			var _pagination = PaginationHelper.GenerateSegment<T>(pagination.pag, pagination.itemsMax, pagination.countResult, list);
			var table = ViewGeneric.GenerateTable<T>(_pagination.newList);

			return new GenericTableComponent() { genericTable = table, pagination = _pagination.pagination, queryProcess = queryProcess, dataQuery = dataQuery };
		}
	}
}