﻿using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Component.Table
{
	public class GenericTable
	{
		public List<string> headers;
		public List<int> columnWidths;
		public List<ContentAssociated> content;

		public GenericTable()
		{
			headers = new List<string>();
			content = new List<ContentAssociated>();
			columnWidths = new List<int>();
		}
	}
}