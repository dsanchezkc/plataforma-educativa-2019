﻿using Component.Button;
using Component.Pagination;
using Component.Select;
using Component.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Component.Panel
{
	public class GenericPanel
	{
		public string title;
		public string description;
		public string singleName;
		public GenericTableComponent genericTableComponent;
		public ManagerSelect managerSelect;
		public List<ButtonComponent> buttons { get; }
		public string typeModel { private set; get; }
		public string viewModelType { private set; get; }
		public bool displayTitle { set; get; } = true;

		private int _groupCode;
		public int groupCode { set { _groupCode = value; UpdateId(value); } get { return _groupCode; } }

		public GenericPanel(Type modelType, Type viewModelType,int groupCode = 1 )
		{
			typeModel = modelType.Name;
			this.viewModelType = viewModelType.Name;
			title = description = singleName = "";
			genericTableComponent = new GenericTableComponent() { groupCode = groupCode };
			buttons = new List<ButtonComponent>();

			this.groupCode = groupCode;
		}

		public GenericPanel AddPagination(GenericPagination pagination)
		{
			genericTableComponent.pagination = pagination;
			return this;
		}

		public GenericPanel AddGenericTableComponent(GenericTableComponent component)
		{
			if (component == null) return this;

			this.genericTableComponent = component;
			this.genericTableComponent.groupCode = groupCode;
			return this;
		}

		public GenericPanel AddManagerSelect(ManagerSelect manager)
		{
			this.managerSelect = manager;
			this.managerSelect.groupCode = groupCode;
			return this;
		}

		public GenericPanel AddButton(ButtonComponent button)
		{
			button.groupCode = groupCode;
			buttons.Add(button);
			return this;
		}

		public void UpdateId(int groupCode)
		{
			if (genericTableComponent != null)
				genericTableComponent.groupCode = groupCode;

			if(managerSelect!=null)
				managerSelect.groupCode = groupCode;

			for (int i = 0; i < buttons.Count; i++)
				buttons[i].groupCode = groupCode;
		}
	}	
}