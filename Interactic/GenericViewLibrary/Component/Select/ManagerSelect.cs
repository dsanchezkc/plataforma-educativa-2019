﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component.Select
{
	public class ManagerSelect
	{
		public bool showVertical;

		public int groupCode;
		public List<SelectComponent> components;
		public string url { get; set; }
		
		public ManagerSelect(string url = "")
		{
			this.url = url;
			components = new List<SelectComponent>();
		}

		public ManagerSelect AddSelect(SelectComponent component)
		{
			components.Add(component);
			return this;
		}

		public SelectComponent FindSelectComponent(SelectCode code)
		{
			return components.FirstOrDefault(p => p.code == code.ToString());
		}

		public ManagerSelect Clone()
		{
			var clone = new ManagerSelect() { groupCode = groupCode, url = url };
			clone.components = new List<SelectComponent>();

			if (components != null)
				foreach (var item in components)
					clone.components.Add(item.Clone());
			
			return clone;
		}
	}
}
