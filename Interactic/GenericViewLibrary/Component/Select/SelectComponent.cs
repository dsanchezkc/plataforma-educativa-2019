﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component.Select
{
	public class SelectComponent
	{
		public string name { set; get; }
		public string placeholder { set; get; }
		public string code { get; private set; }
		public string parentCode { get; private set; }
		public List<string> requiredCodes { get; private set; }
		public string url { get; private set; }
		public string typeModel { get; private set; }

		/// <summary>
		/// Cargar un valor por defecto.
		/// </summary>
		public string defaultValue { get; set; }
	
		public SelectComponent(Type typeModel, SelectCode code, SelectCode parentCode= SelectCode.Unknow, List<SelectCode> requiredCodes = null , string url = "", string name="", string placeholder="", string defaultValue ="")
		{
			this.url = url;
			this.code = Enum.GetName(typeof(SelectCode),code);
			this.requiredCodes = new List<string>();
			this.name = name;
			this.placeholder = placeholder;
			this.parentCode = Enum.GetName(typeof(SelectCode), parentCode);
			this.typeModel = typeModel.Name;
			this.defaultValue = defaultValue;

			if (requiredCodes != null)
				foreach (var item in requiredCodes)
					this.requiredCodes.Add(Enum.GetName(typeof(SelectCode), item));

			if (parentCode != SelectCode.Unknow)
				if (!this.requiredCodes.Contains(this.parentCode))
					this.requiredCodes.Add(this.parentCode);			
		}

		private SelectComponent() { }

		public SelectComponent Clone()
		{
			return new SelectComponent()
			{
				typeModel = typeModel,
				code = code,
				name = name,
				parentCode = parentCode,
				placeholder = placeholder,
				url = url,
				requiredCodes = requiredCodes
			};
		}
	}
}
