﻿namespace Component.Select
{
	public enum SelectCode
	{
		Unknow =0,

		Year = 1,

		Teacher=2,
		Student=3,
		Administrator=4,

		Course = 5,
		Subject = 6,
		Unity= 7,
		SubUnity=8,
		UploadFileStudent=16,

		Province = 9,
		Commune = 10,
		Region =11,

		Role =12,

		EducationalEstablishment = 13,
		EstablishmentPeriod = 14,

		User = 15,
	}
}
