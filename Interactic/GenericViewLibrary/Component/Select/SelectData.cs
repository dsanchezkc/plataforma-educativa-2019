﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component.Select
{
	public class SelectData
	{
		/// <summary>
		/// Texto que se muestra en pantalla
		/// </summary>
		public string text { set; get; }

		/// <summary>
		/// Valor de selección
		/// </summary>
		public string value { set; get; }
	}
}
