﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component.Button
{
	public class ButtonComponent
	{
		public string text;
		public string color;
		public string componentCode;
		public string icon;
		public int groupCode;
	
		public ButtonComponent(string componentCode)
		{
			this.componentCode = componentCode;
		}
	}
}
