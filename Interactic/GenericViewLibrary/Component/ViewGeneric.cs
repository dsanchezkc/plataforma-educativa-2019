﻿using Attributes;
using Component.Panel;
using Component.Table;
using Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Generic
{
	public static class ViewGeneric
	{
		public static GenericTable GenerateTable<T>(IEnumerable<T> objs) where T : ViewModel
		{
			var properties = AttributeHelper.GetProperties<T>(false);
			GenericTable genericTable = new GenericTable();

			foreach (var obj in objs)
			{
				var contentAssociated = new ContentAssociated() { id = obj.IdAssociated(), contentValue = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Serializer.Serialize(obj.ContentAssociated()))) };

				foreach (var property in properties)
				{
					var groupInformation = new GroupInformation();

					if (property.groupAttribute != null)
					{
						var result = contentAssociated.group.FirstOrDefault(p => p.code == property.groupAttribute.code);

						if (result != null)
							groupInformation = result;
						else
						{
							groupInformation.code = property.groupAttribute.code;
							groupInformation.title = groupInformation.title == "" ? property.groupAttribute.title : "";
							contentAssociated.group.Add(groupInformation);
						}
					}
					else
						contentAssociated.group.Add(groupInformation);

					if (property.buttonAttribute != null)
					{
						groupInformation.items.Add(property.buttonAttribute);
						groupInformation.itemsType = ItemType.Button;
					}
					else
					{
						var text = property.GetPropertyValue(obj);
						groupInformation.items.Add(text == null ? "" : text.ToString());
						groupInformation.itemsType = ItemType.String;
					}

					if (property.columnAttribute != null)
						groupInformation.width = property.columnAttribute.width;

					if (property.displayNameAttribute != null)
						groupInformation.title = groupInformation.title == "" ? property.displayNameAttribute.name : groupInformation.title;
					else
						groupInformation.title = groupInformation.title == "" ? property.property.Name : groupInformation.title;
				}
				genericTable.content.Add(contentAssociated);
			}

			var firstGroup = genericTable.content.FirstOrDefault();

			if (firstGroup != null)
			{
				foreach (var group in firstGroup.group)
				{
					genericTable.headers.Add(group.title);
					genericTable.columnWidths.Add(group.width);
				}
			}			

			return genericTable;
		}

		public static GenericPanel GeneratePanel<T>(Type typeModelBase, IEnumerable<T> objs = null) where T : ViewModel
		{
			if (objs == null)
				objs = new List<T>();

			var genericPanel = new GenericPanel(typeModelBase, typeof(T));
			var attributesClass = AttributeHelper.GetAttributesClass<T>();

			genericPanel.title = attributesClass.titleAttribute?.title;
			genericPanel.singleName = attributesClass.singleNameAttribute?.name;
			genericPanel.description = attributesClass.descriptionAttribute?.description;
			genericPanel.genericTableComponent.genericTable = GenerateTable<T>(objs);
			
			return genericPanel;
		}


	
	}

	public class GroupInformation
	{
		public List<object> items;
		public string itemsTypeName { get { return Enum.GetName(typeof(ItemType), itemsType); } }
		public ItemType itemsType;
		public int code = -1;
		public string title;
		public int width = -1;

		public GroupInformation()
		{
			title = "";
			items = new List<object>();
		}
	}

	public enum ItemType { String, Button }
}