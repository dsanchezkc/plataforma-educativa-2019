﻿using Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Reflection
{
	public class PropertyInformation
	{
		public IgnoreAttribute ignoreAttribute;
		public OrderAttribute orderAttribute;
		public DisplayNameAttribute displayNameAttribute;
		public ButtonAttribute buttonAttribute;
		public GroupAttribute groupAttribute;
		public PropertyInfo property;
		public ColumnAttribute columnAttribute;
	
		public object GetPropertyValue(object obj)
		{
			return property.GetValue(obj);
		}
	}
}