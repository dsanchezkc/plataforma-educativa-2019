﻿using Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Reflection
{
	public static class AttributeHelper
	{
		/// <summary>
		/// Te devuelve todas las propiedades de un Objecto
		/// </summary>
		/// <param name="type">Tipo de la clase</param>
		/// <param name="allProperties"> Si es falso, quita de la lista todas las propiedades que tengan el Attributo Ignore con valor TRUE </param>
		/// <returns></returns>
		public static List<PropertyInformation> GetProperties<T>(bool allProperties = true)
		{
			var properties = typeof(T).GetProperties();
			var list = new List<PropertyInformation>();

			for (int i = 0; i < properties.Length; i++)
			{
				var property = new PropertyInformation() { property = properties[i] };
				list.Add(property);

				foreach (var attribute in property.property.CustomAttributes)
				{
					if (attribute.AttributeType == typeof(IgnoreAttribute))
					{
						property.ignoreAttribute = new IgnoreAttribute(GetValue<IgnoreAttribute>(property.property, attribute).value);

						if (!allProperties && property.ignoreAttribute.value)
						{
							list.Remove(property);
							break;
						}
					}

					if (attribute.AttributeType == typeof(OrderAttribute))
						property.orderAttribute = new OrderAttribute(GetValue<OrderAttribute>(property.property, attribute).index);
					
					if (attribute.AttributeType == typeof(DisplayNameAttribute))
						property.displayNameAttribute = new DisplayNameAttribute(GetValue<DisplayNameAttribute>(property.property, attribute).name);

					if (attribute.AttributeType == typeof(ButtonAttribute))
						property.buttonAttribute = new ButtonAttribute(GetValue<ButtonAttribute>(property.property, attribute).componentCode, GetValue<ButtonAttribute>(property.property, attribute).color, GetValue<ButtonAttribute>(property.property, attribute).icon);
					
					if (attribute.AttributeType == typeof(GroupAttribute))
						property.groupAttribute = new GroupAttribute(GetValue<GroupAttribute>(property.property, attribute).code, GetValue<GroupAttribute>(property.property, attribute).title);

					if (attribute.AttributeType == typeof(ColumnAttribute))
						property.columnAttribute = new ColumnAttribute(GetValue<ColumnAttribute>(property.property, attribute).width);
				}
			}

			list.Sort((obj_1, obj_2) =>
			{
				if (obj_1 == null && obj_2 == null) return 0;
				if (obj_1 == null || obj_1.orderAttribute == null) return 1;
				if (obj_2 == null || obj_2.orderAttribute == null) return -1;

				return obj_1.orderAttribute.index.CompareTo(obj_2.orderAttribute.index);
			});

			return list;
		}

		public static AttributesClass GetAttributesClass<T>()
		{
			var attributes = System.Attribute.GetCustomAttributes(typeof(T));
			var attributeClass = new AttributesClass(); 
			
			foreach (var attribute in attributes)
			{
				if (attribute.GetType() == typeof(TitleAttribute))
					attributeClass.titleAttribute = (TitleAttribute)attribute;

				if (attribute.GetType() == typeof(DescriptionAttribute))
					attributeClass.descriptionAttribute = (DescriptionAttribute)attribute;

				if (attribute.GetType() == typeof(SingleNameAttribute))
					attributeClass.singleNameAttribute = (SingleNameAttribute)attribute;
			}

			return attributeClass;
		}

		public static ReflectionData GetReflectionData<T>(bool allProperties = true)
		{
			return new ReflectionData() { attributeClass = GetAttributesClass<T>(), properties = GetProperties<T>(allProperties) };
		}

		public static T GetValue<T>(PropertyInfo property, CustomAttributeData attribute) where T : System.Attribute
		{
			return (T)property.GetCustomAttribute(attribute.AttributeType, false);
		}
	}
}