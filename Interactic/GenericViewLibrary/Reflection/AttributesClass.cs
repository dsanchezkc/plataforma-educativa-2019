﻿using Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reflection
{
	public class AttributesClass
	{
		public SingleNameAttribute singleNameAttribute;
		public TitleAttribute titleAttribute;
		public DescriptionAttribute descriptionAttribute;
	}
}