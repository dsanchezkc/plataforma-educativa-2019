﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reflection
{
	public class ReflectionData
	{
		public AttributesClass attributeClass;
		public List<PropertyInformation> properties;

		public ReflectionData()
		{
			attributeClass = new AttributesClass();
			properties = new List<PropertyInformation>();
		}
	}
}