﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Attributes
{
	public class GroupAttribute : Attribute
	{
		[ScriptIgnore]
		public override object TypeId { get; }

		public int code;
		public string title;

		public GroupAttribute(int code,string title="")
		{
			this.code = code;
			this.title = title;
		}

	}
}
