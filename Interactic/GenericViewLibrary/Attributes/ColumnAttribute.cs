﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Attributes
{
	public class ColumnAttribute : Attribute
	{
		[ScriptIgnore]
		public override object TypeId { get; }

		public int width;

		public ColumnAttribute(int width)
		{
			this.width = width;
		}
	}
}
