﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Attributes
{
	public class SingleNameAttribute : System.Attribute
	{
		public string name;

		public SingleNameAttribute(string name)
		{
			this.name = name;
		}
	}
}