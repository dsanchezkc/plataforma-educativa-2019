﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Attributes
{
	public class IgnoreAttribute : System.Attribute
	{
		public bool value;

		public IgnoreAttribute(bool value = true)
		{
			this.value = value;
		}
	}
}