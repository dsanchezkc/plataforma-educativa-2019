﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Attributes
{
	public class OrderAttribute : System.Attribute
	{
		public int index;

		public OrderAttribute(int index)
		{
			this.index = index;
		}
	}
}