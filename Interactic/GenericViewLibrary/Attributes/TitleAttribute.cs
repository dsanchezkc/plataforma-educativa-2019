﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Attributes
{
	public class TitleAttribute : System.Attribute
	{
		public string title;

		public TitleAttribute(string title)
		{
			this.title = title;
		}
	}
}