﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Attributes
{
	public class DisplayNameAttribute : System.Attribute
	{
		public string name;

		public DisplayNameAttribute(string name)
		{
			this.name = name;
		}
	}
}