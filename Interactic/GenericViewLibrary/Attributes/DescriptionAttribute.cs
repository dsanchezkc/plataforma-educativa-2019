﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Attributes
{
	public class DescriptionAttribute : System.Attribute
	{
		public string description;

		public DescriptionAttribute(string description)
		{
			this.description = description;
		}
	}
}