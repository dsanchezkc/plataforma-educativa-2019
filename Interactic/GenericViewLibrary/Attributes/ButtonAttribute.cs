﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Attributes
{
	public class ButtonAttribute : Attribute
	{
		[ScriptIgnore]
		public override object TypeId { get; }

		public string color;
		public string icon;
		public string componentCode;

		public ButtonAttribute(string componentCode, string color, string icon)
		{
			this.componentCode = componentCode;
			this.color = color;
			this.icon = icon;
		}
	}
}
