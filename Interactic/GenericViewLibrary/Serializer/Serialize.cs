﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Generic
{
	public static class Serializer
	{
		private static readonly JavaScriptSerializer serializer;

		static Serializer()
		{
			serializer = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue };
		}

		public static string Serialize(object t)
		{
			return serializer.Serialize(t);
		}

		public static T Deseralize<T>(string stringObject)
		{
			return serializer.Deserialize<T>(stringObject);
		}
	}
}
