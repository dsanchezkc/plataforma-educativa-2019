﻿using Attributes;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
    public class EventViewModel : Generic.ViewModel
    {
        private Event _event;

        [Ignore]
        public int id { get { return _event.Id; } }

        [Ignore]
        public string title { get { return _event.Title; } }

        [Ignore]
        public string description { get { return _event.Description; } }

        [Ignore]
        public string color { get { return _event.Color; } }

        [Ignore]
        public DateTime start { get { return _event.Start; } }

        [Ignore]
        public DateTime end { get { return _event.End; } }

        [Ignore]
        public bool allDay { get { return _event.AllDay; } }

        public EventViewModel(Event _event)
		{
			this._event = _event;
		}

		public override string IdAssociated()
		{
			return id.ToString();
		}
	}
}