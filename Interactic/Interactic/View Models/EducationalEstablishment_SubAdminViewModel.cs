﻿using Attributes;
using Interactic;
using Model;
using SecurityGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Establecimientos"), Description("Gestión de establecimientos educacionales"), SingleName("establecimiento")]
	public class EducationalEstablishment_SubAdminViewModel : Generic.ViewModel
	{
		private EducationalEstablishment educationalEstablishment;

		[DisplayName("Id"), Order(0)]
		public int Id { get { return educationalEstablishment.Id; } }

		[DisplayName("Nombre"), Order(1)]
		public string Name { get { return educationalEstablishment.Name; } }

		[DisplayName("Dirección"), Order(2)]
		public string Address { get { return educationalEstablishment.Address; } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(3)]
		public string EditButton { get; }

		[Ignore]
		public string NameImage { get { return educationalEstablishment.NameImage; } }

		[Ignore]
		public string PathImage { get { return educationalEstablishment.PathImage; } }		

		[Ignore]
		public Commune Commune { get { return educationalEstablishment.Commune; } }

		public EducationalEstablishment_SubAdminViewModel(EducationalEstablishment educationalEstablishment)
		{
			this.educationalEstablishment = educationalEstablishment;
		}

		public override string IdAssociated()
		{
			return educationalEstablishment.Id.ToString();
		}
	}
}