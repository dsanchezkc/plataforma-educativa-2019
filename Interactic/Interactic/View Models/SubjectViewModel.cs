﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Asignaturas"), Description("Gestionador de asignaturas"), SingleName("asignatura")]
	public class SubjectViewModel : Generic.ViewModel
	{
		private Subject subject;

		[Ignore]
		public int Id { get { return subject.Id; } }

		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return subject.Name; } }

		[DisplayName("Estado"), Order(1)]
		public bool Status { get { return subject.Status; } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(3)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(4)]
		public string DeleteButton { get; }

		[Ignore]
		public virtual List<CourseViewModel> Courses { get; }

		public SubjectViewModel(Subject subject)
		{
			this.subject = subject;
			
			Courses = new List<CourseViewModel>();
			if (subject.CourseSubjects != null)
			{
				var courses = subject.CourseSubjects.Select(p => p.Course);
				if (courses != null)
					foreach (var course in courses)
						Courses.Add(new CourseViewModel(course));
			}
		}

		public override string IdAssociated()
		{
			return subject.Id.ToString();
		}
	}
}