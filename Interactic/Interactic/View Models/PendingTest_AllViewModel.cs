﻿using Attributes;
using Extensions;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViewModels;

namespace ViewModel
{
	[Title(""), Description("Gestionador de revisiones"), SingleName("revisión")]
	public class PendingTest_AllViewModel : Generic.ViewModel
	{
		private PendingTestResponseViewModel PendingTestVM;
        
		[DisplayName("R.U.N"), Order(0)]
		public string RUN { get { return PendingTestVM.StudentAnswer.StudentSpecialTest.User.UserName.WithFormatRUN(); } }

		[DisplayName("Nombre"), Order(1)]
		public string Name { get { return PendingTestVM.StudentAnswer.StudentSpecialTest.User.Name + " " + PendingTestVM.StudentAnswer.StudentSpecialTest.User.FatherLastName; } }
		[DisplayName("Curso"), Order(2)]
		public string Course_Name { get { return PendingTestVM.CourseSubject.Course.Name; } }

		[DisplayName("Asignatura"), Order(3)]
		public string Subject_Name { get { return PendingTestVM.CourseSubject.Subject.Name; } }

        [DisplayName("Test"), Order(4)]
		public string Test_Title { get { return PendingTestVM.QuestionWrittenAnswer.SpecialTest.Title; } }

		[DisplayName("Preg. N."), Order(5)]
		public int Question_Order { get { return PendingTestVM.QuestionWrittenAnswer.Order + 1; } }


		[Button("evaluateButton", StyleConfig.ButtonStyle_Accept, "fa-edit"), Column(width: 75), DisplayName("Evaluar"), Order(6)]
		public string EditButton { get; }

		public PendingTest_AllViewModel(PendingTestResponseViewModel pendingTestVM)
		{
			this.PendingTestVM = pendingTestVM;
		}

		//ID a la pregunta específica no al Test.
		public override string IdAssociated()
		{
			return PendingTestVM.QuestionWrittenAnswer.Id.ToString();
		}

		public override object ContentAssociated()
		{
			return PendingTestVM;
		}
	}
}