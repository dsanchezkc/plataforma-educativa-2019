﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Cursos"), Description("Gestionador de cursos"), SingleName("curso")]
	public class CourseViewModel : Generic.ViewModel
	{
		private Course course;
		
		[Ignore]
		public int Id { get { return course.Id; } }

		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return course.Name; } }

		[Button("configButton", StyleConfig.ButtonStyle_Config, "assignment"), DisplayName("Config. Asignaturas"), Order(3)]
		public string ConfigButton { get; }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(4)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(5)]
		public string DeleteButton { get; }

		public CourseViewModel(Course course)
		{
			this.course = course;
		}

		public override string IdAssociated()
		{
			return Id.ToString();
		}
	}
}