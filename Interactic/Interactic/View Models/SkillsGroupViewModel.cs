﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Grupo de habilidades"), Description("Gestionador de habilidades"), SingleName("grupo de habilidad")]
	public class SkillsGroupViewModel : Generic.ViewModel
	{
		private SkillsGroup skillsGroup;
		
		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return skillsGroup.Name; } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(4)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(5)]
		public string DeleteButton { get; }

		public SkillsGroupViewModel(SkillsGroup skillsGroup)
		{
			this.skillsGroup = skillsGroup;
		}

		public override string IdAssociated()
		{
			return skillsGroup.Id.ToString();
		}
	}
}