﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Tests"), Description("Gestionador de tests"), SingleName("Test")]
	public class TestViewModel : Generic.ViewModel
	{
		private Test test;

		[Ignore]
		public int Id { get { return test.Id; } }

		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return test.Title; } }

		[Button("viewButton", StyleConfig.ButtonStyle_View, "picture_as_pdf"), GroupAttribute(4, "Pdf Test/Corrección"), Order(3)]
		public string ViewButton { get; }

		[Button("viewCorrectionButton", StyleConfig.ButtonStyle_View, "assignment_turned_in"), GroupAttribute(4, "Pdf Test/Corrección"), Order(4)]
		public string viewCorrectionButton { get; }

		[Button("timerButton", StyleConfig.ButtonStyle_Config, "timer"), GroupAttribute(5, "Acciones"), Order(5)]
		public string TimerButton { get; }		

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(6)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(7)]
		public string DeleteButton { get; }

		public TestViewModel(Test test)
		{
			this.test = test;
		}

		public override string IdAssociated()
		{
			return test.Id.ToString();
		}
	}
}