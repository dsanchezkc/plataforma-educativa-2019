﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Temas"), Description("Gestionador de temas"), SingleName("Tema")]
	public class SubUnityViewModel : Generic.ViewModel
	{
		private SubUnity subUnity;

		[Ignore]
		public int Id { get { return subUnity.Id; } }

		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return subUnity.Name; } }

		[DisplayName("Estado"), Order(1)]
		public bool Status { get { return subUnity.Status; } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(3)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(4)]
		public string DeleteButton { get; }

		[Ignore]
		public Unity Unity { get; }

		public SubUnityViewModel(SubUnity subUnity)
		{
			this.subUnity = subUnity;

			Unity = new Unity()
			{
				Id = subUnity.Unity.Id,
				CourseSubject = new CourseSubject()
				{
					Course_Id = subUnity.Unity.CourseSubject.Course_Id,
					Subject_Id = subUnity.Unity.CourseSubject.Subject_Id
				}
			};
		}

		public override string IdAssociated()
		{
			return subUnity.Id.ToString();
		}
	}
}