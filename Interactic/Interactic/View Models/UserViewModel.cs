﻿using Attributes;
using Interactic;
using Extensions;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Usuarios"), Description("Gestionador de usuarios"), SingleName("Usuario")]
	public class UserViewModel : Generic.ViewModel
	{
		private User user;

		[DisplayName("R.U.N"), Order(0)]
		public string RUN { get { return user.UserName.WithFormatRUN(); } }

		[DisplayName("Nombre"), Order(1)]
		public string Name { get { return user.Name; } }

		[DisplayName("Ap. Paterno"), Order(2)]
		public string FatherLastName { get { return user.FatherLastName; } }

		[DisplayName("Ap. Materno"), Order(3)]
		public string MotherLastName { get { return user.MotherLastName; } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(4)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(5)]
		public string DeleteButton { get; }

		public UserViewModel(User user)
		{
			this.user = user;
		}

		public override string IdAssociated()
		{
			return user.Id.ToString();
		}
	}
}