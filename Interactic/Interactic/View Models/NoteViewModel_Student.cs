﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Apuntes"), Description("Mis puntes"), SingleName("Apunte")]
	public class NoteViewModel_Student : Generic.ViewModel
	{
		private Note note;

		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return note.Name; } }

        [DisplayName("Descripción"), Order(1)]
        public string Description { get { return note.Description; } }

        [Button("videoButton", StyleConfig.ButtonStyle_View, "ondemand_video"), GroupAttribute(5, "Acciones"), Order(1)]
        public string VideoButton { get; }

        [Button("downloadButton", StyleConfig.ButtonStyle_View, "file_download"), GroupAttribute(5, "Acciones"), Order(1)]
		public string DownloadButton { get; }

		public NoteViewModel_Student(Note note)
		{
			this.note = note;
		}

		public override string IdAssociated()
		{
			return note.Id.ToString();
		}
	}
}