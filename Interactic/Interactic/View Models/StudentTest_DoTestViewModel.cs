﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Tests"), Description("Tests por realizar"), SingleName("Test")]
	public class StudentTest_DoTestViewModel : Generic.ViewModel
	{
		private StudentTest studentTest;

		[DisplayName("Test"), Order(0)]
		public string Name { get { return studentTest.Test.Title; } }

		[DisplayName("Curso"), Order(1)]
		public string Course { get { return studentTest.Test.SubUnity.Unity.CourseSubject.Course.Name; } }

		[DisplayName("Asignatura"), Order(2)]
		public string Subject { get { return studentTest.Test.SubUnity.Unity.CourseSubject.Subject.Name; } }

		[Button("doTestButton", StyleConfig.ButtonStyle_Do, "assignment"), DisplayName("Realizar"), Order(3)]
		public string doTestButton { get; }

		public StudentTest_DoTestViewModel(StudentTest studentTest)
		{
			this.studentTest = studentTest;
		}

		public override string IdAssociated()
		{
			return studentTest.Test.Id.ToString();
		}
	}
}