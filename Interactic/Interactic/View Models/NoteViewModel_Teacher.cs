﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Apuntes"), Description("Gestionador de apuntes"), SingleName("Apunte")]
	public class NoteViewModel_Teacher : Generic.ViewModel
	{
		private Note note;

		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return note.Name; } }


        [DisplayName("Descripción"), Order(1)]
        public string Description { get { return note.Description; } }

        [Button("downloadButton", StyleConfig.ButtonStyle_View, "file_download"), GroupAttribute(5, "Acciones"), Order(1)]
		public string DownloadButton { get; }

        [Button("videoButton", StyleConfig.ButtonStyle_View, "ondemand_video"), GroupAttribute(5, "Acciones"), Order(1)]
        public string VideoButton { get; }

        [Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(2)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(3)]
		public string DeleteButton { get; }

		public NoteViewModel_Teacher(Note note)
		{
			this.note = note;
		}

		public override string IdAssociated()
		{
			return note.Id.ToString();
		}
	}
}