﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Tests"), Description("Historial de tests"), SingleName("Test")]
	public class StudentTest_HistorialViewModel : Generic.ViewModel
	{
		private StudentTest studentTest;

		[DisplayName("Fecha"), Order(0)]
		public string _Date
		{
			get
			{
				return studentTest.CreatedAt.ToString("dd/MM/yyyy");
			}
		}

		[DisplayName("Nombre"), Order(1)]
		public string Name { get { return studentTest.Test.Title; } }

		[DisplayName("Rendimiento"), Order(2)]
		public string Performance
		{
			get
			{
				var count = 0;
				foreach (var question in studentTest.Test.Questions)
				{
					var _question = studentTest.User.Answers.FirstOrDefault(p => p.Question_Id == question.Id);

					if (_question != null && _question.Choice == question.Choice)
						count++;
				}

				return studentTest.User.Answers.Count == 0 ? "No realizado" : Math.Round(((float)count / (float)studentTest.Test.Questions.Count) * 100) + "%";
			}
		}

		[Button("viewCorrection", StyleConfig.ButtonStyle_View, "assignment"), DisplayName("Correción"), Order(3)]
		public string pdfOptionalButton { get; }

		[Button("viewResult", StyleConfig.ButtonStyle_View, "pie_chart"), DisplayName("Resumen"), Order(4)]
		public string resumeButton { get; }

		public StudentTest_HistorialViewModel(StudentTest studentTest)
		{
			this.studentTest = studentTest;
		}

		public override string IdAssociated()
		{
			return studentTest.Test.Id.ToString();
		}
	}
}