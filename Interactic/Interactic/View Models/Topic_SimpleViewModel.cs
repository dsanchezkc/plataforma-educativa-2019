﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ViewModels;

namespace ViewModel
{
	[Title("Temas"), Description(""), SingleName("Tema")]
	public class Topic_SimpleViewModel : Generic.ViewModel
	{
		private TopicResponseViewModel topicRVM;

		[DisplayName("Título"), Order(0)]
		public string Title { get { return topicRVM.Topic.Title; } }

		[DisplayName("Autor"), Order(1)]
		public string Author { get { return topicRVM.Topic.Author.Name + " " + topicRVM.Topic.Author.FatherLastName; } }

		[DisplayName("Respuestas"), Order(3)]
		public int Publications { get { return topicRVM.Answers; } }

		[DisplayName("Última publicación"), Order(4)]
		public string LastPublication { get { return topicRVM.Answers != 0 ? topicRVM.LastUpdate.ToString("hh:mm tt dd/MM/yy", System.Globalization.CultureInfo.InvariantCulture) : "-"; } }

		[Button("topicButton", StyleConfig.ButtonStyle_Accept, "icon-paper-plane"), Column(width : 50), GroupAttribute(5, "Ir"), Order(5)]
		public string TopicButton { get; }

		public Topic_SimpleViewModel(TopicResponseViewModel topicRVM)
		{
			this.topicRVM = topicRVM;
		}

		public override string IdAssociated()
		{
			return topicRVM.Topic.Id.ToString();
		}
	}
}