﻿using Attributes;
using Interactic;
using Extensions;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Anotaciones"), Description("Gestionador de anotaciones"), SingleName("Anotación")]
	public class ObservationViewModel : Generic.ViewModel
	{
		private Observation observation;

		[DisplayName("Fecha"), Order(0)]
		public string CreateAt { get { return observation.CreatedAt.ToString("dd/MM/yyyy"); } }

		[DisplayName("Anotación"), Order(1)]
		public string Observation { get { return observation.Message.Replace("\n","<br>"); } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(3)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(4)]
		public string DeleteButton { get; }

		public ObservationViewModel(Observation observation)
		{
			this.observation = observation;
		}

		public override string IdAssociated()
		{
			return observation.Id.ToString();
		}
	}
}