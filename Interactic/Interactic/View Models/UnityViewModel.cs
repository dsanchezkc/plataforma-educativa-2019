﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Ejes Temáticos"), Description("Gestionador de ejes temáticos"), SingleName("Eje Temático")]
	public class UnityViewModel : Generic.ViewModel
	{
		private Unity unity;

		[Ignore]
		public int Id { get { return unity.Id; } }

		[DisplayName("Nombre"), Order(0)]
		public string Name { get { return unity.Name; } }

		[DisplayName("Estado"), Order(1)]
		public bool Status { get { return unity.Status; } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(3)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(4)]
		public string DeleteButton { get; }

		[Ignore]
		public CourseSubject CourseSubject { get; }

		public UnityViewModel(Unity unity)
		{
			this.unity = unity;
			CourseSubject = new CourseSubject()
			{
				Course_Id = unity.CourseSubject.Course_Id,
				Subject_Id = unity.CourseSubject.Subject_Id
			};
		}

		public override string IdAssociated()
		{
			return unity.Id.ToString();
		}
	}
}