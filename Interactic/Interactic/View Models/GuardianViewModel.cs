﻿using Attributes;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel
{
	[Title("Apoderados"), Description("Panel apoderados"), SingleName("apoderado")]
	public class GuardianViewModel : Generic.ViewModel
	{
		private User guardian;
		
		[DisplayName("Apoderado"), Order(0)]
		public string Name { get { return guardian.Name + " " + guardian.FatherLastName; } }

		[Button("editButton", StyleConfig.ButtonStyle_Edit, "create"), GroupAttribute(5, "Acciones"), Order(4)]
		public string EditButton { get; }

		[Button("deleteButton", StyleConfig.ButtonStyle_Delete, "remove"), GroupAttribute(5), Order(5)]
		public string DeleteButton { get; }

		public GuardianViewModel(User guardian)
		{
			this.guardian = guardian;
		}

		public override string IdAssociated()
		{
			return guardian.Id.ToString();
		}
	}
}