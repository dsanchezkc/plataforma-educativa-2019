﻿using Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SecurityGuard
{
	public static class ConnectionHelper
	{
		public const string URL_LOCAL = "http://localhost:50785/";
		public const string URL_PUBLIC = "http://191.96.42.216:50785/";

		public static string PATH_LOGIN_TOKEN
		{
			get
			{
				return HttpContext.Current.Request.IsLocal ? URL_LOCAL : URL_PUBLIC;
			}
		}

		public static string UrlReport { get { return PATH_LOGIN_TOKEN + "report/"; } }
		public static string URL_WEBAPI { get { return PATH_LOGIN_TOKEN + "Api/"; } }

		public const string URL_WEBAPI_LOCAL = URL_LOCAL + "Api/"; 
	}
}