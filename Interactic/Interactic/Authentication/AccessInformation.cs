﻿using Model;
using System;

namespace SecurityGuard
{
	public class AccessInformation
	{
        public string Id { private set; get; }
        public User user { private set; get; }
		public int year { private set; get; }

		public EducationalEstablishment educationalEstablishment { private set; get; }

		public string Role { private set; get; }
		public string Token { private set; get; }

		/// <summary>
		/// Es un código que se da para distinguir de una sesión y otra.
		/// </summary>
		public string SessionCode { private set; get; }

		public AccessInformation(User user, string Role, EducationalEstablishment educationalEstablishment, int year, string Token)
		{
            this.Id= user.Id;
            this.user = user;
			this.Role = Role;
			this.educationalEstablishment = educationalEstablishment;
			this.year = year;
			this.Token = Token;
			this.SessionCode = Guid.NewGuid().ToString();
		}		
	}
}