﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Web.SessionState;
using System.Data.Entity;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Generic;
using Communication;

namespace SecurityGuard
{
	public static class Authentication
	{
		public static bool isAuthenticated { get { return currentAccess != null; } }

		public static AccessInformation currentAccess
		{
			get
			{
				if (HttpContext.Current.Session == null) return null;

				var accessInformation = HttpContext.Current.Session["Access"] ?? null;
				return accessInformation == null ? null : (AccessInformation)accessInformation;
			}
		}

		public static async Task<bool> Authenticate(string userName, string password, string role = "", string idEducationalEstablishment="", int year= 0)
		{
			Dictionary<string, string> result = null;

			if (userName.Trim() == "" || password.Trim() == "") return false;

			if (year <= 0)
				year = DateTime.Now.Year;

			if (isAuthenticated)
				HttpContext.Current.Session["Access"] = null;

			using (var client = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true }))
			{
				client.BaseAddress = new Uri(ConnectionHelper.URL_LOCAL);
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				HttpResponseMessage response = await client.PostAsync("api/token", new StringContent("grant_type=password&username=" + userName + "&password=" + password + "&role=" + role + "&idEducationalEstablishment=" + idEducationalEstablishment + "&year=" + year + "", Encoding.UTF8));

				if (response.IsSuccessStatusCode)
				{
					var data = await response.Content.ReadAsStringAsync();
					result = Serializer.Deseralize<Dictionary<string, string>>(data);
				}
				else
					return false;
			}

			var user = new User()
			{
				UserName = result["userName"],
				Name = result["nameUser"],
				PasswordHash = password,
				ImagePath = result["pathImageUser"]
			};

			var educationalEstablishment = new EducationalEstablishment()
			{
				Id = int.Parse(result["idEducationalEstablishment"]),
				Name = result["nameEducationalEstablishment"],
				PathImage = result["pathImageEducationalEstablishment"]
			};

			var accessInformation = new AccessInformation(user, result["role"], educationalEstablishment, int.Parse(result["year"]), result["access_token"]);
			HttpContext.Current.Session.Timeout = 270;
			HttpContext.Current.Session["Access"] = accessInformation;

			HttpCookie aCookie = new HttpCookie("Code");
			aCookie.Values["SessionCode"] = accessInformation.SessionCode;
			HttpContext.Current.Response.Cookies.Add(aCookie);

			return true;
		}

		public static async Task<bool> ChangeAuthenticate(int educationalEstablishment_id, string role, int year)
		{
			var password = currentAccess.user.PasswordHash;
			var userName = currentAccess.user.UserName;

			var _logout = await Logout();

			if (!_logout) return false; 

			Dictionary<string, string> result = null;

			if (year <= 0)
				year = DateTime.Now.Year;			
			
			using (var client = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true }))
			{
				client.BaseAddress = new Uri(ConnectionHelper.URL_LOCAL);
				client.DefaultRequestHeaders.Accept.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

				HttpResponseMessage response = await client.PostAsync("api/token", new StringContent("grant_type=password&username=" + userName + "&password=" + password + "&role=" + role + "&idEducationalEstablishment=" + educationalEstablishment_id + "&year=" + year + "", Encoding.UTF8));

				if (response.IsSuccessStatusCode)
				{
					var data = await response.Content.ReadAsStringAsync();
					result = Serializer.Deseralize<Dictionary<string, string>>(data);
				}
				else
					return false;				
			}

			var user = new User()
			{
				UserName = result["userName"],
				Name = result["nameUser"],
				PasswordHash = password,
				ImagePath = result["pathImageUser"]
			};

			var educationalEstablishment = new EducationalEstablishment()
			{
				Id = int.Parse(result["idEducationalEstablishment"]),
				Name = result["nameEducationalEstablishment"],
				PathImage = result["pathImageEducationalEstablishment"]
			};

			var accessInformation = new AccessInformation(user, result["role"], educationalEstablishment, int.Parse(result["year"]), result["access_token"]);
			HttpContext.Current.Session.Timeout = 270;
			HttpContext.Current.Session["Access"] = accessInformation;

			HttpCookie aCookie = new HttpCookie("Code");
			aCookie.Values["SessionCode"] = accessInformation.SessionCode;
			HttpContext.Current.Response.Cookies.Add(aCookie);
			return true;
		}

		public static async Task<bool> Logout()
		{
			if (!isAuthenticated) return false;		

			var query = new QueryContent()
			{
				url = "Account/Logout",
				method = "GET",
				token = currentAccess.Token
			};

			HttpContext.Current.Session.Clear();
			HttpCookie aCookie = new HttpCookie("Code");
			aCookie.Values["SessionCode"] = "";
			HttpContext.Current.Response.Cookies.Add(aCookie);

			var result = await Repository.Query(query);

			if (result.statusResult.status == Status.Success)
				return true;
			else
				return false;
		}


		public static bool ContainRole(string roles)
		{
			if (roles.Length == 0) return true;

			var _roles = roles.Split(',');
			var currentAccess = Authentication.currentAccess;

			if (_roles.Length == 0) return true;

			for (int i = 0; i < _roles.Length; i++)
			{
				if (currentAccess.Role == _roles[i].Trim())
					return true;
			}

			return false;
		}
	}
}