﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interactic
{
    public static class StyleConfig
    {
		/// <summary>
		/// Botón Aceptar
		/// </summary>
		public const string ButtonStyle_Accept = "btn-info waves-effect waves-light";

		/// <summary>
		/// Botón Guardar
		/// </summary>
		//public const string ButtonStyle_Save = "waves-effect waves-light";

		/// <summary>
		/// Botón Crear
		/// </summary>
		public const string ButtonStyle_Create = "btn-danger waves-effect waves-light";

		/// <summary>
		/// Botón Editar
		/// </summary>
		public const string ButtonStyle_Edit = "btn-warning waves-effect waves-light";

		/// <summary>
		/// Botón Eliminar
		/// </summary>
		public const string ButtonStyle_Delete = "btn-danger waves-effect waves-light";

		/// <summary>
		/// Botón Eliminar
		/// </summary>
		public const string ButtonStyle_Cancel = "btn-success waves-effect waves-light";


		/// <summary>
		/// Botón Acción
		/// </summary>
		public const string ButtonStyle_Action = "btn-success waves-effect waves-light";

        /// <summary>
        /// Botón Configuración
        /// </summary>
        public const string ButtonStyle_Config = "waves-effect waves-light orange darken-1";
        /// <summary>
        /// Botón Configuración
        /// </summary>
        //public const string ButtonStyle_Config = "waves-effect waves-light orange darken-1";

        /// <summary>
        /// Botón Ver
        /// </summary>
        //public const string ButtonStyle_View = "waves-effect waves-light green";

        /// <summary>
        /// Botón Advertencia
        /// </summary>
        //public const string ButtonStyle_Warning = "waves-effect waves-light yellow accent-4";

        /// <summary>
        /// Botón Realizar/Hacer
        /// </summary>
        //public const string ButtonStyle_Do = "waves-effect waves-light red";

        /// <summary>
        /// Botón Siguiente
        /// </summary>
        //public const string ButtonStyle_Next = "waves-effect waves-light";

        /// <summary>
        /// Botón Atrás
        /// </summary>
        //public const string ButtonStyle_Back = "waves-effect waves-light grey";		

        /// <summary>
        /// Backgrounds - Colors
        /// </summary>
        //public const string Delete = "red";
        //public const string Warning = "yellow accent-4";
        //public const string Success = "teal darken-1";
        //public const string Error = "deep-orange accent-4";

        /// <summary>
        /// Botón Ver
        /// </summary>
        public const string ButtonStyle_View = "waves-effect waves-light green";

        /// <summary>
        /// Botón Realizar/Hacer
        /// </summary>
        public const string ButtonStyle_Do = "waves-effect waves-light red";
    }
}
