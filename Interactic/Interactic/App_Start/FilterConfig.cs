﻿using SecurityGuard;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Interactic
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
			filters.Add(new AuthorizeMVCAttribute());
		}
	}

	public class AuthorizeMVCAttribute : AuthorizeAttribute
	{
		public override void OnAuthorization(AuthorizationContext filterContext)
		{
			//base.OnAuthorization(filterContext);

			var infoController = filterContext.RouteData.Values;
			var controllerType = filterContext.ActionDescriptor.ControllerDescriptor.ControllerType;
			var authorizationMethod = controllerType.
									 GetMethods().
									 FirstOrDefault(p => p.Name == infoController["action"].ToString())?.GetCustomAttributes(true)?.
									 FirstOrDefault(p => p.GetType() == typeof(AuthorizeMVCAttribute)) as AuthorizeMVCAttribute;
			var authorizationController = System.Attribute.GetCustomAttributes(controllerType).FirstOrDefault(p => p.GetType() == typeof(AuthorizeMVCAttribute)) as AuthorizeMVCAttribute;

			//Si la clase tiene la etiqueta de anónimo se continua el proceso normal.
			if (System.Attribute.GetCustomAttributes(controllerType).FirstOrDefault(p => p.GetType() == typeof(AllowAnonymousAttribute)) != null)
				return;

			//Si no está autenticado es dirigido al inicio.
			if (!Authentication.isAuthenticated)
			{
				filterContext.Result = RedirectToRoute("Login", "Index");
				return;
			}

			//Se comprueba si se dirije a Home y la autenticación está correcta
			if (infoController["controller"].ToString().ToLower() == "home" && infoController["action"].ToString().ToLower() == "index")
				return;

			var accessMethod = false;
			var accessController = false;

			//Se ejecutan las instrucciones para determinar el acceso - Controller			
			if (authorizationController != null)
				if (authorizationController.Roles.Length != 0 && (accessController = Authentication.ContainRole(authorizationController.Roles)))
					return;

			//Se ejecutan las instrucciones para determinar el acceso - Método
			if (authorizationMethod != null)
				if (authorizationMethod.Roles.Length != 0 && (accessMethod = Authentication.ContainRole(authorizationMethod.Roles)))
					return;

			//Si alguno da con acceso
			if (accessController && authorizationMethod == null)
				return;

			if (authorizationController == null && authorizationMethod == null)
				return;

			if (accessMethod) return;

			if (authorizationController != null && authorizationController.Roles.Length == 0 && authorizationMethod == null)
				return;

			//Si no se encuentra ningún acceso posible se redirecciona a Login/Index
			filterContext.Result = RedirectToRoute("Login", "Index");
		}

		private RedirectToRouteResult RedirectToRoute(string controller, string action)
		{
			return new RedirectToRouteResult(new RouteValueDictionary { { "controller", controller }, { "action", action } });
		}
	}
}
