﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Interactic
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Forum",
				url: "Forum/{id}",
				defaults: new { controller = "Forum", action = "Index", id = UrlParameter.Optional }
			);

			routes.MapRoute(
				name: "ForumAdd",
				url: "Forum/{forum_id}/Topic/Add",
				defaults: new { controller = "Forum", action = "TopicAdd" }
			);

			routes.MapRoute(
				name: "ForumEdit",
				url: "Forum/{forum_id}/Topic/Edit/{topic_id}",
				defaults: new { controller = "Forum", action = "TopicEdit" }
			);

			routes.MapRoute(
				name: "ForumView",
				url: "Forum/{forum_id}/Topic/{topic_id}/View",
				defaults: new { controller = "Forum", action = "TopicView" }
			);

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
