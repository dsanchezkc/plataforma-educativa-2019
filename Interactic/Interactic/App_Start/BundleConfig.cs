﻿using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace Interactic
{
	public class BundleConfig
	{
		// Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			

			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.validate*"));

			// Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
			// para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
					  "~/Scripts/bootstrap.js",
					  "~/Scripts/respond.js"));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/bootstrap.css",
					  "~/Content/site.css"));

			/********************************** CSS ************************************************/
			

			bundles.Add(new ScriptBundle("~/Theme/Css/SideBar-Nav").Include(
					   "~/Content/theme-css/sidebar-nav.min.css"));

			bundles.Add(new StyleBundle("~/Theme/Css/Animate").Include(
					  "~/Content/theme-css/animate.css"));

			bundles.Add(new StyleBundle("~/Theme/Css/Style").Include(
					  "~/Content/theme-css/style.css"));

			bundles.Add(new StyleBundle("~/Theme/Css/Colors").Include(
					  "~/Content/theme-css/colors/megna.css"));

			bundles.Add(new StyleBundle("~/Css/Snackbar").Include(
					  "~/Content/snackbar/material.css",
					  "~/Content/snackbar/snackbar.min.css"));

			bundles.Add(new StyleBundle("~/Css/Auto-Complete").Include(
					  "~/Content/Auto-Complete/auto-complete.css"));

			bundles.Add(new StyleBundle("~/Css/Summernote")
                //.Include("~/Scripts/Summernote/summernote.css")
                .Include("~/bower_components/fontawesome/css/font-awesome.min.css"));

			bundles.Add(new StyleBundle("~/Css/DropZone").Include(
					  "~/bower_components/dropzone/dist/min/dropzone.min.css"));

			bundles.Add(new StyleBundle("~/Css/VenoBox").Include(
					  "~/bower_components/venobox/venobox/venobox.css"));

			bundles.Add(new StyleBundle("~/Css/Flatpickr").Include(
					  "~/Content/Flatpickr/flatpickr.min.css",
					  "~/Content/Flatpickr/material_orange.css"));


            bundles.Add(new StyleBundle("~/FullCalendar/css").Include(
                        "~/Scripts/fullcalendar/fullcalendar.min.css"));

            bundles.Add(new StyleBundle("~/Handsontable/css").Include("~/Content/handsontable/handsontable.full.min.css"));

            bundles.Add(new StyleBundle("~/Mdb/css")
              .Include("~/Scripts/mdb/bootstrap.min.css")
              .Include("~/Scripts/mdb/mdb.min.css"));

            /********************************** JS ************************************************/
            bundles.Add(new ScriptBundle("~/Js/Jquery").Include(
					  "~/bower_components/jquery/dist/jquery.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Bootstrap").Include(
					  "~/Scripts/theme-js/tether.min.js",
					  "~/bower_components/popper.js/dist/umd/popper.min.js",
					  "~/Content/theme-bootstrap/dist/js/bootstrap.min.js",
					  "~/Scripts/theme-js/bootstrap-extension.min.js",
					   "~/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js",
					   "~/bower_components/bootstrap-select/js/i18n/defaults-es_ES.js",
						"~/Scripts/theme-js/cbpFWTabs.js"
					  ));

			bundles.Add(new ScriptBundle("~/Js/Bootstrap-DatePicker").Include(
					  "~/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
					  ));

			bundles.Add(new ScriptBundle("~/Js/SideBar-Nav").Include(
					  "~/Scripts/theme-js/sidebar-nav.min.js"));

			bundles.Add(new ScriptBundle("~/Js/SlimScroll").Include(
					  "~/Scripts/theme-js/jquery.slimscroll.js"));

			bundles.Add(new ScriptBundle("~/Js/Wave").Include(
					  "~/Scripts/theme-js/waves.js"));

			bundles.Add(new ScriptBundle("~/Js/Custom").Include(
					  "~/Scripts/theme-js/custom.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Switcher").Include(
					  "~/Scripts/theme-js/jQuery.style.switcher.js"));

			bundles.Add(new ScriptBundle("~/Js/Mask").Include(
					  "~/Scripts/theme-js/mask.js"));

			bundles.Add(new ScriptBundle("~/Js/Bootstrap-Jasny").Include(
					  "~/Scripts/theme-js/jasny-bootstrap.js"));

			bundles.Add(new ScriptBundle("~/Js/Snackbar").Include(
					  "~/Scripts/snackbar/snackbar.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Interactic").Include(
					  "~/Scripts/Interactic/Interactic-Repository.js",
					  "~/Scripts/Interactic/Interactic-UI.js"));

			bundles.Add(new ScriptBundle("~/Js/Stepper").Include(
					  "~/Scripts/Stepper/stepper-express.js"));

			bundles.Add(new ScriptBundle("~/Js/Parsley").Include(
					  "~/bower_components/parsleyjs/dist/parsley.min.js",
					  "~/bower_components/parsleyjs/src/i18n/es.js"));

			bundles.Add(new ScriptBundle("~/Js/RUN").Include("~/Scripts/jquery.rut.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Auto-Complete").Include("~/Scripts/Auto-Complete/auto-complete.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Summernote").
				NonOrdering().
				Include("~/Scripts/Summernote/summernote.js",
						"~/Scripts/Summernote/lang/summernote-es-ES.min.js",
						"~/Scripts/Interactic/Ext-Math-Summernote/ext-math-summernote.js",
						"~/Scripts/Interactic/Ext-Math-Summernote/ext-math-modules.js",
						"~/bower_components/MathJax/MathJax.js",
						"~/Scripts/html2canvas.js"));

            bundles.Add(new ScriptBundle("~/FullCalendar/js")
            .Include("~/Scripts/fullcalendar/lib/moment.min.js")
            .Include("~/Scripts/fullcalendar/lib/jquery-ui.min.js")
            .Include("~/Scripts/fullcalendar/fullcalendar.min.js")
            .Include("~/Scripts/fullcalendar/locale/es.js"));

            bundles.Add(new ScriptBundle("~/Handsontable/js")
            .Include("~/Scripts/handsontable/handsontable.full.min.js")
            .Include("~/bower_components/numbro/dist/languages.min.js"));

            bundles.Add(new ScriptBundle("~/RUN/js").Include("~/Scripts/jquery.rut.min.js"));

            bundles.Add(new ScriptBundle("~/Flot/js").Include(
            "~/bower_components/flot/jquery.flot.js"));

            bundles.Add(new StyleBundle("~/Others/css")
            .Include("~/Content/nanoscroller.css")
            .Include("~/bower_components/fontawesome/css/font-awesome.min.css")
            .Include("~/assets/material-design-icons/css/material-design-icons.min.css")
            .Include("~/bower_components/ionicons/css/ionicons.min.css")
            .Include("~/bower_components/weather-icons/css/weather-icons.min.css")
            .Include("~/bower_components/nvd3/build/nv.d3.min.css")
            .Include("~/bower_components/jquery-simplecolorpicker/jquery.simplecolorpicker.css"));

            bundles.Add(new ScriptBundle("~/Others/js")
                .Include("~/bower_components/jquery-requestAnimationFrame/dist/jquery.requestAnimationFrame.min.js")
                .Include("~/Scripts/jquery.nanoscroller.min.js")
                .Include("~/bower_components/simpleWeather/jquery.simpleWeather.min.js")
                .Include("~/bower_components/d3/d3.min.js")
                .Include("~/bower_components/nvd3/build/nv.d3.min.js")
                .Include("~/bower_components/Sortable/Sortable.min.js")
                .Include("~/assets/_con/js/_con.js")
                .Include("~/bower_components/code-prettify/src/prettify.js")
                .Include("~/Scripts/global-variables.js")
                .Include("~/bower_components/jquery-simplecolorpicker/jquery.simplecolorpicker.js"));

            bundles.Add(new ScriptBundle("~/Js/DropZone").Include(
					  "~/bower_components/dropzone/dist/min/dropzone.min.js"));

			bundles.Add(new ScriptBundle("~/Js/FileSaver").Include("~/bower_components/file-saver/FileSaver.min.js"));

            bundles.Add(new ScriptBundle("~/FileSaver/js").Include("~/bower_components/file-saver/FileSaver.min.js"));

            bundles.Add(new ScriptBundle("~/FlotPie/js").Include(
            "~/bower_components/flot/jquery.flot.pie.js"));

            bundles.Add(new ScriptBundle("~/Js/VenoBox").Include(
					  "~/bower_components/venobox/venobox/venobox.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Flatpickr").Include(
					  "~/Scripts/Flatpickr/flatpickr.js",
					  "~/Scripts/Flatpickr/es.js"));

			bundles.Add(new ScriptBundle("~/Js/JQuery-UI").Include(
					  "~/Scripts/jquery-ui.min.js"));

			bundles.Add(new ScriptBundle("~/Js/CKEditor").Include(
					  "~/Scripts/ckeditor/ckeditor.js"));

			bundles.Add(new ScriptBundle("~/Js/Sortable").Include(
					  "~/Scripts/Sortable.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Moment").Include(
					  "~/Scripts/moment.min.js"));

			bundles.Add(new ScriptBundle("~/Js/Cookie").Include(
					  "~/Scripts/js.cookie.js"));

            bundles.Add(new StyleBundle("~/Css/Bootstrap").Include(
                      "~/Content/theme-bootstrap/dist/css/bootstrap.min.css",
                      "~/Content/theme-css/bootstrap-extension.css",
                      "~/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css"
                      ));

            bundles.Add(new StyleBundle("~/Css/Bootstrap-DatePicker").Include(
                      "~/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"
                      ));


            bundles.Add(new StyleBundle("~/Materialize/css").Include(
                        "~/bower_components/materialize/dist/css/materialize.min.css"));

            bundles.Add(new ScriptBundle("~/Materialize/js").Include(
                                    "~/bower_components/materialize/dist/js/materialize.min.js"));

            bundles.Add(new ScriptBundle("~/Evaluandome/js")
                .Include("~/Scripts/Evaluandome/Evaluandome-Repository.js")
                .Include("~/Scripts/Evaluandome/Evaluandome-UI.js"));

            bundles.Add(new StyleBundle("~/Loader/css").Include("~/Content/fakeLoader.css"));

            bundles.Add(new ScriptBundle("~/Loader/js").Include("~/Scripts/fakeLoader.min.js"));

            BundleTable.EnableOptimizations = false;
		}
	}

	public class NonOrderingBundleOrderer : IBundleOrderer
	{
		public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
		{
			return files;
		}
	}

	public static class BundleExtentions
	{
		public static Bundle NonOrdering(this Bundle bundle)
		{
			bundle.Orderer = new NonOrderingBundleOrderer();
			return bundle;
		}
	}
}
