﻿using Component.Pagination;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Communication
{
	public class QueryContent
	{
		public string token { set; get; }		
		public string url { set; get; }		
		public string method { set; get; }

		public object data { set; get; }
		public string dataTypeResult { set; get; }
		public string dataConvert { set; get; }
		public string viewModelConvert { set; get; }

		public Pagination pagination { set; get; }

		public QueryContent()
		{
			token = "";
			dataTypeResult = "";
			dataConvert = "";
			method = "GET";
			pagination = new Pagination();
		}

		public void SetDataObject(object data)
		{
			this.data = new JavaScriptSerializer().Serialize(data);
		}

	}
}