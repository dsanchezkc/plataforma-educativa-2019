﻿using Component.Pagination;
using Convert;
using Generic;
using SecurityGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Communication
{
	public class Repository
	{
		public async static Task<ResultContent> Query(QueryContent content, bool onlyResultJson = false)
		{
			ResultContent resultContent= new ResultContent();

			if (Authentication.isAuthenticated && content.token == "")
				content.token = Authentication.currentAccess.Token;

			try
			{
				var jsonContent = await QueryWebApi(content);

				if (onlyResultJson)
				{
					resultContent.resultJson = jsonContent;
					return resultContent;
				}

				if (content.dataTypeResult != "")
				{
					Type itemType = Type.GetType("Model." + content.dataTypeResult + ", ModelsLibrary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");

					if(itemType==null)
						itemType = Type.GetType("ViewModels." + content.dataTypeResult + ", ModelsLibrary, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null");
					
					var constructedListType = typeof(List<>).MakeGenericType(itemType);
					var listObject = Activator.CreateInstance(constructedListType);

					MethodInfo method = typeof(Serializer).GetMethod("Deseralize");
					MethodInfo generic = method.MakeGenericMethod(listObject.GetType());
					var dataList = generic.Invoke(null, new object[] { jsonContent });
					resultContent.data = DataConvert(content.dataConvert, dataList, content);
				}			

				resultContent.dataConvert = content.dataConvert;
				resultContent.statusResult.status = Status.Success;
				resultContent.statusResult.messageResult = "Proceso finalizado con éxito.";
			}
			catch (Exception e)
			{
				resultContent.statusResult.messageResult = e.Message + "\n" + e.InnerException?.InnerException.Message;
				resultContent.statusResult.status = Status.Error;
			}

			return resultContent;
		}

		public async static Task<T> QueryExpress<T>(QueryContent content)
		{
			if (Authentication.isAuthenticated && content.token == "")
				content.token = Authentication.currentAccess.Token;

			try
			{
				var jsonContent = await QueryWebApi(content);
				var serializer = new JavaScriptSerializer();
				serializer.MaxJsonLength = int.MaxValue;
				return serializer.Deserialize<T>(jsonContent as string);
			}
			catch (Exception e)
			{
				return default(T);
			}
		}

		private static async Task<string> QueryWebApi(QueryContent content)
		{
			var resultContent = "";

			using (var client = new HttpClient(new HttpClientHandler() { UseDefaultCredentials = true }))
			{
				client.BaseAddress = new Uri(ConnectionHelper.URL_WEBAPI_LOCAL);
				client.DefaultRequestHeaders.Accept.Clear();

				if (content.token != "")
					client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", content.token);

				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				client.DefaultRequestHeaders.Add("Pagination-Result-Limit", content.pagination.itemsMax.ToString());
				client.DefaultRequestHeaders.Add("Pagination-Result-Pag", content.pagination.pag.ToString());

				HttpResponseMessage response = null;

				switch (content.method.ToUpper())
				{
					case "POST":
						response = await client.PostAsync(content.url, new StringContent("", Encoding.UTF8, "application/json"));
					break;

					case "GET":
						response = await client.GetAsync(content.url + StringFormat_URL(content.data));
					break;

					case "PUT":
						response = await client.PutAsync(content.url, new StringContent("", Encoding.UTF8, "application/json"));
					break;

					case "DELETE":
						response = await client.DeleteAsync(content.url + StringFormat_URL(content.data));
					break;

					default: break;
				}

				if(response==null)
					throw new Exception("No se ha indicado método de envío (POST/PUT/GET/DELETE)");

				try
				{
					var resultTotal = response.Headers.GetValues("Pagination-Result-Total")?.FirstOrDefault();
					if (resultTotal != null)
						content.pagination.countResult = int.Parse(resultTotal);
				} catch (Exception) { }

				if (response.IsSuccessStatusCode)
					resultContent = await response.Content.ReadAsStringAsync();
				else
					throw new Exception("Error " + response.StatusCode);
			}
			return resultContent;
		}

		private static string StringFormat_URL(object obj)
		{
			if (obj == null) return "";

			var _string = "/?";

			var _obj = Serializer.Deseralize<Dictionary<string, string>>(obj.ToString());

			foreach (var __obj in _obj)
				_string += __obj.Key + "=" + __obj.Value + "&";			

			return _string;
		}
		
		private static object DataConvert(string dataConvert, object data, QueryContent content)
		{
			if (data == null) return null;

			switch (dataConvert)
			{
				case "Select": return ConvertCollection.CastSelect(data);

				case "Table": return ConvertCollection.CastTable(data, content.url, content.pagination, content.data, content.viewModelConvert);

				case "ViewModel": return ConvertCollection.CastViewModel(data, content.viewModelConvert);

				default: return data;
			}
		}
	}

	public class ResumeResponse
	{
		public string content;
		public int resultTotal = -1;
		public int resultLimit = -1;
	}
}