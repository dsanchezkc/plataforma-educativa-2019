﻿using Communication;
using Component.Pagination;
using Component.Select;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Convert
{
	public static class ConvertCollection
	{
		public static object CastSelect(object data)
		{
			var list = data as IEnumerable;
			var items = new List<SelectData>();

			if (list != null)
				foreach (var item in list)
				{
					var value = item.GetType().GetProperty("Id").GetValue(item, null);
					var text = item.GetType().GetProperty("Name").GetValue(item, null);

					items.Add(new SelectData() { text = text.ToString(), value = value.ToString() });
				}

			return items;
		}

		public static object CastTable(object data, string queryProcess, Pagination pagination, object dataQuery , string viewModelConvert)
		{
			var viewModels = CastViewModel(data, viewModelConvert);

			Type type = data.GetType();
			Type itemType = type.GetGenericArguments()[0];

			Type itemTypeViewModel = null;

			if (viewModelConvert == null || viewModelConvert == "")
				itemTypeViewModel = Type.GetType("ViewModel." + itemType.Name + "ViewModel");
			else
				itemTypeViewModel = Type.GetType("ViewModel." + viewModelConvert);

			MethodInfo method = typeof(Component.Table.GenericTableComponent).GetMethod("Generate");
			MethodInfo generic = method.MakeGenericMethod(itemTypeViewModel);

			var genericTable = generic.Invoke(null, new object[] { viewModels, queryProcess, pagination, dataQuery });

			return genericTable;
		}

		public static object CastViewModel(object data, string viewModelConvert)
		{			
			var list = data as IEnumerable;

			Type type = data.GetType();

			Type itemType = list==null? data.GetType().BaseType : type.GetGenericArguments()[0];

			if (itemType == typeof(ModelBase))
				itemType = type;

			Type itemTypeViewModel = null;

			if (viewModelConvert == null || viewModelConvert == "")
				itemTypeViewModel = Type.GetType("ViewModel." + itemType.Name + "ViewModel");
			else
				itemTypeViewModel = Type.GetType("ViewModel." + viewModelConvert);

			var constructedListType = typeof(List<>).MakeGenericType(itemTypeViewModel);
			var listViewModel = Activator.CreateInstance(constructedListType) as IList;

			if (list == null)
				list = new List<object>() { data };

			foreach (var item in list)
				listViewModel.Add(Activator.CreateInstance(itemTypeViewModel, new object[] { item }));			

			return listViewModel;
		}
	}
}
