﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Communication
{
	public enum Status { Success =200, Error=500 }

	public class StatusResult
	{
		public Status status { set; get; }
		public string messageResult { set; get; }
		public List<string> errors { set; get; }

		public StatusResult()
		{
			errors = new List<string>();
		}
	}	
}