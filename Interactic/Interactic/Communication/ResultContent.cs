﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Communication
{
	public class ResultContent
	{
		public StatusResult statusResult { set; get; }
		public object data { set; get; }
		public string dataConvert { set; get; }
		public string resultJson { set; get; }

		public ResultContent()
		{
			statusResult = new StatusResult();
			resultJson = "";
		}
	}
}