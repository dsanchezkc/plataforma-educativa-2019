﻿using Communication;
using Component.Pagination;
using Component.Select;
using Generic;
using Interactic;
using Model;
using SecurityGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC]
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var managerSelect = new ManagerSelect() { showVertical = true };
			managerSelect
				.AddSelect(new SelectComponent(
					typeModel: typeof(Region),
					code: SelectCode.Region,
					url: "Region",
					name: "Región",
					placeholder: "Seleccione una región"
					)
				).AddSelect(new SelectComponent(
					typeModel: typeof(Province),
					code: SelectCode.Province,
					parentCode: SelectCode.Region,
					url: "Province",
					name: "Provincia",
					placeholder: "Seleccione una provincia"
				)
				).AddSelect(new SelectComponent(
					typeModel: typeof(Commune),
					code: SelectCode.Commune,
					parentCode: SelectCode.Province,
					url: "Commune",
					name: "Comuna",
					placeholder: "Selecciona una comuna"
				)
				).AddSelect(new SelectComponent(
					typeModel: typeof(EducationalEstablishment),
					code: SelectCode.EducationalEstablishment,
					parentCode: SelectCode.Commune,
					url: "EducationalEstablishment/GetEducationalEstablishmentsByCommuneFilteredByUser",
					name: "Establecimiento",
					placeholder: "Seleccione establecimiento"
				)
				).AddSelect(new SelectComponent(
					typeModel: typeof(Role),
					code: SelectCode.Role,
					parentCode: SelectCode.EducationalEstablishment,
					url: "Role/GetRolesByEducationalEstablishmentFilteredByUser",
					name: "Rol",
					placeholder: "Seleccione un rol"
				));

			managerSelect.groupCode = 1;

			return View(managerSelect);
		}

		[HttpGet]
		public async Task<ActionResult> RefreshInformationCurrentUser()
		{
			var query = new QueryContent()
			{
				url = "User/GetCurrentUser",
				method = "GET",
				dataTypeResult = "User"
			};

			var result = await Repository.Query(query, true);

			try
			{
				var _user = new JavaScriptSerializer().Deserialize<User>(result.resultJson as string);

				if (_user != null && _user.Id != null && _user.Id != "")
				{
					var user = Authentication.currentAccess.user;
					user.ImagePath = _user.ImagePath;
					user.Name = _user.Name;
					user.MotherLastName = _user.MotherLastName;
					user.FatherLastName = _user.FatherLastName;
					user.Address = _user.Address;
				}

				return new HttpStatusCodeResult(HttpStatusCode.OK);
			}
			catch (Exception)
			{
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
			}
		}

		[HttpGet]
		public async Task<ActionResult> RefreshInformationCurrentEstablishment()
		{
			var query = new QueryContent()
			{
				url = "EducationalEstablishment/GetCurrentEducationalEstablishment",
				method = "GET",
				dataTypeResult = "EducationalEstablishment"
			};

			var result = await Repository.Query(query, true);

			try
			{
				var _educationalEstablishment = new JavaScriptSerializer().Deserialize<EducationalEstablishment>(result.resultJson as string);

				if (_educationalEstablishment != null && _educationalEstablishment.Id != 0)
				{
					var educationalEstablishment = Authentication.currentAccess.educationalEstablishment;
					educationalEstablishment.PathImage = _educationalEstablishment.PathImage;
					educationalEstablishment.Name = _educationalEstablishment.Name;
				}

				return new HttpStatusCodeResult(HttpStatusCode.OK);
			}
			catch (Exception)
			{
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
			}
		}
	}
}