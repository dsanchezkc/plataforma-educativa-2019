﻿using Communication;
using Component.Button;
using Component.Select;
using Component.Table;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador")]
	public class SubjectController : Controller
	{
		public async Task<ActionResult> Index()
		{
			var query = new QueryContent()
			{
				url = "Subject",
				method = "GET",
				dataConvert = "Table",
				dataTypeResult = "Subject"
			};

			var result = await Repository.Query(query);
			var table = result.data as GenericTableComponent;

			var content = ViewGeneric.GeneratePanel<SubjectViewModel>(typeof(Subject))
									.AddGenericTableComponent(table)
									.AddButton(new ButtonComponent("addButton")
									{
										text = "Crear asignatura",
										color = "btn right " + StyleConfig.ButtonStyle_Create,
										icon = "add"
									});
			content.groupCode = 1;

			return View(content);
		}

		public async Task<ActionResult> Add()
		{
			var query = new QueryContent()
			{
				url = "Course",
				method = "GET",
			};

			var courses = await Repository.QueryExpress<List<Course>>(query);

			return View(courses);
		}

		public async Task<ActionResult> Edit(int id)
		{
			var query = new QueryContent()
			{
				url = "Subject",
				method = "GET",
			};
			query.SetDataObject(new { id = id });

			var subject = await Repository.QueryExpress<Subject>(query);

			if (subject == null || subject.Id == 0)
				return RedirectToAction("Index", "Course");

			query = new QueryContent()
			{
				url = "Course",
				method = "GET",
			};

			var courses = await Repository.QueryExpress<List<Course>>(query);

			return View(new Object[] { subject, courses });
		}
	}
}