﻿using Communication;
using Component.Button;
using Component.Pagination;
using Component.Select;
using GeneralHelper;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
    [AuthorizeMVC(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
    public class SpecialTest : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> AnsweringTest(int? id)
        {
            var query = new QueryContent()
            {
                url = "SpecialTest",
                method = "GET"
            };

            query.SetDataObject(new { id = id });
            var result = await Repository.QueryExpress<object>(query);

            return View("AnsweringTest", result.ObjectToStringBase64() as Object);
        }


    }
}