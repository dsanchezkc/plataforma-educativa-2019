﻿using Communication;
using Component.Button;
using Component.Select;
using Component.Table;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;
using ViewModels;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador, Profesor")]
	public class PendingTestController : Controller
	{
		public ActionResult Index()
		{
			var content_1 = ViewGeneric.GeneratePanel<PendingTest_AllViewModel>(typeof(PendingTestResponseViewModel));
			content_1.groupCode = 1;

			var content_2 = ViewGeneric.GeneratePanel<PendingTest_AllViewModel>(typeof(PendingTestResponseViewModel));
			content_2.AddManagerSelect(GetManagerSelect());
			content_2.groupCode = 2;

			return View(new Object[] { content_1, content_2 });
		}

		private ManagerSelect GetManagerSelect()
		{
			ManagerSelect select = null;

			switch (SecurityGuard.Authentication.currentAccess.Role)
			{
				case "Administrador":
				case "SubAdministrador":
					select = new ManagerSelect("SpecialTest/GetAnswerTestsBySubUnityAndUser")
					.AddSelect
						(
							new SelectComponent(
							typeModel: typeof(Course),
							code: SelectCode.Course,
							url: "Course",
							name: "Curso",
							placeholder: "Seleccione un curso"
							)
						).AddSelect(new SelectComponent(
							typeModel: typeof(Subject),
							code: SelectCode.Subject,
							parentCode: SelectCode.Course,
							url: "Subject/GetSubjectsByCourse",
							name: "Asignatura",
							placeholder: "Seleccione una asignatura"
						)
						).AddSelect(new SelectComponent(
							typeModel: typeof(Unity),
							code: SelectCode.Unity,
							parentCode: SelectCode.Subject,
							url: "Unity/GetUnitiesBySubjectAndCourse",
							name: "Eje temático",
							placeholder: "Seleccione una unidad",
							requiredCodes: new List<SelectCode>() { SelectCode.Course }
						));
					break;

				case "Profesor":
					select = new ManagerSelect(url: "SpecialTest/GetAnswerTestsBySubUnityAndUser")
					.AddSelect(new SelectComponent(
						typeModel: typeof(Course),
						code: SelectCode.Course,
						url: "Course/GetCoursesFilteredByUser",
						name: "Curso",
						placeholder: "Seleccione un curso"
					))
					.AddSelect(new SelectComponent(
						typeModel: typeof(Subject),
						code: SelectCode.Subject,
						parentCode: SelectCode.Course,
						url: "Subject/GetSubjectsByCourseFilteredByUser",
						name: "Asignatura",
						placeholder: "Seleccione una asignatura"
					))
					.AddSelect(new SelectComponent(
						typeModel: typeof(Unity),
						code: SelectCode.Unity,
						parentCode: SelectCode.Subject,
						url: "Unity/GetUnitiesBySubjectAndCourse",
						name: "Unidad",
						placeholder: "Seleccione una unidad",
						requiredCodes: new List<SelectCode>() { SelectCode.Course }
					));
					break;
			}

			select.AddSelect(new SelectComponent(
						typeModel: typeof(SubUnity),
						code: SelectCode.SubUnity,
						parentCode: SelectCode.Unity,
						url: "SubUnity/GetSubUnitiesByUnity",
						name: "Sub-Unidad",
						placeholder: "Seleccione una sub-unidad",
						requiredCodes: new List<SelectCode>() { SelectCode.SubUnity }
					)).
					AddSelect(new SelectComponent(
						typeModel: typeof(User),
						code: SelectCode.User,
						parentCode: SelectCode.SubUnity,
						url: "User/GetUsersByCourse",
						name: "Estudiantes",
						placeholder: "Seleccione un alumno",
						requiredCodes: new List<SelectCode>() { SelectCode.Course }
					));

			return select;
		}


	}
}