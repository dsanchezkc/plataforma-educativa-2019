﻿using Extensions;
using SecurityGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Interactic.Controllers
{
	[AllowAnonymous]
	public class LoginController : Controller
	{
		public async Task<ActionResult> Index(Model.User user)
		{
			if (user != null && user.UserName != null && user.PasswordHash != null)
				await Authentication.Authenticate(user.UserName.ToString(), user.PasswordHash);

			if (Authentication.isAuthenticated)
				return RedirectToAction("Index", "Home");

			user.UserName = user.UserName?.WithFormatRUN();
			return View(user);
		}

		[HttpGet]
		public async Task<ActionResult> ReLogin(int educationalestablishment_id, string role, int year)
		{
			if (!Authentication.isAuthenticated)
				return RedirectToAction("Index", "Login");

			var result = await Authentication.ChangeAuthenticate(educationalestablishment_id, role, year);

			return new HttpStatusCodeResult((result) ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
		}

		public async Task<ActionResult> Logout()
		{
			if (!Authentication.isAuthenticated)
				return RedirectToAction("Index", "Login");

			await Authentication.Logout();

			return View("Index", new Model.User());
		}
	}
}