﻿using Communication;
using Component.Button;
using Component.Select;
using Generic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace EvaluandomeWeb.Controllers
{
    public class InicioController : Controller
    {
        // GET: Inicio
        public async Task<ActionResult> Index()
        {
            return await Inicio();
        }

        private async Task<ActionResult> Inicio()
        {
            return View("Index");
        }
    }
}