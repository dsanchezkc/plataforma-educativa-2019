﻿using Communication;
using Component.Button;
using Component.Panel;
using Component.Select;
using Component.Table;
using Generic;
using Interactic;
using Model;
using SecurityGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador")]
	public class UserController : Controller
	{
		public ActionResult Index()
		{
			var managerSelect = new ManagerSelect("User/GetUsersByRole");

			managerSelect.AddSelect
				(
					new SelectComponent(
					typeModel: typeof(Role),
					code: SelectCode.Role,
					url: "Role/GetRolesFiltererAdminAndSubAdmin",
					name: "Tipo de cuenta",
					placeholder: "Seleccione un tipo de cuenta"
					)
				);

			var content = (Authentication.currentAccess.Role == "Administrador") ? 
					ViewGeneric.GeneratePanel<UserViewModel>(typeof(User)) : ViewGeneric.GeneratePanel<User_SubAdminViewModel>(typeof(User));

			content.AddManagerSelect(managerSelect)
				.AddButton(new ButtonComponent("addStudentsButton")
				{
					text = "Registrar lista de alumnos",
					color = "btn right " + StyleConfig.ButtonStyle_Create,
					icon = "add"
				})
				.AddButton(new ButtonComponent("addButton")
				{
					text = "Crear usuario",
					color = "btn right " + StyleConfig.ButtonStyle_Create,
					icon = "add"
				});

			content.groupCode = 1;		

			return View(content);
		}

		public ActionResult Add()
		{
			return View();
		}
		
		public async Task<ActionResult> Edit(string id)
		{
			var query = new QueryContent()
			{
				url = "User",
				method = "GET",
				dataTypeResult = "User"
			};
			query.SetDataObject(new { id = id });

			var user = await Repository.QueryExpress<User>(query);

			if (user == null || user.UserName == null)
				return RedirectToAction("Add", "User");

			return View(user);
		}

		public ActionResult AddExcel()
		{
			return View();
		}
	}
}