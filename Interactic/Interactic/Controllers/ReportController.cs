﻿using Communication;
using Component.Button;
using Component.Pagination;
using Component.Select;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
	public class ReportController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}		
	}
}