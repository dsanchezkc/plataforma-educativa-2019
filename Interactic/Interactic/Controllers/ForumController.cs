﻿using Communication;
using Component.Button;
using Component.Pagination;
using Component.Select;
using Component.Table;
using Generic;
using Interactic;
using Model;
using SecurityGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;
using ViewModels;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador, Profesor, Alumno")]
	public class ForumController : Controller
	{
		public async Task<ActionResult> Index(int id)
		{
			switch (Authentication.currentAccess.Role)
			{
				case "Alumno":
					return await GetViewResult_Student(id);
				case "Profesor":
					return await GetViewResult_Admin(id);
				case "Administrador":
					return await GetViewResult_Admin(id);
				case "SubAdministrador":
					return await GetViewResult_Admin(id);
			}

			return RedirectToAction("Index", "Home");
		}

		private async Task<ViewResult> GetViewResult_Student(int id)
		{
			var query = new QueryContent()
			{
				dataConvert = "Table",
				url = "Topic/GetTopicsResponseViewModelByForum",
				dataTypeResult = "TopicResponseViewModel",
				viewModelConvert = "Topic_SimpleViewModel",
				method = "GET"
			};

			query.SetDataObject(new { forum_id = id });

			var result = await Repository.Query(query);
			var table = result.data as GenericTableComponent;

			var content = ViewGeneric.GeneratePanel<Topic_SimpleViewModel>(typeof(TopicResponseViewModel))
									 .AddGenericTableComponent(table);									
			content.groupCode = 1;

			query = new QueryContent()
			{
				url = "Forum/Get",
				method = "GET"
			};

			query.SetDataObject(new { id = id });
			var forum = await Repository.QueryExpress<Forum>(query);

			if (forum.Type == Enums.ForumType.Social)
				content.AddButton(new ButtonComponent("addButton")
				{
					text = "Crear tema",
					color = "btn right " + StyleConfig.ButtonStyle_Create,
					icon = "add"
				});
			


			return View("Index", new Object[] { content, id, forum });
		}

		private async Task<ViewResult> GetViewResult_Admin(int id)
		{
			var query = new QueryContent()
			{
				dataConvert = "Table",
				url = "Topic/GetTopicsResponseViewModelByForum",
				dataTypeResult = "TopicResponseViewModel",
				viewModelConvert = "TopicViewModel",
				method = "GET"
			};

			query.SetDataObject(new { forum_id = id });

			var result = await Repository.Query(query);
			var table = result.data as GenericTableComponent;

			var content = ViewGeneric.GeneratePanel<TopicViewModel>(typeof(TopicResponseViewModel))
									 .AddGenericTableComponent(table)
									 .AddButton(new ButtonComponent("addButton")
									 {
										 text = "Crear tema",
										 color = "btn right " + StyleConfig.ButtonStyle_Create,
										 icon = "add"
									 });
			content.groupCode = 1;

			query = new QueryContent()
			{
				url = "Forum/Get",
				method = "GET"
			};

			query.SetDataObject(new { id = id });
			var forum = await Repository.QueryExpress<Forum>(query);

			return View("Index", new Object[] { content, id, forum });
		}

		public ActionResult TopicAdd(int forum_id)
		{
			return View("Add", forum_id);
		}

		public async Task<ActionResult> TopicEdit(int forum_id, int topic_id)
		{
			var query = new QueryContent()
			{
				url = "Topic",
				method = "GET",
			};
			query.SetDataObject(new { id = topic_id });

			var topic = await Repository.QueryExpress<Topic>(query);

			if (topic == null || topic.Id == 0)
				return RedirectToAction("Index", "Forum", new { id = forum_id });

			return View("Edit", new Object[] { forum_id, topic });
		}

		public async Task<ActionResult> TopicView(int forum_id, int topic_id)
		{
			var query = new QueryContent()
			{
				url = "Topic/GetTopicWithPosts",
				method = "GET",
			};

			query.SetDataObject(new { id = topic_id });
			var topic = await Repository.QueryExpress<Topic>(query);
			if (topic == null || topic.Id == 0)
				return RedirectToAction("Index", "Forum", new { id = forum_id });

			query = new QueryContent()
			{
				url = "Course/GetStudentsByForum",
				method = "GET",
			};

			query.SetDataObject(new { id = forum_id });
			var users = await Repository.QueryExpress<List<User>>(query);

			return View("TopicView", new Object[] { forum_id, topic, users });
		}		
	}
}