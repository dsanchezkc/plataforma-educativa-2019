﻿using Communication;
using Component.Button;
using Component.Select;
using Component.Table;
using Generic;
using Interactic;
using Model;
using SecurityGuard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador")]
	public class EstablishmentController : Controller
	{
		public async Task<ActionResult> Index()
		{
			var query = new QueryContent()
			{
				url = "EducationalEstablishment/GetEducationalEstablishmentsFilteredByUser_AdminAndSubAdmin",
				method = "GET",
				dataConvert = "Table",
				dataTypeResult = "EducationalEstablishment",
				viewModelConvert = (Authentication.currentAccess.Role == "Administrador") ? null : "EducationalEstablishment_SubAdminViewModel"
			};

			var result = await Repository.Query(query);
			var table = result.data as GenericTableComponent;

			var content = (Authentication.currentAccess.Role == "Administrador") ?
				ViewGeneric.GeneratePanel<EducationalEstablishmentViewModel>(typeof(EducationalEstablishment)) : ViewGeneric.GeneratePanel<EducationalEstablishment_SubAdminViewModel>(typeof(EducationalEstablishment));

			if (Authentication.currentAccess.Role == "Administrador")
				content.AddButton(new ButtonComponent("addButton")
				{
					text = "Crear establecimiento",
					color = "btn right " + StyleConfig.ButtonStyle_Create,
					icon = "add"
				});

			content.AddGenericTableComponent(table);				  
			content.groupCode = 1;

			return View(content);
		}

		public ActionResult Add()
		{
			return View();
		}

		public async Task<ActionResult> Edit(int id)
		{
			var query = new QueryContent()
			{
				url = "EducationalEstablishment",
				method = "GET",
			};
			query.SetDataObject(new { id = id });

			var establishment = await Repository.QueryExpress<EducationalEstablishment>(query);

			if (establishment == null || establishment.Id == 0)
				return RedirectToAction("Index", "Establishment");

			return View(establishment);
		}
	}
}