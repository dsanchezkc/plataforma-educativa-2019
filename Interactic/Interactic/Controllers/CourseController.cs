﻿using Communication;
using Component.Button;
using Component.Table;
using Generic;
using Interactic;
using Model;
using System.Threading.Tasks;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador")]
	public class CourseController : Controller
	{
		public async Task<ActionResult> Index()
		{
			var query = new QueryContent()
			{
				dataConvert = "Table",
				url = "Course",
				dataTypeResult = "Course",
				method = "GET"
			};

			var result = await Repository.Query(query);
			var table = result.data as GenericTableComponent;

			var content = ViewGeneric.GeneratePanel<CourseViewModel>(typeof(Course))
									.AddGenericTableComponent(table)
									.AddButton(new ButtonComponent("addButton")
									{
										text = "Crear curso",
										color = "btn right " + StyleConfig.ButtonStyle_Create,
										icon = "add"
									});
			content.groupCode = 1;

			return View(content);
		}

		public ActionResult Add()
		{
			return View();
		}

		public async Task<ActionResult> Edit(int id)
		{
			var query = new QueryContent()
			{
				url = "Course",
				method = "GET",
			};
			query.SetDataObject(new { id = id });

			var course = await Repository.QueryExpress<Course>(query);

			if (course == null || course.Id == 0)
				return RedirectToAction("Index", "Course");

			return View(course);
		}
	}
}