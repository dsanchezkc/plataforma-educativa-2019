﻿using Communication;
using Component.Button;
using Component.Pagination;
using Component.Select;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
	/// <summary>
	/// Esta clase es igual a SubUnityController con la diferencia que está basado solamente para
	/// alumnos
	/// </summary>
	[AuthorizeMVC(Roles = "Alumno")]
	public class ActivityController : Controller
	{
		public ActionResult Index()
		{
			return View(GetManagerSelect());
		}

		public static ManagerSelect GetManagerSelect()
		{

			return new ManagerSelect(url: "SubUnity/GetSubUnitiesByUnity")
				.AddSelect(new SelectComponent(
					typeModel: typeof(Course),
					code: SelectCode.Course,
					url: "Course/GetCoursesFilteredByUser",
					name: "Curso",
					placeholder: "Seleccione un curso"
				))
				.AddSelect(new SelectComponent(
					typeModel: typeof(Subject),
					code: SelectCode.Subject,
					parentCode: SelectCode.Course,
					url: "Subject/GetSubjectsByCourseFilteredByUser",
					name: "Asignatura",
					placeholder: "Selecciona una asignatura"
				))
				.AddSelect(new SelectComponent(
					typeModel: typeof(Unity),
					code: SelectCode.Unity,
					parentCode: SelectCode.Subject,
					url: "Unity/GetUnitiesBySubjectAndCourse",
					name: "Eje temático",
					placeholder: "Seleccione unidad",
					requiredCodes: new List<SelectCode>() { SelectCode.Course }
				));
		}
	}
}