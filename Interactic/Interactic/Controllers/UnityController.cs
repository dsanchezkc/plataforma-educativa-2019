﻿using Communication;
using Component.Button;
using Component.Pagination;
using Component.Select;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador, Profesor")]
	public class UnityController : Controller
	{
		public ActionResult Index()
		{
			var managerSelect = GetManagerSelect();

			var content = ViewGeneric.GeneratePanel<UnityViewModel>(typeof(Unity))
									 .AddManagerSelect(managerSelect)
									 .AddButton(new ButtonComponent("addButton")
									 {
										 text = "Crear unidad",
										 color = "btn right " + StyleConfig.ButtonStyle_Create,
										 icon = "add"
									 });
			content.groupCode = 1;

			return View(content);
		}

		public ActionResult Add()
		{
			return View(GetManagerSelect());
		}

		public async Task<ActionResult> Edit(int id)
		{
			var query = new QueryContent()
			{
				url = "Unity",
				method = "GET",
			};
			query.SetDataObject(new { id = id });

			var unity = await Repository.QueryExpress<Unity>(query);

			if (unity == null || unity.Id == 0)
				return RedirectToAction("Index", "Unity");

			return View(new Object[] { unity, GetManagerSelect() });
		}

		private ManagerSelect GetManagerSelect()
		{
			ManagerSelect select = null;

			switch (SecurityGuard.Authentication.currentAccess.Role)
			{
				case "Administrador":
				case "SubAdministrador":
					select = new ManagerSelect(url: "Unity/GetUnitiesBySubjectAndCourse")
								.AddSelect(new SelectComponent(
									typeModel: typeof(Course),
									code: SelectCode.Course,
									url: "Course",
									name: "Curso",
									placeholder: "Seleccione un curso"
								))
								.AddSelect(new SelectComponent(
									typeModel: typeof(Subject),
									code: SelectCode.Subject,
									parentCode: SelectCode.Course,
									url: "Subject/GetSubjectsByCourse",
									name: "Asignatura",
									placeholder: "Selecciona una asignatura"
								));
					break;

				case "Profesor":
					select = new ManagerSelect(url: "Unity/GetUnitiesBySubjectAndCourse")
								.AddSelect(new SelectComponent(
									typeModel: typeof(Course),
									code: SelectCode.Course,
									url: "Course/GetCoursesFilteredByUser",
									name: "Curso",
									placeholder: "Seleccione un curso"
								))
								.AddSelect(new SelectComponent(
									typeModel: typeof(Subject),
									code: SelectCode.Subject,
									parentCode: SelectCode.Course,
									url: "Subject/GetSubjectsByCourseFilteredByUser",
									name: "Asignatura",
									placeholder: "Selecciona una asignatura"
								));
					break;
			}

			return select;
		}
	}
}