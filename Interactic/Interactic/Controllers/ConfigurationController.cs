﻿using Communication;
using Interactic;
using Model;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace Interactic.Controllers
{
	[AuthorizeMVC]
	public class ConfigurationController : Controller
	{
		public async Task<ActionResult> Index()
		{
			var query = new QueryContent()
			{
				url = "User/GetCurrentUser",
				method = "GET",
			};

			var user = await Repository.QueryExpress<User>(query);

			if (user == null || user.UserName == null)
				return RedirectToAction("Index", "Home");

			return View(user);
		}
	}
}