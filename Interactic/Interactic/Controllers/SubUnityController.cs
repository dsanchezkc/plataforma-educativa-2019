﻿using Communication;
using Component.Button;
using Component.Pagination;
using Component.Select;
using Generic;
using Interactic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
	[AuthorizeMVC(Roles = "Administrador, SubAdministrador, Profesor")]
	public class SubUnityController : Controller
	{
		public ActionResult Index()
		{
			return View(GetManagerSelect());
		}

		public ActionResult Add()
		{
			return View(GetManagerSelect());
		}

		public async Task<ActionResult> Edit(int id)
		{
			var query = new QueryContent()
			{
				url = "SubUnity",
				method = "GET",
			};
			query.SetDataObject(new { id = id });

			var subUnity = await Repository.QueryExpress<SubUnity>(query);

			if (subUnity == null || subUnity.Id == 0)
				return RedirectToAction("Index", "SubUnity");

			return View(new Object[] { subUnity, GetManagerSelect() });
		}		

		public static ManagerSelect GetManagerSelect()
		{
			ManagerSelect select = null;

			switch (SecurityGuard.Authentication.currentAccess.Role)
			{
				case "Administrador":
				case "SubAdministrador":
					select = new ManagerSelect("SubUnity/GetSubUnitiesByUnity")
					.AddSelect
						(
							new SelectComponent(
							typeModel: typeof(Course),
							code: SelectCode.Course,
							url: "Course",
							name: "Curso",
							placeholder: "Seleccione un curso"
							)
						).AddSelect(new SelectComponent(
							typeModel: typeof(Subject),
							code: SelectCode.Subject,
							parentCode: SelectCode.Course,
							url: "Subject/GetSubjectsByCourse",
							name: "Asignatura",
							placeholder: "Selecciona una asignatura"
						)
						).AddSelect(new SelectComponent(
							typeModel: typeof(Unity),
							code: SelectCode.Unity,
							parentCode: SelectCode.Subject,
							url: "Unity/GetUnitiesBySubjectAndCourse",
							name: "Eje temático",
							placeholder: "Selecciona unidad",
							requiredCodes: new List<SelectCode>() { SelectCode.Course }
						));
					break;

				case "Profesor":
					select = new ManagerSelect(url: "SubUnity/GetSubUnitiesByUnity")
					.AddSelect(new SelectComponent(
						typeModel: typeof(Course),
						code: SelectCode.Course,
						url: "Course/GetCoursesFilteredByUser",
						name: "Curso",
						placeholder: "Seleccione un curso"
					))
					.AddSelect(new SelectComponent(
						typeModel: typeof(Subject),
						code: SelectCode.Subject,
						parentCode: SelectCode.Course,
						url: "Subject/GetSubjectsByCourseFilteredByUser",
						name: "Asignatura",
						placeholder: "Selecciona una asignatura"
					))
					.AddSelect(new SelectComponent(
						typeModel: typeof(Unity),
						code: SelectCode.Unity,
						parentCode: SelectCode.Subject,
						url: "Unity/GetUnitiesBySubjectAndCourse",
						name: "Eje temático",
						placeholder: "Seleccione unidad",
						requiredCodes: new List<SelectCode>() { SelectCode.Course }
					));
				break;
			}

			return select;
		}
	}
}