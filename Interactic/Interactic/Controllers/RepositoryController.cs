﻿using System.Web.Mvc;
using Generic;
using Communication;
using System.Threading.Tasks;

namespace Interactic.Controllers
{
	[AllowAnonymous]
	public class RepositoryController : Controller
	{
		[HttpPost]
		public async Task<ActionResult> Query(QueryContent content)
		{
			if (content == null)
				content = new QueryContent();

			var resultContent =await Repository.Query(content);

			var jsonResult = Json(Serializer.Serialize(resultContent), JsonRequestBehavior.AllowGet);
			jsonResult.MaxJsonLength = int.MaxValue;
			return jsonResult;
		}
	}	
}