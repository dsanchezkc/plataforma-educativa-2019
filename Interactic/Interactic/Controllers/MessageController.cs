﻿using Component.Pagination;
using Generic;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace Interactic.Controllers
{
    [AuthorizeMVC]
    public class MessageController : Controller
    {
        [HttpGet]
        [Route("Message/{id}")]
        public ActionResult Index(int? id)
        {
            if (id.HasValue)
                return View(id.Value);
            else
                return View();
        }
    }
}