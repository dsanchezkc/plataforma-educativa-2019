﻿/*
Descripción: Realiza una consula al repositorio del sistema

Modo de uso:
Repository.query(
{
    queryProcess: "CountryProcess.GetCountries",
    dataConvert: "Select",
    data: { id: 12 },
    success: function (json) {},
    error: function (xhr, status) {}
});

Resumen:
    queryProcess - Nombre de Objeto al que se le pedirá ejecutar
    dataConvert - El resultado que se obtenga será convertido en lo indicado acá
    data - Datos que se enviarán
    success: Función que se ejecuta cuando la petición fue exitosa
    error: Función que se ejecuta cuando hubo algún problema
*/

Repository = {

    urlServer: "",
    token:"",

	query: function (queryContent, resultToast = false) {
		if (queryContent.url === null || queryContent.url === "")
			return;

		if (queryContent.data != null)
			queryContent.data = JSON.stringify(queryContent.data);

		var data = JSON.stringify(queryContent);

		$.ajax(
			{
				url: '/Repository/Query/',
				data: data,
				type: 'POST',
				contentType: 'application/json',
				processData: true,
				traditional: true,
				success: function (json) {

					var resultContent = JSON.parse(json);

					switch (resultContent.statusResult.status) {
						case ResultContent.status.Success:

							if (typeof queryContent.success === 'function')
								queryContent.success(resultContent.data);

							if (resultToast)
								Materialize.toast(resultContent.statusResult.messageResult, 5000, 'teal darken-1');
							break;

						case ResultContent.status.Error:
							if (typeof queryContent.error === 'function')
								queryContent.error(resultContent.statusResult, Repository.getErrorsResumeHtml(resultContent.statusResult));
							if (resultToast)
								Materialize.toast(Repository.getErrorsResumeHtml(resultContent.statusResult), 5000, 'deep-orange accent-4');
							break;

						default:
							if (typeof queryContent.error === 'function')
								queryContent.error(resultContent.statusResult, Repository.getErrorsResumeHtml(resultContent.statusResult));
							if (resultToast)
								Materialize.toast(Repository.getErrorsResumeHtml(resultContent.statusResult), 5000, 'deep-orange accent-4');
					}
				},
				error: function (xhr, status) {
					var statusResult = { status: 500, messageResult: "Error del servidor, intente más tarde.", errors: [] };

					if (typeof queryContent.error === 'function')
						queryContent.error(statusResult, Repository.getErrorsResumeHtml(statusResult));

					if (resultToast)
						Materialize.toast(Repository.getErrorsResumeHtml(statusResult), 5000, 'deep-orange accent-4');
				}
			});
	},

	getErrorsResumeHtml: function (statusResult) {

		var structure = '<ul>';

		var errors = statusResult.errors;
		var messageResult = statusResult.messageResult;

		if (messageResult != null && messageResult != "")
			structure += '<li>'+messageResult+'</li>';

		for (var i = 0; i < errors.length; i++)
			structure += '<li>' + errors[i] + '</li>';

		structure += '</ul>';

		return structure;
    },

    queryApi: function (queryContent, resultToast = false, fullscreenloader = false) {

        if (queryContent.url === null || queryContent.url === "")
            return;

        if (queryContent.data != undefined && queryContent.data instanceof FormData)
            queryContent.processData = false;
        else
            queryContent.processData = true;

        var information = "";
        var headers = { "Authorization": "Bearer " + Repository.token };

        switch (queryContent.method.toLowerCase())
        {
            case "post": break;

            case "put":
                headers["X-HTTP-Method-Override"] = "PUT";
                queryContent.method = "POST";
            break;

            case "delete":
                information = Repository.stringFormat_URL(queryContent.data);
                headers["X-HTTP-Method-Override"] = "DELETE";
                queryContent.method = "GET";
                queryContent.data = null;
            break;

            case "get":
                information = Repository.stringFormat_URL(queryContent.data);
                queryContent.data = null;
            break;            
        }

        if (queryContent.data != null && queryContent.processData)
            queryContent.data = JSON.stringify(queryContent.data);

        var urlServerLocal = "";

        if (queryContent.urlServerTemporal === undefined || queryContent.urlServerTemporal === "")
            urlServerLocal = Repository.urlServer;
        else
        {
            urlServerLocal = queryContent.urlServerTemporal;
            queryContent.url = "";
        }

		if (fullscreenloader)
			Repository.generateFullscreenLoader();

        $.ajax(
            {
                url: urlServerLocal + queryContent.url + information,
                data: queryContent.data,
                type: queryContent.method,
                headers: headers,
                contentType: queryContent.processData ? "application/json; charset=utf-8" : false,
                processData: queryContent.processData,
                cache: queryContent.processData ? undefined : false,
                success: function (result)
				{
					if (resultToast)
						Materialize.toast("Proceso realizado.", 5000, 'teal darken-1');					

                    if (typeof queryContent.success === 'function')
                        queryContent.success(result);                    
                },
                error: function (xhr, status)
                {
                    if (typeof queryContent.error === 'function')
                        queryContent.error(status);

					if (resultToast)
						if ((xhr.responseText == null || xhr.responseText == "") || xhr.status==500)
							Materialize.toast("No es posible realizar esta operación ahora, intente más tarde.", 5000, 'deep-orange accent-4');
						else
							Materialize.toast(xhr.responseText.substring(1, xhr.responseText.length - 1), 5000, 'deep-orange accent-4');
				},
				complete: function (r, status)
				{
					var loader = $("#div-fullscreenloader");

					if (loader.length)
					{
						setTimeout(function () {
							loader.fadeOut("slow", function () {
								loader.remove();
							});
						}, 500);
											
					}
				}
            });
	},

	generateFullscreenLoader: function ()
	{
		var loader = $("#div-fullscreenloader");

		if (!loader.length)
			$("body").append('<div id="div-fullscreenloader"></div>');

		var loader = $("#div-fullscreenloader");

		loader.fakeLoader(
			{
				timeToHide: 300000,
				zIndex: "5000",//Default zIndex
				spinner: "spinner5",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
				bgColor: "rgba(45,47,64,0.5)", //Hex, RGB or RGBA colors
			});
	},

    stringFormat_URL: function (obj) {
        if (obj == null) return "";

        var _string = "/?";

        for (var property in obj)
            if (obj.hasOwnProperty(property))
                _string += property.toString() + "=" + obj[property] + "&";       

        return _string;
    }

};

DownloadManager =
{
	//Forma de descarga de archivos en base64
	//Requiere por obligación la libreria FileSaver.js
	//https://github.com/eligrey/FileSaver.js/
	downloadBase64: function (data, name, type) {
		saveAs(DownloadManager.base64toBlob(data, type), name);
	},

	base64toBlob: function (b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, { type: contentType });
		return blob;
	}
}

ResultContent = {
	status: { Success : 200 , Error : 500 }
};