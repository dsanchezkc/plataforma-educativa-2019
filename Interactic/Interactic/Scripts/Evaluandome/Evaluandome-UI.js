﻿EventDispatcher = {

    events: {},

    on: function (event, callback) {
        var handlers = this.events[event] || [];
        handlers.push(callback);
        this.events[event] = handlers;
    },

    trigger: function (event, data) {
        var handlers = this.events[event];

        if (!handlers || handlers.length < 1)
            return;

        [].forEach.call(handlers, function (handler) {
            handler(data);
        });
    }
};

GenericSystemSelect = {

    //Evento el cual se activa cuando se selecciona el último Select de un grupo de Selects
    lastSelectedEvent: "LastSelected-ManagerSelect",
    
    //Descripción:  Carga el contenido del Select - Toda información que tenía almacenada anteriormente es borrada.
    //select: Objeto del DOM que hace referencia al elemento Select
    //data: Datos que debe cargar dentro de el. La estructura de carga corresponde a la clase SelectData.cs.
    //      En este caso data es un List de SelectData - List<SelectData>
    populateDropdown: function (select, data) {
        select.find('option').not(':first').remove();
        $.each(data, function (id, option) {
            select.append($('<option></option>').val(option.value).html(option.text));
        });

        select.material_select();
    },

    //Descripción: Obtiene el componente select dentro del DOM
    // code(Component-Code): Código del component
    // groupCode: Código de grupo (Grupo de Selects)
    getSelect: function (code, groupCode = null) {
        var element = null;

        $("select").each(function (index) {
            if (groupCode == null) {
                if ($(this).attr("component-code") == code) {
                    element = $(this);
                    return false;
                }
            }
            else
                if ($(this).attr("component-code") == code && $(this).attr("group-code") == groupCode) {
                    element = $(this);
                    return false;
                }
        });

        return element;
    },

    //Descripción: Obtiene un grupo de componentes select dentro del DOM
    // code(Component-Code): Código del component
    // groupCode: Código de grupo (Grupo de Selects)
    getSelects: function (code, groupCode = null) {
        var elements = [];

        $("select").each(function (index) {
            if (groupCode == null) {
                if ($(this).attr("component-code") == code)
                    elements.push($(this));
            }
            else
                if ($(this).attr("component-code") == code && $(this).attr("group-code") == groupCode)
                    elements.push($(this));
        });

        return elements;
	},

	getSelectsByGroupCode: function (groupCode) {
		var elements = [];

		$("select").each(function (index) {
			if ($(this).attr("group-code") == groupCode)
				elements.push($(this));
		});

		return elements;
	}
	,

    //Descripción: Deshabilita todos los Selects que dependen de otros selects
    // components: Lista de SelectComponent.cs - Component.Select.SelectComponent
    // code(Component-Code): Código del component Padre
    // groupCode: Código de grupo (Grupo de Selects)
    disableSelectDependent: function (components, code, groupCode) {
        for (var i = 0; i < components.length; i++) {
            if (components[i].parentCode == code) {
                GenericSystemSelect.getSelect(components[i].code, groupCode).prop('disabled', true);
                GenericSystemSelect.getSelect(components[i].code, groupCode).prop("selectedIndex", 0);
                GenericSystemSelect.getSelect(components[i].code, groupCode).material_select();
                GenericSystemSelect.disableSelectDependent(components, components[i].code, groupCode);
            }
        }
    },

    //Descripción: Cambia el estado de los selects dependientes cuando se realizar un cambio en el grupo
    //             También realiza las llamadas al sistema para cargar los Selects que sean activados.
    // components: Lista de SelectComponent.cs - Component.Select.SelectComponent
    // groupCode: Código de grupo (Grupo de Selects)
    ChangeSelect: function (components, groupCode) {
        $("select").change(function () {
            var value = $(this).val();
			var code = $(this).attr("component-code");
			var groupCodeSelect = $(this).attr("group-code");

			if (groupCode != groupCodeSelect) return;

            GenericSystemSelect.disableSelectDependent(components, code, groupCode);

            if (value == null || value == 0)
                return;

            for (var i = 0; i < components.length; i++) {
                if (components[i].parentCode == code)
                    GenericSystemSelect.loadComponent(components[i], groupCode);
            }

            if (components.length != 0) {
                var element = components[components.length - 1];

				if (element.code == code)
					EventDispatcher.trigger(GenericSystemSelect.lastSelectedEvent, { groupCode: groupCode, componentCode: code, components: components });
				
				}
        });
    },

    //Descripción: Carga un componente Select con su información acorde a lo recibido por el llamado al Sistema.
    // component: SelectComponent.cs - Component.Select.SelectComponent
    // groupCode: Código de grupo (Grupo de Selects)
    loadComponent: function (component, groupCode = null) {
        var data = {};

        for (var i = 0; i < component.requiredCodes.length; i++)
            data[component.requiredCodes[i].toLowerCase()+"_id"] = GenericSystemSelect.getSelect(component.requiredCodes[i], groupCode).val();

        Repository.query(
            {
                url: component.url,
                dataConvert: "Select",
                method: "GET",
                dataTypeResult: component.typeModel,
                data: data,
                success: function (data) {
                    GenericSystemSelect.getSelect(component.code, groupCode).prop('disabled', false);
                    GenericSystemSelect.populateDropdown(GenericSystemSelect.getSelect(component.code, groupCode), data);
                },
                error: function (error, errorHtml) {
					
                }
            });
	}	
};

GenericSystemTable = {

    paginationEventActive: false,
    paginationEvent: "PaginationEvent",

    //Descripción:  Carga el contenido de la tabla - Toda información que tenía almacenada anteriormente es borrada.
    //groupCode:  Código de grupo (Grupo de Tables)
    //data: Datos que debe cargar dentro de el. La estructura de carga corresponde a la clase GenericTableComponent.cs.
    populateTable: function (groupCode, data) {
        var table = GenericSystemTable.getTable(groupCode);
        var alert = GenericSystemTable.getAlert(groupCode);

        if (data.genericTable.content.length == 0) {
            table.addClass("hide");
            alert.removeClass("hide");
        }
        else {
            table.removeClass("hide");
            alert.addClass("hide");
        }

        GenericSystemTable.addHeader(groupCode, data.genericTable.headers);
        GenericSystemTable.populateTable_TBody(groupCode, data.genericTable.content);

        table.children("tfoot").children("tr").children("th").prop("colspan", data.genericTable.headers.length);
        GenericSystemTable.populatePagination(groupCode, data.pagination);
    },

    //Descripción: Obtiene el componente Table dentro del DOM
    //groupCode: Código de grupo (Grupo de Tables)
    getTable: function (groupCode) {
        var element = null;

        $("table").each(function (index) {
            if ($(this).attr("group-code") == groupCode) {
                element = $(this);
                return false;
            }
        });

        return element;
    },

    //Descripción: Carga el contenido de la tabla(Sección TBody) - Toda información que tenía almacenada anteriormente es borrada.
    //groupCode: Código de grupo (Grupo de Tables)
    //content: Información a cargar por Fila - List<ContentAssociated> 
    populateTable_TBody: function (groupCode, content) {
        var tBody = GenericSystemTable.getTable(groupCode).children('tbody');
        tBody.html('');

        for (var i = 0; i < content.length; i++)
            GenericSystemTable.addRow(groupCode, content[i].group, content[i].id);
    },

    //Descripción: Carga el contenido de la tabla(Sección THead) - Toda información que tenía almacenada anteriormente es borrada.
    //groupCode: Código de grupo (Grupo de Tables)
    //headers: Información a cargar - List<String> 
    addHeader: function (groupCode, headers) {
        var tHead = GenericSystemTable.getTable(groupCode).children('thead');
        tHead.html('');

        var head = '<tr>';
        for (var i = 0; i < headers.length; i++)
            head += '<th class="center-align">' + headers[i] + '</th>';
        head += '</tr>';

        tHead.append(head);
    },

    //Descripción: Carga el contenido de una fila en la tabla.
    //groupCode: Código de grupo (Grupo de Tables)
    //group: Grupo de información a cargar - List<GroupInformation> 
    addRow: function (groupCode, group, idValue) {
        var tBody = GenericSystemTable.getTable(groupCode).children('tbody');

        var row = '<tr>';

        for (var i = 0; i < group.length; i++) {
            var type = group[i].itemsTypeName;
            var autoMargin = type == "String" ? "" : "margin:0 auto;";

            row += '<td>' +
                '<div class="valign-wrapper">';

            for (var j = 0; j < group[i].items.length; j++) {
                var item = group[i].items[j];

                row += '<div style="' + autoMargin + '">';
                switch (type) {
                    case "String":
                        row += item;
                        break;
                      
                    case "Button":
                        row += '<button class="btn-floating ' + item.color + '" id-value="' + idValue + '" group-code="' + groupCode + '" component-code="' + item.componentCode + '">' +
                            '<i class="material-icons">' + item.icon + '</i>' +
                            '</button>';
                        break;
                }
                row += "</div>";
            }
            row += '</div>' +
                '</td>';
        }
        row += '</tr>';

        tBody.append(row);
    },

    getAlert: function (groupCode) {
        var element = null;

        $(".alert").each(function (index) {
            if ($(this).attr("group-code") != null && $(this).attr("group-code") == groupCode) {
                element = $(this);
                return false;
            }
        });

        return element;
    },
    populatePagination: function (groupCode, data) {
        var pagination = GenericSystemTable.getPagination(groupCode);
        pagination.html('');

        if (data.endPag == 0) return;
        var structure =
            '<li class="waves-effect">' +
            '<a class="paginationButton" id-value="' + data.backPage + '" group-code="' + groupCode + '">' +
            '<i class="material-icons">chevron_left</i></a></li>';

        for (var i = data.startPag; i <= data.endPag; i++) {
            var colorSelected = i == data.pag ? "active light-blue darken-2" : "waves-effect";
            structure +=
                '<li class="' + colorSelected + '">' +
                '<a class="paginationButton" id-value="' + i + '" group-code="' + groupCode + '">' + (i + 1) + '</a></li>';
        }

        structure +=
            '<li class="waves-effect">' +
            '<a class="paginationButton" id-value="' + data.nextPage + '" group-code="' + groupCode + '">' +
            '<i class="material-icons">chevron_right</i></a></li>';

        pagination.append(structure);

        if (data.pages >= 2)
            pagination.removeClass("hide");
        else
            pagination.addClass("hide");
    },

    getPagination: function (groupCode)
    {
        var element = null;

        $(".pagination").each(function (index) {
            if ($(this).attr("group-code") != null && $(this).attr("group-code") == groupCode) {
                element = $(this);
                return false;
            }
        });

        return element;
    },

    paginationEvent_Init: function ()
    {
        if (!GenericSystemTable.paginationEventActive) {
            GenericSystemTable.paginationEventActive = true;

            $(document.body).on('click', '.paginationButton', function () {
                var pag = $(this).attr("id-value");
                var groupCode = $(this).attr("group-code");

                EventDispatcher.trigger(GenericSystemTable.paginationEvent, { groupCode: groupCode, pag: pag });
            });
        }
    }
};

GenericSystemButton = {

    buttonEventActive: false,
    buttonEvent: "ButtonEvent",

    buttonEvent_Init: function () {

        if (!GenericSystemButton.buttonEventActive) {
            GenericSystemButton.buttonEventActive = true;

            $(document.body).on('click', 'button', function () {
                var idValue = $(this).attr("id-value");
                var groupCode = $(this).attr("group-code");
                var componentCode = $(this).attr("component-code");

                EventDispatcher.trigger(GenericSystemButton.buttonEvent, { idValue: idValue, groupCode: groupCode, componentCode: componentCode });
            });
        }

    }

};

/******************Iniciando Eventos******************/
$(document).ready(function ()
{
    GenericSystemTable.paginationEvent_Init();
    GenericSystemButton.buttonEvent_Init();
});


/********************** Materialize ********************/

/**************************
	* Auto complete plugin  *
	*************************/
$.fn.autocompleteV2 = function (options) {
	// Defaults
	var defaults = {
		data: {},
		limit: Infinity,
		onAutocomplete: null,
		minLength: 1
	};

	options = $.extend(defaults, options);

	return this.each(function () {
		var $input = $(this);
		var data = options.data,
			count = 0,
			activeIndex = -1,
			oldVal,
			$inputDiv = $input.closest('.input-field'); // Div to append on

		// Check if data isn't empty
		if (!$.isEmptyObject(data)) {
			var $autocomplete = $('<ul class="autocomplete-content dropdown-content"></ul>');
			var $oldAutocomplete;

			// Append autocomplete element.
			// Prevent double structure init.
			if ($inputDiv.length) {
				$oldAutocomplete = $inputDiv.children('.autocomplete-content.dropdown-content').first();
				if (!$oldAutocomplete.length) {
					$inputDiv.append($autocomplete); // Set ul in body
				}
			} else {
				$oldAutocomplete = $input.next('.autocomplete-content.dropdown-content');
				if (!$oldAutocomplete.length) {
					$input.after($autocomplete);
				}
			}
			if ($oldAutocomplete.length) {
				$autocomplete = $oldAutocomplete;
			}

			// Highlight partial match.
			var highlight = function (string, $el) {
				var img = $el.find('img');
				var matchStart = $el.text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
					matchEnd = matchStart + string.length - 1,
					beforeMatch = $el.text().slice(0, matchStart),
					matchText = $el.text().slice(matchStart, matchEnd + 1),
					afterMatch = $el.text().slice(matchEnd + 1);
				$el.html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
				if (img.length) {
					$el.prepend(img);
				}
			};

			// Reset current element position
			var resetCurrentElement = function () {
				activeIndex = -1;
				$autocomplete.find('.active').removeClass('active');
			}

			// Remove autocomplete elements
			var removeAutocomplete = function () {
				$autocomplete.empty();
				resetCurrentElement();
				oldVal = undefined;
			};

			$input.off('blur.autocomplete').on('blur.autocomplete', function () {
				removeAutocomplete();
			});

			// Perform search
			$input.off('keyup.autocomplete focus.autocomplete').on('keyup.autocomplete focus.autocomplete', function (e) {
				// Reset count.
				count = 0;
				var val = $input.val().toLowerCase();

				// Don't capture enter or arrow key usage.
				if (e.which === 13 ||
					e.which === 38 ||
					e.which === 40) {
					return;
				}


				// Check if the input isn't empty
				if (oldVal !== val) {
					removeAutocomplete();

					if (val.length >= options.minLength) {
						for (var key in data) {
							if (data.hasOwnProperty(key) &&
								key.toLowerCase().indexOf(val) !== -1 &&
								key.toLowerCase() !== val) {
								// Break if past limit
								if (count >= options.limit) {
									break;
								}

								var autocompleteOption = $('<li id-value="' + data[key] + '"></li>');
								if (!!data[key]) {
									autocompleteOption.append('<span>' + key + '</span>');
								} else {
									autocompleteOption.append('<span>' + key + '</span>');
								}

								$autocomplete.append(autocompleteOption);
								highlight(val, autocompleteOption);
								count++;
							}
						}
					}
				}

				// Update oldVal
				oldVal = val;
			});

			$input.off('keydown.autocomplete').on('keydown.autocomplete', function (e) {
				// Arrow keys and enter key usage
				var keyCode = e.which,
					liElement,
					numItems = $autocomplete.children('li').length,
					$active = $autocomplete.children('.active').first();

				// select element on Enter
				if (keyCode === 13 && activeIndex >= 0) {
					liElement = $autocomplete.children('li').eq(activeIndex);
					if (liElement.length) {
						liElement.trigger('mousedown.autocomplete');
						e.preventDefault();
					}
					return;
				}

				// Capture up and down key
				if (keyCode === 38 || keyCode === 40) {
					e.preventDefault();

					if (keyCode === 38 &&
						activeIndex > 0) {
						activeIndex--;
					}

					if (keyCode === 40 &&
						activeIndex < (numItems - 1)) {
						activeIndex++;
					}

					$active.removeClass('active');
					if (activeIndex >= 0) {
						$autocomplete.children('li').eq(activeIndex).addClass('active');
					}
				}
			});

			// Set input value
			$autocomplete.on('mousedown.autocomplete touchstart.autocomplete', 'li', function () {
				var text = $(this).text().trim();
				$input.val(text);
				$input.trigger('change');
				removeAutocomplete();

				// Handle onAutocomplete callback.
				if (typeof (options.onAutocomplete) === "function") {
					options.onAutocomplete.call(this, { text: text, value: $(this).attr("id-value") });
				}
			});
		}
	});
};

/********************************** Componentes Especiales *************************************/

$.fn.generateSelect = function (items, getName, getValue)
{
	if (typeof getName != 'function')
		getName = function (item) { return item.Name; };

	if (typeof getValue != 'function')
		getValue = function (item) { return item.Id; };

	var option = '';

	for (var i = 0; i < items.length; i++)
		option += '<option value="' + getValue(items[i]) + '">' + getName(items[i]) + '</option>';	

	$(this).find('option').not(':first').remove();
	$(this).find('option:first').prop('selected', 'selected');
	$(this).append(option);
}


$.fn.material_chip_async = function (options) {
	$(this).material_chip();
	var input = $(this).find('input');
	var callId = 0;
	var component = $(this);
	var lastDataAutoCompleted = null;

	input.focusout(function () {
		input.val("");

		if (options.cancelled != undefined)
			options.cancelled();
	});

	component.on('chip.add', function (e, chip) {
		var dataValues = component.material_chip('data');
		var contain = false;

		if (lastDataAutoCompleted != null)
			for (var i = lastDataAutoCompleted.length - 1; i >= 0; i--)
				if (chip.tag == lastDataAutoCompleted[i]) {
					contain = true;
					break;
				}

		if (lastDataAutoCompleted == null || !contain) {
			for (var i = dataValues.length - 1; i >= 0; i--)
				if (dataValues[i].tag === chip.tag)
					dataValues.splice(i, 1);

			update(dataValues);
		}

	});

	var inputEvent = function () {
		callId++;

		if (input.val().trim().length >= options.minLength) {
			var localId = callId;
			var localInput = input.val();

			if (options.starting != undefined)
				options.starting();

			setTimeout(function () {
				if (localId != callId) return;

				Repository.queryApi(
					{
						url: options.url,
						method: "GET",
						data: { search: localInput },
						success: function (data) {
							if (localId != callId) return;
							autoComplete(data);
						}
					});


			}, options.delay);
		}
	};

	input.keyup(inputEvent);

	var autoComplete = function (newData) {
		var oldData = component.material_chip('data');
		var inputValue = input.val();
		var newDataFormated = {};
		lastDataAutoCompleted = newData;

		for (var i = 0; i < newData.length; i++)
			newDataFormated[newData[i]] = null;

		component.material_chip({
			data: oldData,
			autocompleteOptions: {
				data: newDataFormated,
				limit: 20,
				minLength: 2
			}
		});

		input = component.find('input');
		input.keyup(inputEvent);
		input.val(inputValue);
		input.focus();
		input.focusout(function () {
			input.val("");
			if (options.cancelled != undefined)
				options.cancelled();
		});


		if (options.completed != undefined)
			options.completed();
	};

	var update = function (selectedData) {
		var inputValue = input.val();
		var newDataFormated = {};

		for (var i = 0; i < lastDataAutoCompleted.length; i++)
			newDataFormated[lastDataAutoCompleted[i]] = null;

		component.material_chip({
			data: selectedData,
			autocompleteOptions: {
				data: newDataFormated,
				limit: 20,
				minLength: 2
			}
		});

		input = component.find('input');
		input.keyup(inputEvent);
		input.val(inputValue);
		input.focus();
		input.focusout(function () {
			input.val("");
			if (options.cancelled != undefined)
				options.cancelled();
		});
	}
}