/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

'use strict';

$.fn.ButtonClick = function (component,simbols)
{
	



	console.log(simbols)
	var textArea = component.closest('tbody').find('textArea');
	textArea.focus();
	textArea.val($.fn.procesar_seleccion_HTML(textArea, simbols));
	textArea.keyup();
}

$.fn.procesar_seleccion_HTML= function(component, simbols) {
	var texto = component.val();
	var inicio = component[0].selectionStart;
	var fin = component[0].selectionEnd;	
	return $.fn.procesar_seleccion(texto, inicio, fin, simbols);
}

$.fn.procesar_seleccion = function (texto, inicio_seleccion, fin_seleccion, simbols) {
	console.log(texto);
	var longitud = texto.length;

	var inicio = texto.slice(0, inicio_seleccion);
	var medio = texto.slice(inicio_seleccion, fin_seleccion);
	var fin = texto.slice(fin_seleccion, longitud);	
	
	return inicio + simbols.replace("$", medio) + fin;
}

CKEDITOR.dialog.add( 'mathjax', function( editor ) {

	var preview,
		lang = editor.lang.mathjax;

	var getCustomPanel = function ()
	{
		var button = '<button onclick="$.fn.ButtonClick($(this),\'{$}\');">Agrupar ()</button>';
		return button;
	};	

	return {
		title: lang.title,
		minWidth: 350,
		minHeight: 100,
		contents: [
			{
				id: 'info',
				elements: [
					{
						id: 'documentation_2',
						type: 'html',
						html:
						'<div style="width:100%;text-align:right;margin:-8px 0 10px">' +
							getCustomPanel()+
						'</div>'
					},
					{
						id: 'equation',
						type: 'textarea',
						label: lang.dialogInput,

						onLoad: function() {
							var that = this;

							if (!(CKEDITOR.env.ie && CKEDITOR.env.version == 8)) {
								
								this.getInputElement().on( 'keyup', function() {
									// Add \( and \) for preview.
									preview.setValue( '\\(' + that.getInputElement().getValue() + '\\)' );
								});

								$(document).on('keyup', that.getInputElement(), function () {
									preview.setValue('\\(' + that.getInputElement().getValue() + '\\)');
								});

							}
						},

						setup: function( widget ) {
							// Remove \( and \).
							this.setValue( CKEDITOR.plugins.mathjax.trim( widget.data.math ) );
						},

						commit: function( widget ) {
							// Add \( and \) to make TeX be parsed by MathJax by default.
							widget.setData( 'math', '\\(' + this.getValue() + '\\)' );
						}
					},
					{
						id: 'documentation',
						type: 'html',
						html:
							'<div style="width:100%;text-align:right;margin:-8px 0 10px">' +
								'<a class="cke_mathjax_doc" href="' + lang.docUrl + '" target="_black" style="cursor:pointer;color:#00B2CE;text-decoration:underline">' +
									lang.docLabel +
								'</a>' +
							'</div>'
					},
					( !( CKEDITOR.env.ie && CKEDITOR.env.version == 8 ) ) && {
						id: 'preview',
						type: 'html',
						html:
							'<div style="width:100%;text-align:center;">' +
								'<iframe style="border:0;width:0;height:0;font-size:20px" scrolling="no" frameborder="0" allowTransparency="true" src="' + CKEDITOR.plugins.mathjax.fixSrc + '"></iframe>' +
							'</div>',

						onLoad: function() {
							var iFrame = CKEDITOR.document.getById( this.domId ).getChild( 0 );
							preview = new CKEDITOR.plugins.mathjax.frameWrapper( iFrame, editor );
						},

						setup: function( widget ) {
							preview.setValue( widget.data.math );
						}
					}
				]
			}
		]
	};
} );
