﻿$.fn.InitStepper = function (config)
{
	$(".stageStepper").each(function () { $(this).hide(); });
    StepperExpress.GenerateBar($(this), config.stages);
    StepperExpress.AsignInformation(config.stages);
	$.fn.Stages = config.stages;

	$(".next-stage").each(function ()
	{
		if ($(this).attr("n-stage") == null)
			$(this).html("Finalizar");
		else
			$(this).html("Continuar");
	});

	for (var i = 0; i < config.stages.length; i++)
		if (i == 0)
			$("#" + config.stages[i].id).show();
		else
		{
			$("#" + config.stages[i].id).hide();
			$("#tab_" + config.stages[i].id).removeClass("active");
		}	
};

$('body').on('click', '.next-stage', function ()
{
    if (!$(this).closest("form").parsley().validate()) return;

    var nextStage = $(this).attr("n-stage");
    var currentStage = $(this).attr("c-stage");

    if (nextStage != undefined && currentStage != undefined)
    {
		$("#tab_" + nextStage).addClass("active");

		$("#" + currentStage).hide();
		$("#" + nextStage).show();

        EventDispatcher.trigger(StepperExpress.ChangedStageEvent, { action: "next", id: nextStage });
        return;
    }

    EventDispatcher.trigger(StepperExpress.FinishedLastStageEvent, null);
});

$('body').on('click', '.back-stage', function ()
{ 
    var backStage = $(this).attr("b-stage");
    var currentStage = $(this).attr("c-stage");
    
    if (backStage != undefined && currentStage != undefined)
	{
		$("#tab_" + currentStage).removeClass("active");
		$("#tab_" + backStage).addClass("active");

		$("#" + currentStage).hide();
		$("#" + backStage).show();

        EventDispatcher.trigger(StepperExpress.ChangedStageEvent, { action: "back", id: backStage });
    }
});

//El sistema máximo soporta 12 títulos
StepperExpress =
{
    ChangedStageEvent: "ChangedStageEvent",
    FinishedLastStageEvent: "FinishedLastStageEvent",

    GenerateBar: function (element, stages)
    {
		element.attr("current-stage", 1);
		element.addClass("row line-steps");
		var structure = '';		
		var spaces = [];

		switch (stages.length)
		{
			case  1: spaces = [12]; break;
			case  2: spaces = [6, 6]; break;
			case  3: spaces = [4, 4, 4]; break;
			case  4: spaces = [3, 3, 3, 3]; break;
			case  5: spaces = [3, 2, 2, 2, 3]; break;
			case  6: spaces = [2, 2, 2, 2, 2, 2]; break;
			case  7: spaces = [2, 2, 1, 2, 1, 2, 2]; break;
			case  8: spaces = [2, 1, 2, 1, 1, 2, 1, 2]; break;
			case  9: spaces = [2, 1, 1, 1, 2, 1, 1, 1, 2]; break;
			case 10: spaces = [2, 1, 1, 1, 1, 1, 1, 1, 1, 2]; break;
			case 11: spaces = [1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1]; break;
			case 12: spaces = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]; break;
		}		

		for (var i = 0; i < stages.length; i++)
			structure += '<div id="tab_' + stages[i].id + '" div-stage="' + stages[i].id + '" class="col-md-' + spaces[i] + ' column-step ' + (i == 0 ? (stages.length == 1? "active":"start active ") : " ") + (i + 1 == stages.length && stages.length!=1 ? "finish " : " ") + '">' +
				'<div class="step-number"><i class="' + (stages[i].icon == undefined ?"fa fa-pencil": stages[i].icon) + '"></i></div>' +
				'<div class="step-title">' + stages[i].title + '</div>' +
				'<div class="step-info"></div>' +
				'</div>';
		
		element.html("");
        element.append(structure);
    },
    AsignInformation: function (stages)
	{
		for (var i = 0; i < stages.length; i++)
			if (i + 1 == stages.length)
				$("#" + stages[i].id).find(".next-stage").removeAttr("n-stage");
			else
				$("#" + stages[i].id).find(".next-stage").attr("n-stage", stages[i + 1].id);

        for (var i = stages.length - 1; i >= 1; i--)
            $("#" + stages[i].id).find(".back-stage").attr("b-stage", stages[i - 1].id);

        for (var i = 0; i < stages.length; i++)
        {
            var nextStage = $("#" + stages[i].id).find(".next-stage");
            if (nextStage != null) nextStage.attr("c-stage", stages[i].id);

            var backStage = $("#" + stages[i].id).find(".back-stage");
            if (backStage != null) backStage.attr("c-stage", stages[i].id);
        }        
    }
}