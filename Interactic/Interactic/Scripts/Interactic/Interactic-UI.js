﻿EventDispatcher = {

    events: {},

    on: function (event, callback) {
        var handlers = this.events[event] || [];
        handlers.push(callback);
        this.events[event] = handlers;
    },

    trigger: function (event, data) {
        var handlers = this.events[event];

        if (!handlers || handlers.length < 1)
            return;

        [].forEach.call(handlers, function (handler) {
            handler(data);
        });
    }
};

GenericSystemSelect = {

    //Evento el cual se activa cuando se selecciona el último Select de un grupo de Selects
    lastSelectedEvent: "LastSelected-ManagerSelect",
    
    //Descripción:  Carga el contenido del Select - Toda información que tenía almacenada anteriormente es borrada.
    //select: Objeto del DOM que hace referencia al elemento Select
    //data: Datos que debe cargar dentro de el. La estructura de carga corresponde a la clase SelectData.cs.
    //      En este caso data es un List de SelectData - List<SelectData>
    populateDropdown: function (select, data) {
        select.find('option').not(':first').remove();
        $.each(data, function (id, option) {
            select.append($('<option></option>').val(option.value).html(option.text));
        });

        //select.material_select();
    },

    //Descripción: Obtiene el componente select dentro del DOM
    // code(Component-Code): Código del component
    // groupCode: Código de grupo (Grupo de Selects)
    getSelect: function (code, groupCode = null) {
        var element = null;

        $("select").each(function (index) {
            if (groupCode == null) {
                if ($(this).attr("component-code") == code) {
                    element = $(this);
                    return false;
                }
            }
            else
                if ($(this).attr("component-code") == code && $(this).attr("group-code") == groupCode) {
                    element = $(this);
                    return false;
                }
        });

        return element;
    },

    //Descripción: Obtiene un grupo de componentes select dentro del DOM
    // code(Component-Code): Código del component
    // groupCode: Código de grupo (Grupo de Selects)
    getSelects: function (code, groupCode = null) {
        var elements = [];

        $("select").each(function (index) {
            if (groupCode == null) {
                if ($(this).attr("component-code") == code)
                    elements.push($(this));
            }
            else
                if ($(this).attr("component-code") == code && $(this).attr("group-code") == groupCode)
                    elements.push($(this));
        });

        return elements;
	},

	getSelectsByGroupCode: function (groupCode) {
		var elements = [];

		$("select").each(function (index) {
			if ($(this).attr("group-code") == groupCode)
				elements.push($(this));
		});

		return elements;
	}
	,

    //Descripción: Deshabilita todos los Selects que dependen de otros selects
    // components: Lista de SelectComponent.cs - Component.Select.SelectComponent
    // code(Component-Code): Código del component Padre
    // groupCode: Código de grupo (Grupo de Selects)
    disableSelectDependent: function (components, code, groupCode) {
        for (var i = 0; i < components.length; i++) {
            if (components[i].parentCode == code) {
                GenericSystemSelect.getSelect(components[i].code, groupCode).prop('disabled', true);
                GenericSystemSelect.getSelect(components[i].code, groupCode).prop("selectedIndex", 0);
                //GenericSystemSelect.getSelect(components[i].code, groupCode).material_select();
                GenericSystemSelect.disableSelectDependent(components, components[i].code, groupCode);
            }
        }
    },

    //Descripción: Cambia el estado de los selects dependientes cuando se realizar un cambio en el grupo
    //             También realiza las llamadas al sistema para cargar los Selects que sean activados.
    // components: Lista de SelectComponent.cs - Component.Select.SelectComponent
    // groupCode: Código de grupo (Grupo de Selects)
    ChangeSelect: function (components, groupCode) {
        $("select").change(function () {
            var value = $(this).val();
			var code = $(this).attr("component-code");
			var groupCodeSelect = $(this).attr("group-code");

			if (groupCode != groupCodeSelect) return;

            GenericSystemSelect.disableSelectDependent(components, code, groupCode);

            if (value == null || value == 0)
                return;

            for (var i = 0; i < components.length; i++) {
                if (components[i].parentCode == code)
                    GenericSystemSelect.loadComponent(components[i], groupCode);
            }

            if (components.length != 0) {
                var element = components[components.length - 1];

				if (element.code == code)
					EventDispatcher.trigger(GenericSystemSelect.lastSelectedEvent, { groupCode: groupCode, componentCode: code, components: components });
				
				}
        });
    },

    //Descripción: Carga un componente Select con su información acorde a lo recibido por el llamado al Sistema.
    // component: SelectComponent.cs - Component.Select.SelectComponent
    // groupCode: Código de grupo (Grupo de Selects)
    loadComponent: function (component, groupCode = null) {
        var data = {};

        for (var i = 0; i < component.requiredCodes.length; i++)
            data[component.requiredCodes[i].toLowerCase()+"_id"] = GenericSystemSelect.getSelect(component.requiredCodes[i], groupCode).val();

        Repository.query(
            {
                url: component.url,
                dataConvert: "Select",
                method: "GET",
                dataTypeResult: component.typeModel,
                data: data,
                success: function (data) {
                    GenericSystemSelect.getSelect(component.code, groupCode).prop('disabled', false);
					GenericSystemSelect.populateDropdown(GenericSystemSelect.getSelect(component.code, groupCode), data);

					if (component.defaultValue != null && component.defaultValue != "")
					{
						GenericSystemSelect.getSelect(component.code, groupCode).val(component.defaultValue);
						GenericSystemSelect.getSelect(component.code, groupCode).trigger("change");
						component.defaultValue = null;
					}

                },
                error: function (error, errorHtml) {
					
                }
            });
	}	
};

GenericSystemTable = {

    paginationEventActive: false,
    paginationEvent: "PaginationEvent",

    //Descripción:  Carga el contenido de la tabla - Toda información que tenía almacenada anteriormente es borrada.
    //groupCode:  Código de grupo (Grupo de Tables)
    //data: Datos que debe cargar dentro de el. La estructura de carga corresponde a la clase GenericTableComponent.cs.
    populateTable: function (groupCode, data) {

        var table = GenericSystemTable.getTable(groupCode);
        var alert = GenericSystemTable.getAlert(groupCode);

        if (data.genericTable.content.length == 0) {
            table.addClass("hide");
            alert.removeClass("hide");
        }
        else {
            table.removeClass("hide");
            alert.addClass("hide");
        }

        GenericSystemTable.addHeader(groupCode, data.genericTable);
        GenericSystemTable.populateTable_TBody(groupCode, data.genericTable.content);

        table.children("tfoot").children("tr").children("th").prop("colspan", data.genericTable.headers.length);
        GenericSystemTable.populatePagination(groupCode, data.pagination);
    },

    //Descripción: Obtiene el componente Table dentro del DOM
    //groupCode: Código de grupo (Grupo de Tables)
    getTable: function (groupCode) {
        var element = null;

        $("table").each(function (index) {
            if ($(this).attr("group-code") == groupCode) {
                element = $(this);
                return false;
            }
        });

        return element;
    },

    //Descripción: Carga el contenido de la tabla(Sección TBody) - Toda información que tenía almacenada anteriormente es borrada.
    //groupCode: Código de grupo (Grupo de Tables)
    //content: Información a cargar por Fila - List<ContentAssociated> 
    populateTable_TBody: function (groupCode, content) {
        var tBody = GenericSystemTable.getTable(groupCode).children('tbody');
        tBody.html('');

        for (var i = 0; i < content.length; i++)
			GenericSystemTable.addRow(groupCode, content[i].group, content[i].id, content[i].contentValue);
    },

    //Descripción: Carga el contenido de la tabla(Sección THead) - Toda información que tenía almacenada anteriormente es borrada.
    //groupCode: Código de grupo (Grupo de Tables)
    //headers: Información a cargar - List<String> 
    addHeader: function (groupCode, component) {
        var tHead = GenericSystemTable.getTable(groupCode).children('thead');
        tHead.html('');

		var headers = component.headers;
		var widths = component.columnWidths;

		var head = '<tr>';
		for (var i = 0; i < headers.length; i++)
		{ 
			var width = widths[i];
			width = width == "-1" ? "" : "width:" + width + "px;";

			head += '<th class="text-center" style="' + width + '" >' + headers[i] + '</th>';
		}
        head += '</tr>';

        tHead.append(head);
    },

    //Descripción: Carga el contenido de una fila en la tabla.
    //groupCode: Código de grupo (Grupo de Tables)
    //group: Grupo de información a cargar - List<GroupInformation> 
    addRow: function (groupCode, group, idValue, contentValue) {
        var tBody = GenericSystemTable.getTable(groupCode).children('tbody');

        var row = '<tr>';

        for (var i = 0; i < group.length; i++) {
            var type = group[i].itemsTypeName;			

            row += '<td>' +
                '<div class="row">';

            for (var j = 0; j < group[i].items.length; j++) {
                var item = group[i].items[j];

				row += '<div>';
                switch (type) {
                    case "String":
                        row += item;
                        break;
                      
                    case "Button":
						row += '<div class="col-xs-6">' +
							'<button class="btn ' + item.color + '" id-value="' + idValue + '" group-code="' + groupCode + '" component-code="' + item.componentCode + '" content-value="'+contentValue+'">' +
							'<i class="fa '+item.icon+'"></i>' +
							'</button>' +
							'</div>';
                        break;
                }
                row += "</div>";
            }
            row += '</div>' +
                '</td>';
        }
        row += '</tr>';

        tBody.append(row);
    },

    getAlert: function (groupCode) {
        var element = null;

        $(".alert").each(function (index) {
            if ($(this).attr("group-code") != null && $(this).attr("group-code") == groupCode) {
                element = $(this);
                return false;
            }
        });

        return element;
    },
    populatePagination: function (groupCode, data) {
        var pagination = GenericSystemTable.getPagination(groupCode);
        pagination.html('');

        if (data.endPag == 0) return;
        var structure =
            '<li>' +
            '<a class="paginationButton waves-effect waves-light" id-value="' + data.backPage + '" group-code="' + groupCode + '">' +
            '<i class="fa fa-angle-double-left"></i></a></li>';

        for (var i = data.startPag; i <= data.endPag; i++) {
			var colorSelected = i == data.pag ? "background-color:#01c0c8; border-color:#01c0c8;" : "";
			var active = i == data.pag ? "active" : "";

            structure +=
				'<li class="' + active + '">' +
				'<a class="paginationButton" style="' + colorSelected + '" id-value="' + i + '" group-code="' + groupCode + '">' + (i + 1) + '</a></li>';
        }

        structure +=
            '<li>' +
            '<a class="paginationButton waves-effect waves-light" id-value="' + data.nextPage + '" group-code="' + groupCode + '">' +
            '<i class="fa fa-angle-double-right"></i></a></li>';

        pagination.append(structure);

        if (data.pages >= 2)
            pagination.removeClass("hide");
        else
            pagination.addClass("hide");
    },

    getPagination: function (groupCode)
    {
        var element = null;

        $(".pagination").each(function (index) {
            if ($(this).attr("group-code") != null && $(this).attr("group-code") == groupCode) {
                element = $(this);
                return false;
            }
        });

        return element;
    },

    paginationEvent_Init: function ()
    {
        if (!GenericSystemTable.paginationEventActive) {
            GenericSystemTable.paginationEventActive = true;

            $(document.body).on('click', '.paginationButton', function () {
                var pag = $(this).attr("id-value");
                var groupCode = $(this).attr("group-code");

                EventDispatcher.trigger(GenericSystemTable.paginationEvent, { groupCode: groupCode, pag: pag });
            });
        }
    }
};

GenericSystemButton = {

    buttonEventActive: false,
    buttonEvent: "ButtonEvent",

    buttonEvent_Init: function () {

        if (!GenericSystemButton.buttonEventActive) {
            GenericSystemButton.buttonEventActive = true;

            $(document.body).on('click', 'button', function () {
                var idValue = $(this).attr("id-value");
                var groupCode = $(this).attr("group-code");
				var componentCode = $(this).attr("component-code");

				var contentValue = $(this).attr("content-value");
				if (contentValue == undefined || contentValue == null)
					contentValue = {};
				else
					contentValue = StringCast.StringToObject_Decode(contentValue);

                EventDispatcher.trigger(GenericSystemButton.buttonEvent, { idValue: idValue, groupCode: groupCode, componentCode: componentCode, contentValue : contentValue });
            });
        }

    }

};

/******************Iniciando Eventos******************/
$(document).ready(function ()
{
    GenericSystemTable.paginationEvent_Init();
    GenericSystemButton.buttonEvent_Init();
});

/********************************** Componentes Especiales *************************************/

$.fn.generateSelect = function (items, getName, getValue)
{
	if (typeof getName != 'function')
		getName = function (item) { return item.Name; };

	if (typeof getValue != 'function')
		getValue = function (item) { return item.Id; };

	var option = '';

	for (var i = 0; i < items.length; i++)
		option += '<option value="' + getValue(items[i]) + '">' + getName(items[i]) + '</option>';	

	$(this).find('option').not(':first').remove();
	$(this).find('option:first').prop('selected', 'selected');
	$(this).append(option);
}

StringCast =
	{
		// base64 encoded ascii to ucs-2 string
		ObjectToString_Encode: function (str) {
			return window.btoa(unescape(encodeURIComponent(JSON.stringify(str))));
		},
		// base64 encoded ascii to ucs-2 string
		StringToObject_Decode: function (str) {
			return JSON.parse(decodeURIComponent(escape(window.atob(str))));
		},
		NumberToLetter: function(value)	{

			var baseChar = ("A").charCodeAt(0), letters = "";
			value += 1;

			do {
				value -= 1;
				letters = String.fromCharCode(baseChar + (value % 26)) + letters;
				value = (value / 26) >> 0;
			} while (value > 0);

			return letters;
		}
	}