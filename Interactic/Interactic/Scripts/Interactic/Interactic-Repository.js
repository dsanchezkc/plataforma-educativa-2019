﻿/*
Descripción: Realiza una consula al repositorio del sistema

Modo de uso:
Repository.query(
{
    queryProcess: "CountryProcess.GetCountries",
    dataConvert: "Select",
    data: { id: 12 },
    success: function (json) {},
    error: function (xhr, status) {}
});

Resumen:
    queryProcess - Nombre de Objeto al que se le pedirá ejecutar
    dataConvert - El resultado que se obtenga será convertido en lo indicado acá
    data - Datos que se enviarán
    success: Función que se ejecuta cuando la petición fue exitosa
    error: Función que se ejecuta cuando hubo algún problema
*/

Repository = {

    urlServer: "",
    token: "",

    query: function (queryContent, resultToast = false, fullscreenloader = false) {
        if (queryContent.url === null || queryContent.url === "")
            return;

        if (queryContent.data != null)
            queryContent.data = JSON.stringify(queryContent.data);

        var data = JSON.stringify(queryContent);

        if (fullscreenloader)
            Repository.generateFullscreenLoader();
        $.ajax(
            {
                url: '/Repository/Query/',
                data: data,
                type: 'POST',
                contentType: 'application/json',
                processData: true,
                traditional: true,
                success: function (json) {

                    var resultContent = JSON.parse(json);

                    switch (resultContent.statusResult.status) {
                        case ResultContent.status.Success:

                            if (typeof queryContent.success === 'function')
                                queryContent.success(resultContent.data);

                            if (resultToast)
                                Toast.success(resultContent.statusResult.messageResult);
                            break;

                        case ResultContent.status.Error:
                            if (typeof queryContent.error === 'function')
                                queryContent.error(resultContent,resultContent.statusResult, Repository.getErrorsResumeHtml(resultContent.statusResult));
                            if (resultToast)
                                Toast.error(Repository.getErrorsResumeHtml(resultContent.statusResult));
                            break;

                        default:
                            if (typeof queryContent.error === 'function')
                                queryContent.error(resultContent.statusResult, Repository.getErrorsResumeHtml(resultContent.statusResult));
                            if (resultToast)
                                Toast.error(Repository.getErrorsResumeHtml(resultContent.statusResult));
                    }
                },
                error: function (xhr, status) {
                    var statusResult = { status: 500, messageResult: "Error del servidor, intente más tarde.", errors: [] };

                    if (typeof queryContent.error === 'function')
                        queryContent.error(statusResult, Repository.getErrorsResumeHtml(statusResult));

                    if (resultToast)
                        Toast.error(Repository.getErrorsResumeHtml(statusResult));
                },
                complete: function (r, status) {
                    var loader = $("#div-fullscreenloader");

                    if (loader.length) {
                        setTimeout(function () {
                            loader.fadeOut("slow", function () {
                                loader.remove();
                            });
                        }, 500);
                    }
                }
            });
    },

    getErrorsResumeHtml: function (statusResult) {

        var structure = '<ul>';

        var errors = statusResult.errors;
        var messageResult = statusResult.messageResult;

        if (messageResult != null && messageResult != "")
            structure += '<li>' + messageResult + '</li>';

        for (var i = 0; i < errors.length; i++)
            structure += '<li>' + errors[i] + '</li>';

        structure += '</ul>';

        return structure;
    },

    queryApi: function (queryContent, resultToast = false, fullscreenloader = false, refreshpage = false) {

        if (queryContent.url === null || queryContent.url === "")
            return;

        if (queryContent.data != undefined && queryContent.data instanceof FormData)
            queryContent.processData = false;
        else
            queryContent.processData = true;

        var information = "";
        var headers = { "Authorization": "Bearer " + Repository.token };

        switch (queryContent.method.toLowerCase()) {
            case "post": break;

            case "put":
                headers["X-HTTP-Method-Override"] = "PUT";
                queryContent.method = "POST";
                break;

            case "delete":
                information = Repository.stringFormat_URL(queryContent.data);
                headers["X-HTTP-Method-Override"] = "DELETE";
                queryContent.method = "GET";
                queryContent.data = null;
                break;

            case "get":
                information = Repository.stringFormat_URL(queryContent.data);
                queryContent.data = null;
                break;
        }

        if (queryContent.data != null && queryContent.processData)
            queryContent.data = JSON.stringify(queryContent.data);

        var urlServerLocal = "";

        if (queryContent.urlServerTemporal === undefined || queryContent.urlServerTemporal === "")
            urlServerLocal = Repository.urlServer;
        else {
            urlServerLocal = queryContent.urlServerTemporal;
            queryContent.url = "";
        }

        if (fullscreenloader)
            Repository.generateFullscreenLoader();

        $.ajax(
            {
                url: urlServerLocal + queryContent.url + information,
                data: queryContent.data,
                type: queryContent.method,
                headers: headers,
                contentType: queryContent.processData ? "application/json; charset=utf-8" : false,
                processData: queryContent.processData,
                cache: queryContent.processData ? undefined : false,
                success: function (result) {
                    if (resultToast)
                        Toast.success("Proceso realizado.");

                    if (typeof queryContent.success === 'function')
                        queryContent.success(result);
                },
                error: function (xhr, status) {
                    if (typeof queryContent.error === 'function')
                        queryContent.error(status);

                    if (resultToast)
                        if ((xhr.responseText == null || xhr.responseText == "") || xhr.status == 500)
                            Toast.error("No es posible realizar esta operación ahora, intente más tarde.");
                        else
                            Toast.error(xhr.responseText.substring(1, xhr.responseText.length - 1));

                    refreshpage = false;
                },
                complete: function (r, status) {
                    if (refreshpage) {
                        Cookies.set('refreshPage-Position', window.pageYOffset);
                        window.location.reload();
                        return;
                    }

                    var loader = $("#div-fullscreenloader");

                    if (loader.length) {
                        setTimeout(function () {
                            loader.fadeOut("slow", function () {
                                loader.remove();
                            });
                        }, 500);

                    }
                }
            });
    },

    generateFullscreenLoader: function () {
        var loader = $("#div-fullscreenloader");

        if (!loader.length)
            $("body").append('<div id="div-fullscreenloader" class="preloader"><div class="cssload-speeding-wheel"></div></div>');
    },

    stringFormat_URL: function (obj) {
        if (obj == null) return "";

        var _string = "/?";

        for (var property in obj)
            if (obj.hasOwnProperty(property))
                _string += property.toString() + "=" + obj[property] + "&";

        return _string;
    }
};

DownloadManager =
    {
        //Forma de descarga de archivos en base64
        //Requiere por obligación la libreria FileSaver.js
        //https://github.com/eligrey/FileSaver.js/
        downloadBase64: function (data, name, type) {
            saveAs(DownloadManager.base64toBlob(data, type), name);
        },

        base64toBlob: function (b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }
    }

ResultContent = {
    status: { Success: 200, Error: 500 }
};

Toast =
    {
        error: function (text) {
            var options = {
                content: text, // text of the snackbar
                style: "toast-error", // add a custom class to your snackbar
                timeout: 7500 // time in milliseconds after the snackbar autohides, 0 is disabled
            }

            $.snackbar(options);
        },
        success: function (text) {
            var options = {
                content: text, // text of the snackbar
                style: "toast-success", // add a custom class to your snackbar
                timeout: 5000 // time in milliseconds after the snackbar autohides, 0 is disabled
            }

            $.snackbar(options);
        }
    }

$(function () {
    if (Cookies.get('refreshPage-Position') !== "undefined") {
        var jumpTo = Cookies.get('refreshPage-Position');
        window.scrollTo(0, jumpTo);
        Cookies.remove('refreshPage-Position');
    }
});

$(function () {

    var code = Cookies.get('Code');
    var block = false;


    var reload = function () {
        var currentCode = Cookies.get('Code');

        if (currentCode != undefined) {

            if (code == undefined)
                code = Cookies.get('Code');
            else
                if (code != Cookies.get('Code')) {
                    Repository.generateFullscreenLoader();
                    location.reload();
                    block = true;
                }
        }

        if (!block)
            setTimeout(reload, 2000);
    };

    reload();
});