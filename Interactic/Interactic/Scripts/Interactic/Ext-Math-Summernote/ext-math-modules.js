
var ExtMathModules =
{
    modules:
    [
        { 
            title: "Matemática",
            columns: 
            [
                {
                    width: 1, // Valor para col-md-(width) value 1 to 12
                    buttons:
                    [
                        { code : "\\times", colorClass: "btn-success"  },
                        { code : "\\div", colorClass: "btn-success"  },
                        { code : "\\pm", colorClass: "btn-success"  }    
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\sqrt{a}"    , colorClass: "btn-info"  },
                        { code : "\\sqrt[n]{a}" , colorClass: "btn-info"  },
                        { code : "\\lg{az}"     , colorClass: "btn-info"  },
                        { code : "\\log_{a}{b}" , colorClass: "btn-info"  }                
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "a^{b}"                           , colorClass: "btn-danger"  },
                        { code : "a_{b}"                           , colorClass: "btn-danger"  },
                        { code : "c_a^b"                           , colorClass: "btn-danger"  },
                        { code : "\\frac{ab}{cd}"                  , colorClass: "btn-danger"  },
                        { code : "\\frac{\\partial a}{\\partial b}", colorClass: "btn-danger"  },
                        { code : "\\frac{\\text{d}x}{\\text{d}y}"  , colorClass: "btn-danger"  }                
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\overleftarrow{ab}" , colorClass: "btn-default"  },
                        { code : "\\overrightarrow{ab}", colorClass: "btn-default"  },
                        { code : "\\overbrace{ab}"     , colorClass: "btn-default"  },
                        { code : "\\underbrace{ab}"    , colorClass: "btn-default"  },
                        { code : "\\underline{ab}"     , colorClass: "btn-default"  },
                        { code : "\\overline{ab}"      , colorClass: "btn-default"  },
                        { code : "\\widehat{ab}"       , colorClass: "btn-default"  }                
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\lim_{a \\rightarrow b}", colorClass: "btn-default"  },
                        { code : "\\int_{a}^{b}"           , colorClass: "btn-default"  },
                        { code : "\\oint_a^b"              , colorClass: "btn-default"  },
                        { code : "\\prod_a^b"              , colorClass: "btn-default"  },
                        { code : "\\coprod_a^b"            , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\bigcap_a^b"  , colorClass: "btn-default"  },
                        { code : "\\bigcup_a^b"  , colorClass: "btn-default"  },
                        { code : "\\bigvee_a^b"  , colorClass: "btn-default"  },
                        { code : "\\bigwedge_a^b", colorClass: "btn-default"  },
                        { code : "\\bigsqcup_a^b", colorClass: "btn-default"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\sum_a^b"                                          , colorClass: "btn-warning"  },
                        { code : "\\left(\\begin{array}{c}a\\\\ b\\end{array}\\right)", colorClass: "btn-warning"  }                       
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\begin{cases}a & x = 0\\\\b & x > 0\\end{cases}", colorClass: "btn-warning"  },
                        { code : "\\begin{bmatrix}a & b \\\\c & d \\end{bmatrix}"  , colorClass: "btn-default"  }                       
                    ]
                }

            ]
        },
        { 
            title: "Lógica",
            columns: 
            [
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\neq"    , colorClass: "btn-default"  },
                        { code : "\\leq"    , colorClass: "btn-default"  },
                        { code : "\\geq"    , colorClass: "btn-default"  },
                        { code : "\\sim"    , colorClass: "btn-default"  },
                        { code : "\\approx" , colorClass: "btn-default"  },
                        { code : "\\cong"   , colorClass: "btn-default"  }                    
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\equiv" , colorClass: "btn-default"  },
                        { code : "\\propto", colorClass: "btn-default"  },
                        { code : "\\ll"    , colorClass: "btn-default"  },
                        { code : "\\gg"    , colorClass: "btn-default"  },
                        { code : "\\in"    , colorClass: "btn-default"  },
                        { code : "\\subset", colorClass: "btn-default"  }                    
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\subseteq", colorClass: "btn-default"  },
                        { code : "\\prec"    , colorClass: "btn-default"  },
                        { code : "\\succ"    , colorClass: "btn-default"  },
                        { code : "\\succeq"  , colorClass: "btn-default"  },
                        { code : "\\preceq"  , colorClass: "btn-default"  },
                        { code : "\\simeq"   , colorClass: "btn-default"  },
                     ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\sqsubseteq", colorClass: "btn-default"  },
                        { code : "\\sqsupseteq", colorClass: "btn-default"  },
                        { code : "\\ni"        , colorClass: "btn-default"  },
                        { code : "\\models"    , colorClass: "btn-default"  },                    
                        { code : "\\asymp"     , colorClass: "btn-default"  },
                        { code : "\\doteq"     , colorClass: "btn-default"  }                    
                   ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\vdash"   , colorClass: "btn-default"  },
                        { code : "\\dashv"   , colorClass: "btn-default"  },
                        { code : "\\perp"    , colorClass: "btn-default"  },
                        { code : "\\mid"     , colorClass: "btn-default"  },                    
                        { code : "\\parallel", colorClass: "btn-default"  },
                        { code : "\\smile"   , colorClass: "btn-default"  },                    
                        { code : "\\frown"   , colorClass: "btn-default"  },
                        { code : "\\bowtie"  , colorClass: "btn-default"  },                    
                        { code : "\\unlhd"   , colorClass: "btn-default"  },
                        { code : "\\unrhd"   , colorClass: "btn-default"  }                    
                   
                   ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\hat{a}"  , colorClass: "btn-default"  },
                        { code : "\\check{a}", colorClass: "btn-default"  },
                        { code : "\\breve{a}", colorClass: "btn-default"  },
                        { code : "\\acute{a}", colorClass: "btn-default"  },                    
                        { code : "\\grave{a}", colorClass: "btn-default"  },
                        { code : "\\tilde{a}", colorClass: "btn-default"  },                    
                        { code : "\\bar{a}"  , colorClass: "btn-default"  },
                        { code : "\\vec{a}"  , colorClass: "btn-default"  },                    
                        { code : "\\dot{a}"  , colorClass: "btn-default"  },
                        { code : "\\ddot{a}" , colorClass: "btn-default"  }                    
                   
                   ]
                }
            ]
        },
        { 
            title: "Alf. Griego",
            columns: 
            [
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\alpha"  , colorClass: "btn-default"  },
                        { code : "\\eta"    , colorClass: "btn-default"  },
                        { code : "\\nu"     , colorClass: "btn-default"  },
                        { code : "\\upsilon", colorClass: "btn-default"  }                                      
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\beta" , colorClass: "btn-default"  },
                        { code : "\\theta", colorClass: "btn-default"  },
                        { code : "\\xi"   , colorClass: "btn-default"  },
                        { code : "\\phi"  , colorClass: "btn-default"  }                                      
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\gamma", colorClass: "btn-default"  },
                        { code : "\\iota" , colorClass: "btn-default"  },
                        { code : "\\pi"   , colorClass: "btn-default"  },
                        { code : "\\chi"  , colorClass: "btn-default"  }                                      
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\delta", colorClass: "btn-default"  },
                        { code : "\\kappa", colorClass: "btn-default"  },
                        { code : "\\rho"  , colorClass: "btn-default"  },
                        { code : "\\psi"  , colorClass: "btn-default"  }                                      
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\epsilon", colorClass: "btn-default"  },
                        { code : "\\lambda", colorClass: "btn-default"  },
                        { code : "\\sigma"  , colorClass: "btn-default"  },
                        { code : "\\omega"  , colorClass: "btn-default"  }                                      
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\zeta", colorClass: "btn-default"  },
                        { code : "\\mu", colorClass: "btn-default"  },
                        { code : "\\tau"  , colorClass: "btn-default"  }                       
                    ]
                }
            ]
        },
        { 
            title: "Funciones",
            columns: 
            [
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\cos"   , colorClass: "btn-success"  },
                        { code : "\\arccos", colorClass: "btn-success"  },
                        { code : "\\csc"   , colorClass: "btn-success"  },
                        { code : "\\cosh"  , colorClass: "btn-default"  },
                        { code : "\\cot"   , colorClass: "btn-default"  },
                        { code : "\\coth"  , colorClass: "btn-default"  }                                                                 
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\sin"   , colorClass: "btn-info"  },
                        { code : "\\arcsin", colorClass: "btn-info"  },
                        { code : "\\sec"   , colorClass: "btn-info"  },
                        { code : "\\sinh"  , colorClass: "btn-default"  }                                                                             
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\tan"   , colorClass: "btn-danger"  },
                        { code : "\\arctan", colorClass: "btn-danger"  },
                        { code : "\\tanh"  , colorClass: "btn-default" }                                                                                             
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\ln"  , colorClass: "btn-warning"  },
                        { code : "\\log" , colorClass: "btn-warning"  },
                        { code : "\\max" , colorClass: "btn-default"  },
                        { code : "\\min" , colorClass: "btn-default"  },
                        { code : "\\exp" , colorClass: "btn-warning"  },
                        { code : "\\bmod", colorClass: "btn-warning"  }                                                                 
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\inf", colorClass: "btn-primary"  },
                        { code : "\\lim", colorClass: "btn-primary"  },
                        { code : "\\sup", colorClass: "btn-default"  },
                        { code : "\\gcd", colorClass: "btn-default"  },
                        { code : "\\hom", colorClass: "btn-default"  },
                        { code : "\\ker", colorClass: "btn-default"  },
                        { code : "\\det", colorClass: "btn-default"  },
                                                                                            
                    ]
                }

            ]
        },
        { 
            title: "Flechas y Parentesis",
            columns: 
            [
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\left(AB \\right)"     , colorClass: "btn-success"  },
                        { code : "\\left[ AB \\right]"    , colorClass: "btn-success"  },
                        { code : "\\left\\{ AB \\right\\}", colorClass: "btn-success"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\wedge", colorClass: "btn-info"     },
                        { code : "\\vee"  , colorClass: "btn-info"     },
                        { code : "\\cap"  , colorClass: "btn-info"     },
                        { code : "\\cup"  , colorClass: "btn-info"     },                        
                        { code : "\\sqcap", colorClass: "btn-default"  },
                        { code : "\\sqcup", colorClass: "btn-default"  },
                        { code : "\\uplus", colorClass: "btn-default"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\langle", colorClass: "btn-default"  },
                        { code : "\\rangle", colorClass: "btn-default"  },
                        { code : "\\lfloor", colorClass: "btn-default"  },
                        { code : "\\rfloor", colorClass: "btn-default"  },
                        { code : "\\lceil" , colorClass: "btn-default"  },
                        { code : "\\rceil" , colorClass: "btn-default"  },
                        { code : "\\wr"    , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\leftarrow", colorClass: "btn-default"  },
                        { code : "\\mapsto", colorClass: "btn-default"  },
                        { code : "\\rightharpoonup", colorClass: "btn-default"  },
                        { code : "\\Updownarrow", colorClass: "btn-default"  },
                        { code : "\\Rrightarrow" , colorClass: "btn-default"  },
                        { code : "\\looparrowright" , colorClass: "btn-default"  },
                        { code : "\\dashrightarrow"    , colorClass: "btn-default"  },
                        { code : "\\upharpoonright"    , colorClass: "btn-default"  },
                        { code : "\\nleftarrow"    , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\Leftarrow", colorClass: "btn-default"  },
                        { code : "\\leftharpoonup", colorClass: "btn-default"  },
                        { code : "\\uparrow", colorClass: "btn-default"  },
                        { code : "\\leftleftarrows", colorClass: "btn-default"  },
                        { code : "\\twoheadleftarrow" , colorClass: "btn-default"  },
                        { code : "\\curvearrowleft" , colorClass: "btn-default"  },
                        { code : "\\Lsh"    , colorClass: "btn-default"  },
                        { code : "\\downharpoonleft"    , colorClass: "btn-default"  },
                        { code : "\\nrightarrow"    , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\rightarrow", colorClass: "btn-default"  },
                        { code : "\\leftharpoondown", colorClass: "btn-default"  },
                        { code : "\\Uparrow", colorClass: "btn-default"  },
                        { code : "\\rightrightarrows", colorClass: "btn-default"  },
                        { code : "\\twoheadrightarrow" , colorClass: "btn-default"  },
                        { code : "\\curvearrowright" , colorClass: "btn-default"  },
                        { code : "\\Rsh"    , colorClass: "btn-default"  },
                        { code : "\\downharpoonright"    , colorClass: "btn-default"  },
                        { code : "\\nLeftarrow"    , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\Rightarrow", colorClass: "btn-default"  },
                        { code : "\\rightleftharpoons", colorClass: "btn-default"  },
                        { code : "\\downarrow", colorClass: "btn-default"  },
                        { code : "\\leftrightarrows", colorClass: "btn-default"  },
                        { code : "\\leftarrowtail" , colorClass: "btn-default"  },
                        { code : "\\circlearrowleft" , colorClass: "btn-default"  },
                        { code : "\\upuparrows"    , colorClass: "btn-default"  },
                        { code : "\\rightsquigarrow"    , colorClass: "btn-default"  },
                        { code : "\\nRightarrow"    , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\leftrightarrow", colorClass: "btn-default"  },
                        { code : "\\leftrightharpoons", colorClass: "btn-default"  },
                        { code : "\\Downarrow", colorClass: "btn-default"  },
                        { code : "\\rightleftarrows", colorClass: "btn-default"  },
                        { code : "\\rightarrowtail" , colorClass: "btn-default"  },
                        { code : "\\circlearrowright" , colorClass: "btn-default"  },
                        { code : "\\downdownarrows"    , colorClass: "btn-default"  },
                        { code : "\\leftrightsquigarrow"    , colorClass: "btn-default"  },
                        { code : "\\nleftrightarrow"    , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\Leftrightarrow", colorClass: "btn-default"  },
                        { code : "\\hookrightarrow", colorClass: "btn-default"  },
                        { code : "\\updownarrow", colorClass: "btn-default"  },
                        { code : "\\Lleftarrow", colorClass: "btn-default"  },
                        { code : "\\looparrowleft" , colorClass: "btn-default"  },
                        { code : "\\dashleftarrow" , colorClass: "btn-default"  },
                        { code : "\\upharpoonleft"    , colorClass: "btn-default"  },
                        { code : "\\multimap"    , colorClass: "btn-default"  },
                        { code : "\\nLeftrightarrow"    , colorClass: "btn-default"  }
                    ]
                }
            ]
        },
        { 
            title: "Simbolos",
            columns: 
            [
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\dots"         , colorClass: "btn-success"  },
                        { code : "\\exists"       , colorClass: "btn-success"  },
                        { code : "\\forall"       , colorClass: "btn-success"  },
                        { code : "\\top"          , colorClass: "btn-default"  },
                        { code : "\\surd"         , colorClass: "btn-default"  },
                        { code : "\\triangleright", colorClass: "btn-default"  },
                        { code : "\\triangleleft" , colorClass: "btn-default"  },
                        { code : "\\triangledown" , colorClass: "btn-default"  },
                        { code : "\\triangle"     , colorClass: "btn-default"  },
                        { code : "\\lrcorner"     , colorClass: "btn-default"  },
                        { code : "\\llcorner"     , colorClass: "btn-default"  },
                        { code : "\\urcorner"     , colorClass: "btn-default"  },
                        { code : "\\ulcorner"     , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\nexists"       , colorClass: "btn-success"  },
                        { code : "\\bot"           , colorClass: "btn-success"  },
                        { code : "\\sphericalangle", colorClass: "btn-default"  },
                        { code : "\\varnothing"    , colorClass: "btn-default"  },
                        { code : "\\backprime"     , colorClass: "btn-default"  },
                        { code : "\\heartsuit"     , colorClass: "btn-default"  },
                        { code : "\\Cup"           , colorClass: "btn-default"  },
                        { code : "\\Cap"           , colorClass: "btn-default"  },
                        { code : "\\rtimes"        , colorClass: "btn-default"  },
                        { code : "\\ltimes"        , colorClass: "btn-default"  },
                        { code : "\\dotplus"       , colorClass: "btn-default"  },
                        { code : "\\Game"          , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\leftthreetimes" , colorClass: "btn-success"  },
                        { code : "\\rightthreetimes", colorClass: "btn-success"  },
                        { code : "\\curlywedge"     , colorClass: "btn-default"  },
                        { code : "\\curlyvee"       , colorClass: "btn-default"  },
                        { code : "\\diagup"         , colorClass: "btn-default"  },
                        { code : "\\diagdown"       , colorClass: "btn-default"  },
                        { code : "\\barwedge"       , colorClass: "btn-default"  },
                        { code : "\\veebar"         , colorClass: "btn-default"  },
                        { code : "\\doublebarwedge" , colorClass: "btn-default"  },
                        { code : "\\Box"            , colorClass: "btn-default"  },
                        { code : "\\boxplus"        , colorClass: "btn-default"  },
                        { code : "\\Finv"           , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\therefore"    , colorClass: "btn-success"  },
                        { code : "\\because"      , colorClass: "btn-success"  },
                        { code : "\\oplus"        , colorClass: "btn-default"  },
                        { code : "\\ominus"       , colorClass: "btn-default"  },
                        { code : "\\otimes"       , colorClass: "btn-default"  },
                        { code : "\\oslash"       , colorClass: "btn-default"  },
                        { code : "\\boxminus"     , colorClass: "btn-default"  },
                        { code : "\\boxtimes"     , colorClass: "btn-default"  },
                        { code : "\\boxdot"       , colorClass: "btn-default"  },
                        { code : "\\circledast"   , colorClass: "btn-default"  },
                        { code : "\\circledcirc"  , colorClass: "btn-default"  },
                        { code : "\\divideontimes", colorClass: "btn-default"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\infty"      , colorClass: "btn-success"  },
                        { code : "\\partial"    , colorClass: "btn-success"  },
                        { code : "\\mho"        , colorClass: "btn-success"  },
                        { code : "\\eth"        , colorClass: "btn-success"  },
                        { code : "\\hslash"     , colorClass: "btn-default"  },
                        { code : "\\hbar"       , colorClass: "btn-default"  },
                        { code : "\\diamondsuit", colorClass: "btn-default"  },
                        { code : "\\diamond"    , colorClass: "btn-default"  },
                        { code : "\\circ"       , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\imath"    , colorClass: "btn-default"  },
                        { code : "\\jmath"    , colorClass: "btn-default"  },
                        { code : "\\ell"      , colorClass: "btn-default"  },
                        { code : "\\wp"       , colorClass: "btn-default"  },
                        { code : "\\Im"       , colorClass: "btn-default"  },
                        { code : "\\prime"    , colorClass: "btn-default"  },
                        { code : "\\angle"    , colorClass: "btn-default"  },
                        { code : "\\flat"     , colorClass: "btn-default"  },
                        { code : "\\natural"  , colorClass: "btn-default"  },
                        { code : "\\sharp"    , colorClass: "btn-default"  },
                        { code : "\\S"        , colorClass: "btn-default"  },
                        { code : "\\checkmark", colorClass: "btn-default"  }
                    ]
                },
                {
                    width:1,
                    buttons:
                    [
                        { code : "\\blacktriangleleft" , colorClass: "btn-default"  },
                        { code : "\\blacktriangleright", colorClass: "btn-default"  },
                        { code : "\\blacklozenge"      , colorClass: "btn-default"  },
                        { code : "\\blacktriangle"     , colorClass: "btn-default"  },
                        { code : "\\blacktriangledown" , colorClass: "btn-default"  },
                        { code : "\\blacksquare"       , colorClass: "btn-default"  },
                        { code : "\\clubsuit"          , colorClass: "btn-default"  },
                        { code : "\\spadesuit"         , colorClass: "btn-default"  },
                        { code : "\\bigstar"           , colorClass: "btn-default"  },
                        { code : "\\maltese"           , colorClass: "btn-default"  }                           
                    ]
                }
            ]
        },
        { 
            title: "Fuentes",
            columns: 
            [
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\textbf{Bold}", colorClass: "btn-default"  },
                        { code : "\\textit{Italic}"       , colorClass: "btn-default"  },
                        { code : "\\underline{Underline}"       , colorClass: "btn-default"  }
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\huge Texto" , colorClass: "btn-default"  },
                        { code : "\\large Texto"       , colorClass: "btn-default"  },
                        { code : "\\small Texto"       , colorClass: "btn-default"  },
                        { code : "\\tiny Texto"       , colorClass: "btn-default"  }                           
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\color{aqua} {Celeste}" , colorClass: "btn-default"  },
                        { code : "\\color{blue} {Azul}"       , colorClass: "btn-default"  },
                        { code : "\\color{fuchsia} {Rosa}"       , colorClass: "btn-default"  },
                        { code : "\\color{green} {Verde}"       , colorClass: "btn-default"  },
                        { code : "\\color{maroon} {Café}"       , colorClass: "btn-default"  },
                        { code : "\\color{yellow} {Amarillo}"       , colorClass: "btn-default"  }                                                
                    ]
                },
                {
                    width:2,
                    buttons:
                    [
                        { code : "\\color{black} {Negro}" , colorClass: "btn-default"  },
                        { code : "\\color{gray} {Gris}"       , colorClass: "btn-default"  },
                        { code : "\\color{silver} {Plateado}"       , colorClass: "btn-default"  },
                        { code : "\\color{purple} {Morado}"       , colorClass: "btn-default"  },
                        { code : "\\color{orange} {Naranjo}"       , colorClass: "btn-default"  },  
                        { code : "\\color{red} {Rojo}"       , colorClass: "btn-default"  }                                              
                    ]
                }
            ]
        }
    ],
    generatePanel : function()
    {
        var structure ='<div class="vtabs customvtab" style="background-color: white; width: 100%;">'+
                        '<ul class="nav tabs-vertical">';

        for(var i=0; i < ExtMathModules.modules.length;i++)
            structure+='<li class="tab nav-item" aria-expanded="true">'+
                            '<a data-toggle="tab" class="nav-link '+(i==0 ? 'active':'')+'" href="#ext-math-panel-mod_'+i+'" aria-expanded="'+(i==0 ? 'true':'false')+'"> <span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs">'+ExtMathModules.modules[i].title+'</span></a>'+
                        '</li>';
        structure+='</ul>'


        structure+='<div class="tab-content" style="max-height: 100px;">';
 
        for(var i=0; i < ExtMathModules.modules.length;i++)
        {
            structure+= '<div id="ext-math-panel-mod_'+i+'" class="tab-pane '+(i==0 ? 'active':'')+'" aria-expanded="'+(i==0 ? 'true':'false')+'" style="height:350px; width:725px; overflow-x:auto; overflow-y:auto;">'+
                        '<div class="row" style="width:1000px;">';
           
            for(var j=0; j < ExtMathModules.modules[i].columns.length; j++)
            {
                structure+='<div class="col-md-'+ExtMathModules.modules[i].columns[j].width+'">';

                for(var x =0; x < ExtMathModules.modules[i].columns[j].buttons.length; x++)
                {
                    var button = ExtMathModules.modules[i].columns[j].buttons[x];
                    structure+='<button class="ext-math-button-panel-code btn btn-block btn-outline '+button.colorClass+'" content-code="'+button.code+'">$'+button.code+'$</button>';                   
                }

                structure+='</div>';
            }           

            structure+='</div></div>';
        }

        structure+='</div></div>';

        return structure;
    }
}