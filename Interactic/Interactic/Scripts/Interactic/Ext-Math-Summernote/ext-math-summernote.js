/*
 Requiere para funcionar en el siguiente orden

1.- Summernote (Js)
	Insertar dentro de Toolbar -> insert -> 'ext-math'
	Línea Aprox 7085

	// toolbar
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
			['insert', ['link', 'picture', 'video', 'ext-math']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
2.- Ext-Math-Summernote (Js)
3.- Ext-Math-Modules (Js)
4.- MathJax(Js)
5.- Html2Canvas (Js)

Integrar después de lo de arriba

<script type="text/x-mathjax-config">
			MathJax.Hub.Config({
				jax: ["input/TeX","input/MathML","input/AsciiMath","output/CommonHTML"],
				extensions: ["tex2jax.js","mml2jax.js","asciimath2jax.js","MathMenu.js","MathZoom.js","AssistiveMML.js", "a11y/accessibility-menu.js"],
				TeX: { extensions: ["AMSmath.js","AMSsymbols.js","noErrors.js","noUndefined.js"] },
				tex2jax: {inlineMath: [["$","$"],["\\(","\\)"]]},
				showMathMenu: false
			});
</script>
 
 */



(function (factory) {
  /* global define */
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node/CommonJS
    module.exports = factory(require('jquery'));
  } else {
    // Browser globals
    factory(window.jQuery);
  }
}

(function ($) {

  // Extends plugins for adding hello.
  //  - plugin is external module for customizing.

  $.extend($.summernote.plugins, {
    /**
     * @param {Object} context - context object has status of editor.
     */
    'ext-math': function (context) {
      var self = this;

      // ui has renders to build ui elements.
      //  - you can create a button with `ui.button`
      var ui = $.summernote.ui;

      // add hello button
      context.memo('button.ext-math', function () {
        // create button
        var button = ui.button({
          contents: '<i class="icon-calculator"></i><span> Insertar ecuación</span>',
          tooltip: 'Insertar ecuación',
          click: function ()
		  {			
              context.invoke('editor.saveRange');
              self.$textarea.val('x = {-b \\pm \\sqrt{b^2-4ac} \\over 2a}.');
			  self.$renderContent.html('');
			  self.$insert.text("Insertar");
			  self.$insert.prop("disabled", false);
			  self.$textarea.prop("disabled", false);

              var noteControlSelection = $(".note-control-selection");

               if(noteControlSelection!=undefined &&  noteControlSelection.css("display")=="block" && 
                  noteControlSelection.data('target').hasClass('ext-math-content-render'))
                {
				   self.$textarea.val(decodeURIComponent(escape(window.atob(noteControlSelection.data('target').attr("ext-math-content-code")))));
                  $(".note-popover.popover.in.note-image-popover.bottom").hide();
                }
                else
                self.$insert.prop("disabled", true);

			    self.$textarea.trigger('propertychange');			   
			    self.$panel.modal('show');
			    MathJax.Hub.Queue(["Typeset", MathJax.Hub, "body"]);
          }
        });

        // create jQuery object from button instance.
        var $hello = button.render();
        return $hello;
      });

      // This events will be attached when editor is initialized.
      this.events = {
        // This will be called after modules are initialized.
        'summernote.init': function (we, e) {
         
       
        },
        // This will be called when user releases a key on editable.
        'summernote.keyup': function (we, e) {
        
        }
      };

      // This method will be called when editor is initialized by $('..').summernote();
      // You can create elements for plugin
      this.initialize = function ()
	  {
		  $("#ext-math-modal").remove();

		  this.$panel = $('<div id="ext-math-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static" style="display: none; z-index:2000;" aria-hidden="true">' +
			  '<div style="max-width: 900px; margin: 30px auto;">' +
			  '<div class="modal-content">' +

				  '<div class="modal-header">' +
					'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>' +
					'<h4 class="modal-title">Generador de ecuaciones</h4>' +
				  '</div>' +

				  '<div class="modal-body" style="height: 720px;overflow-y: auto;">' +

						'<div class="col-sm-12">' +

							  '<div class="row">' +
								ExtMathModules.generatePanel() +
							  '</div>' +

							  '<div class="row">' +

								  '<div class="col-md-12">' +
									'<label>Ingrese ecuación:</label>' +
									'<a href="https://en.wikibooks.org/wiki/LaTeX/Mathematics" target="_blank" class="card-link" style="float:right;">Más Información</a>' +
									' <div class="input-group">' +
										'<textarea class="form-control" rows="3" style="resize:none; border-color:#888888;"></textarea>' +
									 '</div>' +
								  '</div>' +

								  '<div class="col-md-12" style="margin-top:10px; overflow-x:auto; overflow-y:auto; min-height:120px;">' +
									'<label>Visualización a generar:</label>' +
									'<div style="display:table; margin:0 auto; padding-top:15px;">' +
										'<div id="ext-math-content" style="float:left; margin:0 auto;"></div>' +
									'</div>' +
								  '</div>' +
							  '</div>' +
						  '</div>' +
						  '<div class="modal-footer" style="margin-top:5px; margin-bottom:0px;">' +
						    '<button type="button" class="btn btn-success waves-effect text-left button-ext-math-insert">Insertar</button>'+
                          '</div>' +
			  '</div>' +
			  '</div>' +
			  '</div>');

        this.$panel.appendTo('body');
        this.$textarea = this.$panel.find("textarea");
        this.$renderContent = $("#ext-math-content");
        this.$insert = this.$panel.find(".button-ext-math-insert");

        this.$textarea.bind('input propertychange', function()
        {
           self.$insert.prop("disabled", true);

           if(self.$textarea.val()==null || self.$textarea.val()=='')
              return;

            self.$renderContent.html('$'+self.$textarea.val()+'$');
            MathJax.Hub.Queue(["Typeset",MathJax.Hub, "ext-math-content"], function()
                {
                  self.$renderContent.find(".mjx-chtml.MathJax_CHTML").css("font-size",25);
				  self.$insert.prop("disabled", false);
				  MathJax.Hub.Queue(["Typeset", MathJax.Hub, "body"]);
                });
        });        

        this.$insert.click(function()
		{
			$(this).prop("disabled", true);
			$(this).text("Procesando...");
			self.$textarea.prop("disabled", true);

			setTimeout(function ()
			{ 
             MathJax.Hub.Queue(["Typeset",MathJax.Hub, "ext-math-content"], function ()
              {
                html2canvas($("#ext-math-content")[0]).then(function(canvas)
                {
                  context.invoke('editor.restoreRange');
                  var noteControlSelection = $(".note-control-selection");

                  if(noteControlSelection!=undefined &&  noteControlSelection.css("display")=="block" && 
                     noteControlSelection.data('target').hasClass('ext-math-content-render'))
                    {
                      noteControlSelection.data('target').attr("src",canvas.toDataURL("image/png")); 
					  noteControlSelection.data('target').attr("ext-math-content-code", window.btoa(unescape(encodeURIComponent(self.$textarea.val()))));                      
                    }
                    else
                      context.invoke('editor.insertNode', $('<img class="ext-math-content-render" ext-math-content-code="' + window.btoa(unescape(encodeURIComponent(self.$textarea.val())))+'" src='+canvas.toDataURL("image/png")+'>')[0]);
                  
					$("#ext-math-modal").modal('hide');					
                });
				});
			}, 1000);
        });

        $('body').on('dblclick', '.note-control-selection', function() {
            if($(this).data('target').hasClass('ext-math-content-render'))
			{
				if (self.$panel == null)
					return;

    		  self.$textarea.val(decodeURIComponent(escape(window.atob($(this).data('target').attr("ext-math-content-code")))));
              self.$renderContent.html('$'+self.$textarea.val()+'$');
              MathJax.Hub.Queue(["Typeset",MathJax.Hub, "ext-math-content"], function()
                {
                  self.$renderContent.find(".mjx-chtml.MathJax_CHTML").css("font-size",25);
                });
              
				$(".note-popover.popover.in.note-image-popover.bottom").hide();

				self.$insert.text("Insertar");
				self.$insert.prop("disabled", false);
				self.$textarea.prop("disabled", false);
				self.$panel.modal("show");
            }
        });

        $('body').on('click', '.ext-math-button-panel-code', function()
		{
			if (self.$textarea.is(':disabled'))
				return;

              var txtarea = self.$textarea;
              var myValue = $(this).attr('content-code');
              
              var start = self.$textarea[0].selectionStart;
              txtarea.val(txtarea.val().substring(0, start)+myValue+txtarea.val().substring(start,txtarea.val().length));
              txtarea.focus();
              self.$textarea[0].selectionStart=start+myValue.length;
              self.$textarea[0].selectionEnd =start+myValue.length;
              self.$textarea.trigger("propertychange");
		  });

		  MathJax.Hub.Queue(["Typeset", MathJax.Hub, "body"]);
     };

      // This methods will be called when editor is destroyed by $('..').summernote('destroy');
      // You should remove elements on `initialize`.
      this.destroy = function () {
        this.$panel.remove();
        this.$panel = null;
		};
    }
  });
}));